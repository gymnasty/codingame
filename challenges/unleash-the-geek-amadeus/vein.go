package main

func initialVeinExpectation(p *position) float64 {
	return veinExpectationTable[p.y][p.x]
}

func veinCost(p *position) float64 {
	return veinCostTable[p.x]
}

func veinExpectation(cellEstimated *cell) float64 {
	// map tells 100% Amadeusium existence
	if cellEstimated.Amadeusium > 0 {
		return 1
	}
	if cellEstimated.Amadeusium == 0 {
		return 0
	}

	// probability of getting Amadeusium from vein
	digAmadeusiumTable := []float64{
		1,                       // 1st try
		float64(2) / float64(3), // 2nd try
		0.5,                     // 3rd try
	}

	var result float64
	// calculate expectation of getting Amadeusium
	for i := range digAmadeusiumTable {
		result += cellEstimated.DigCountProbability(i) * digAmadeusiumTable[i]
	}
	if cellEstimated.Amadeusium == amadeusiumUnknown {
		result *= initialVeinExpectation(cellEstimated.position)
	}
	return result
}

func veinScore(c *cell) float64 {
	cost := veinCost(c.position)
	return veinExpectation(c) / cost
}

func showVeinScore(g *grid) {
	// header
	debugLog("    ")
	for i := range g.cells[0] {
		debugLog("  %2d  ", i)
	}
	debugLogLine("")
	// body
	for i, cs := range g.cells {
		debugLog("%2d :", i)
		for _, c := range cs {
			debugLog("%0.3f|", veinScore(c))
		}
		debugLogLine("")
	}
}

func findNearVeinImpl(r *robot, gridEstimated *grid, requireAmadeusiumCount int) (*cell, int) {
	turnToTarget := 10000
	var target *cell
	for _, cc := range gridEstimated.cells {
		for _, c := range cc {
			if c.Amadeusium >= requireAmadeusiumCount && !c.IsMyTrap && c.EnemyTrapProbability == 0 {
				turn := calcDigAndGoHeadquarterTurns(r, c.position)
				if turn > turnToTarget {
					continue
				}
				if turn == turnToTarget && c.Amadeusium < target.Amadeusium {
					continue
				}
				turnToTarget = turn
				target = c
			}
		}
	}
	return target, turnToTarget
}

func findNearVein(r *robot, gridEstimated *grid) (*cell, int) {
	return findNearVeinImpl(r, gridEstimated, 1)
}

func findNearVeinForTrap(r *robot, gridEstimated *grid) (*cell, int) {
	return findNearVeinImpl(r, gridEstimated, 2)
}

func veinCount(gridEstimated *grid) int {
	var count int
	for _, cc := range gridEstimated.cells {
		for _, c := range cc {
			if c.Amadeusium > 0 {
				count++
			}
		}
	}
	return count
}

func veins(gridEstimated *grid) []*cell {
	veins := make([]*cell, 0, maxHeight*maxWidth)
	for _, cc := range gridEstimated.cells {
		for _, c := range cc {
			if c.Amadeusium > 0 {
				veins = append(veins, c)
			}
		}
	}
	return veins
}
