package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type turnInput struct {
	turn                        int
	myScore, enemyScore         int
	radarCoolDown, trapCoolDown int
	myRobots, enemyRobots       []*robot
	radars, traps               []*entity
	grid                        *grid
}

func (i *turnInput) Show() {
	debugLogLine("Turn: %d", i.turn)
	debugLogLine("MyScore: %d, EnemyScore: %d", i.myScore, i.enemyScore)
	debugLogLine("RadarCoolDown: %d, TrapCoolDown: %d", i.radarCoolDown, i.trapCoolDown)
	i.grid.Show()
	for _, v := range i.myRobots {
		v.Show()
	}
	for _, v := range i.enemyRobots {
		v.Show()
	}
	for _, v := range i.radars {
		v.Show()
	}
	for _, v := range i.traps {
		v.Show()
	}
}

type inputReader struct {
	scanner       *bufio.Scanner
	width, height int
}

func newInputReader() *inputReader {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, 1000000), 1000000)
	return &inputReader{
		scanner: scanner,
	}
}

func (r *inputReader) readInitialInput() (width, height int) {
	r.scanner.Scan()
	_, _ = fmt.Sscan(r.scanner.Text(), &width, &height)
	r.width = width
	r.height = height
	return
}

func (r *inputReader) readTurnInput(turn int) *turnInput {
	// score
	var myScore, opponentScore int
	r.scanner.Scan()
	_, _ = fmt.Sscan(r.scanner.Text(), &myScore, &opponentScore)

	// grid
	grid := newGrid(r.width, r.height)
	for y := 0; y < r.height; y++ {
		r.scanner.Scan()
		inputs := strings.Split(r.scanner.Text(), " ")
		for x := 0; x < r.width; x++ {
			oreStr := inputs[2*x]
			holeStr := inputs[2*x+1]
			var ore int64
			if oreStr[0] == oreUnknown {
				ore = amadeusiumUnknown
			} else {
				ore, _ = strconv.ParseInt(oreStr, 10, 32)
			}
			grid.cells[y][x].IsHole = holeStr == "1"
			grid.cells[y][x].Amadeusium = int(ore)
		}
	}

	// misc
	var entityCount, radarCoolDown, trapCoolDown int
	r.scanner.Scan()
	_, _ = fmt.Sscan(r.scanner.Text(), &entityCount, &radarCoolDown, &trapCoolDown)

	// entities
	myRobots := make([]*robot, 0, 100)
	enemyRobots := make([]*robot, 0, 100)
	radars := make([]*entity, 0, 100)
	traps := make([]*entity, 0, 100)
	for i := 0; i < entityCount; i++ {
		var id, entityType, x, y, item int
		r.scanner.Scan()
		_, _ = fmt.Sscan(r.scanner.Text(), &id, &entityType, &x, &y, &item)

		switch entityType {
		case entityTypeMyRobot:
			myRobots = append(myRobots, newRobot(id, entityType, x, y, item))
		case entityTypeEnemyRobot:
			enemyRobots = append(enemyRobots, newRobot(id, entityType, x, y, item))
		case entityTypeRadar:
			radars = append(radars, newEntity(id, entityType, x, y))
		case entityTypeTrap:
			traps = append(traps, newEntity(id, entityType, x, y))
		default:
			debugLogLine("Unsupported entity type: %d", entityType)
		}
	}
	return &turnInput{
		turn:          turn,
		myScore:       myScore,
		enemyScore:    opponentScore,
		radarCoolDown: radarCoolDown,
		trapCoolDown:  trapCoolDown,
		myRobots:      myRobots,
		enemyRobots:   enemyRobots,
		radars:        radars,
		traps:         traps,
		grid:          grid,
	}
}
