package main

import (
	"fmt"
)

const (
	// ore
	oreUnknown        = '?'
	amadeusiumUnknown = -1
	amadeusiumWasVein = -2
)

type cell struct {
	*position
	Amadeusium            int
	IsHole                bool
	digCountProbabilities []float64
	IsMyTrap              bool
	EnemyTrapProbability  float64
}

func newCell(x, y int) *cell {
	digCountProbabilities := make([]float64, 4)
	digCountProbabilities[0] = 1
	return &cell{
		position:              newPosition(x, y),
		Amadeusium:            amadeusiumUnknown,
		IsHole:                false,
		digCountProbabilities: digCountProbabilities,
		IsMyTrap:              false,
		EnemyTrapProbability:  0,
	}
}

func (c *cell) Show() {
	holeMark := " "
	if c.IsHole {
		holeMark = "*"
	}
	ore := "  "
	if c.Amadeusium >= 0 {
		ore = fmt.Sprintf("%2d", c.Amadeusium)
	}
	debugLog("%s %s", ore, holeMark)
}

func (c *cell) Dig(probability float64) {
	restProbability := 1 - probability
	p0 := c.digCountProbabilities[0]
	p1 := c.digCountProbabilities[1]
	p2 := c.digCountProbabilities[2]
	p3 := c.digCountProbabilities[3]
	c.digCountProbabilities[0] = p0 * restProbability
	c.digCountProbabilities[1] = p0*probability + p1*restProbability
	c.digCountProbabilities[2] = p1*probability + p2*restProbability
	c.digCountProbabilities[3] = p2*probability + p3*restProbability
}

func (c *cell) DigCountProbability(count int) float64 {
	return c.digCountProbabilities[count]
}

func filterOutHole(cells []*cell) []*cell {
	if cells == nil {
		return nil
	}
	newVeins := make([]*cell, 0, len(cells))
	for _, c := range cells {
		if !c.IsHole {
			newVeins = append(newVeins, c)
		}
	}
	return newVeins
}

func filterOutKnown(cells []*cell) []*cell {
	if cells == nil {
		return nil
	}
	newVeins := make([]*cell, 0, len(cells))
	for _, c := range cells {
		if c.Amadeusium == amadeusiumUnknown {
			newVeins = append(newVeins, c)
		}
	}
	return newVeins
}

func filterOutMyTrap(cells []*cell) []*cell {
	if cells == nil {
		return nil
	}
	newVeins := make([]*cell, 0, len(cells))
	for _, c := range cells {
		if !c.IsMyTrap {
			newVeins = append(newVeins, c)
		}
	}
	return newVeins
}

func filterOutTrap(cells []*cell) []*cell {
	if cells == nil {
		return nil
	}
	newVeins := make([]*cell, 0, len(cells))
	for _, c := range cells {
		if !c.IsMyTrap && c.EnemyTrapProbability == 0 {
			newVeins = append(newVeins, c)
		}
	}
	return newVeins
}

func filterOutByAmadeusiumCount(cells []*cell, requiredCount int) []*cell {
	if cells == nil {
		return nil
	}
	newVeins := make([]*cell, 0, len(cells))
	for _, c := range cells {
		if c.Amadeusium >= requiredCount {
			newVeins = append(newVeins, c)
		}
	}
	return newVeins
}

func splitByDistance(cells []*cell, pos *position, distance int) (inVeins []*cell, outVeins []*cell) {
	if cells == nil {
		return nil, nil
	}
	inVeins = make([]*cell, 0, len(cells))
	outVeins = make([]*cell, 0, len(cells))
	for _, c := range cells {
		if c.Distance(pos) <= distance {
			inVeins = append(inVeins, c)
		} else {
			outVeins = append(outVeins, c)
		}
	}
	return inVeins, outVeins
}
