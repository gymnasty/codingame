package main

import (
	"math/rand"
	"runtime"
	"time"
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			debugLogLine("[ERROR] %s\n", err)
			for depth := 0; ; depth++ {
				_, file, line, ok := runtime.Caller(depth)
				if !ok {
					break
				}
				debugLogLine("======> %d: %v:%d", depth, file, line)
			}
		}
	}()

	// rand
	rand.Seed(time.Now().UnixNano())

	inputReader := newInputReader()
	width, height := inputReader.readInitialInput()
	_ = width
	_ = height

	// variables for estimation
	robotHistories := make(map[int]*robotHistory)

	turn := 0
	var previousGrid *grid
	var previousEnemyRobots []*robot
	var gridEstimated *grid
	processTurn := func() {
		// input
		turnInput := inputReader.readTurnInput(turn)

		// update turn and previous grid/enemyRobots at the end of turn
		defer func() {
			previousGrid = turnInput.grid
			previousEnemyRobots = turnInput.enemyRobots
			turn++
		}()

		// initialize history
		if turn == 0 {
			for _, r := range turnInput.myRobots {
				robotHistories[r.ID] = newRobotHistory(r.ID)
			}
			for _, r := range turnInput.enemyRobots {
				robotHistories[r.ID] = newRobotHistory(r.ID)
			}
		}
		// store robot position
		for _, r := range turnInput.myRobots {
			robotHistories[r.ID].AddPosition(r.position)
		}
		for _, r := range turnInput.enemyRobots {
			robotHistories[r.ID].AddPosition(r.position)
		}

		// apply hole by my robots to previous grid
		previousGrid = applyHoleByMyRobots(previousGrid, turnInput, robotHistories)

		// estimate previous action of enemy robots
		estimatePreviousEnemyRobotAction(turn, turnInput, previousGrid, previousEnemyRobots, robotHistories)

		// store robot item
		for _, r := range turnInput.myRobots {
			robotHistories[r.ID].AddItem(r.item)
		}
		for _, r := range turnInput.enemyRobots {
			robotHistories[r.ID].AddItem(r.item)
		}

		// estimate grid
		previousGrid = nil
		gridEstimated = estimateGrid(turn, turnInput, gridEstimated, robotHistories)

		// decide action plans
		actionPlans := make([]plans, len(turnInput.myRobots))
		for i, r := range turnInput.myRobots {
			actionPlans[i] = decideActionPlans(r, turnInput, gridEstimated, robotHistories)
		}
		// coordinate action plans
		for _, p := range coordinatePlans(actionPlans) {
			p.Exec()
			robotHistories[p.robot.ID].AddAction(p.nextAction)
		}

		// debug
		if DEBUG {
			//gridEstimated.Show()
			//turnInput.Show()
			//showVeinScore(gridEstimated)
			//showEnemyTrapProbability(gridEstimated)
		}
	}

	// each turn
	for {
		processTurn()
	}
}
