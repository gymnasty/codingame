package main

import (
	"fmt"
)

const (
	// action
	actionRequestCmd = "REQUEST"
	actionMoveCmd    = "MOVE"
	actionDigCmd     = "DIG"
	actionWaitCmd    = "WAIT"
)
const (
	actionTypeMove = iota
	actionTypeDig
	actionTypeRequest
	actionTypeRequestRadar
	actionTypeRequestTrap
	actionTypeWait
)

func strActionType(t int) string {
	if s, ok := map[int]string{
		actionTypeMove:         "Move",
		actionTypeDig:          "Dig",
		actionTypeRequest:      "Request",
		actionTypeRequestRadar: "RequestRadar",
		actionTypeRequestTrap:  "RequestTrap",
		actionTypeWait:         "Wait",
	}[t]; ok {
		return s
	}
	return "UnexpectedActionType"
}

type action struct {
	action     int
	position   *position
	candidates []*position
}

func newMoveAction(p *position) *action {
	return &action{action: actionTypeMove, position: p}
}

func newRequestAction() *action {
	return &action{action: actionTypeRequest}
}

func newRequestRadarAction() *action {
	return &action{action: actionTypeRequestRadar}
}

func newRequestTrapAction() *action {
	return &action{action: actionTypeRequestTrap}
}

func newDigAction(p *position) *action {
	return &action{action: actionTypeDig, position: p}
}

func newDigActionWithCandidate(p []*position) *action {
	return &action{action: actionTypeDig, candidates: p}
}

func newWaitAction() *action {
	return &action{action: actionTypeWait}
}

func (a *action) String() string {
	s := strActionType(a.action)
	if a.position != nil {
		s += fmt.Sprintf("(%d,%d)", a.position.x, a.position.y)
	}
	return s
}

func (a *action) Exec() {
	switch a.action {
	case actionTypeMove:
		move(a.position.x, a.position.y)
	case actionTypeRequestRadar:
		request(itemNameRadar)
	case actionTypeRequestTrap:
		request(itemNameTrap)
	case actionTypeDig:
		dig(a.position.x, a.position.y)
	case actionTypeWait:
		wait()
	default:
		panic("unexpected action type")
	}
}

// action
func request(itemName string, message ...string) {
	if DEBUG {
		if message != nil {
			fmt.Println(fmt.Sprintf("%s %s %s", actionRequestCmd, itemName, message))
			return
		}
	}
	fmt.Println(fmt.Sprintf("%s %s", actionRequestCmd, itemName))
}
func move(x int, y int, message ...string) {
	if DEBUG {
		if message != nil {
			fmt.Println(fmt.Sprintf("%s %d %d %s", actionMoveCmd, x, y, message))
			return
		}
	}
	fmt.Println(fmt.Sprintf("%s %d %d", actionMoveCmd, x, y))
}
func dig(x, y int, message ...string) {
	if DEBUG {
		if message != nil {
			fmt.Println(fmt.Sprintf("%s %d %d %s", actionDigCmd, x, y, message))
			return
		}
	}
	fmt.Println(fmt.Sprintf("%s %d %d", actionDigCmd, x, y))
}
func wait(message ...string) {
	if DEBUG {
		if message != nil {
			fmt.Println(fmt.Sprintf("%s %s", actionWaitCmd, message))
			return
		}
	}
	fmt.Println(actionWaitCmd)
}
