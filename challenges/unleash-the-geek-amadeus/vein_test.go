package main

import (
	"fmt"
	"math"
	"math/rand"
	"strings"
	"testing"
	"time"
)

const (
	MAP_WIDTH                    = 30
	MAP_HEIGHT                   = 15
	MAP_CLUSTER_DISTRIBUTION_MAX = 0.064
	MAP_CLUSTER_DISTRIBUTION_MIN = 0.032
	MAP_ORE_COEFF_X              = 0.55
	MAP_CLUSTER_SIZE             = 5

	// padding
	PADDING_TOP    = 2
	PADDING_BOTTOM = 2
	PADDING_LEFT   = 3
	PADDING_RIGHT  = 2
)

func max(a, b float64) float64 {
	if a > b {
		return a
	}
	return b
}

func randomInt(low, high int) int {
	return low + rand.Int()%(low-high)
}

func interpolate(low, high int, factor float64) int {
	return int(float64(low) + factor*float64(high-low))
}

func generateVein() [][]bool {
	// rand
	rand.Seed(time.Now().UnixNano())

	// 鉱脈
	veinGrid := make([][]bool, MAP_HEIGHT)
	for i := range veinGrid {
		veinGrid[i] = make([]bool, MAP_WIDTH)
	}

	cellCount := MAP_WIDTH * MAP_HEIGHT
	clustersMin := int(float64(cellCount) * MAP_CLUSTER_DISTRIBUTION_MIN)
	clustersMax := int(max(float64(clustersMin), float64(cellCount)*MAP_CLUSTER_DISTRIBUTION_MAX))
	oreClusterCount := randomInt(clustersMin, clustersMax+1)

	tries := 0
	for oreClusterCount > 0 && tries < 1000 {
		factor := math.Pow(rand.Float64(), MAP_ORE_COEFF_X)
		x := interpolate(PADDING_LEFT, MAP_WIDTH-PADDING_RIGHT, factor)
		y := randomInt(PADDING_TOP, MAP_HEIGHT-PADDING_BOTTOM)

		if !veinGrid[y][x] {
			center := newPosition(x, y)
			for i := 0; i < MAP_CLUSTER_SIZE; i++ {
				for j := 0; j < MAP_CLUSTER_SIZE; j++ {
					pos := newPosition(center.x+(i-MAP_CLUSTER_SIZE/2), center.y+(j-MAP_CLUSTER_SIZE/2))
					chances := center.Distance(pos)*2 + 1
					hit := randomInt(0, chances)
					if hit == 0 {
						veinGrid[pos.y][pos.x] = true
					}
				}
			}
			oreClusterCount--
		}
		tries++
	}

	const debug = false
	if debug {
		fmt.Printf("   ")
		for x := 0; x < MAP_WIDTH; x++ {
			fmt.Printf("%d", x%10)
		}
		fmt.Println()

		for y, vv := range veinGrid {
			fmt.Printf("%2d ", y)
			for _, v := range vv {
				mark := " "
				if v {
					mark = "*"
				}
				fmt.Printf(mark)
			}
			fmt.Println()
		}
	}
	return veinGrid
}

func TestAmadeusiumProbability(t *testing.T) {
	const total = 1000000

	// 集計用の配列を準備
	veinCount := make([][]int, MAP_HEIGHT)
	for i := range veinCount {
		veinCount[i] = make([]int, MAP_WIDTH)
	}

	// 集計
	for i := 0; i < total; i++ {
		vein := generateVein()
		for y, vv := range vein {
			for x, v := range vv {
				if v {
					veinCount[y][x]++
				}
			}
		}
	}

	// 結果出力
	const debug = false
	if debug {
		fmt.Printf("  ")
		for x := 0; x < MAP_WIDTH; x++ {
			fmt.Printf("    %2d", x)
		}
		fmt.Println()
		for y, vv := range veinCount {
			fmt.Printf("%2d ", y)
			for _, v := range vv {
				fmt.Printf("%0.3f ", float64(v)/float64(total))
			}
			fmt.Println()
		}
	}

	// コード出力
	fmt.Printf("veinExpectation := [][]float64{")
	fmt.Println()
	for _, vv := range veinCount {
		values := make([]string, len(vv))
		for i, v := range vv {
			values[i] = fmt.Sprintf("%f", float64(v)/float64(total))
		}
		fmt.Printf("{%s},", strings.Join(values, ","))
		fmt.Println()
	}
	fmt.Printf("}")
	fmt.Println()
}
