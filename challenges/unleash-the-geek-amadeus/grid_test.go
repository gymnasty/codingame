package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGrid_Cluster(t *testing.T) {
	g := newGrid(MAP_WIDTH, MAP_HEIGHT)

	// normal
	cc := g.Cluster(newPosition(5, 5), 1)
	assert.ElementsMatch(t, []*cell{
		newCell(4, 5),
		newCell(5, 4),
		newCell(5, 5),
		newCell(5, 6),
		newCell(6, 5),
	}, cc)

	// left-top
	cc = g.Cluster(newPosition(0, 0), 1)
	assert.ElementsMatch(t, []*cell{
		newCell(0, 0),
		newCell(0, 1),
		newCell(1, 0),
	}, cc)

	// left-bottom
	cc = g.Cluster(newPosition(0, 14), 1)
	assert.ElementsMatch(t, []*cell{
		newCell(0, 14),
		newCell(1, 14),
		newCell(0, 13),
	}, cc)
}
