package main

import (
	"fmt"
	"os"
)

func abs(a int) int {
	if a > 0 {
		return a
	}
	return -a
}

func minInt(a int, b int) int {
	if a > b {
		return b
	}
	return a
}

func maxInt(a int, b int) int {
	if a < b {
		return b
	}
	return a
}

func maxIndex(a []float64) int {
	if len(a) == 0 {
		return -1
	}
	index := 0
	m := a[index]
	for i := 1; i < len(a); i++ {
		if m < a[i] {
			index = i
			m = a[i]
		}
	}
	return index
}

func debugLog(format string, a ...interface{}) {
	if !DEBUG {
		return
	}
	_, _ = fmt.Fprintf(os.Stderr, format, a...)
}

func debugLogLine(format string, a ...interface{}) {
	if !DEBUG {
		return
	}
	debugLog(format+"\n", a...)
}
