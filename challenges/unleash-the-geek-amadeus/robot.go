package main

import (
	"fmt"
)

const (
	// item
	itemDummy       = -3
	itemRadarOrTrap = -2
	itemNone        = -1
	itemRadar       = 2
	itemTrap        = 3
	itemAmadeusium  = 4
	// item name
	itemNameRadar = "RADAR"
	itemNameTrap  = "TRAP"
	// velocity
	velocity = 4
)

// robot
type robot struct {
	*entity
	item int
}

func newRobot(ID, Type, x, y, item int) *robot {
	return &robot{
		entity: newEntity(ID, Type, x, y),
		item:   item,
	}
}

func (r *robot) TurnsTo(pos *position) int {
	d := r.Distance(pos)
	return turnToMove(d)
}

func (r *robot) TurnsToHeadquarter() int {
	return turnToHeadquarter(r.position)
}

func (r *robot) IsDead() bool {
	return r.x == -1 && r.y == -1
}

func (r *robot) InHeadquarter() bool {
	return r.x == 0
}

func (r *robot) HasAmadeusium() bool {
	return r.item == itemAmadeusium
}

func (r *robot) HasRadar() bool {
	return r.item == itemRadar
}

func (r *robot) HasTrap() bool {
	return r.item == itemTrap
}

func (r *robot) HasNothing() bool {
	return r.item == itemNone
}

func (r *robot) RequestRadar(message ...string) {
	request(itemNameRadar, message...)
}

func (r *robot) RequestTrap(message ...string) {
	request(itemNameTrap, message...)
}

func (r *robot) Move(x int, y int, message ...string) {
	move(x, y, message...)
}

func (r *robot) Dig(x int, y int, message ...string) {
	dig(x, y, message...)
}

func (r *robot) Wait(message ...string) {
	wait(message...)
}

func (r *robot) String() string {
	var item string
	switch r.item {
	case itemRadarOrTrap:
		item = "with Radar or Trap"
	case itemNone:
		item = ""
	case itemAmadeusium:
		item = "with Amadeusium"
	case itemRadar:
		item = "with Radar"
	case itemTrap:
		item = "with Trap"
	default:
		item = fmt.Sprintf("with UnknownItem(%d)", r.item)
	}
	return r.entity.String() + " " + item
}

func (r *robot) Show() {
	debugLogLine(r.String())
}

func turnToMove(distance int) int {
	t := distance / velocity
	if distance%velocity > 0 {
		t++
	}
	return t
}

func turnToHeadquarter(currentPos *position) int {
	return turnToMove(currentPos.x)
}

func findDigPosition(r *robot, pos *position) *position {
	d := r.Distance(pos)
	if d <= 1 {
		// can dig from current position
		return r.position
	}
	if r.x < pos.x {
		return newPosition(pos.x-1, pos.y)
	}
	switch d % velocity {
	case 0:
		return pos
	case 1:
		if r.y == pos.y {
			return newPosition(pos.x+1, pos.y)
		}
		if r.y > pos.y {
			return newPosition(pos.x, pos.y+1)
		}
		if r.y < pos.y {
			return newPosition(pos.x, pos.y-1)
		}
	case 2:
		return newPosition(pos.x-1, pos.y)
	case 3:
		return newPosition(pos.x-1, pos.y)
	}
	return pos
}

func calcDigAndGoHeadquarterTurns(r *robot, pos *position) int {
	p := findDigPosition(r, pos)
	return r.TurnsTo(p) + newRobot(0, entityTypeEnemyRobot, p.x, p.y, itemNone).TurnsToHeadquarter()
}

func checkMove(previous, current *robot) bool {
	if current.Distance(previous.position) != 0 {
		// moving
		return true
	}
	return false
}

func checkRequest(previous, current *robot) bool {
	if current.x != 0 {
		return false
	}
	if current.Distance(previous.position) == 0 {
		// request something
		return true
	}
	return false
}

func checkDig(previousGrid, grid *grid, robot *robot) (bool, []*position) {
	targets := grid.DigTargets(robot.position)
	candidates := make([]*position, 0, 5)
	// check change hole state for dig targets
	for _, t := range targets {
		if t.IsHole {
			// TODO: check other hole state change
			candidates = append(candidates, t.position)
			if !previousGrid.GetCell(t.position).IsHole {
				return true, []*position{t.position}
			}
		}
	}
	// check hole around the robot
	if len(candidates) == 0 {
		return false, nil
	} else {
		return true, candidates
	}
}

func findNextPositions(robot *robot, destination *position) []*position {
	d := robot.Distance(destination)
	if d <= velocity {
		return []*position{destination}
	}
	// TODO: optimize calculation cost
	positions := make([]*position, 0, 5)
	for _, m := range moveTable {
		p := newPosition(robot.x+m.x, robot.y+m.y)
		if p.IsValid(maxWidth, maxHeight) && p.Distance(destination) == d-velocity {
			positions = append(positions, p)
		}
	}
	return positions
}

func countRobot(robots []*robot) int {
	c := 0
	for _, r := range robots {
		if !r.IsDead() {
			c++
		}
	}
	return c
}
