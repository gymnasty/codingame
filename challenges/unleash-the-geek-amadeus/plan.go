package main

import (
	"fmt"
	"math/rand"
	"sort"
)

const (
	planTypePutRadar = iota
	planTypePutTrap
	planTypeDigVein
	planTypeSearchVein
	planTypeDeliverAmadeusium
	planTypeWait
	planTypeDummyRequest
	planTypeTriggerTrap
)

func strPlanType(t int) string {
	if s, ok := map[int]string{
		planTypePutRadar:          "PutRadar",
		planTypePutTrap:           "PutTrap",
		planTypeDigVein:           "Dig",
		planTypeSearchVein:        "SearchVein",
		planTypeDeliverAmadeusium: "Deliver",
		planTypeWait:              "Wait",
	}[t]; ok {
		return s
	}
	return "UnexpectedPlanType"
}

// 優先順位
// 新規の鉱脈
// 本部に近い鉱脈
// 爆弾が置かれている確率が低い鉱脈

type plan struct {
	planType        int
	turn            int
	robot           *robot
	target          *cell
	score           float64
	turnsToComplete int
	nextAction      *action
}

func (p *plan) String() string {
	s := fmt.Sprintf("%s: Score: %f ", strPlanType(p.planType), p.Score())
	if p.target != nil {
		s += fmt.Sprintf("Target:(%d,%d)+%f ", p.target.x, p.target.y, p.target.EnemyTrapProbability)
	}
	if p.nextAction != nil {
		s += p.nextAction.String()
	}
	return s
}

func (p *plan) Exec() {
	p.nextAction.Exec()
	debugLogLine("E: %s", p.String())
}

func (p *plan) Score() float64 {
	score := p.score
	if p.planType == planTypePutRadar {
		score *= 3
	}
	if p.planType == planTypePutTrap {
		score *= 2
	}
	if isDigPlanType(p.planType) {
		if p.target.Amadeusium > 0 && !p.target.IsHole {
			score *= 1.1
		} else {
			score -= p.target.EnemyTrapProbability * float64(maxTurn-p.turn) * 0.1
		}
	}
	return score / float64(p.turnsToComplete)
}

func newPutRadarPlan(turn int, robot *robot, target *cell, score float64, turnToPutRadar int, nextAction *action) *plan {
	return &plan{
		planType:        planTypePutRadar,
		turn:            turn,
		robot:           robot,
		target:          target,
		score:           score,
		turnsToComplete: turnToPutRadar,
		nextAction:      nextAction,
	}
}

func newPutTrapPlan(turn int, robot *robot, target *cell, score float64, turnToPutTrap int, nextAction *action) *plan {
	return &plan{
		planType:        planTypePutTrap,
		turn:            turn,
		robot:           robot,
		target:          target,
		score:           score,
		turnsToComplete: turnToPutTrap,
		nextAction:      nextAction,
	}
}

func newDeliverAmadeusiumPlan(turn int, robot *robot, headquarterTarget *cell, turnToCompleteDeliverAmadeusium int, nextAction *action) *plan {
	return &plan{
		planType:        planTypeDeliverAmadeusium,
		turn:            turn,
		robot:           robot,
		target:          headquarterTarget,
		score:           1,
		turnsToComplete: turnToCompleteDeliverAmadeusium,
		nextAction:      nextAction,
	}
}

func newDigVeinPlan(turn int, robot *robot, veinTarget *cell, score float64, turnToCompleteDeliverAmadeusium int, nextAction *action) *plan {
	return &plan{
		planType:        planTypeDigVein,
		turn:            turn,
		robot:           robot,
		target:          veinTarget,
		score:           score,
		turnsToComplete: turnToCompleteDeliverAmadeusium,
		nextAction:      nextAction,
	}
}

func newSearchVeinPlan(turn int, robot *robot, veinTarget *cell, score float64, turnToCompleteDeliverAmadeusium int, nextAction *action) *plan {
	return &plan{
		planType:        planTypeSearchVein,
		turn:            turn,
		robot:           robot,
		target:          veinTarget,
		score:           score,
		turnsToComplete: turnToCompleteDeliverAmadeusium,
		nextAction:      nextAction,
	}
}

func newWaitPlan(turn int, robot *robot) *plan {
	return &plan{
		planType:        planTypeWait,
		turn:            turn,
		robot:           robot,
		target:          nil,
		score:           0,
		turnsToComplete: 1,
		nextAction:      newWaitAction(),
	}
}

func newDummyRequestPlan(turn int, robot *robot) *plan {
	return &plan{
		planType:        planTypeDummyRequest,
		turn:            turn,
		robot:           robot,
		target:          nil,
		score:           0,
		turnsToComplete: 1,
		nextAction:      newWaitAction(),
	}
}

func newTriggerTrapPlan(turn int, robot *robot, p *position) *plan {
	return &plan{
		planType:        planTypeTriggerTrap,
		turn:            turn,
		robot:           robot,
		target:          nil,
		score:           0,
		turnsToComplete: 1,
		nextAction:      newDigAction(p),
	}
}

type plans []*plan

func (p plans) Len() int {
	return len(p)
}
func (p plans) Less(i, j int) bool {
	return p[i].Score() < p[j].Score()
}
func (p plans) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func decideActionPlans(r *robot, turnInput *turnInput, gridEstimated *grid, robotHistories map[int]*robotHistory) []*plan {
	var actionPlans plans
	actionPlans = make([]*plan, 0, 1000)

	isRequest := false

	// dead robots can wait only
	if r.IsDead() {
		actionPlans = append(actionPlans, newWaitPlan(turnInput.turn, r))
		return actionPlans
	}

	// kill enemies
	for _, t := range turnInput.traps {
		if r.Distance(t.position) > 1 {
			continue
		}
		affectedMyRobots := 0
		for _, r := range turnInput.myRobots {
			if r.Distance(t.position) <= 1 {
				affectedMyRobots++
			}
		}
		affectedEnemyRobots := 0
		for _, r := range turnInput.enemyRobots {
			if r.Distance(t.position) <= 1 {
				affectedEnemyRobots++
			}
		}
		if affectedMyRobots < affectedEnemyRobots || affectedMyRobots == affectedEnemyRobots && r.HasNothing() {
			return []*plan{newTriggerTrapPlan(turnInput.turn, r, t.position)}
		}
	}

	// deliver Amadeusium to headquarter
	if r.HasAmadeusium() {
		turn := r.TurnsToHeadquarter()
		// TODO: evaluate other positions
		hqPos := newPosition(0, r.y)
		actionPlans = append(actionPlans, newDeliverAmadeusiumPlan(turnInput.turn, r, gridEstimated.GetCell(hqPos), turn, newMoveAction(hqPos)))
		return actionPlans
	}

	// radar
	if r.InHeadquarter() && r.HasNothing() && turnInput.radarCoolDown == 0 || r.HasRadar() {
		radarScores := listRadarScore(turnInput, gridEstimated)
		for _, s := range radarScores {
			if r.HasRadar() {
				digPosition := findDigPosition(r, s.position)
				turn := r.TurnsTo(digPosition) + 1
				if r.Distance(digPosition) == 0 {
					actionPlans = append(actionPlans, newPutRadarPlan(turnInput.turn, r, gridEstimated.GetCell(s.position), s.score, turn, newDigAction(s.position)))
				} else {
					for _, p := range findNextPositions(r, digPosition) {
						actionPlans = append(actionPlans, newPutRadarPlan(turnInput.turn, r, gridEstimated.GetCell(s.position), s.score, turn, newMoveAction(p)))
					}
				}
			} else {
				isRequest = true
				digPosition := findDigPosition(r, s.position)
				turn := 1 + r.TurnsTo(digPosition) + 1
				actionPlans = append(actionPlans, newPutRadarPlan(turnInput.turn, r, gridEstimated.GetCell(s.position), s.score, turn, newRequestRadarAction()))
			}
		}
	}
	// TODO: go back to headquarter to get radar

	// list veins
	veins := veins(gridEstimated)
	veinsForDig, veinsForMove := splitByDistance(veins, r.position, 1)

	// trap
	if turnInput.turn > 20 && turnInput.turn < 100 && r.InHeadquarter() && r.HasNothing() && turnInput.trapCoolDown == 0 || r.HasTrap() {
		nearVein, turnToNearVein := findNearVeinForTrap(r, gridEstimated)
		if nearVein != nil {
			digPosition := findDigPosition(r, nearVein.position)
			if r.HasTrap() {
				if r.Distance(digPosition) == 0 {
					actionPlans = append(actionPlans, newPutTrapPlan(turnInput.turn, r, nearVein, 1, turnToNearVein, newDigAction(nearVein.position)))
				} else {
					for _, p := range findNextPositions(r, digPosition) {
						actionPlans = append(actionPlans, newPutTrapPlan(turnInput.turn, r, nearVein, 1, turnToNearVein, newMoveAction(p)))
					}
				}
			} else {
				isRequest = true
				actionPlans = append(actionPlans, newPutTrapPlan(turnInput.turn, r, nearVein, 1, turnToNearVein+1, newRequestTrapAction()))
			}
		}
	}

	// dummy request
	if r.InHeadquarter() && r.HasNothing() && !isRequest {
		if rand.Float64() < 0.2 {
			return []*plan{newWaitPlan(turnInput.turn, r)}
		}
	}

	// dig
	{
		digTargets := filterOutMyTrap(veinsForDig)
		turnToHeadquarter := r.TurnsToHeadquarter()
		for _, target := range digTargets {
			e := veinExpectation(gridEstimated.GetCell(target.position))
			if e == 1 {
				actionPlans = append(actionPlans, newDigVeinPlan(turnInput.turn, r, target, e, turnToHeadquarter+1, newDigAction(target.position)))
			} else {
				actionPlans = append(actionPlans, newSearchVeinPlan(turnInput.turn, r, target, e, turnToHeadquarter+1, newDigAction(target.position)))
			}
		}
	}

	// move and dig
	{
		for _, vein := range filterOutMyTrap(veinsForMove) {
			digPosition := findDigPosition(r, vein.position)
			e := veinExpectation(vein)
			turnToHeadquarter := r.TurnsTo(digPosition) + 1 + turnToHeadquarter(digPosition)
			for _, p := range findNextPositions(r, digPosition) {
				actionPlans = append(actionPlans, newDigVeinPlan(turnInput.turn, r, vein, e, turnToHeadquarter, newMoveAction(p)))
			}
		}
	}

	// search
	{
		unknownCells := filterOutKnown(gridEstimated.AllCells())
		cellsForDig, cellsForMove := splitByDistance(unknownCells, r.position, 1)
		for _, c := range cellsForDig {
			e := veinExpectation(c)
			turn := 1 + r.TurnsToHeadquarter()
			actionPlans = append(actionPlans, newSearchVeinPlan(turnInput.turn, r, c, e, turn, newDigAction(c.position)))
		}
		for _, c := range cellsForMove {
			e := veinExpectation(c)
			digPosition := findDigPosition(r, c.position)
			turn := r.TurnsTo(digPosition) + 1 + turnToHeadquarter(digPosition)
			for _, p := range findNextPositions(r, digPosition) {
				actionPlans = append(actionPlans, newSearchVeinPlan(turnInput.turn, r, c, e, turn, newMoveAction(p)))
			}
		}
	}

	// wait
	actionPlans = append(actionPlans, newWaitPlan(turnInput.turn, r))

	// sort
	sort.Sort(sort.Reverse(actionPlans))

	return actionPlans
}

func isDigPlanType(planType int) bool {
	return planType == planTypeDigVein || planType == planTypePutTrap || planType == planTypePutRadar || planType == planTypeSearchVein
}

func coordinatePlans(pp []plans) []*plan {
	coordinatedPlansIndex := make([]int, len(pp))
	for {
		coordinatedPlans := make([]*plan, len(pp))
		for i, p := range pp {
			coordinatedPlans[i] = p[coordinatedPlansIndex[i]]
		}

		conflicts := checkConflict(coordinatedPlans)
		if len(conflicts) <= 1 {
			return coordinatedPlans
		}

		// resolve conflicts
		scores := make([]float64, len(conflicts))
		for i, v := range conflicts {
			scores[i] = coordinatedPlans[v].Score()
		}
		selectIndex := conflicts[maxIndex(scores)]
		for _, v := range conflicts {
			if v != selectIndex {
				coordinatedPlansIndex[v]++
			}
		}
	}
}

func checkConflict(pp []*plan) []int {
	mapToList := func(m map[int]bool) []int {
		l := make([]int, 0, len(m))
		for k := range m {
			l = append(l, k)
		}
		return l
	}

	// check radar request
	duplicates := make(map[int]bool)
	for i, p1 := range pp {
		if p1.planType == planTypePutRadar && p1.nextAction.action == actionTypeRequestRadar {
			for j, p2 := range pp[i+1:] {
				if p2.planType == planTypePutRadar && p2.nextAction.action == actionTypeRequestRadar {
					duplicates[i] = true
					duplicates[i+j+1] = true
				}
			}
			if len(duplicates) > 1 {
				return mapToList(duplicates)
			}
		}
	}

	// check trap request
	duplicates = make(map[int]bool)
	for i, p1 := range pp {
		if p1.planType == planTypePutTrap && p1.nextAction.action == actionTypeRequestTrap {
			for j, p2 := range pp[i+1:] {
				if p2.planType == planTypePutTrap && p2.nextAction.action == actionTypeRequestTrap {
					duplicates[i] = true
					duplicates[i+j+1] = true
				}
			}
			if len(duplicates) > 1 {
				return mapToList(duplicates)
			}
		}
	}

	// check dig target
	duplicates = make(map[int]bool)
	for i, p1 := range pp {
		if isDigPlanType(p1.planType) {
			for j, p2 := range pp[i+1:] {
				if isDigPlanType(p2.planType) {
					if p1.target.Distance(p2.target.position) == 0 {
						duplicates[i] = true
						duplicates[i+j+1] = true
					}
				}
			}
			if len(duplicates) > 1 {
				return mapToList(duplicates)
			}
		}
	}

	// next position
	duplicates = make(map[int]bool)
	for i, p1 := range pp {
		if isDigPlanType(p1.planType) && p1.nextAction.position != nil {
			for j, p2 := range pp[i+1:] {
				if isDigPlanType(p2.planType) && p2.nextAction.position != nil {
					if p1.nextAction.position.Distance(p2.nextAction.position) <= 2 {
						duplicates[i] = true
						duplicates[i+j+1] = true
					}
				}
			}
			if len(duplicates) > 1 {
				return mapToList(duplicates)
			}
		}
	}

	return []int{}
}
