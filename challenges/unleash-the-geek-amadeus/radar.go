package main

var radarCount = 0

type radarScore struct {
	score    float64
	position *position
}

// TODO: optimize calculation cost
func getRadarScore(gridEstimated *grid, pos *position) float64 {
	cc := gridEstimated.Cluster(pos, 4)
	var score float64
	for _, c := range cc {
		if c.Amadeusium == amadeusiumUnknown {
			score += veinScore(c)
		}
	}
	return score
}

func listRadarScore(turnInput *turnInput, gridEstimated *grid) []*radarScore {
	r := make([]*radarScore, 0, maxWidth*maxHeight)
	for _, cc := range gridEstimated.cells {
		for x, c := range filterOutMyTrap(cc) {
			if x == 0 {
				continue
			}
			score := getRadarScore(gridEstimated, c.position)
			r = append(r, &radarScore{score, c.position})
		}
	}
	return r
}

func getNextRadarPosition(turnInput *turnInput, gridEstimated *grid) (*position, float64) {
	var maxRadarScore float64
	var maxPosition *position
	for _, cc := range gridEstimated.cells {
		for x, c := range cc {
			if x == 0 {
				continue
			}
			if len(turnInput.radars) == 0 && x > 5 {
				continue
			}
			score := getRadarScore(gridEstimated, c.position)
			if maxPosition == nil || score > maxRadarScore || score == maxRadarScore && c.x < maxPosition.x {
				maxRadarScore = score
				maxPosition = c.position
			}
		}
	}
	if DEBUG {
		// debugLogLine("RadarTargetPosition: (%d, %d) score:%f", maxPosition.x, maxPosition.y, maxRadarScore)
	}
	return maxPosition, maxRadarScore
}

func putRadar() {
	radarCount++
}
