package main

func showEnemyTrapProbability(g *grid) {
	// header
	debugLog("    ")
	for i := range g.cells[0] {
		debugLog("  %2d  ", i)
	}
	debugLogLine("")
	// body
	for i, cs := range g.cells {
		debugLog("%2d :", i)
		for _, c := range cs {
			debugLog("%0.3f|", c.EnemyTrapProbability)
		}
		debugLogLine("")
	}
}
