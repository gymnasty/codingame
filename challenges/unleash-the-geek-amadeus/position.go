package main

// position
type position struct {
	x, y int
}

func newPosition(x, y int) *position {
	return &position{
		x: x,
		y: y,
	}
}

func (p position) Distance(pos *position) int {
	return abs(p.x-pos.x) + abs(p.y-pos.y)
}

func (p position) IsValid(width, height int) bool {
	return p.x >= 0 && p.y >= 0 && p.x < width && p.y < height
}
