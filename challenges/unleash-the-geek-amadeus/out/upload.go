package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"time"
)

const (
	// action
	actionRequestCmd = "REQUEST"
	actionMoveCmd    = "MOVE"
	actionDigCmd     = "DIG"
	actionWaitCmd    = "WAIT"
)
const (
	actionTypeMove = iota
	actionTypeDig
	actionTypeRequest
	actionTypeRequestRadar
	actionTypeRequestTrap
	actionTypeWait
)

func strActionType(t int) string {
	if s, ok := map[int]string{
		actionTypeMove:         "Move",
		actionTypeDig:          "Dig",
		actionTypeRequest:      "Request",
		actionTypeRequestRadar: "RequestRadar",
		actionTypeRequestTrap:  "RequestTrap",
		actionTypeWait:         "Wait",
	}[t]; ok {
		return s
	}
	return "UnexpectedActionType"
}

type action struct {
	action     int
	position   *position
	candidates []*position
}

func newMoveAction(p *position) *action {
	return &action{action: actionTypeMove, position: p}
}

func newRequestAction() *action {
	return &action{action: actionTypeRequest}
}

func newRequestRadarAction() *action {
	return &action{action: actionTypeRequestRadar}
}

func newRequestTrapAction() *action {
	return &action{action: actionTypeRequestTrap}
}

func newDigAction(p *position) *action {
	return &action{action: actionTypeDig, position: p}
}

func newDigActionWithCandidate(p []*position) *action {
	return &action{action: actionTypeDig, candidates: p}
}

func newWaitAction() *action {
	return &action{action: actionTypeWait}
}

func (a *action) String() string {
	s := strActionType(a.action)
	if a.position != nil {
		s += fmt.Sprintf("(%d,%d)", a.position.x, a.position.y)
	}
	return s
}

func (a *action) Exec() {
	switch a.action {
	case actionTypeMove:
		move(a.position.x, a.position.y)
	case actionTypeRequestRadar:
		request(itemNameRadar)
	case actionTypeRequestTrap:
		request(itemNameTrap)
	case actionTypeDig:
		dig(a.position.x, a.position.y)
	case actionTypeWait:
		wait()
	default:
		panic("unexpected action type")
	}
}

// action
func request(itemName string, message ...string) {
	if DEBUG {
		if message != nil {
			fmt.Println(fmt.Sprintf("%s %s %s", actionRequestCmd, itemName, message))
			return
		}
	}
	fmt.Println(fmt.Sprintf("%s %s", actionRequestCmd, itemName))
}
func move(x int, y int, message ...string) {
	if DEBUG {
		if message != nil {
			fmt.Println(fmt.Sprintf("%s %d %d %s", actionMoveCmd, x, y, message))
			return
		}
	}
	fmt.Println(fmt.Sprintf("%s %d %d", actionMoveCmd, x, y))
}
func dig(x, y int, message ...string) {
	if DEBUG {
		if message != nil {
			fmt.Println(fmt.Sprintf("%s %d %d %s", actionDigCmd, x, y, message))
			return
		}
	}
	fmt.Println(fmt.Sprintf("%s %d %d", actionDigCmd, x, y))
}
func wait(message ...string) {
	if DEBUG {
		if message != nil {
			fmt.Println(fmt.Sprintf("%s %s", actionWaitCmd, message))
			return
		}
	}
	fmt.Println(actionWaitCmd)
}

const (
	// ore
	oreUnknown        = '?'
	amadeusiumUnknown = -1
	amadeusiumWasVein = -2
)

type cell struct {
	*position
	Amadeusium            int
	IsHole                bool
	digCountProbabilities []float64
	IsMyTrap              bool
	EnemyTrapProbability  float64
}

func newCell(x, y int) *cell {
	digCountProbabilities := make([]float64, 4)
	digCountProbabilities[0] = 1
	return &cell{
		position:              newPosition(x, y),
		Amadeusium:            amadeusiumUnknown,
		IsHole:                false,
		digCountProbabilities: digCountProbabilities,
		IsMyTrap:              false,
		EnemyTrapProbability:  0,
	}
}

func (c *cell) Show() {
	holeMark := " "
	if c.IsHole {
		holeMark = "*"
	}
	ore := "  "
	if c.Amadeusium >= 0 {
		ore = fmt.Sprintf("%2d", c.Amadeusium)
	}
	debugLog("%s %s", ore, holeMark)
}

func (c *cell) Dig(probability float64) {
	restProbability := 1 - probability
	p0 := c.digCountProbabilities[0]
	p1 := c.digCountProbabilities[1]
	p2 := c.digCountProbabilities[2]
	p3 := c.digCountProbabilities[3]
	c.digCountProbabilities[0] = p0 * restProbability
	c.digCountProbabilities[1] = p0*probability + p1*restProbability
	c.digCountProbabilities[2] = p1*probability + p2*restProbability
	c.digCountProbabilities[3] = p2*probability + p3*restProbability
}

func (c *cell) DigCountProbability(count int) float64 {
	return c.digCountProbabilities[count]
}

func filterOutHole(cells []*cell) []*cell {
	if cells == nil {
		return nil
	}
	newVeins := make([]*cell, 0, len(cells))
	for _, c := range cells {
		if !c.IsHole {
			newVeins = append(newVeins, c)
		}
	}
	return newVeins
}

func filterOutKnown(cells []*cell) []*cell {
	if cells == nil {
		return nil
	}
	newVeins := make([]*cell, 0, len(cells))
	for _, c := range cells {
		if c.Amadeusium == amadeusiumUnknown {
			newVeins = append(newVeins, c)
		}
	}
	return newVeins
}

func filterOutMyTrap(cells []*cell) []*cell {
	if cells == nil {
		return nil
	}
	newVeins := make([]*cell, 0, len(cells))
	for _, c := range cells {
		if !c.IsMyTrap {
			newVeins = append(newVeins, c)
		}
	}
	return newVeins
}

func filterOutTrap(cells []*cell) []*cell {
	if cells == nil {
		return nil
	}
	newVeins := make([]*cell, 0, len(cells))
	for _, c := range cells {
		if !c.IsMyTrap && c.EnemyTrapProbability == 0 {
			newVeins = append(newVeins, c)
		}
	}
	return newVeins
}

func filterOutByAmadeusiumCount(cells []*cell, requiredCount int) []*cell {
	if cells == nil {
		return nil
	}
	newVeins := make([]*cell, 0, len(cells))
	for _, c := range cells {
		if c.Amadeusium >= requiredCount {
			newVeins = append(newVeins, c)
		}
	}
	return newVeins
}

func splitByDistance(cells []*cell, pos *position, distance int) (inVeins []*cell, outVeins []*cell) {
	if cells == nil {
		return nil, nil
	}
	inVeins = make([]*cell, 0, len(cells))
	outVeins = make([]*cell, 0, len(cells))
	for _, c := range cells {
		if c.Distance(pos) <= distance {
			inVeins = append(inVeins, c)
		} else {
			outVeins = append(outVeins, c)
		}
	}
	return inVeins, outVeins
}

const (
	DEBUG = false

	maxTurn               = 200
	maxWidth              = 30
	maxHeight             = 15
	radarCoolDownInterval = 5
	trapCoolDownInterval  = 5
)

const (
	// entityType
	entityTypeMyRobot    = 0
	entityTypeEnemyRobot = 1
	entityTypeRadar      = 2
	entityTypeTrap       = 3
)

// entity
type entity struct {
	*position
	ID   int
	Type int
}

func newEntity(ID, Type, x, y int) *entity {
	return &entity{
		position: newPosition(x, y),
		ID:       ID,
		Type:     Type,
	}
}

func (e *entity) String() string {
	var typeName string
	switch e.Type {
	case entityTypeMyRobot:
		typeName = "MyRobot"
	case entityTypeEnemyRobot:
		typeName = "EnemyRobot"
	case entityTypeRadar:
		typeName = "Radar"
	case entityTypeTrap:
		typeName = "Trap"
	default:
		typeName = fmt.Sprintf("UnsupportedEntityType(%d)", e.Type)
	}
	return fmt.Sprintf("%s: %d: pos(%d, %d)", typeName, e.ID, e.x, e.y)
}

func (e *entity) Show() {
	debugLogLine(e.String())
}

func estimateGrid(turn int, turnInput *turnInput, gridEstimated *grid, robotHistories map[int]*robotHistory) *grid {
	if turn == 0 {
		return turnInput.grid
	}
	// update from robotHistories
	for _, robot := range turnInput.myRobots {
		previousAction := robotHistories[robot.ID].GetAction(turn - 1)
		if previousAction.action == actionTypeDig {
			pos := previousAction.position
			cellEstimated := gridEstimated.GetCell(pos)
			if robot.HasAmadeusium() {
				cellEstimated.Amadeusium = amadeusiumWasVein
			} else {
				cellEstimated.Amadeusium = 0
			}
			cellEstimated.Dig(1)
		}
	}
	// update from grid
	for _, cc := range turnInput.grid.cells {
		for _, c := range cc {
			cellEstimated := gridEstimated.GetCell(c.position)
			if c.Amadeusium >= 0 {
				cellEstimated.Amadeusium = c.Amadeusium
			}
			cellEstimated.IsHole = c.IsHole
		}
	}
	// update from traps
	for _, cc := range gridEstimated.cells {
		for _, c := range cc {
			c.IsMyTrap = false
		}
	}
	for _, t := range turnInput.traps {
		cellEstimated := gridEstimated.GetCell(t.position)
		cellEstimated.IsMyTrap = true
	}
	// update from enemy robotHistories
	enemyPutPositions := make([]*position, 0, len(turnInput.enemyRobots))
	for _, robot := range turnInput.enemyRobots {
		previousAction := robotHistories[robot.ID].GetAction(turn - 1)
		previousItem := robotHistories[robot.ID].GetItem(turn - 1)
		if previousAction.action == actionTypeDig {
			if previousAction.position != nil {
				cellEstimated := gridEstimated.GetCell(previousAction.position)
				if previousItem == itemRadarOrTrap {
					enemyPutPositions = append(enemyPutPositions, previousAction.position)
					cellEstimated.EnemyTrapProbability = 0.5
					debugLogLine("set enemy trap probability: %f, %v", cellEstimated.EnemyTrapProbability, cellEstimated.position)
				} else {
					// no trap if enemy digs the cell except other enemy robot put trap in this turn
					func() {
						for _, pos := range enemyPutPositions {
							if pos.Distance(previousAction.position) == 0 {
								return
							}
						}
						cellEstimated.EnemyTrapProbability = 0
						debugLogLine("reset enemy trap probability: %f, %v", cellEstimated.EnemyTrapProbability, cellEstimated.position)
					}()
				}
				cellEstimated.Dig(1)
			} else {
				e := float64(1) / float64(len(previousAction.candidates))
				for _, p := range previousAction.candidates {
					cellEstimated := gridEstimated.GetCell(p)
					if previousItem == itemRadarOrTrap {
						enemyPutPositions = append(enemyPutPositions, p)
						cellEstimated.EnemyTrapProbability = 0.5 * e
						debugLogLine("set enemy trap probability: %f, %v", cellEstimated.EnemyTrapProbability, cellEstimated.position)
					}
					cellEstimated.Dig(e)
				}
			}
		}
	}
	return gridEstimated
}

func estimatePreviousEnemyRobotAction(turn int, turnInput *turnInput, previousGrid *grid, previousEnemyRobots []*robot, robotHistories map[int]*robotHistory) {
	if turn == 0 {
		return
	}
	for i, r := range turnInput.enemyRobots {
		previous := previousEnemyRobots[i]
		current := r
		history := robotHistories[r.ID]
		// check move
		if checkMove(previous, current) {
			history.AddAction(newMoveAction(current.position))
			current.item = previous.item
			continue
		}
		// check request
		if checkRequest(previous, current) {
			history.AddAction(newRequestAction())
			current.item = itemRadarOrTrap
			continue
		}
		// check dig
		if dig, pos := checkDig(previousGrid, turnInput.grid, current); dig {
			current.item = itemNone
			if len(pos) == 1 {
				history.AddAction(newDigAction(pos[0]))
			} else {
				history.AddAction(newDigActionWithCandidate(pos))
			}
			continue
		}
		// wait
		history.AddAction(newWaitAction())
		current.item = previous.item
	}
}

func calcEnemyTrapProbability(currentTrapProbability, putTrapProbability float64) float64 {
	return (1-currentTrapProbability)*putTrapProbability + currentTrapProbability
}

type grid struct {
	cells  [][]*cell
	width  int
	height int
}

func newGrid(width, height int) *grid {
	cells := make([][]*cell, height)
	for i := range cells {
		cells[i] = make([]*cell, width)
	}
	for y, cc := range cells {
		for x, _ := range cc {
			cells[y][x] = newCell(x, y)
		}
	}
	return &grid{
		cells:  cells,
		width:  width,
		height: height,
	}
}

func (g *grid) SetCell(c *cell) {
	g.cells[c.y][c.x] = c
}

func (g *grid) GetCell(pos *position) *cell {
	if pos.x >= 0 && pos.y >= 0 && pos.x < g.width && pos.y < g.height {
		return g.cells[pos.y][pos.x]
	}
	return nil
}

func (g *grid) AllCells() []*cell {
	cells := make([]*cell, 0, g.width*g.height)
	for _, cc := range g.cells {
		cells = append(cells, cc...)
	}
	return cells
}

func (g *grid) Cluster(center *position, distance int) []*cell {
	clusterSize := 1 + 2*distance*(distance+1)
	cluster := make([]*cell, 0, clusterSize)
	for x := center.x - distance; x <= center.x+distance; x++ {
		for y := center.y - distance; y <= center.y+distance; y++ {
			if c := g.GetCell(newPosition(x, y)); c != nil {
				if center.Distance(c.position) <= distance {
					cluster = append(cluster, c)
				}
			}
		}
	}
	return cluster
}

func (g *grid) DigTargets(p *position) []*cell {
	neighbors := g.Cluster(p, 1)
	targets := make([]*cell, 0, len(neighbors))
	for _, n := range neighbors {
		if n.x > 0 {
			targets = append(targets, n)
		}
	}
	if p.x > 0 {
		targets = append(targets, g.GetCell(p))
	}
	return targets
}

func (g *grid) Show() {
	// header
	debugLog("    ")
	for i := range g.cells[0] {
		debugLog(" %2d  ", i)
	}
	debugLogLine("")
	// body
	for i, cs := range g.cells {
		debugLog("%2d :", i)
		for _, c := range cs {
			c.Show()
			debugLog("|")
		}
		debugLogLine("")
	}
}

func applyHoleByMyRobots(previousGrid *grid, turnInput *turnInput, robotHistories map[int]*robotHistory) *grid {
	if turnInput.turn == 0 {
		return previousGrid
	}
	for _, r := range turnInput.myRobots {
		action := robotHistories[r.ID].GetAction(turnInput.turn - 1)
		if action.action == actionTypeDig {
			previousGrid.GetCell(action.position).IsHole = true
		}
	}
	return previousGrid
}

type robotHistory struct {
	ID              int
	positionHistory []*position
	actionHistory   []*action
	itemHistory     []int
}

func newRobotHistory(id int) *robotHistory {
	return &robotHistory{
		ID:              id,
		positionHistory: make([]*position, 0, maxTurn),
		actionHistory:   make([]*action, 0, maxTurn),
	}
}

func (h *robotHistory) AddPosition(p *position) {
	h.positionHistory = append(h.positionHistory, p)
}

func (h *robotHistory) GetPosition(turn int) *position {
	return h.positionHistory[turn]
}

func (h *robotHistory) AddAction(a *action) {
	h.actionHistory = append(h.actionHistory, a)
}

func (h *robotHistory) GetAction(turn int) *action {
	return h.actionHistory[turn]
}

func (h *robotHistory) AddItem(a int) {
	h.itemHistory = append(h.itemHistory, a)
}

func (h *robotHistory) GetItem(turn int) int {
	return h.itemHistory[turn]
}

func (h *robotHistory) HasItem() bool {
	l := len(h.actionHistory)
	for i := 0; i < l; i++ {
		a := h.actionHistory[l-i]
		if a.action == actionTypeDig {
			return false
		}
		if a.action == actionTypeRequest {
			return true
		}
	}
	return false
}

var veinExpectationTable [][]float64
var veinCostTable []float64
var moveTable []position

func init() {
	veinExpectationTable = [][]float64{
		{0.000000, 0.000771, 0.002907, 0.006657, 0.011818, 0.017735, 0.022971, 0.028071, 0.032770, 0.036992, 0.041516, 0.045225, 0.048782, 0.052130, 0.055649, 0.059175, 0.062198, 0.065354, 0.068045, 0.071130, 0.073923, 0.076807, 0.078858, 0.081395, 0.084524, 0.087896, 0.077125, 0.060849, 0.035181, 0.015943},
		{0.000000, 0.001815, 0.006696, 0.016089, 0.028258, 0.042328, 0.055792, 0.067214, 0.078082, 0.088110, 0.097835, 0.105639, 0.113610, 0.122330, 0.129235, 0.137371, 0.143600, 0.149932, 0.156611, 0.162677, 0.168853, 0.173979, 0.179222, 0.184838, 0.191321, 0.198494, 0.177348, 0.143802, 0.079183, 0.035512},
		{0.000000, 0.003181, 0.012556, 0.033711, 0.062561, 0.092162, 0.119901, 0.144670, 0.167423, 0.187695, 0.206968, 0.224889, 0.241288, 0.257769, 0.272984, 0.286818, 0.301127, 0.314718, 0.326699, 0.338678, 0.349217, 0.360664, 0.371790, 0.381857, 0.392042, 0.406848, 0.380459, 0.329495, 0.144397, 0.062332},
		{0.000000, 0.004062, 0.016240, 0.042283, 0.078035, 0.114923, 0.148522, 0.178374, 0.205881, 0.229869, 0.252860, 0.273477, 0.292861, 0.311316, 0.327483, 0.344305, 0.359971, 0.374590, 0.387985, 0.400809, 0.414415, 0.426082, 0.437234, 0.447897, 0.459908, 0.475949, 0.445744, 0.386409, 0.181537, 0.079481},
		{0.000000, 0.004827, 0.018935, 0.049135, 0.089134, 0.130597, 0.168318, 0.201308, 0.230064, 0.256990, 0.281000, 0.303865, 0.325339, 0.344135, 0.362090, 0.380102, 0.396495, 0.412389, 0.425969, 0.438672, 0.452360, 0.465196, 0.477061, 0.487123, 0.500443, 0.516686, 0.483405, 0.419934, 0.207059, 0.092809},
		{0.000000, 0.005014, 0.018779, 0.048880, 0.088434, 0.129684, 0.166913, 0.199735, 0.229215, 0.255130, 0.280106, 0.301639, 0.322442, 0.342292, 0.359708, 0.376819, 0.393297, 0.407441, 0.422925, 0.436024, 0.447916, 0.460595, 0.472203, 0.482847, 0.494304, 0.511704, 0.478836, 0.416984, 0.203736, 0.091629},
		{0.000000, 0.004896, 0.018900, 0.049081, 0.088454, 0.129773, 0.166535, 0.199705, 0.228924, 0.255044, 0.279618, 0.301666, 0.321563, 0.340543, 0.358615, 0.375500, 0.391057, 0.405796, 0.420830, 0.433855, 0.447122, 0.459176, 0.470517, 0.481962, 0.494114, 0.510470, 0.476816, 0.416230, 0.203676, 0.091087},
		{0.000000, 0.004913, 0.018873, 0.048793, 0.088661, 0.129832, 0.166989, 0.199463, 0.228343, 0.254295, 0.279089, 0.301988, 0.321721, 0.340863, 0.358648, 0.375940, 0.391474, 0.406514, 0.420756, 0.434897, 0.447464, 0.458886, 0.471542, 0.482339, 0.494071, 0.510121, 0.477729, 0.414758, 0.203381, 0.091278},
		{0.000000, 0.004800, 0.018993, 0.048904, 0.088734, 0.130010, 0.166622, 0.199840, 0.228422, 0.255847, 0.278548, 0.300938, 0.321328, 0.340784, 0.358748, 0.375902, 0.391292, 0.406848, 0.420488, 0.434368, 0.447189, 0.459849, 0.470968, 0.481722, 0.493791, 0.509834, 0.478846, 0.415559, 0.203595, 0.090976},
		{0.000000, 0.004941, 0.018695, 0.049080, 0.088452, 0.130099, 0.167849, 0.200083, 0.229226, 0.255706, 0.279702, 0.301382, 0.322807, 0.341881, 0.359397, 0.375790, 0.391512, 0.407395, 0.422986, 0.435612, 0.448751, 0.460926, 0.472578, 0.482797, 0.495205, 0.511748, 0.478236, 0.416261, 0.204602, 0.091617},
		{0.000000, 0.004821, 0.019085, 0.048783, 0.088891, 0.130365, 0.167979, 0.201051, 0.230344, 0.257366, 0.281887, 0.304339, 0.324869, 0.344693, 0.362367, 0.380138, 0.396489, 0.412608, 0.425959, 0.439406, 0.452966, 0.464739, 0.476376, 0.488205, 0.499805, 0.515371, 0.483157, 0.419858, 0.206597, 0.092477},
		{0.000000, 0.004008, 0.016148, 0.042508, 0.078135, 0.115418, 0.149040, 0.179030, 0.205148, 0.230059, 0.252746, 0.273586, 0.292791, 0.311161, 0.328174, 0.344264, 0.360094, 0.375226, 0.388522, 0.401103, 0.414819, 0.425505, 0.437393, 0.448000, 0.459693, 0.475029, 0.445186, 0.386220, 0.181363, 0.079459},
		{0.000000, 0.003074, 0.012179, 0.033569, 0.062623, 0.092478, 0.119995, 0.144400, 0.166640, 0.187693, 0.206266, 0.224542, 0.241613, 0.257983, 0.273685, 0.287837, 0.302538, 0.314960, 0.326777, 0.338798, 0.350628, 0.360936, 0.371687, 0.381620, 0.391799, 0.404895, 0.379441, 0.328394, 0.144531, 0.061638},
		{0.000000, 0.001767, 0.006696, 0.016195, 0.028494, 0.042476, 0.055725, 0.066810, 0.077964, 0.088055, 0.096960, 0.106243, 0.114176, 0.122554, 0.129687, 0.137108, 0.144515, 0.150792, 0.157153, 0.162837, 0.168842, 0.173945, 0.179411, 0.184554, 0.190448, 0.198841, 0.177137, 0.143176, 0.079291, 0.035389},
		{0.000000, 0.000779, 0.002917, 0.006636, 0.011752, 0.017455, 0.022955, 0.028064, 0.032693, 0.036987, 0.041065, 0.045158, 0.048775, 0.052597, 0.055986, 0.059991, 0.062624, 0.065219, 0.068447, 0.071044, 0.073531, 0.076321, 0.079205, 0.081817, 0.084195, 0.087943, 0.076425, 0.060583, 0.034792, 0.015824},
	}
	veinCostTable = []float64{1, 1, 3, 3, 3, 3, 5, 5, 5, 5, 7, 7, 7, 7, 9, 9, 9, 9, 11, 11, 11, 11, 13, 13, 13, 13, 15, 15, 15, 15}
	moveTable = []position{
		{-4, 0},
		{-3, -1},
		{-3, 1},
		{-2, -2},
		{-2, 2},
		{-1, -3},
		{-1, 3},
		{0, -4},
		{0, 4},
		{1, -3},
		{1, 3},
		{2, -2},
		{2, 2},
		{3, -1},
		{3, 1},
		{4, 0},
	}
}

type turnInput struct {
	turn                        int
	myScore, enemyScore         int
	radarCoolDown, trapCoolDown int
	myRobots, enemyRobots       []*robot
	radars, traps               []*entity
	grid                        *grid
}

func (i *turnInput) Show() {
	debugLogLine("Turn: %d", i.turn)
	debugLogLine("MyScore: %d, EnemyScore: %d", i.myScore, i.enemyScore)
	debugLogLine("RadarCoolDown: %d, TrapCoolDown: %d", i.radarCoolDown, i.trapCoolDown)
	i.grid.Show()
	for _, v := range i.myRobots {
		v.Show()
	}
	for _, v := range i.enemyRobots {
		v.Show()
	}
	for _, v := range i.radars {
		v.Show()
	}
	for _, v := range i.traps {
		v.Show()
	}
}

type inputReader struct {
	scanner       *bufio.Scanner
	width, height int
}

func newInputReader() *inputReader {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, 1000000), 1000000)
	return &inputReader{
		scanner: scanner,
	}
}

func (r *inputReader) readInitialInput() (width, height int) {
	r.scanner.Scan()
	_, _ = fmt.Sscan(r.scanner.Text(), &width, &height)
	r.width = width
	r.height = height
	return
}

func (r *inputReader) readTurnInput(turn int) *turnInput {
	// score
	var myScore, opponentScore int
	r.scanner.Scan()
	_, _ = fmt.Sscan(r.scanner.Text(), &myScore, &opponentScore)

	// grid
	grid := newGrid(r.width, r.height)
	for y := 0; y < r.height; y++ {
		r.scanner.Scan()
		inputs := strings.Split(r.scanner.Text(), " ")
		for x := 0; x < r.width; x++ {
			oreStr := inputs[2*x]
			holeStr := inputs[2*x+1]
			var ore int64
			if oreStr[0] == oreUnknown {
				ore = amadeusiumUnknown
			} else {
				ore, _ = strconv.ParseInt(oreStr, 10, 32)
			}
			grid.cells[y][x].IsHole = holeStr == "1"
			grid.cells[y][x].Amadeusium = int(ore)
		}
	}

	// misc
	var entityCount, radarCoolDown, trapCoolDown int
	r.scanner.Scan()
	_, _ = fmt.Sscan(r.scanner.Text(), &entityCount, &radarCoolDown, &trapCoolDown)

	// entities
	myRobots := make([]*robot, 0, 100)
	enemyRobots := make([]*robot, 0, 100)
	radars := make([]*entity, 0, 100)
	traps := make([]*entity, 0, 100)
	for i := 0; i < entityCount; i++ {
		var id, entityType, x, y, item int
		r.scanner.Scan()
		_, _ = fmt.Sscan(r.scanner.Text(), &id, &entityType, &x, &y, &item)

		switch entityType {
		case entityTypeMyRobot:
			myRobots = append(myRobots, newRobot(id, entityType, x, y, item))
		case entityTypeEnemyRobot:
			enemyRobots = append(enemyRobots, newRobot(id, entityType, x, y, item))
		case entityTypeRadar:
			radars = append(radars, newEntity(id, entityType, x, y))
		case entityTypeTrap:
			traps = append(traps, newEntity(id, entityType, x, y))
		default:
			debugLogLine("Unsupported entity type: %d", entityType)
		}
	}
	return &turnInput{
		turn:          turn,
		myScore:       myScore,
		enemyScore:    opponentScore,
		radarCoolDown: radarCoolDown,
		trapCoolDown:  trapCoolDown,
		myRobots:      myRobots,
		enemyRobots:   enemyRobots,
		radars:        radars,
		traps:         traps,
		grid:          grid,
	}
}

func main() {
	defer func() {
		if err := recover(); err != nil {
			debugLogLine("[ERROR] %s\n", err)
			for depth := 0; ; depth++ {
				_, file, line, ok := runtime.Caller(depth)
				if !ok {
					break
				}
				debugLogLine("======> %d: %v:%d", depth, file, line)
			}
		}
	}()

	// rand
	rand.Seed(time.Now().UnixNano())

	inputReader := newInputReader()
	width, height := inputReader.readInitialInput()
	_ = width
	_ = height

	// variables for estimation
	robotHistories := make(map[int]*robotHistory)

	turn := 0
	var previousGrid *grid
	var previousEnemyRobots []*robot
	var gridEstimated *grid
	processTurn := func() {
		// input
		turnInput := inputReader.readTurnInput(turn)

		// update turn and previous grid/enemyRobots at the end of turn
		defer func() {
			previousGrid = turnInput.grid
			previousEnemyRobots = turnInput.enemyRobots
			turn++
		}()

		// initialize history
		if turn == 0 {
			for _, r := range turnInput.myRobots {
				robotHistories[r.ID] = newRobotHistory(r.ID)
			}
			for _, r := range turnInput.enemyRobots {
				robotHistories[r.ID] = newRobotHistory(r.ID)
			}
		}
		// store robot position
		for _, r := range turnInput.myRobots {
			robotHistories[r.ID].AddPosition(r.position)
		}
		for _, r := range turnInput.enemyRobots {
			robotHistories[r.ID].AddPosition(r.position)
		}

		// apply hole by my robots to previous grid
		previousGrid = applyHoleByMyRobots(previousGrid, turnInput, robotHistories)

		// estimate previous action of enemy robots
		estimatePreviousEnemyRobotAction(turn, turnInput, previousGrid, previousEnemyRobots, robotHistories)

		// store robot item
		for _, r := range turnInput.myRobots {
			robotHistories[r.ID].AddItem(r.item)
		}
		for _, r := range turnInput.enemyRobots {
			robotHistories[r.ID].AddItem(r.item)
		}

		// estimate grid
		previousGrid = nil
		gridEstimated = estimateGrid(turn, turnInput, gridEstimated, robotHistories)

		// decide action plans
		actionPlans := make([]plans, len(turnInput.myRobots))
		for i, r := range turnInput.myRobots {
			actionPlans[i] = decideActionPlans(r, turnInput, gridEstimated, robotHistories)
		}
		// coordinate action plans
		for _, p := range coordinatePlans(actionPlans) {
			p.Exec()
			robotHistories[p.robot.ID].AddAction(p.nextAction)
		}

		// debug
		if DEBUG {
			//gridEstimated.Show()
			//turnInput.Show()
			//showVeinScore(gridEstimated)
			//showEnemyTrapProbability(gridEstimated)
		}
	}

	// each turn
	for {
		processTurn()
	}
}

const (
	planTypePutRadar = iota
	planTypePutTrap
	planTypeDigVein
	planTypeSearchVein
	planTypeDeliverAmadeusium
	planTypeWait
	planTypeDummyRequest
	planTypeTriggerTrap
)

func strPlanType(t int) string {
	if s, ok := map[int]string{
		planTypePutRadar:          "PutRadar",
		planTypePutTrap:           "PutTrap",
		planTypeDigVein:           "Dig",
		planTypeSearchVein:        "SearchVein",
		planTypeDeliverAmadeusium: "Deliver",
		planTypeWait:              "Wait",
	}[t]; ok {
		return s
	}
	return "UnexpectedPlanType"
}

// 優先順位
// 新規の鉱脈
// 本部に近い鉱脈
// 爆弾が置かれている確率が低い鉱脈

type plan struct {
	planType        int
	turn            int
	robot           *robot
	target          *cell
	score           float64
	turnsToComplete int
	nextAction      *action
}

func (p *plan) String() string {
	s := fmt.Sprintf("%s: Score: %f ", strPlanType(p.planType), p.Score())
	if p.target != nil {
		s += fmt.Sprintf("Target:(%d,%d)+%f ", p.target.x, p.target.y, p.target.EnemyTrapProbability)
	}
	if p.nextAction != nil {
		s += p.nextAction.String()
	}
	return s
}

func (p *plan) Exec() {
	p.nextAction.Exec()
	debugLogLine("E: %s", p.String())
}

func (p *plan) Score() float64 {
	score := p.score
	if p.planType == planTypePutRadar {
		score *= 3
	}
	if p.planType == planTypePutTrap {
		score *= 2
	}
	if isDigPlanType(p.planType) {
		if p.target.Amadeusium > 0 && !p.target.IsHole {
			score *= 1.1
		} else {
			score -= p.target.EnemyTrapProbability * float64(maxTurn-p.turn) * 0.1
		}
	}
	return score / float64(p.turnsToComplete)
}

func newPutRadarPlan(turn int, robot *robot, target *cell, score float64, turnToPutRadar int, nextAction *action) *plan {
	return &plan{
		planType:        planTypePutRadar,
		turn:            turn,
		robot:           robot,
		target:          target,
		score:           score,
		turnsToComplete: turnToPutRadar,
		nextAction:      nextAction,
	}
}

func newPutTrapPlan(turn int, robot *robot, target *cell, score float64, turnToPutTrap int, nextAction *action) *plan {
	return &plan{
		planType:        planTypePutTrap,
		turn:            turn,
		robot:           robot,
		target:          target,
		score:           score,
		turnsToComplete: turnToPutTrap,
		nextAction:      nextAction,
	}
}

func newDeliverAmadeusiumPlan(turn int, robot *robot, headquarterTarget *cell, turnToCompleteDeliverAmadeusium int, nextAction *action) *plan {
	return &plan{
		planType:        planTypeDeliverAmadeusium,
		turn:            turn,
		robot:           robot,
		target:          headquarterTarget,
		score:           1,
		turnsToComplete: turnToCompleteDeliverAmadeusium,
		nextAction:      nextAction,
	}
}

func newDigVeinPlan(turn int, robot *robot, veinTarget *cell, score float64, turnToCompleteDeliverAmadeusium int, nextAction *action) *plan {
	return &plan{
		planType:        planTypeDigVein,
		turn:            turn,
		robot:           robot,
		target:          veinTarget,
		score:           score,
		turnsToComplete: turnToCompleteDeliverAmadeusium,
		nextAction:      nextAction,
	}
}

func newSearchVeinPlan(turn int, robot *robot, veinTarget *cell, score float64, turnToCompleteDeliverAmadeusium int, nextAction *action) *plan {
	return &plan{
		planType:        planTypeSearchVein,
		turn:            turn,
		robot:           robot,
		target:          veinTarget,
		score:           score,
		turnsToComplete: turnToCompleteDeliverAmadeusium,
		nextAction:      nextAction,
	}
}

func newWaitPlan(turn int, robot *robot) *plan {
	return &plan{
		planType:        planTypeWait,
		turn:            turn,
		robot:           robot,
		target:          nil,
		score:           0,
		turnsToComplete: 1,
		nextAction:      newWaitAction(),
	}
}

func newDummyRequestPlan(turn int, robot *robot) *plan {
	return &plan{
		planType:        planTypeDummyRequest,
		turn:            turn,
		robot:           robot,
		target:          nil,
		score:           0,
		turnsToComplete: 1,
		nextAction:      newWaitAction(),
	}
}

func newTriggerTrapPlan(turn int, robot *robot, p *position) *plan {
	return &plan{
		planType:        planTypeTriggerTrap,
		turn:            turn,
		robot:           robot,
		target:          nil,
		score:           0,
		turnsToComplete: 1,
		nextAction:      newDigAction(p),
	}
}

type plans []*plan

func (p plans) Len() int {
	return len(p)
}
func (p plans) Less(i, j int) bool {
	return p[i].Score() < p[j].Score()
}
func (p plans) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func decideActionPlans(r *robot, turnInput *turnInput, gridEstimated *grid, robotHistories map[int]*robotHistory) []*plan {
	var actionPlans plans
	actionPlans = make([]*plan, 0, 1000)

	isRequest := false

	// dead robots can wait only
	if r.IsDead() {
		actionPlans = append(actionPlans, newWaitPlan(turnInput.turn, r))
		return actionPlans
	}

	// kill enemies
	for _, t := range turnInput.traps {
		if r.Distance(t.position) > 1 {
			continue
		}
		affectedMyRobots := 0
		for _, r := range turnInput.myRobots {
			if r.Distance(t.position) <= 1 {
				affectedMyRobots++
			}
		}
		affectedEnemyRobots := 0
		for _, r := range turnInput.enemyRobots {
			if r.Distance(t.position) <= 1 {
				affectedEnemyRobots++
			}
		}
		if affectedMyRobots < affectedEnemyRobots || affectedMyRobots == affectedEnemyRobots && r.HasNothing() {
			return []*plan{newTriggerTrapPlan(turnInput.turn, r, t.position)}
		}
	}

	// deliver Amadeusium to headquarter
	if r.HasAmadeusium() {
		turn := r.TurnsToHeadquarter()
		// TODO: evaluate other positions
		hqPos := newPosition(0, r.y)
		actionPlans = append(actionPlans, newDeliverAmadeusiumPlan(turnInput.turn, r, gridEstimated.GetCell(hqPos), turn, newMoveAction(hqPos)))
		return actionPlans
	}

	// radar
	if r.InHeadquarter() && r.HasNothing() && turnInput.radarCoolDown == 0 || r.HasRadar() {
		radarScores := listRadarScore(turnInput, gridEstimated)
		for _, s := range radarScores {
			if r.HasRadar() {
				digPosition := findDigPosition(r, s.position)
				turn := r.TurnsTo(digPosition) + 1
				if r.Distance(digPosition) == 0 {
					actionPlans = append(actionPlans, newPutRadarPlan(turnInput.turn, r, gridEstimated.GetCell(s.position), s.score, turn, newDigAction(s.position)))
				} else {
					for _, p := range findNextPositions(r, digPosition) {
						actionPlans = append(actionPlans, newPutRadarPlan(turnInput.turn, r, gridEstimated.GetCell(s.position), s.score, turn, newMoveAction(p)))
					}
				}
			} else {
				isRequest = true
				digPosition := findDigPosition(r, s.position)
				turn := 1 + r.TurnsTo(digPosition) + 1
				actionPlans = append(actionPlans, newPutRadarPlan(turnInput.turn, r, gridEstimated.GetCell(s.position), s.score, turn, newRequestRadarAction()))
			}
		}
	}
	// TODO: go back to headquarter to get radar

	// list veins
	veins := veins(gridEstimated)
	veinsForDig, veinsForMove := splitByDistance(veins, r.position, 1)

	// trap
	if turnInput.turn > 20 && turnInput.turn < 100 && r.InHeadquarter() && r.HasNothing() && turnInput.trapCoolDown == 0 || r.HasTrap() {
		nearVein, turnToNearVein := findNearVeinForTrap(r, gridEstimated)
		if nearVein != nil {
			digPosition := findDigPosition(r, nearVein.position)
			if r.HasTrap() {
				if r.Distance(digPosition) == 0 {
					actionPlans = append(actionPlans, newPutTrapPlan(turnInput.turn, r, nearVein, 1, turnToNearVein, newDigAction(nearVein.position)))
				} else {
					for _, p := range findNextPositions(r, digPosition) {
						actionPlans = append(actionPlans, newPutTrapPlan(turnInput.turn, r, nearVein, 1, turnToNearVein, newMoveAction(p)))
					}
				}
			} else {
				isRequest = true
				actionPlans = append(actionPlans, newPutTrapPlan(turnInput.turn, r, nearVein, 1, turnToNearVein+1, newRequestTrapAction()))
			}
		}
	}

	// dummy request
	if r.InHeadquarter() && r.HasNothing() && !isRequest {
		if rand.Float64() < 0.2 {
			return []*plan{newWaitPlan(turnInput.turn, r)}
		}
	}

	// dig
	{
		digTargets := filterOutMyTrap(veinsForDig)
		turnToHeadquarter := r.TurnsToHeadquarter()
		for _, target := range digTargets {
			e := veinExpectation(gridEstimated.GetCell(target.position))
			if e == 1 {
				actionPlans = append(actionPlans, newDigVeinPlan(turnInput.turn, r, target, e, turnToHeadquarter+1, newDigAction(target.position)))
			} else {
				actionPlans = append(actionPlans, newSearchVeinPlan(turnInput.turn, r, target, e, turnToHeadquarter+1, newDigAction(target.position)))
			}
		}
	}

	// move and dig
	{
		for _, vein := range filterOutMyTrap(veinsForMove) {
			digPosition := findDigPosition(r, vein.position)
			e := veinExpectation(vein)
			turnToHeadquarter := r.TurnsTo(digPosition) + 1 + turnToHeadquarter(digPosition)
			for _, p := range findNextPositions(r, digPosition) {
				actionPlans = append(actionPlans, newDigVeinPlan(turnInput.turn, r, vein, e, turnToHeadquarter, newMoveAction(p)))
			}
		}
	}

	// search
	{
		unknownCells := filterOutKnown(gridEstimated.AllCells())
		cellsForDig, cellsForMove := splitByDistance(unknownCells, r.position, 1)
		for _, c := range cellsForDig {
			e := veinExpectation(c)
			turn := 1 + r.TurnsToHeadquarter()
			actionPlans = append(actionPlans, newSearchVeinPlan(turnInput.turn, r, c, e, turn, newDigAction(c.position)))
		}
		for _, c := range cellsForMove {
			e := veinExpectation(c)
			digPosition := findDigPosition(r, c.position)
			turn := r.TurnsTo(digPosition) + 1 + turnToHeadquarter(digPosition)
			for _, p := range findNextPositions(r, digPosition) {
				actionPlans = append(actionPlans, newSearchVeinPlan(turnInput.turn, r, c, e, turn, newMoveAction(p)))
			}
		}
	}

	// wait
	actionPlans = append(actionPlans, newWaitPlan(turnInput.turn, r))

	// sort
	sort.Sort(sort.Reverse(actionPlans))

	return actionPlans
}

func isDigPlanType(planType int) bool {
	return planType == planTypeDigVein || planType == planTypePutTrap || planType == planTypePutRadar || planType == planTypeSearchVein
}

func coordinatePlans(pp []plans) []*plan {
	coordinatedPlansIndex := make([]int, len(pp))
	for {
		coordinatedPlans := make([]*plan, len(pp))
		for i, p := range pp {
			coordinatedPlans[i] = p[coordinatedPlansIndex[i]]
		}

		conflicts := checkConflict(coordinatedPlans)
		if len(conflicts) <= 1 {
			return coordinatedPlans
		}

		// resolve conflicts
		scores := make([]float64, len(conflicts))
		for i, v := range conflicts {
			scores[i] = coordinatedPlans[v].Score()
		}
		selectIndex := conflicts[maxIndex(scores)]
		for _, v := range conflicts {
			if v != selectIndex {
				coordinatedPlansIndex[v]++
			}
		}
	}
}

func checkConflict(pp []*plan) []int {
	mapToList := func(m map[int]bool) []int {
		l := make([]int, 0, len(m))
		for k := range m {
			l = append(l, k)
		}
		return l
	}

	// check radar request
	duplicates := make(map[int]bool)
	for i, p1 := range pp {
		if p1.planType == planTypePutRadar && p1.nextAction.action == actionTypeRequestRadar {
			for j, p2 := range pp[i+1:] {
				if p2.planType == planTypePutRadar && p2.nextAction.action == actionTypeRequestRadar {
					duplicates[i] = true
					duplicates[i+j+1] = true
				}
			}
			if len(duplicates) > 1 {
				return mapToList(duplicates)
			}
		}
	}

	// check trap request
	duplicates = make(map[int]bool)
	for i, p1 := range pp {
		if p1.planType == planTypePutTrap && p1.nextAction.action == actionTypeRequestTrap {
			for j, p2 := range pp[i+1:] {
				if p2.planType == planTypePutTrap && p2.nextAction.action == actionTypeRequestTrap {
					duplicates[i] = true
					duplicates[i+j+1] = true
				}
			}
			if len(duplicates) > 1 {
				return mapToList(duplicates)
			}
		}
	}

	// check dig target
	duplicates = make(map[int]bool)
	for i, p1 := range pp {
		if isDigPlanType(p1.planType) {
			for j, p2 := range pp[i+1:] {
				if isDigPlanType(p2.planType) {
					if p1.target.Distance(p2.target.position) == 0 {
						duplicates[i] = true
						duplicates[i+j+1] = true
					}
				}
			}
			if len(duplicates) > 1 {
				return mapToList(duplicates)
			}
		}
	}

	// next position
	duplicates = make(map[int]bool)
	for i, p1 := range pp {
		if isDigPlanType(p1.planType) && p1.nextAction.position != nil {
			for j, p2 := range pp[i+1:] {
				if isDigPlanType(p2.planType) && p2.nextAction.position != nil {
					if p1.nextAction.position.Distance(p2.nextAction.position) <= 2 {
						duplicates[i] = true
						duplicates[i+j+1] = true
					}
				}
			}
			if len(duplicates) > 1 {
				return mapToList(duplicates)
			}
		}
	}

	return []int{}
}

// position
type position struct {
	x, y int
}

func newPosition(x, y int) *position {
	return &position{
		x: x,
		y: y,
	}
}

func (p position) Distance(pos *position) int {
	return abs(p.x-pos.x) + abs(p.y-pos.y)
}

func (p position) IsValid(width, height int) bool {
	return p.x >= 0 && p.y >= 0 && p.x < width && p.y < height
}

var radarCount = 0

type radarScore struct {
	score    float64
	position *position
}

// TODO: optimize calculation cost
func getRadarScore(gridEstimated *grid, pos *position) float64 {
	cc := gridEstimated.Cluster(pos, 4)
	var score float64
	for _, c := range cc {
		if c.Amadeusium == amadeusiumUnknown {
			score += veinScore(c)
		}
	}
	return score
}

func listRadarScore(turnInput *turnInput, gridEstimated *grid) []*radarScore {
	r := make([]*radarScore, 0, maxWidth*maxHeight)
	for _, cc := range gridEstimated.cells {
		for x, c := range filterOutMyTrap(cc) {
			if x == 0 {
				continue
			}
			score := getRadarScore(gridEstimated, c.position)
			r = append(r, &radarScore{score, c.position})
		}
	}
	return r
}

func getNextRadarPosition(turnInput *turnInput, gridEstimated *grid) (*position, float64) {
	var maxRadarScore float64
	var maxPosition *position
	for _, cc := range gridEstimated.cells {
		for x, c := range cc {
			if x == 0 {
				continue
			}
			if len(turnInput.radars) == 0 && x > 5 {
				continue
			}
			score := getRadarScore(gridEstimated, c.position)
			if maxPosition == nil || score > maxRadarScore || score == maxRadarScore && c.x < maxPosition.x {
				maxRadarScore = score
				maxPosition = c.position
			}
		}
	}
	if DEBUG {
		// debugLogLine("RadarTargetPosition: (%d, %d) score:%f", maxPosition.x, maxPosition.y, maxRadarScore)
	}
	return maxPosition, maxRadarScore
}

func putRadar() {
	radarCount++
}

const (
	// item
	itemDummy       = -3
	itemRadarOrTrap = -2
	itemNone        = -1
	itemRadar       = 2
	itemTrap        = 3
	itemAmadeusium  = 4
	// item name
	itemNameRadar = "RADAR"
	itemNameTrap  = "TRAP"
	// velocity
	velocity = 4
)

// robot
type robot struct {
	*entity
	item int
}

func newRobot(ID, Type, x, y, item int) *robot {
	return &robot{
		entity: newEntity(ID, Type, x, y),
		item:   item,
	}
}

func (r *robot) TurnsTo(pos *position) int {
	d := r.Distance(pos)
	return turnToMove(d)
}

func (r *robot) TurnsToHeadquarter() int {
	return turnToHeadquarter(r.position)
}

func (r *robot) IsDead() bool {
	return r.x == -1 && r.y == -1
}

func (r *robot) InHeadquarter() bool {
	return r.x == 0
}

func (r *robot) HasAmadeusium() bool {
	return r.item == itemAmadeusium
}

func (r *robot) HasRadar() bool {
	return r.item == itemRadar
}

func (r *robot) HasTrap() bool {
	return r.item == itemTrap
}

func (r *robot) HasNothing() bool {
	return r.item == itemNone
}

func (r *robot) RequestRadar(message ...string) {
	request(itemNameRadar, message...)
}

func (r *robot) RequestTrap(message ...string) {
	request(itemNameTrap, message...)
}

func (r *robot) Move(x int, y int, message ...string) {
	move(x, y, message...)
}

func (r *robot) Dig(x int, y int, message ...string) {
	dig(x, y, message...)
}

func (r *robot) Wait(message ...string) {
	wait(message...)
}

func (r *robot) String() string {
	var item string
	switch r.item {
	case itemRadarOrTrap:
		item = "with Radar or Trap"
	case itemNone:
		item = ""
	case itemAmadeusium:
		item = "with Amadeusium"
	case itemRadar:
		item = "with Radar"
	case itemTrap:
		item = "with Trap"
	default:
		item = fmt.Sprintf("with UnknownItem(%d)", r.item)
	}
	return r.entity.String() + " " + item
}

func (r *robot) Show() {
	debugLogLine(r.String())
}

func turnToMove(distance int) int {
	t := distance / velocity
	if distance%velocity > 0 {
		t++
	}
	return t
}

func turnToHeadquarter(currentPos *position) int {
	return turnToMove(currentPos.x)
}

func findDigPosition(r *robot, pos *position) *position {
	d := r.Distance(pos)
	if d <= 1 {
		// can dig from current position
		return r.position
	}
	if r.x < pos.x {
		return newPosition(pos.x-1, pos.y)
	}
	switch d % velocity {
	case 0:
		return pos
	case 1:
		if r.y == pos.y {
			return newPosition(pos.x+1, pos.y)
		}
		if r.y > pos.y {
			return newPosition(pos.x, pos.y+1)
		}
		if r.y < pos.y {
			return newPosition(pos.x, pos.y-1)
		}
	case 2:
		return newPosition(pos.x-1, pos.y)
	case 3:
		return newPosition(pos.x-1, pos.y)
	}
	return pos
}

func calcDigAndGoHeadquarterTurns(r *robot, pos *position) int {
	p := findDigPosition(r, pos)
	return r.TurnsTo(p) + newRobot(0, entityTypeEnemyRobot, p.x, p.y, itemNone).TurnsToHeadquarter()
}

func checkMove(previous, current *robot) bool {
	if current.Distance(previous.position) != 0 {
		// moving
		return true
	}
	return false
}

func checkRequest(previous, current *robot) bool {
	if current.x != 0 {
		return false
	}
	if current.Distance(previous.position) == 0 {
		// request something
		return true
	}
	return false
}

func checkDig(previousGrid, grid *grid, robot *robot) (bool, []*position) {
	targets := grid.DigTargets(robot.position)
	candidates := make([]*position, 0, 5)
	// check change hole state for dig targets
	for _, t := range targets {
		if t.IsHole {
			// TODO: check other hole state change
			candidates = append(candidates, t.position)
			if !previousGrid.GetCell(t.position).IsHole {
				return true, []*position{t.position}
			}
		}
	}
	// check hole around the robot
	if len(candidates) == 0 {
		return false, nil
	} else {
		return true, candidates
	}
}

func findNextPositions(robot *robot, destination *position) []*position {
	d := robot.Distance(destination)
	if d <= velocity {
		return []*position{destination}
	}
	// TODO: optimize calculation cost
	positions := make([]*position, 0, 5)
	for _, m := range moveTable {
		p := newPosition(robot.x+m.x, robot.y+m.y)
		if p.IsValid(maxWidth, maxHeight) && p.Distance(destination) == d-velocity {
			positions = append(positions, p)
		}
	}
	return positions
}

func countRobot(robots []*robot) int {
	c := 0
	for _, r := range robots {
		if !r.IsDead() {
			c++
		}
	}
	return c
}

func showEnemyTrapProbability(g *grid) {
	// header
	debugLog("    ")
	for i := range g.cells[0] {
		debugLog("  %2d  ", i)
	}
	debugLogLine("")
	// body
	for i, cs := range g.cells {
		debugLog("%2d :", i)
		for _, c := range cs {
			debugLog("%0.3f|", c.EnemyTrapProbability)
		}
		debugLogLine("")
	}
}

func abs(a int) int {
	if a > 0 {
		return a
	}
	return -a
}

func minInt(a int, b int) int {
	if a > b {
		return b
	}
	return a
}

func maxInt(a int, b int) int {
	if a < b {
		return b
	}
	return a
}

func maxIndex(a []float64) int {
	if len(a) == 0 {
		return -1
	}
	index := 0
	m := a[index]
	for i := 1; i < len(a); i++ {
		if m < a[i] {
			index = i
			m = a[i]
		}
	}
	return index
}

func debugLog(format string, a ...interface{}) {
	if !DEBUG {
		return
	}
	_, _ = fmt.Fprintf(os.Stderr, format, a...)
}

func debugLogLine(format string, a ...interface{}) {
	if !DEBUG {
		return
	}
	debugLog(format+"\n", a...)
}

func initialVeinExpectation(p *position) float64 {
	return veinExpectationTable[p.y][p.x]
}

func veinCost(p *position) float64 {
	return veinCostTable[p.x]
}

func veinExpectation(cellEstimated *cell) float64 {
	// map tells 100% Amadeusium existence
	if cellEstimated.Amadeusium > 0 {
		return 1
	}
	if cellEstimated.Amadeusium == 0 {
		return 0
	}

	// probability of getting Amadeusium from vein
	digAmadeusiumTable := []float64{
		1,                       // 1st try
		float64(2) / float64(3), // 2nd try
		0.5,                     // 3rd try
	}

	var result float64
	// calculate expectation of getting Amadeusium
	for i := range digAmadeusiumTable {
		result += cellEstimated.DigCountProbability(i) * digAmadeusiumTable[i]
	}
	if cellEstimated.Amadeusium == amadeusiumUnknown {
		result *= initialVeinExpectation(cellEstimated.position)
	}
	return result
}

func veinScore(c *cell) float64 {
	cost := veinCost(c.position)
	return veinExpectation(c) / cost
}

func showVeinScore(g *grid) {
	// header
	debugLog("    ")
	for i := range g.cells[0] {
		debugLog("  %2d  ", i)
	}
	debugLogLine("")
	// body
	for i, cs := range g.cells {
		debugLog("%2d :", i)
		for _, c := range cs {
			debugLog("%0.3f|", veinScore(c))
		}
		debugLogLine("")
	}
}

func findNearVeinImpl(r *robot, gridEstimated *grid, requireAmadeusiumCount int) (*cell, int) {
	turnToTarget := 10000
	var target *cell
	for _, cc := range gridEstimated.cells {
		for _, c := range cc {
			if c.Amadeusium >= requireAmadeusiumCount && !c.IsMyTrap && c.EnemyTrapProbability == 0 {
				turn := calcDigAndGoHeadquarterTurns(r, c.position)
				if turn > turnToTarget {
					continue
				}
				if turn == turnToTarget && c.Amadeusium < target.Amadeusium {
					continue
				}
				turnToTarget = turn
				target = c
			}
		}
	}
	return target, turnToTarget
}

func findNearVein(r *robot, gridEstimated *grid) (*cell, int) {
	return findNearVeinImpl(r, gridEstimated, 1)
}

func findNearVeinForTrap(r *robot, gridEstimated *grid) (*cell, int) {
	return findNearVeinImpl(r, gridEstimated, 2)
}

func veinCount(gridEstimated *grid) int {
	var count int
	for _, cc := range gridEstimated.cells {
		for _, c := range cc {
			if c.Amadeusium > 0 {
				count++
			}
		}
	}
	return count
}

func veins(gridEstimated *grid) []*cell {
	veins := make([]*cell, 0, maxHeight*maxWidth)
	for _, cc := range gridEstimated.cells {
		for _, c := range cc {
			if c.Amadeusium > 0 {
				veins = append(veins, c)
			}
		}
	}
	return veins
}
