package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPlan_Score(t *testing.T) {
	robot := newRobot(0, entityTypeMyRobot, 0, 0, itemNone)
	target := newCell(0, 0)

	// Radar
	plan := newPutRadarPlan(1, robot, target, 100, 10, nil)
	assert.Equal(t, float64(30), plan.Score())
	// Trap
	plan = newPutTrapPlan(1, robot, target, 100, 10, nil)
	assert.Equal(t, float64(10), plan.Score())
	// Dig
	plan = newDigVeinPlan(1, robot, target, 100, 10, nil)
	assert.Equal(t, float64(10), plan.Score())
	// Deliver
	plan = newDeliverAmadeusiumPlan(1, robot, target, 10, nil)
	assert.Equal(t, 0.1, plan.Score())

	// Enemy trap consumption
	target.EnemyTrapProbability = 0.5
	// Radar
	plan = newPutRadarPlan(0, robot, target, 100, 10, nil)
	assert.Equal(t, float64(20), plan.Score())
	// Trap
	plan = newPutTrapPlan(0, robot, target, 100, 10, nil)
	assert.Equal(t, float64(0), plan.Score())
	// Dig
	plan = newDigVeinPlan(0, robot, target, 100, 10, nil)
	assert.Equal(t, float64(0), plan.Score())
	// Deliver
	plan = newDeliverAmadeusiumPlan(0, robot, target, 10, nil)
	assert.Equal(t, 0.1, plan.Score())
}
