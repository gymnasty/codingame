package main

func estimateGrid(turn int, turnInput *turnInput, gridEstimated *grid, robotHistories map[int]*robotHistory) *grid {
	if turn == 0 {
		return turnInput.grid
	}
	// update from robotHistories
	for _, robot := range turnInput.myRobots {
		previousAction := robotHistories[robot.ID].GetAction(turn - 1)
		if previousAction.action == actionTypeDig {
			pos := previousAction.position
			cellEstimated := gridEstimated.GetCell(pos)
			if robot.HasAmadeusium() {
				cellEstimated.Amadeusium = amadeusiumWasVein
			} else {
				cellEstimated.Amadeusium = 0
			}
			cellEstimated.Dig(1)
		}
	}
	// update from grid
	for _, cc := range turnInput.grid.cells {
		for _, c := range cc {
			cellEstimated := gridEstimated.GetCell(c.position)
			if c.Amadeusium >= 0 {
				cellEstimated.Amadeusium = c.Amadeusium
			}
			cellEstimated.IsHole = c.IsHole
		}
	}
	// update from traps
	for _, cc := range gridEstimated.cells {
		for _, c := range cc {
			c.IsMyTrap = false
		}
	}
	for _, t := range turnInput.traps {
		cellEstimated := gridEstimated.GetCell(t.position)
		cellEstimated.IsMyTrap = true
	}
	// update from enemy robotHistories
	enemyPutPositions := make([]*position, 0, len(turnInput.enemyRobots))
	for _, robot := range turnInput.enemyRobots {
		previousAction := robotHistories[robot.ID].GetAction(turn - 1)
		previousItem := robotHistories[robot.ID].GetItem(turn - 1)
		if previousAction.action == actionTypeDig {
			if previousAction.position != nil {
				cellEstimated := gridEstimated.GetCell(previousAction.position)
				if previousItem == itemRadarOrTrap {
					enemyPutPositions = append(enemyPutPositions, previousAction.position)
					cellEstimated.EnemyTrapProbability = 0.5
					debugLogLine("set enemy trap probability: %f, %v", cellEstimated.EnemyTrapProbability, cellEstimated.position)
				} else {
					// no trap if enemy digs the cell except other enemy robot put trap in this turn
					func() {
						for _, pos := range enemyPutPositions {
							if pos.Distance(previousAction.position) == 0 {
								return
							}
						}
						cellEstimated.EnemyTrapProbability = 0
						debugLogLine("reset enemy trap probability: %f, %v", cellEstimated.EnemyTrapProbability, cellEstimated.position)
					}()
				}
				cellEstimated.Dig(1)
			} else {
				e := float64(1) / float64(len(previousAction.candidates))
				for _, p := range previousAction.candidates {
					cellEstimated := gridEstimated.GetCell(p)
					if previousItem == itemRadarOrTrap {
						enemyPutPositions = append(enemyPutPositions, p)
						cellEstimated.EnemyTrapProbability = 0.5 * e
						debugLogLine("set enemy trap probability: %f, %v", cellEstimated.EnemyTrapProbability, cellEstimated.position)
					}
					cellEstimated.Dig(e)
				}
			}
		}
	}
	return gridEstimated
}

func estimatePreviousEnemyRobotAction(turn int, turnInput *turnInput, previousGrid *grid, previousEnemyRobots []*robot, robotHistories map[int]*robotHistory) {
	if turn == 0 {
		return
	}
	for i, r := range turnInput.enemyRobots {
		previous := previousEnemyRobots[i]
		current := r
		history := robotHistories[r.ID]
		// check move
		if checkMove(previous, current) {
			history.AddAction(newMoveAction(current.position))
			current.item = previous.item
			continue
		}
		// check request
		if checkRequest(previous, current) {
			history.AddAction(newRequestAction())
			current.item = itemRadarOrTrap
			continue
		}
		// check dig
		if dig, pos := checkDig(previousGrid, turnInput.grid, current); dig {
			current.item = itemNone
			if len(pos) == 1 {
				history.AddAction(newDigAction(pos[0]))
			} else {
				history.AddAction(newDigActionWithCandidate(pos))
			}
			continue
		}
		// wait
		history.AddAction(newWaitAction())
		current.item = previous.item
	}
}

func calcEnemyTrapProbability(currentTrapProbability, putTrapProbability float64) float64 {
	return (1-currentTrapProbability)*putTrapProbability + currentTrapProbability
}
