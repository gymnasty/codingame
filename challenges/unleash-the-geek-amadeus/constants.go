package main

const (
	DEBUG = false

	maxTurn               = 200
	maxWidth              = 30
	maxHeight             = 15
	radarCoolDownInterval = 5
	trapCoolDownInterval  = 5
)
