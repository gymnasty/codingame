package main

import (
	"fmt"
)

const (
	// entityType
	entityTypeMyRobot    = 0
	entityTypeEnemyRobot = 1
	entityTypeRadar      = 2
	entityTypeTrap       = 3
)

// entity
type entity struct {
	*position
	ID   int
	Type int
}

func newEntity(ID, Type, x, y int) *entity {
	return &entity{
		position: newPosition(x, y),
		ID:       ID,
		Type:     Type,
	}
}

func (e *entity) String() string {
	var typeName string
	switch e.Type {
	case entityTypeMyRobot:
		typeName = "MyRobot"
	case entityTypeEnemyRobot:
		typeName = "EnemyRobot"
	case entityTypeRadar:
		typeName = "Radar"
	case entityTypeTrap:
		typeName = "Trap"
	default:
		typeName = fmt.Sprintf("UnsupportedEntityType(%d)", e.Type)
	}
	return fmt.Sprintf("%s: %d: pos(%d, %d)", typeName, e.ID, e.x, e.y)
}

func (e *entity) Show() {
	debugLogLine(e.String())
}
