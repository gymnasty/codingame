package main

type robotHistory struct {
	ID              int
	positionHistory []*position
	actionHistory   []*action
	itemHistory     []int
}

func newRobotHistory(id int) *robotHistory {
	return &robotHistory{
		ID:              id,
		positionHistory: make([]*position, 0, maxTurn),
		actionHistory:   make([]*action, 0, maxTurn),
	}
}

func (h *robotHistory) AddPosition(p *position) {
	h.positionHistory = append(h.positionHistory, p)
}

func (h *robotHistory) GetPosition(turn int) *position {
	return h.positionHistory[turn]
}

func (h *robotHistory) AddAction(a *action) {
	h.actionHistory = append(h.actionHistory, a)
}

func (h *robotHistory) GetAction(turn int) *action {
	return h.actionHistory[turn]
}

func (h *robotHistory) AddItem(a int) {
	h.itemHistory = append(h.itemHistory, a)
}

func (h *robotHistory) GetItem(turn int) int {
	return h.itemHistory[turn]
}

func (h *robotHistory) HasItem() bool {
	l := len(h.actionHistory)
	for i := 0; i < l; i++ {
		a := h.actionHistory[l-i]
		if a.action == actionTypeDig {
			return false
		}
		if a.action == actionTypeRequest {
			return true
		}
	}
	return false
}
