package main

type grid struct {
	cells  [][]*cell
	width  int
	height int
}

func newGrid(width, height int) *grid {
	cells := make([][]*cell, height)
	for i := range cells {
		cells[i] = make([]*cell, width)
	}
	for y, cc := range cells {
		for x, _ := range cc {
			cells[y][x] = newCell(x, y)
		}
	}
	return &grid{
		cells:  cells,
		width:  width,
		height: height,
	}
}

func (g *grid) SetCell(c *cell) {
	g.cells[c.y][c.x] = c
}

func (g *grid) GetCell(pos *position) *cell {
	if pos.x >= 0 && pos.y >= 0 && pos.x < g.width && pos.y < g.height {
		return g.cells[pos.y][pos.x]
	}
	return nil
}

func (g *grid) AllCells() []*cell {
	cells := make([]*cell, 0, g.width*g.height)
	for _, cc := range g.cells {
		cells = append(cells, cc...)
	}
	return cells
}

func (g *grid) Cluster(center *position, distance int) []*cell {
	clusterSize := 1 + 2*distance*(distance+1)
	cluster := make([]*cell, 0, clusterSize)
	for x := center.x - distance; x <= center.x+distance; x++ {
		for y := center.y - distance; y <= center.y+distance; y++ {
			if c := g.GetCell(newPosition(x, y)); c != nil {
				if center.Distance(c.position) <= distance {
					cluster = append(cluster, c)
				}
			}
		}
	}
	return cluster
}

func (g *grid) DigTargets(p *position) []*cell {
	neighbors := g.Cluster(p, 1)
	targets := make([]*cell, 0, len(neighbors))
	for _, n := range neighbors {
		if n.x > 0 {
			targets = append(targets, n)
		}
	}
	if p.x > 0 {
		targets = append(targets, g.GetCell(p))
	}
	return targets
}

func (g *grid) Show() {
	// header
	debugLog("    ")
	for i := range g.cells[0] {
		debugLog(" %2d  ", i)
	}
	debugLogLine("")
	// body
	for i, cs := range g.cells {
		debugLog("%2d :", i)
		for _, c := range cs {
			c.Show()
			debugLog("|")
		}
		debugLogLine("")
	}
}

func applyHoleByMyRobots(previousGrid *grid, turnInput *turnInput, robotHistories map[int]*robotHistory) *grid {
	if turnInput.turn == 0 {
		return previousGrid
	}
	for _, r := range turnInput.myRobots {
		action := robotHistories[r.ID].GetAction(turnInput.turn - 1)
		if action.action == actionTypeDig {
			previousGrid.GetCell(action.position).IsHole = true
		}
	}
	return previousGrid
}
