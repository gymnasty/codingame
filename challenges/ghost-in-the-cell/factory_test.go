package main

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type FactoryTestSuite struct {
	suite.Suite
}

func (suite *FactoryTestSuite) SetupSuite() {
}

func (suite *FactoryTestSuite) TearDownSuite() {
}

func (suite *FactoryTestSuite) SetupTest() {
}

func (suite *FactoryTestSuite) TearDownTest() {
}

func TestFactoryTestSuite(t *testing.T) {
	suite.Run(t, new(FactoryTestSuite))
}

func (suite *FactoryTestSuite) TestNewFactory() {
	f := newFactory(1)
	suite.Equal(f.id, 1)
	suite.Empty(f.links)
}

func (suite *FactoryTestSuite) TestLinks() {
	f := newFactory(1)
	f.addLink(1, 10)
	suite.Equal(1, len(f.links))
	suite.Equal(10, f.links[1])
	f.addLink(2, 20)
	suite.Equal(2, len(f.links))
	suite.Equal(20, f.links[2])
	f.addLink(3, 15)
	suite.Equal(3, len(f.links))
	suite.Equal(15, f.links[3])
	// sort
	f.setLinkOrder()
	suite.Equal([]int{1, 3, 2}, f.linkOrder)
}

func (suite *FactoryTestSuite) TestUpdate() {
	f := newFactory(1)
	f.update(1, 2, 3, 0)
	suite.Equal(1, f.side)
	suite.Equal(2, f.cyborgs)
	suite.Equal(3, f.production)
}

func (suite *FactoryTestSuite) TestCopyFactory() {
	f := newFactory(1)
	f.addLink(1, 10)
	f.addLink(2, 20)
	f.addLink(3, 15)
	f.setLinkOrder()
	f.update(1, 2, 3, 0)
	c := copyFactory(f)
	suite.Equal(f, c)
}

func (suite *FactoryTestSuite) TestCopyFactories() {
	f1 := newFactory(1)
	f1.addLink(1, 10)
	f1.addLink(2, 20)
	f1.addLink(3, 15)
	f1.setLinkOrder()
	f1.update(1, 2, 3, 0)

	f2 := newFactory(2)
	f2.addLink(21, 210)
	f2.addLink(22, 220)
	f2.addLink(23, 215)
	f2.setLinkOrder()
	f2.update(21, 22, 23, 0)

	factories := []*factory{f1, f2}
	c := copyFactories(factories)

	suite.Equal(factories, c)
}
