package main

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type FutureTestSuite struct {
	suite.Suite
}

func (suite *FutureTestSuite) SetupSuite() {
}

func (suite *FutureTestSuite) TearDownSuite() {
}

func (suite *FutureTestSuite) SetupTest() {
}

func (suite *FutureTestSuite) TearDownTest() {
}

func TestFutureTestSuite(t *testing.T) {
	suite.Run(t, new(FutureTestSuite))
}

func (suite *FutureTestSuite) TestProduction() {
	f0 := newFactory(0)
	f0.update(sideMine, 10, 3, 0)
	f1 := newFactory(1)
	f1.update(sideEnemy, 10, 2, 0)
	factories := []*factory{f0, f1}
	troops := []*troop{}
	bombs := []*bomb{}

	f, t, b := calcNextTurn(factories, troops, bombs)
	suite.Equal(troops, t)
	suite.Equal(bombs, b)
	suite.Equal(13, f[0].cyborgs)
	suite.Equal(12, f[1].cyborgs)

	// cyborg 以外の一致を確認
	f[0].cyborgs = 10
	f[1].cyborgs = 10
	suite.Equal(factories, f)
}

func (suite *FutureTestSuite) TestSleep() {
	f0 := newFactory(0)
	f0.update(sideMine, 10, 3, 5)
	f1 := newFactory(1)
	f1.update(sideEnemy, 10, 2, 0)
	factories := []*factory{f0, f1}
	troops := []*troop{}
	bombs := []*bomb{}

	f, t, b := calcNextTurn(factories, troops, bombs)
	suite.Equal(troops, t)
	suite.Equal(bombs, b)
	suite.Equal(10, f[0].cyborgs)
	suite.Equal(4, f[0].sleep)
	suite.Equal(12, f[1].cyborgs)
	suite.Equal(0, f[1].sleep)
}

func (suite *FutureTestSuite) TestTroop() {
	f0 := newFactory(0)
	f0.update(sideMine, 10, 0, 0)
	f1 := newFactory(1)
	f1.update(sideEnemy, 10, 0, 0)
	t0 := newTroop(0, sideEnemy, 1, 0, 3, 3)
	t1 := newTroop(1, sideMine, 0, 1, 2, 2)
	factories := []*factory{f0, f1}
	troops := []*troop{t0, t1}
	bombs := []*bomb{}

	f, t, b := calcNextTurn(factories, troops, bombs)
	suite.Equal(factories, f)
	suite.Equal(bombs, b)
	for i := range t {
		suite.Equal(troops[i].timeToArrive-1, t[i].timeToArrive)
		// もとに戻す
		t[i].timeToArrive = troops[i].timeToArrive
	}
	// 残りターン数以外の一致を確認
	suite.Equal(troops, t)
}

func (suite *FutureTestSuite) TestBattle() {
	f0 := newFactory(0)
	f0.update(sideMine, 10, 3, 0)
	f1 := newFactory(1)
	f1.update(sideEnemy, 10, 3, 0)
	f2 := newFactory(2)
	f2.update(sideEnemy, 10, 3, 0)
	f3 := newFactory(3)
	f3.update(sideMine, 10, 3, 0)
	t0 := newTroop(0, sideEnemy, 1, 0, 13, 1)
	t1 := newTroop(1, sideMine, 0, 1, 8, 1)
	t2 := newTroop(2, sideMine, 0, 1, 4, 2)
	t3 := newTroop(3, sideMine, 0, 2, 15, 1)
	t4 := newTroop(4, sideMine, 0, 3, 1, 1)
	factories := []*factory{f0, f1, f2, f3}
	troops := []*troop{t0, t1, t2, t3, t4}
	bombs := []*bomb{}

	f, t, b := calcNextTurn(factories, troops, bombs)
	// サイボーグが生産されたあとに戦闘する仕様
	// 戦闘後に爆弾が爆発する仕様
	suite.Equal(0, f[0].cyborgs)
	suite.Equal(sideMine, f[0].side)
	suite.Equal(5, f[1].cyborgs)
	suite.Equal(sideEnemy, f[1].side)
	suite.Equal(2, f[2].cyborgs)
	suite.Equal(sideMine, f[2].side)
	suite.Equal(14, f[3].cyborgs)
	suite.Equal(sideMine, f[3].side)
	// 到着したサイボーグ・爆弾は削除される
	suite.Equal(1, len(t))
	suite.Equal(bombs, b)
}

func (suite *FutureTestSuite) TestExplosion() {
	f0 := newFactory(0)
	f0.update(sideMine, 10, 3, 0)
	f1 := newFactory(1)
	f1.update(sideMine, 10, 3, 0)
	f2 := newFactory(2)
	f2.update(sideMine, 10, 3, 0)
	f3 := newFactory(3)
	f3.update(sideMine, 20, 3, 0)
	t0 := newTroop(0, sideEnemy, 0, 1, 10, 1)
	t1 := newTroop(1, sideMine, 0, 2, 4, 1)
	factories := []*factory{f0, f1, f2, f3}
	troops := []*troop{t0, t1}
	b1 := newBomb(1, sideMine, 0, 1, 1)
	b2 := newBomb(2, sideMine, 0, 2, 1)
	b3 := newBomb(3, sideMine, 0, 3, 1)
	b4 := newBomb(4, sideMine, 0, 3, 2)
	b5 := newBomb(5, sideEnemy, 1, -1, -1)
	bombs := []*bomb{b1, b2, b3, b4, b5}

	f, t, b := calcNextTurn(factories, troops, bombs)
	// 戦闘後に爆弾が爆発する仕様
	suite.Equal(13, f[0].cyborgs)
	suite.Equal(sideMine, f[0].side)
	suite.Equal(0, f[0].sleep)
	suite.Equal(0, f[1].cyborgs)
	suite.Equal(sideMine, f[1].side)
	suite.Equal(5, f[1].sleep)
	suite.Equal(7, f[2].cyborgs)
	suite.Equal(sideMine, f[2].side)
	suite.Equal(5, f[2].sleep)
	suite.Equal(12, f[3].cyborgs)
	suite.Equal(sideMine, f[3].side)
	suite.Equal(5, f[3].sleep)
	// 到着したサイボーグ・爆弾は削除される
	suite.Empty(t)
	suite.Equal(2, len(b))
	suite.Equal(b5, b[1])
}
