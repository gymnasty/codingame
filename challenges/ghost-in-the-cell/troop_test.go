package main

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type TroopTestSuite struct {
	suite.Suite
}

func (suite *TroopTestSuite) SetupSuite() {
}

func (suite *TroopTestSuite) TearDownSuite() {
}

func (suite *TroopTestSuite) SetupTest() {
}

func (suite *TroopTestSuite) TearDownTest() {
}

func TestTroopTestSuite(t *testing.T) {
	suite.Run(t, new(TroopTestSuite))
}

func (suite *TroopTestSuite) TestNewTroop() {
	t := newTroop(0, 1, 2, 3, 4, 5)
	suite.Equal(1, t.side)
	suite.Equal(2, t.from)
	suite.Equal(3, t.to)
	suite.Equal(4, t.cyborgs)
	suite.Equal(5, t.timeToArrive)
}

func (suite *TroopTestSuite) TestCopyTroop() {
	t := newTroop(0, 1, 2, 3, 4, 5)
	c := copyTroop(t)
	suite.Equal(t, c)
}

func (suite *TroopTestSuite) TestCopyTroops() {
	t1 := newTroop(0, 1, 2, 3, 4, 5)
	t2 := newTroop(6, 7, 8, 9, 10, 11)
	troops := []*troop{t1, t2}
	c := copyTroops(troops)
	suite.Equal(troops, c)
}
