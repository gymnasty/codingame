package main

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type ActionTestSuite struct {
	suite.Suite
}

func (suite *ActionTestSuite) SetupSuite() {
}

func (suite *ActionTestSuite) TearDownSuite() {
}

func (suite *ActionTestSuite) SetupTest() {
}

func (suite *ActionTestSuite) TearDownTest() {
}

func TestActionTestSuite(t *testing.T) {
	suite.Run(t, new(ActionTestSuite))
}

func (suite *ActionTestSuite) TestWait() {
	a := newAction()
	c := a.getCommandString()
	suite.Equal("WAIT", c)
}

func (suite *ActionTestSuite) TestMove() {
	a := newAction()
	a.move(1, 2, 3)
	c := a.getCommandString()
	suite.Equal("MOVE 1 2 3", c)
}

func (suite *ActionTestSuite) TestBomb() {
	a := newAction()
	a.bomb(1, 2)
	c := a.getCommandString()
	suite.Equal("BOMB 1 2", c)
}

func (suite *ActionTestSuite) TestIncrease() {
	a := newAction()
	a.increase(1)
	c := a.getCommandString()
	suite.Equal("INC 1", c)
}

func (suite *ActionTestSuite) TestMsg() {
	a := newAction()
	msg := "test message"
	a.message(msg)
	c := a.getCommandString()
	suite.Equal("MSG test message", c)
}

func (suite *ActionTestSuite) TestMultActions() {
	a := newAction()
	a.move(1, 2, 3)
	a.message("test")
	a.move(3, 4, 5)
	c := a.getCommandString()
	suite.Equal("MOVE 1 2 3; MSG test; MOVE 3 4 5", c)
}
