package main

import (
	"fmt"
	"math/rand"
	"sort"
	"strings"
	"time"
)

// Injections

var gameInputReader func() gameInput
var turnInputReader func() turnInput

// Constants

const (
	// action
	actionMove     string = "MOVE"
	actionWait     string = "WAIT"
	actionMsg      string = "MSG"
	actionBomb     string = "BOMB"
	actionIncrease string = "INC"
	// entity
	entityFactory string = "FACTORY"
	entityTroop   string = "TROOP"
	entityBomb    string = "BOMB"
	// side
	sideMine    int = 1
	sideEnemy   int = -1
	sideNeutral int = 0
	// distance
	maxDistance int = 20
	// turn
	maxTurn int = 200
)

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func abs(a int) int {
	if a > 0 {
		return a
	}
	return -a
}

func opponentSide(side int) int {
	return -1 * side
}

// Game variables
var (
	factoryNum   int
	currentTurn  int
	myBombNum    int
	enemyBombNum int
)

// Input

type link struct {
	factory1 int
	factory2 int
	distance int
}

func (l link) String() string {
	return fmt.Sprintf("%d <- %d -> %d", l.factory1, l.distance, l.factory2)
}

type entity struct {
	id                           int
	entityType                   string
	arg1, arg2, arg3, arg4, arg5 int
}

func (e entity) String() string {
	return fmt.Sprintf("%s[%d]: (%d, %d, %d, %d, %d)", e.entityType, e.id, e.arg1, e.arg2, e.arg3, e.arg4, e.arg5)
}

type gameInput struct {
	factoryCount int
	linkCount    int
	links        []link
}

func (i gameInput) String() string {
	s := fmt.Sprintf("factory count: %d\n", i.factoryCount)
	s += fmt.Sprintf("link count: %d\n", i.linkCount)
	for _, l := range i.links {
		s += fmt.Sprintln(l)
	}
	return s
}

type turnInput struct {
	entityCount int
	entities    []entity
}

func (i turnInput) String() string {
	s := fmt.Sprintf("entity count: %d\n", i.entityCount)
	for _, e := range i.entities {
		s += fmt.Sprintln(e)
	}
	return s
}

func readGameInput() gameInput {
	input := gameInput{}
	_, _ = fmt.Scan(&input.factoryCount)
	_, _ = fmt.Scan(&input.linkCount)
	input.links = make([]link, input.linkCount)
	for i := 0; i < input.linkCount; i++ {
		_, _ = fmt.Scan(&input.links[i].factory1, &input.links[i].factory2, &input.links[i].distance)
	}
	return input
}

func readTurnInput() turnInput {
	input := turnInput{}
	_, _ = fmt.Scan(&input.entityCount)
	input.entities = make([]entity, input.entityCount)
	for i := 0; i < input.entityCount; i++ {
		_, _ = fmt.Scan(&input.entities[i].id, &input.entities[i].entityType, &input.entities[i].arg1, &input.entities[i].arg2, &input.entities[i].arg3, &input.entities[i].arg4, &input.entities[i].arg5)
	}
	return input
}

func sideString(side int) string {
	switch side {
	case sideMine:
		return "Mine"
	case sideEnemy:
		return "Enemy"
	case sideNeutral:
		return "Neutral"
	}
	return "Unknown"
}

// action

type action struct {
	commands []string
}

func newAction() *action {
	return &action{
		commands: []string{},
	}
}

func (a *action) move(source int, destination int, cyborgs int) {
	cmd := fmt.Sprintf("%s %d %d %d", actionMove, source, destination, cyborgs)
	a.commands = append(a.commands, cmd)
}

func (a *action) bomb(source int, destination int) {
	cmd := fmt.Sprintf("%s %d %d", actionBomb, source, destination)
	a.commands = append(a.commands, cmd)
}

func (a *action) increase(target int) {
	cmd := fmt.Sprintf("%s %d", actionIncrease, target)
	a.commands = append(a.commands, cmd)
}

func (a *action) message(msg string) {
	cmd := fmt.Sprintf("%s %s", actionMsg, msg)
	a.commands = append(a.commands, cmd)
}

func (a *action) getCommandString() string {
	if len(a.commands) > 0 {
		return strings.Join(a.commands, "; ")
	}
	return actionWait
}

func (a *action) do() {
	fmt.Println(a.getCommandString())
}

// 到着するサイボーグの数を計算する
// arrival[turn]
func calcCyborgArrival(troops []*troop, factoryID int, side int) []int {
	arrival := make([]int, maxDistance)
	for _, t := range troops {
		if t.to == factoryID && t.side == side {
			arrival[t.timeToArrive] += t.cyborgs
		}
	}
	return arrival
}

// arrivals[factoryID][turn]
func calcCyborgArrivals(troops []*troop, side int) [][]int {
	arrivals := make([][]int, factoryNum)
	for i := range arrivals {
		arrivals[i] = make([]int, maxDistance)
	}
	for _, t := range troops {
		if t.side == side {
			arrivals[t.to][t.timeToArrive] += t.cyborgs
		}
	}
	return arrivals
}

type turnOver struct {
	minCyborgs int
	turnOver   bool
}

func getTurnOvers(factories []*factory, futureFactories [][]*factory) []turnOver {
	turnOvers := make([]turnOver, len(factories))
	for i, f := range factories {
		if f.side == sideMine {
			// 現在占領中の工場に対して
			turnOvers[i].minCyborgs = f.cyborgs
			for _, ff := range futureFactories {
				// 将来のサイボーグの最小値を計算
				if ff[i].side == sideMine {
					if turnOvers[i].minCyborgs > ff[i].cyborgs {
						turnOvers[i].minCyborgs = ff[i].cyborgs
					}
				} else {
					turnOvers[i].minCyborgs = 0
					turnOvers[i].turnOver = true
					break
				}
			}
		}
	}
	return turnOvers
}

// 工場を維持するために必要なサイボーグの数を取得
// N ターン後に追加で必要になるサイボーグ数を表します。（0 は次のターンで到着する必要があるサイボーグの数）
func getRequiredCyborgs(f *factory, troops []*troop) []int {
	requiredCyborgs := make([]int, maxDistance)
	if f.side == sideNeutral {
		return requiredCyborgs
	}

	arrival := calcCyborgArrival(troops, f.id, f.side)
	opponent := opponentSide(f.side)
	opponentArrival := calcCyborgArrival(troops, f.id, opponent)
	cyborgs := f.cyborgs
	for i := 0; i < maxDistance; i++ {
		if f.sleep <= i {
			cyborgs += f.production
		}
		cyborgs += arrival[i]
		cyborgs -= opponentArrival[i]
		if cyborgs < 0 {
			requiredCyborgs[i] = -cyborgs
		}
	}
	return requiredCyborgs
}

func getNextAction(factories []*factory, troops []*troop, futureFactories [][]*factory) *action {
	action := newAction()

	// サイボーグの数と生産数のバランスを確認する
	cyborgsBalance, productionBalance := calcCyborgBalance(factories, troops)
	action.message(fmt.Sprintf("c:%d, p:%d", cyborgsBalance, productionBalance))

	// ターンオーバーの可能性と余剰サイボーグを確認
	turnOvers := getTurnOvers(factories, futureFactories)

	// 爆弾回避

	// 防御
	for to, t := range turnOvers {
		// 敵に占領される可能性のある工場にサイボーグを送る
		if t.turnOver {
			// 必要なサイボーグ数を確認
			requiredCyborgs := getRequiredCyborgs(factories[to], troops)
			// 近い順にサイボーグを送る
			// 愚直に近い方から埋めていくとすべてを満たせない可能性がある
			// ランダムに散らしてすべてを解決する方法を模索すると良いかもしれない
			f := factories[to]
			sent := 0
			for _, from := range f.linkOrder {
				dist := f.links[from]
				// 送るサイボーグの数を決定する
				send := requiredCyborgs[dist] - sent
				if turnOvers[from].minCyborgs < send {
					send = turnOvers[from].minCyborgs
				}
				if send > 0 {
					action.move(from, to, send)
					sent += send
					turnOvers[from].minCyborgs -= send
				}
			}
		}
	}

	// 各工場のスコアを計算
	factoryScores := calcFactoryScores(factories)
	sideScores := calcSideScore(factories)
	attackScores := calcAttackScore(factoryScores, sideScores)

	// 攻撃
	for _, s := range attackScores {
		//fmt.Fprintln(os.Stderr, s)
		if s.score < 0 {
			continue
		}
		to := s.factoryID
		f := factories[to]
		if f.side != sideMine {
			sent := 0
			for _, from := range f.linkOrder {
				dist := f.links[from]
				ff := futureFactories[dist][to]
				if factories[from].side == sideMine && ff.side != sideMine {
					requiredCyborgs := ff.cyborgs - sent
					send := min(requiredCyborgs+1, turnOvers[from].minCyborgs)
					if send > 0 {
						action.move(from, to, send)
						turnOvers[from].minCyborgs -= send
						turnOvers[to].minCyborgs -= send
						sent += send
					}
				}
			}
		}
	}

	// 爆弾
	// 最前線の工場に送ってターンオーバーを狙うというのもよいかも
	if myBombNum > 0 {
		enemyArrivals := calcCyborgArrivals(troops, sideEnemy)
		var maxBombScore, factoryID, turn int
		for to, arrivals := range enemyArrivals {
			for dist, a := range arrivals {
				f := futureFactories[dist][to]
				score := min(a-f.cyborgs, 0)
				if factories[to].side == sideEnemy && f.side == sideEnemy {
					score += f.production * (5 - f.sleep)
				}
				if maxBombScore < score {
					maxBombScore = score
					factoryID = to
					turn = dist
				}
			}
		}
		// Bomb
		if maxBombScore >= 15 {
			for i, dist := range factories[factoryID].links {
				if dist == turn-1 {
					from := i
					if factories[from].side == sideMine {
						action.bomb(from, factoryID)
						myBombNum--
						break
					}
				}
			}
		}
	}

	// 生産量増加
	for i, t := range turnOvers {
		f := factories[i]
		if t.minCyborgs > 10 && f.production < 3 {
			action.increase(i)
			turnOvers[i].minCyborgs -= 10
			f.production += 1
		}
	}

	// バランス調整
	for to := range sideScores {
		f := factories[to]
		if f.production == 3 || f.side != sideMine {
			continue
		}
		// TODO: 送るサイボーグの数はもう少し削れるかもしれない
		reqCyborgs := max(10-turnOvers[to].minCyborgs, 0)
		turnOvers[to].minCyborgs -= 10 - reqCyborgs
		if reqCyborgs > 0 {

			for _, from := range f.linkOrder {
				t := turnOvers[from]
				if t.minCyborgs > 0 {
					send := min(reqCyborgs, t.minCyborgs)
					if send > 0 {
						action.move(from, to, send)
						t.minCyborgs -= send
						reqCyborgs -= send
					}
				}
				if reqCyborgs <= 0 {
					break
				}
			}
		}
	}

	return action
}

type factory struct {
	id         int
	links      map[int]int
	linkOrder  []int
	side       int
	cyborgs    int
	production int
	sleep      int
}

func newFactory(id int) *factory {
	return &factory{
		id:    id,
		links: map[int]int{},
	}
}

func copyFactory(src *factory) *factory {
	return &factory{
		id:         src.id,
		links:      src.links,
		linkOrder:  src.linkOrder,
		side:       src.side,
		cyborgs:    src.cyborgs,
		production: src.production,
		sleep:      src.sleep,
	}
}

func copyFactories(src []*factory) []*factory {
	dst := make([]*factory, len(src))
	for i, f := range src {
		dst[i] = copyFactory(f)
	}
	return dst
}

func (f *factory) addLink(to int, distance int) {
	f.links[to] = distance
}

func (f *factory) update(side, cyborgs, production, sleep int) {
	f.side = side
	f.cyborgs = cyborgs
	f.production = production
	f.sleep = sleep
}

func (f *factory) setLinkOrder() {
	type tmp struct {
		to, distance int
	}
	links := make([]tmp, len(f.links))
	i := 0
	for to, dist := range f.links {
		links[i] = tmp{to, dist}
		i++
	}
	sort.Slice(links, func(i, j int) bool { return links[i].distance < links[j].distance })
	f.linkOrder = make([]int, len(links))
	for i, l := range links {
		f.linkOrder[i] = l.to
	}
}

func (f *factory) String() string {
	return fmt.Sprintf(
		"Factory(%d) [%s] %d cyborgs, %d production, %v",
		f.id,
		sideString(f.side),
		f.cyborgs,
		f.production,
		f.links)
}

type troop struct {
	id           int
	side         int
	from         int
	to           int
	cyborgs      int
	timeToArrive int
}

func newTroop(id, side, from, to, cyborgs, timeToArrive int) *troop {
	return &troop{
		id:           id,
		side:         side,
		from:         from,
		to:           to,
		cyborgs:      cyborgs,
		timeToArrive: timeToArrive,
	}
}

func copyTroop(src *troop) *troop {
	return &troop{
		id:           src.id,
		side:         src.side,
		from:         src.from,
		to:           src.to,
		cyborgs:      src.cyborgs,
		timeToArrive: src.timeToArrive,
	}
}

func copyTroops(src []*troop) []*troop {
	dst := make([]*troop, len(src))
	for i, t := range src {
		dst[i] = copyTroop(t)
	}
	return dst
}

func (t *troop) String() string {
	return fmt.Sprintf(
		"Troop [%s] (%d -> %d) %d cyborgs in %d turns",
		sideString(t.side),
		t.from,
		t.to,
		t.cyborgs,
		t.timeToArrive)
}

type bomb struct {
	id           int
	side         int
	from         int
	to           int
	timeToArrive int
}

func newBomb(id, side, from, to, timeToArrive int) *bomb {
	return &bomb{
		id:           id,
		side:         side,
		from:         from,
		to:           to,
		timeToArrive: timeToArrive,
	}
}

func copyBomb(src *bomb) *bomb {
	return &bomb{
		id:           src.id,
		side:         src.side,
		from:         src.from,
		to:           src.to,
		timeToArrive: src.timeToArrive,
	}
}

func copyBombs(src []*bomb) []*bomb {
	dst := make([]*bomb, len(src))
	for i, t := range src {
		dst[i] = copyBomb(t)
	}
	return dst
}

func (b *bomb) String() string {
	if b.side == sideMine {
		return fmt.Sprintf(
			"Bomb [%s] (%d -> %d) in %d turns",
			sideString(b.side),
			b.from,
			b.to,
			b.timeToArrive)
	}
	return fmt.Sprintf(
		"Bomb [%s] (%d -> ?) in ? turns",
		sideString(b.side),
		b.from)
}

type score struct {
	factoryID int
	score     int
}

func newScore(id int) *score {
	return &score{
		factoryID: id,
	}
}

func (s *score) add(score int) {
	s.score += score
}

func (s *score) multiple(score int) {
	s.score *= score
}

func (s *score) String() string {
	return fmt.Sprintf("Factory[%d] %d", s.factoryID, s.score)
}

func sortScores(scores []*score) {
	sort.Slice(scores, func(i, j int) bool { return scores[i].score > scores[j].score })
}

func sortScoresReverse(scores []*score) {
	sort.Slice(scores, func(i, j int) bool { return scores[i].score < scores[j].score })
}

// 各工場の重要度を計算する
// 重要度が高いほど占領したときのメリットが大きくなる
func calcFactoryScores(factories []*factory) []*score {
	scores := make([]*score, len(factories))
	for i, f := range factories {
		scores[i] = newScore(i)
		scores[i].add(100)

		// production
		scores[i].add(f.production * 30)
		if f.side == sideNeutral {
			scores[i].add(-1 * f.cyborgs)
		}
	}
	sortScores(scores)
	return scores
}

// 各工場の勢力値を計算する
// 負数が大きいほど敵勢力内、正数が大きいほど味方勢力内であることを表す。
// 敵勢力内であるほど占領するのが難しく、ターンオーバーの可能性も上がる。
func calcSideScore(factories []*factory) []*score {
	scores := make([]*score, len(factories))
	for i := range factories {
		scores[i] = newScore(i)
		for j := range factories {
			f := factories[j]
			if i != j {
				scores[i].add(f.side * 20 * (10*f.production + f.cyborgs) / f.links[i])
			} else {
				scores[i].add(f.side * 20 * (10*f.production + f.cyborgs) * 2)
			}
		}
	}
	sortScores(scores)
	return scores
}

// 攻撃対象の工場のスコアを計算する
// 値が大きいほど優先的に攻撃すべき工場であることを表す。
func calcAttackScore(factoryScores, sideScores []*score) []*score {
	scores := make([]*score, factoryNum)
	for i := 0; i < factoryNum; i++ {
		scores[i] = newScore(i)
		scores[i].add(1)
	}
	for _, s := range factoryScores {
		scores[s.factoryID].multiple(s.score)
	}
	for _, s := range sideScores {
		scores[s.factoryID].multiple(s.score)
	}
	sortScores(scores)
	return scores
}

func calcCyborgBalance(factories []*factory, troops []*troop) (currentCyborgs, production int) {
	currentCyborgs = 0
	production = 0
	for _, t := range troops {
		if t.side == sideMine {
			currentCyborgs += t.cyborgs
		} else if t.side == sideEnemy {
			currentCyborgs -= t.cyborgs
		}
	}
	for _, f := range factories {
		if f.side == sideMine {
			currentCyborgs += f.cyborgs
			production += f.production
		} else if f.side == sideEnemy {
			currentCyborgs -= f.cyborgs
			production -= f.production
		}
	}
	return currentCyborgs, production
}

// 次の N ターンで生成されるサイボーグの数を計算する
func calcProduction(f *factory, turns int) int {
	production := f.production * (turns - f.sleep)
	if production < 0 {
		return 0
	}
	return production
}

func calcNextTurn(factories []*factory, troops []*troop, bombs []*bomb) ([]*factory, []*troop, []*bomb) {
	nextFactories := make([]*factory, len(factories))
	for i, f := range factories {
		nf := copyFactory(f)
		// product cyborgs
		if nf.sleep > 0 {
			nf.sleep--
		} else {
			if nf.side != sideNeutral {
				nf.cyborgs += nf.production
			}
		}
		nextFactories[i] = nf
	}
	nextTroops := make([]*troop, 0)
	for _, t := range troops {
		nt := copyTroop(t)
		// move
		nt.timeToArrive--
		// arrive
		// TODO: 到着したサイボーグを差し引いてから決着をつけなければならない
		if nt.timeToArrive == 0 {
			target := nextFactories[nt.to]
			if target.side == nt.side {
				target.cyborgs += nt.cyborgs
			} else {
				if target.cyborgs < nt.cyborgs {
					// win
					target.side = nt.side
					target.cyborgs = nt.cyborgs - target.cyborgs
				} else {
					// lose
					target.cyborgs -= nt.cyborgs
				}
			}
		} else {
			nextTroops = append(nextTroops, nt)
		}
	}
	nextBombs := make([]*bomb, 0)
	for _, b := range bombs {
		nb := copyBomb(b)
		if nb.side == sideMine {
			// move
			nb.timeToArrive--
			// arrive
			if nb.timeToArrive == 0 {
				// kill cyborgs
				if nextFactories[nb.to].cyborgs < 10 {
					nextFactories[nb.to].cyborgs = 0
				} else if nextFactories[nb.to].cyborgs < 20 {
					nextFactories[nb.to].cyborgs -= 10
				} else {
					nextFactories[nb.to].cyborgs -= nextFactories[nb.to].cyborgs / 2
				}
				// sleep
				nextFactories[nb.to].sleep = 5
			} else {
				nextBombs = append(nextBombs, nb)
			}
		} else {
			nextBombs = append(nextBombs, nb)
		}
	}
	return nextFactories, nextTroops, nextBombs
}

func calcFuture(factories []*factory, troops []*troop, bombs []*bomb) [][]*factory {
	futureFactories := make([][]*factory, maxDistance)
	nf := copyFactories(factories)
	nt := copyTroops(troops)
	nb := copyBombs(bombs)
	for i := 0; i < maxDistance; i++ {
		nf, nt, nb = calcNextTurn(nf, nt, nb)
		futureFactories[i] = nf
	}
	return futureFactories
}

func processGameInput() []*factory {
	gameInput := gameInputReader()
	//fmt.Fprintln(os.Stderr, gameInput)

	factoryNum = gameInput.factoryCount
	myBombNum = 2
	enemyBombNum = 2

	factories := make([]*factory, gameInput.factoryCount)
	for i := 0; i < gameInput.factoryCount; i++ {
		factories[i] = newFactory(i)
	}

	for _, l := range gameInput.links {
		factories[l.factory1].addLink(l.factory2, l.distance)
		factories[l.factory2].addLink(l.factory1, l.distance)
	}

	for _, f := range factories {
		f.setLinkOrder()
	}

	return factories
}

func processTurnInput(factories []*factory) *action {
	turnInput := turnInputReader()
	//fmt.Fprintln(os.Stderr, turnInput)

	currentTurn++

	// troops / bombs
	troops := make([]*troop, 0)
	bombs := make([]*bomb, 0)
	// update
	for _, e := range turnInput.entities {
		switch e.entityType {
		case entityFactory:
			factories[e.id].update(e.arg1, e.arg2, e.arg3, e.arg4)
			break
		case entityTroop:
			troops = append(troops, newTroop(e.id, e.arg1, e.arg2, e.arg3, e.arg4, e.arg5))
			break
		case entityBomb:
			bombs = append(bombs, newBomb(e.id, e.arg1, e.arg2, e.arg3, e.arg4))
			break
		}
	}

	// 盤面予測
	futureFactories := calcFuture(factories, troops, bombs)

	// action
	action := getNextAction(factories, troops, futureFactories)

	return action
}

func main() {
	// rand
	rand.Seed(time.Now().UnixNano())

	// injections
	gameInputReader = readGameInput
	turnInputReader = readTurnInput

	factories := processGameInput()
	for {
		action := processTurnInput(factories)
		action.do()
	}

	// debug
	// fmt.Fprintln(os.Stderr, "")
}
