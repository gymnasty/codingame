package main

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type BombTestSuite struct {
	suite.Suite
}

func (suite *BombTestSuite) SetupSuite() {
}

func (suite *BombTestSuite) TearDownSuite() {
}

func (suite *BombTestSuite) SetupTest() {
}

func (suite *BombTestSuite) TearDownTest() {
}

func TestBombTestSuite(t *testing.T) {
	suite.Run(t, new(BombTestSuite))
}

func (suite *BombTestSuite) TestNewBomb() {
	t := newBomb(0, 1, 2, 3, 4)
	suite.Equal(1, t.side)
	suite.Equal(2, t.from)
	suite.Equal(3, t.to)
	suite.Equal(4, t.timeToArrive)
}

func (suite *BombTestSuite) TestCopyBomb() {
	t := newBomb(0, 1, 2, 3, 4)
	c := copyBomb(t)
	suite.Equal(t, c)
}

func (suite *BombTestSuite) TestCopyBombs() {
	t1 := newBomb(0, 1, 2, 3, 4)
	t2 := newBomb(5, 6, 7, 8, 9)
	bombs := []*bomb{t1, t2}
	c := copyBombs(bombs)
	suite.Equal(bombs, c)
}
