# Rule

## Initialization input

* Line 1:factoryCount, the number of factories.
* Line 2:linkCount, the number of links between factories.
* Next linkCount lines: 3 space-separated integers factory1, factory2 and distance, where distance is the number of turns needed for a troop to travel between factory1 and factory2.

## Input for one game turn

* Line 1: an integer entityCount, the number of entities.
* Next entityCount lines: an integer entityId, a string entityType and 5 integers arg1, arg2, arg3, arg4 and arg5.

### If entityType equals FACTORY then the arguments are

* arg1: player that owns the factory: 1 for you, -1 for your opponent and 0 if neutral
* arg2: number of cyborgs in the factory
* arg3: factory production (between 0 and 3)
* arg4: number of turns before the factory starts producing again (0 means that the factory produces normally)
* arg5: unused

### If entityType equals TROOP then the arguments are

* arg1: player that owns the troop: 1 for you or -1 for your opponent
* arg2: identifier of the factory from where the troop leaves
* arg3: identifier of the factory targeted by the troop
* arg4: number of cyborgs in the troop (positive integer)
* arg5: remaining number of turns before the troop arrives (positive integer)

### If entityType equals BOMB then the arguments are

* arg1: player that send the bomb: 1 if it is you, -1 if it is your opponent
* arg2: identifier of the factory from where the bomb is launched
* arg3: identifier of the targeted factory if it's your bomb, -1 otherwise
* arg4: remaining number of turns before the bomb explodes (positive integer) if that's your bomb, -1 otherwise
* arg5: unused

## Game Turn

One game turn is computed as follows

1. Move existing troops and bombs
2. Execute user orders
3. Produce new cyborgs in all factories
4. Solve battles
5. Make the bombs explode
6. Check end conditions

### Battles

To conquer a factory, you must send cyborgs to the coveted factory. Battles are played in this order:

Cyborgs that reach the same destination on the same turn fight between themselves.
Remaining cyborgs fight against the ones already present in the factory (beware that the cyborgs currently leaving do not fight).
If the number of attacking cyborgs is greater than the number of cyborgs in defense, the factory will then belong to the attacking player and it will start producing new cyborgs for this player on the next turn.

### Bombs

Each player possesses 2 bombs for each game. A bomb can be sent from any factory you control to any factory. The corresponding action is: BOMB source destination, where source is the identifier of the source factory, and destination is the identifier of the destination factory.

When a bomb reaches a factory, half of the cyborgs in the factory are destroyed (floored), for a minimum of 10 destroyed cyborgs. For example, if there are 33 cyborgs, 16 will be destroyed. But if there are only 13 cyborgs, 10 will be destroyed.

Following a bomb explosion, the factory won't be able to produce any new cyborgs during 5 turns.

Be careful, your radar is able to detect the launch of a bomb but you don't know where its target is!

It is impossible to send a bomb and a troop at the same time from the same factory and to the same destination. If you try to do so, only the bomb will be sent.

### Production Increase

At any moment, you can decide to sacrifice 10 cyborgs in a factory to indefinitely increase its production by one cyborg per turn. A factory will not be able to produce more than 3 cyborgs per turn. The corresponding action is: INC factory, where factory is the identifier of the factory that you want to improve.
