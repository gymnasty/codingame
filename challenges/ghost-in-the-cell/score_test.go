package main

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type ScoreTestSuite struct {
	suite.Suite
}

func (suite *ScoreTestSuite) SetupSuite() {
}

func (suite *ScoreTestSuite) TearDownSuite() {
}

func (suite *ScoreTestSuite) SetupTest() {
}

func (suite *ScoreTestSuite) TearDownTest() {
}

func TestScoreTestSuite(t *testing.T) {
	suite.Run(t, new(ScoreTestSuite))
}

func (suite *ScoreTestSuite) TestNewScore() {
	t := newScore(1)
	suite.Equal(1, t.factoryID)
	suite.Equal(0, t.score)
}

func (suite *ScoreTestSuite) TestAdd() {
	t := newScore(1)
	t.add(123)
	suite.Equal(1, t.factoryID)
	suite.Equal(123, t.score)
}

func (suite *ScoreTestSuite) TestSort() {
	t1 := newScore(1)
	t1.add(100)
	t2 := newScore(2)
	t2.add(200)
	t3 := newScore(3)
	t3.add(150)
	scores := []*score{t1, t2, t3}
	sortScores(scores)
	suite.Equal([]*score{t2, t3, t1}, scores)
}
