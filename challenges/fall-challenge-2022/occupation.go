package main

// 占有情報
type Occupation struct {
	myMinSteps       map[Position]int
	opponentMinSteps map[Position]int
	occupationMap    map[Position]int // 正の数なら相手の領域、負の数なら自分の領域
}

// TODO: 経路情報も含める？
// 各ステップでどこから来たかを記録しておいて、移動後の occupation map のトータルが良いものを採用する？
func NewOccupation() *Occupation {
	return &Occupation{
		myMinSteps:       make(map[Position]int),
		opponentMinSteps: make(map[Position]int),
		occupationMap:    make(map[Position]int),
	}
}

func (o *Occupation) Extend(occupation *Occupation) {
	for pos, step := range occupation.myMinSteps {
		o.myMinSteps[pos] = step
	}
	for pos, step := range occupation.opponentMinSteps {
		o.opponentMinSteps[pos] = step
	}
	for pos, step := range occupation.occupationMap {
		o.occupationMap[pos] = step
	}
}

// 占有しているタイルの差を取得
// 正の数なら有利、負の数なら不利
func (o *Occupation) MyOccupationTiles() int {
	occupations := 0
	for _, occupation := range o.occupationMap {
		if occupation > 0 {
			occupations--
		} else if occupation < 0 {
			occupations++
		}
	}
	return occupations
}

// 占有の対象を取得
func (o *Occupation) OccupationTargets(step int) []Position {
	positions := make([]Position, 0, len(o.occupationMap))
	for pos, s := range o.occupationMap {
		if s == step {
			positions = append(positions, pos)
		}
	}
	return positions
}

func GetOccupation(tiles map[Position]*ExTile) *Occupation {
	// 最小ステップ数を作成
	myMinSteps := calcMinSteps(tiles, false)
	opponentMinSteps := calcMinSteps(tiles, true)

	// 最小ステップ数の差を作成
	// 正の数なら相手の領域、負の数なら自分の領域
	occupationMap := make(map[Position]int, len(tiles))
	for pos, myMinStep := range myMinSteps {
		occupationMap[pos] = myMinStep - opponentMinSteps[pos]
	}

	return &Occupation{
		myMinSteps:       myMinSteps,
		opponentMinSteps: opponentMinSteps,
		occupationMap:    occupationMap,
	}
}

func calcMinSteps(tiles map[Position]*ExTile, opponent bool) map[Position]int {
	minSteps := make(map[Position]int, len(tiles))
	unused := make(map[Position]struct{}, len(tiles))

	isTarget := func(t *ExTile) bool {
		return t.IsMyArea()
	}
	if opponent {
		isTarget = func(t *ExTile) bool {
			return t.IsOpponentArea()
		}
	}

	// 占有中の領域を記録
	for pos, tile := range tiles {
		if isTarget(tile) {
			// すでに占有中の領域は 0
			minSteps[pos] = 0
		} else {
			minSteps[pos] = -1
			unused[pos] = struct{}{}
		}
	}

	// 全部使うまで繰り返す
	for len(unused) > 0 {
		for pos, steps := range minSteps {
			if steps >= 0 {
				continue
			}

			const maxStep = 10000000
			minStep := maxStep
			for _, neighborPos := range GetNeighborPositionsRaw(pos) {
				neighborTile, ok := tiles[neighborPos]
				if !ok {
					continue
				}

				if _, ok := unused[neighborPos]; !ok {
					if neighborTile.Robots() > 0 {
						// ロボットがいる占有中のタイルの隣は +1
						step := minSteps[neighborPos] + 1
						if step < minStep {
							minStep = step
						}
					} else if !neighborTile.HasRecycler() {
						// ロボットがいない占有中のタイルの隣は +2
						// ロボットがいない占有中でないタイルの隣は +1
						step := minSteps[neighborPos] + 1
						if minSteps[neighborPos] == 0 && neighborTile.Robots() == 0 {
							step++
						}
						if step < minStep {
							minStep = step
						}
					}
				}
			}
			if minStep < maxStep {
				// 最小手数を記録して使用済みにする
				minSteps[pos] = minStep
				delete(unused, pos)
			}
		}
	}

	return minSteps
}
