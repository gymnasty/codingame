package main

import (
	"bufio"
	"fmt"
	"os"
)

type Game struct {
	height    Y          // Y 軸のサイズ
	width     X          // X 軸のサイズ
	positions []Position // すべての位置
}

func NewGame(height, width int) *Game {
	positions := make([]Position, 0, height*width)
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			pos := NewPosition(x, y)
			positions = append(positions, pos)
		}
	}
	return &Game{
		height:    Y(height),
		width:     X(width),
		positions: positions,
	}
}

func (g *Game) Positions() []Position {
	return g.positions
}

// タイルの数を取得します。
func (g *Game) GetTileNum() int {
	return int(g.height) * int(g.width)
}

var turnNo int

type Turn struct {
	turn           int                // ターン数
	myMatter       int                // 自分の素材
	opponentMatter int                // 敵の素材
	tiles          map[Position]*Tile // タイルの情報
}

type InputReader struct {
	scanner *bufio.Scanner
	game    *Game
}

func NewInputReader() *InputReader {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, 1000000), 1000000)
	return &InputReader{
		scanner: scanner,
	}
}

func (r *InputReader) Read() (*Game, *Turn) {
	if r.game == nil {
		r.game = r.readGame()
	}
	turn := r.readTurn()
	return r.game, turn
}

func (r *InputReader) readGame() *Game {
	r.scanner.Scan()
	var width, height int
	fmt.Sscan(r.scanner.Text(), &width, &height)
	return NewGame(height, width)
}

func (r *InputReader) readTurn() *Turn {
	turnNo++

	// 素材
	myMatter, opponentMatter := r.readMatter()

	// タイル
	tiles := make(map[Position]*Tile, r.game.GetTileNum())
	for i := 0; i < int(r.game.height); i++ {
		for j := 0; j < int(r.game.width); j++ {
			pos := NewPosition(j, i)
			tiles[pos] = r.readTile()
		}
	}
	return &Turn{
		turn:           turnNo,
		myMatter:       myMatter,
		opponentMatter: opponentMatter,
		tiles:          tiles,
	}
}

func (r *InputReader) readMatter() (myMatter, opponentMatter int) {
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &myMatter, &opponentMatter)
	return myMatter, opponentMatter
}

func (r *InputReader) readTile() *Tile {
	tile := &Tile{}
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &tile.scrapAmount, &tile.owner, &tile.units, &tile.recycler, &tile.canBuild, &tile.canSpawn, &tile.inRangeOfRecycler)
	return tile
}
