package main

// ボード全体の情報
type Board struct {
	// ボードの幅
	width X
	// ボードの高さ
	height Y
	// タイル情報
	*ExTiles
	// 領域
	myArea       int
	opponentArea int
	// ロボット
	myRobots       int
	opponentRobots int
	// リサイクラー
	myRecycler       int
	opponentRecycler int
	// 資源
	myMatter       int
	opponentMatter int
}

func GetBoard(game *Game, turn *Turn) *Board {
	exTiles := GetExTiles(game, turn)
	var myArea, opponentArea, myRobots, opponentRobots, myRecycler, opponentRecycler int
	for _, tile := range exTiles {
		if tile.IsMyArea() {
			myArea++
			myRobots += tile.Robots()
			if tile.HasRecycler() {
				myRecycler += 1
			}
		}
		if tile.IsOpponentArea() {
			opponentArea++
			opponentRobots += tile.Robots()
			if tile.HasRecycler() {
				opponentRecycler += 1
			}
		}
	}
	return &Board{
		width:            X(game.width),
		height:           Y(game.height),
		ExTiles:          NewExTiles(exTiles),
		myArea:           myArea,
		opponentArea:     opponentArea,
		myRobots:         myRobots,
		opponentRobots:   opponentRobots,
		myRecycler:       myRecycler,
		opponentRecycler: opponentRecycler,
		myMatter:         turn.myMatter,
		opponentMatter:   turn.opponentMatter,
	}
}
