package main

import "sort"

// タイルの情報
type Tile struct {
	scrapAmount       int // スクラップの数
	owner             int // 1: 自陣 0: 敵陣 -1: それ以外
	units             int // ロボットの数
	recycler          int // リサイクラーがあるかどうか
	canBuild          int // 1: リサイクラー作成可能
	canSpawn          int // 1: ロボット作成可能
	inRangeOfRecycler int // 1: リサイクラーの範囲内
}

func (t Tile) Scraps() int {
	return t.scrapAmount
}

func (t Tile) Robots() int {
	return t.units
}

func (t Tile) HasRecycler() bool {
	return t.recycler == 1
}

func (t Tile) IsMyArea() bool {
	return t.owner == 1
}

func (t Tile) IsOpponentArea() bool {
	return t.owner == 0
}

func (t Tile) IsFreeArea() bool {
	return t.owner == -1
}

func (t Tile) CanAddRobot() bool {
	return t.canSpawn == 1
}

func (t Tile) CanAddRecycler() bool {
	return t.canBuild == 1
}

func (t Tile) InRangeOfRecycler() bool {
	return t.inRangeOfRecycler == 1
}

// 拡張タイル情報
type ExTile struct {
	// 元のタイル情報
	*Tile
	// 取得可能なスクラップの数
	AccessibleScraps int
	// 将来的に削除されるかどうか
	WillRemoved bool
	// どちらにも属さないタイルだけで構成されている
	FreePart bool
	// 敵のタイル以外のタイルだけで構成されている -> 自身の安全エリア
	MyPart bool
	// 自分のタイル以外のタイルだけで構成されている -> 敵の安全エリア
	OpponentPart bool
	// スコア
	Score *Score
}

// 拡張タイル情報を取得する
func GetExTiles(game *Game, turn *Turn) map[Position]*ExTile {
	exTiles := make(map[Position]*ExTile, len(turn.tiles))
	for pos, tile := range turn.tiles {
		score, accessibleScraps, willRemoved := getTileInfo(game, turn.tiles, pos)
		exTiles[pos] = &ExTile{
			Tile:             tile,
			AccessibleScraps: accessibleScraps,
			Score:            score,
			WillRemoved:      willRemoved,
		}
	}
	return exTiles
}

func getTileInfo(game *Game, tiles map[Position]*Tile, pos Position) (score *Score, accessibleScraps int, willRemoved bool) {
	score = NewScore()
	tile := tiles[pos]
	accessibleScraps = tile.Scraps()
	score.FindTile(tile)
	if tile.HasRecycler() {
		willRemoved = true
	}
	for _, p := range GetNeighborPositions(pos, game.width, game.height) {
		neighbor := tiles[p]
		if neighbor.HasRecycler() {
			score.FindNeighborRecycler(tile, neighbor)
			if neighbor.Scraps() > tile.Scraps() {
				willRemoved = true
			}
		}
		if neighbor.IsFreeArea() {
			score.FindNeighborFreeTile(tile, neighbor)
		}
		if neighbor.IsMyArea() {
			score.FindNeighborMyTile(tile, neighbor)
		}
		if neighbor.IsOpponentArea() {
			score.FindNeighborOpponentTile(tile, neighbor)
		}
		accessibleScraps += MaxInt(neighbor.Scraps(), tile.Scraps())
	}
	score.FindAccessibleScraps(accessibleScraps)
	return score, accessibleScraps, willRemoved
}

// 複数のタイルを扱う構造体
type ExTiles struct {
	exTiles map[Position]*ExTile
}

func NewExTiles(exTiles map[Position]*ExTile) *ExTiles {
	return &ExTiles{
		exTiles: exTiles,
	}
}

func (b *ExTiles) MyPositions() []Position {
	positions := make([]Position, 0, len(b.exTiles))
	for pos, tile := range b.exTiles {
		if tile.IsMyArea() {
			positions = append(positions, pos)
		}
	}
	return positions
}

func (b *ExTiles) OpponentPositions() []Position {
	positions := make([]Position, 0, len(b.exTiles))
	for pos, tile := range b.exTiles {
		if tile.IsOpponentArea() {
			positions = append(positions, pos)
		}
	}
	return positions
}

func (b *ExTiles) FreePositions() []Position {
	positions := make([]Position, 0, len(b.exTiles))
	for pos, tile := range b.exTiles {
		if tile.IsFreeArea() && tile.Scraps() > 0 {
			positions = append(positions, pos)
		}
	}
	return positions
}

func (b *ExTiles) MovablePositionMap() *PositionMap {
	positions := NewPositionMap()
	for pos, tile := range b.exTiles {
		if !tile.HasRecycler() && tile.Scraps() > 0 {
			positions.Add(pos)
		}
	}
	return positions
}

func (b *ExTiles) MaxRecycler() *Position {
	var maxPos *Position
	var maxScore = -100000000
	for pos, tile := range b.exTiles {
		posCopy := pos
		if tile.IsMyArea() && tile.CanAddRecycler() {
			if maxPos == nil || tile.Score.recycle > maxScore {
				maxPos = &posCopy
				maxScore = tile.Score.recycle
			}
		}
	}
	return maxPos
}

func (b *ExTiles) ObtainOrder() []Position {
	type tmp struct {
		pos   Position
		score int
	}
	tiles := make([]tmp, 0, len(b.exTiles))
	for p, t := range b.exTiles {
		tiles = append(tiles, tmp{p, t.Score.obtain})
	}

	sort.Slice(tiles, func(i, j int) bool { return tiles[i].score > tiles[j].score })

	positions := make([]Position, len(tiles))
	for i, t := range tiles {
		positions[i] = t.pos
	}
	return positions
}
