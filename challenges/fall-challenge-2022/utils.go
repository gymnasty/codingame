package main

import (
	"fmt"
	"math/rand"
	"os"
)

const DEBUG = true

func DebugLog(format string, a ...interface{}) {
	if !DEBUG {
		return
	}
	_, _ = fmt.Fprintf(os.Stderr, format, a...)
}

func DebugLogLine(format string, a ...interface{}) {
	if !DEBUG {
		return
	}
	DebugLog(format+"\n", a...)
}

func MinInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func MaxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func AbsInt(a int) int {
	if a > 0 {
		return a
	}
	return -a
}

// N 未満の整数をランダムに生成
func RandomInt(n int) int {
	return rand.Intn(n)
}
