package main

type Score struct {
	recycle int // リサイクル
	obtain  int // タイルの取得・維持
}

func NewScore() *Score {
	return &Score{}
}

func (s *Score) FindTile(tile *Tile) {
	if tile.IsOpponentArea() {
		s.obtain += 100
	}
	if tile.IsFreeArea() {
		s.obtain += 50
	}
}

func (s *Score) FindNeighborRecycler(tile, neighbor *Tile) {
	if tile.scrapAmount <= neighbor.scrapAmount {
		s.obtain -= 1000
	}
}

func (s *Score) FindAccessibleScraps(scraps int) {
	s.recycle += scraps
}

func (s *Score) FindNeighborFreeTile(tile, neighbor *Tile) {
	if tile.scrapAmount >= neighbor.scrapAmount {
		s.recycle -= 2
	}
}

func (s *Score) FindNeighborMyTile(tile, neighbor *Tile) {
	if tile.scrapAmount >= neighbor.scrapAmount && !neighbor.HasRecycler() {
		s.recycle -= 5
	}
}

func (s *Score) FindNeighborOpponentTile(tile, neighbor *Tile) {
	if tile.scrapAmount <= neighbor.scrapAmount && !neighbor.HasRecycler() {
		s.recycle += 5
	}
	s.obtain += 10
}
