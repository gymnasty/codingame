package main

func main() {
	// 入力読み込み用の reader を作成
	reader := NewInputReader()

	for {
		// 入力を読み込み
		game, turn := reader.Read()

		// 行動を決定
		actions := GetNextActions(game, turn)

		// 行動を実行
		actions.Do()
	}
}
