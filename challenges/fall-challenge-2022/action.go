package main

import (
	"fmt"
)

type ActionType string

const (
	ActionMove    ActionType = "MOVE"
	ActionBuild   ActionType = "BUILD"
	ActionSpawn   ActionType = "SPAWN"
	ActionWait    ActionType = "WAIT"
	ActionMessage ActionType = "MESSAGE"
)

type Actions struct {
	actions string
}

func NewActions() *Actions {
	return &Actions{}
}

// アクションを実行する
func (a *Actions) appendAction(action string) {
	if a.actions == "" {
		a.actions = action
	} else {
		a.actions += ";" + action
	}
}

// アクションを実行する
func (a *Actions) Do() {
	if a.actions == "" {
		fmt.Println(string(ActionWait))
	} else {
		fmt.Println(a.actions)
	}
}

// ロボットを移動させる
func (a *Actions) Move(amount int, from Position, to Position) {
	action := fmt.Sprintf("%s %d %d %d %d %d", ActionMove, amount, from.X, from.Y, to.X, to.Y)
	a.appendAction(action)
}

// リサイクラーを作成する
func (a *Actions) Build(pos Position) {
	action := fmt.Sprintf("%s %d %d", ActionBuild, pos.X, pos.Y)
	a.appendAction(action)
}

// ロボットを作成する
func (a *Actions) Spawn(amount int, pos Position) {
	action := fmt.Sprintf("%s %d %d %d", ActionSpawn, amount, pos.X, pos.Y)
	a.appendAction(action)
}

// メッセージを追加する
func (a *Actions) Message(msg string) {
	action := fmt.Sprintf("%s %s", ActionMessage, msg)
	a.appendAction(action)
}
