package main

import "sort"

// ボード上の独立した部分の情報
type Part struct {
	*ExTiles
	hasMyTile       bool
	hasOpponentTile bool
	hasFreeTile     bool
	myRobots        int
	opponentRobots  int
}

func NewPart() *Part {
	return &Part{
		ExTiles: NewExTiles(make(map[Position]*ExTile)),
	}
}

func (p *Part) Add(pos Position, exTile *ExTile) {
	p.ExTiles.exTiles[pos] = exTile
}

func GetParts(board *Board) []*Part {
	parts := make([]*Part, 0)
	unused := board.MovablePositionMap()

	for pos := unused.Select(); pos != nil; pos = unused.Select() {
		// 到達可能な位置を取得する
		positions := findPartPositions(unused, *pos)

		// 重心
		var wPos Position

		part := NewPart()
		for _, pos := range positions {
			exTile := board.exTiles[pos]
			part.Add(pos, exTile)

			// 重心計算用
			wPos.X += pos.X
			wPos.Y += pos.Y

			if exTile.IsMyArea() {
				part.hasMyTile = true
				part.myRobots += exTile.Robots()
			} else if exTile.IsOpponentArea() {
				part.hasOpponentTile = true
				part.opponentRobots += exTile.Robots()
			} else if exTile.IsFreeArea() {
				part.hasFreeTile = true
			}
		}

		// 重心
		wPos.X /= X(len(part.exTiles))
		wPos.Y /= Y(len(part.exTiles))

		// 部分ごとのスコアを処理
		for pos, tile := range part.exTiles {
			dist := AbsInt(int(pos.X-wPos.X)) + AbsInt(int(pos.Y-wPos.Y)) + 1
			tile.Score.obtain += 1000 / dist
		}

		parts = append(parts, part)
	}

	// 面積が広い順に並べ替え
	sort.Slice(parts, func(i, j int) bool { return len(parts[i].exTiles) > len(parts[j].exTiles) })

	return parts
}

func findPartPositions(unused *PositionMap, pos Position) []Position {
	positions := make([]Position, 0)

	// 使用済みの場合は追加せず
	if !unused.Has(pos) {
		return positions
	}

	// 現在位置を追加
	positions = append(positions, pos)
	unused.Delete(pos)

	// 隣の可能性がある位置を列挙
	neighbors := []Position{
		{X: pos.X - 1, Y: pos.Y},
		{X: pos.X, Y: pos.Y - 1},
		{X: pos.X + 1, Y: pos.Y},
		{X: pos.X, Y: pos.Y + 1},
	}
	for _, pos := range neighbors {
		if unused.Has(pos) {
			// そこからさらに移動可能な位置を追加
			positions = append(positions, findPartPositions(unused, pos)...)
		}
	}
	return positions
}
