package main

// 次の行動を決定する
func GetNextActions(game *Game, turn *Turn) *Actions {
	// ボード情報を取得
	board := GetBoard(game, turn)
	// 部分情報を取得
	parts := GetParts(board)
	// 占有情報を取得
	occupationTiles := 0
	occupation := NewOccupation()
	for _, part := range parts {
		if part.hasMyTile && part.hasOpponentTile {
			occupation.Extend(GetOccupation(part.exTiles))
			continue
		}
		if part.hasMyTile {
			occupationTiles += len(part.exTiles)
		}
		if part.hasOpponentTile {
			occupationTiles -= len(part.exTiles)
		}
	}
	occupationTiles += occupation.MyOccupationTiles()
	t := occupation.OccupationTargets(0)
	for _, pos := range t {
		DebugLogLine("(%d, %d)", pos.X, pos.Y)
	}

	// 行動を決定
	actions := NewActions()

	// 最初の数ターンは領域の獲得を最優先
	// TODO: 経路の最適化
	// 総移動距離が最小になるような組み合わせを選びたい
	// ロボットの作成位置も考えたい

	// 勝利確定
	if !isWin(parts) {
		// リサイクラーを作る
		addRecyclerAction(actions, turn, board, parts, occupation, occupationTiles)
	}

	// 敗北確定

	// ロボットを作る
	addRobotAction(actions, parts, occupation)

	// ロボットを移動させる
	addMoveActions(actions, turn, parts)

	return actions
}

func addRecyclerAction(actions *Actions, turn *Turn, board *Board, parts []*Part, occupation *Occupation, occupationTiles int) {
	// 勝っていて材料の差が少ない場合はリサイクラーを作らない
	//if turn.turn < 5 {
	//	return
	//}
	if occupationTiles >= 0 && AbsInt(board.myMatter-board.opponentMatter) < 10 {
		return
	}
	// 対立している領域でロボット数が負けている領域がある場合はロボットを作りたい
	needsToMakeRobot := false
	for _, part := range parts {
		if part.hasMyTile && part.hasOpponentTile && part.myRobots <= part.opponentRobots {
			needsToMakeRobot = true
		}
	}
	if !needsToMakeRobot {
		return
	}
	pos := board.MaxRecycler()
	if pos != nil {
		actions.Build(*pos)
	}
}

func addRobotAction(actions *Actions, parts []*Part, occupation *Occupation) {
	for _, part := range parts {
		// 敵のタイルが存在しない場合
		if !part.hasOpponentTile {
			// フリーのタイルも存在しない
			if !part.hasFreeTile {
				continue
			}
			// 自身のロボットが存在する
			if part.myRobots > 0 {
				continue
			}
		}

		// 不足しているロボットの数
		needsRobots := part.opponentRobots - part.myRobots + 1

		for pos, tile := range part.exTiles {
			if needsRobots <= 0 {
				continue
			}
			if tile.CanAddRobot() {
				var isEdge bool
				for _, p := range GetNeighborPositionsRaw(pos) {
					t, ok := part.exTiles[p]
					if ok {
						if t.IsFreeArea() && t.Scraps() > 0 || t.IsOpponentArea() && !t.HasRecycler() {
							isEdge = true
						}
					}
				}
				if isEdge {
					actions.Spawn(1, pos)
					needsRobots--
				}
			}
		}
	}

	// 余った時用
	for i := 0; i < 10; i++ {
		for _, part := range parts {
			// 敵のタイルが存在しない場合
			if !part.hasOpponentTile {
				// フリーのタイルも存在しない
				if !part.hasFreeTile {
					continue
				}
				// 自身のロボットが存在する
				if part.myRobots > 0 {
					continue
				}
			}

			for pos, tile := range part.exTiles {
				if tile.CanAddRobot() {
					var isEdge bool
					for _, p := range GetNeighborPositionsRaw(pos) {
						t, ok := part.exTiles[p]
						if ok {
							if t.IsFreeArea() && t.Scraps() > 0 || t.IsOpponentArea() && !t.HasRecycler() {
								isEdge = true
							}
						}
					}
					if isEdge {
						actions.Spawn(1, pos)
					}
				}
			}
		}
	}
}

func addMoveActions(actions *Actions, turn *Turn, parts []*Part) {
	for _, part := range parts {
		targets := make(map[Position]struct{}, len(part.exTiles))
		obtainablePositions := part.OpponentPositions()
		obtainablePositions = append(obtainablePositions, part.FreePositions()...)
		for pos, tile := range part.exTiles {
			if tile.IsMyArea() && tile.Robots() > 0 {
				moveRobots := 0

				// 周囲のタイルを取得
				neighbors := GetNeighborPositionsRaw(pos)
				neighborTiles := make(map[Position]*ExTile, 4)
				for _, neighbor := range neighbors {
					tile := part.ExTiles.exTiles[neighbor]
					if tile != nil {
						neighborTiles[neighbor] = tile
					}
				}
				tiles := NewExTiles(neighborTiles)

				for _, neighbor := range tiles.ObtainOrder() {
					if moveRobots >= tile.Robots() {
						continue
					}
					tile := part.ExTiles.exTiles[neighbor]
					if !tile.IsMyArea() && !tile.HasRecycler() && tile.Score.obtain > 0 {
						moveRobots++
						actions.Move(1, pos, neighbor)
					}
				}

				// その他の領域へ移動
				positions := make([]Position, 0, len(obtainablePositions))
				for _, p := range obtainablePositions {
					if _, ok := targets[p]; !ok {
						positions = append(positions, p)
					}
				}
				for i := moveRobots; i < tile.Robots(); i++ {
					if len(positions) > 0 {
						idx := RandomInt(len(positions))
						actions.Move(1, pos, positions[idx])
					}
				}
			}
		}
	}
}

// すでに勝っているかどうかを判定する
func isWin(parts []*Part) bool {
	var myArea, opponentArea int
	for _, part := range parts {
		if !part.hasOpponentTile && part.hasMyTile {
			for _, tile := range part.ExTiles.exTiles {
				if !tile.WillRemoved {
					myArea += 1
				}
			}
		}
		if part.hasOpponentTile && !part.hasMyTile {
			for _, tile := range part.ExTiles.exTiles {
				if !tile.WillRemoved {
					opponentArea += 1
				}
			}
		}
		if part.hasOpponentTile && part.hasMyTile {
			for _, tile := range part.ExTiles.exTiles {
				if !tile.WillRemoved {
					opponentArea += 1
				}
			}
		}
	}
	return myArea > opponentArea
}
