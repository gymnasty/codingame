package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"sort"
)

type ActionType string

const (
	ActionMove    ActionType = "MOVE"
	ActionBuild   ActionType = "BUILD"
	ActionSpawn   ActionType = "SPAWN"
	ActionWait    ActionType = "WAIT"
	ActionMessage ActionType = "MESSAGE"
)

type Actions struct {
	actions string
}

func NewActions() *Actions {
	return &Actions{}
}

// アクションを実行する
func (a *Actions) appendAction(action string) {
	if a.actions == "" {
		a.actions = action
	} else {
		a.actions += ";" + action
	}
}

// アクションを実行する
func (a *Actions) Do() {
	if a.actions == "" {
		fmt.Println(string(ActionWait))
	} else {
		fmt.Println(a.actions)
	}
}

// ロボットを移動させる
func (a *Actions) Move(amount int, from Position, to Position) {
	action := fmt.Sprintf("%s %d %d %d %d %d", ActionMove, amount, from.X, from.Y, to.X, to.Y)
	a.appendAction(action)
}

// リサイクラーを作成する
func (a *Actions) Build(pos Position) {
	action := fmt.Sprintf("%s %d %d", ActionBuild, pos.X, pos.Y)
	a.appendAction(action)
}

// ロボットを作成する
func (a *Actions) Spawn(amount int, pos Position) {
	action := fmt.Sprintf("%s %d %d %d", ActionSpawn, amount, pos.X, pos.Y)
	a.appendAction(action)
}

// メッセージを追加する
func (a *Actions) Message(msg string) {
	action := fmt.Sprintf("%s %s", ActionMessage, msg)
	a.appendAction(action)
}

// ボード全体の情報
type Board struct {
	// ボードの幅
	width X
	// ボードの高さ
	height Y
	// タイル情報
	*ExTiles
	// 領域
	myArea       int
	opponentArea int
	// ロボット
	myRobots       int
	opponentRobots int
	// リサイクラー
	myRecycler       int
	opponentRecycler int
	// 資源
	myMatter       int
	opponentMatter int
}

func GetBoard(game *Game, turn *Turn) *Board {
	exTiles := GetExTiles(game, turn)
	var myArea, opponentArea, myRobots, opponentRobots, myRecycler, opponentRecycler int
	for _, tile := range exTiles {
		if tile.IsMyArea() {
			myArea++
			myRobots += tile.Robots()
			if tile.HasRecycler() {
				myRecycler += 1
			}
		}
		if tile.IsOpponentArea() {
			opponentArea++
			opponentRobots += tile.Robots()
			if tile.HasRecycler() {
				opponentRecycler += 1
			}
		}
	}
	return &Board{
		width:            X(game.width),
		height:           Y(game.height),
		ExTiles:          NewExTiles(exTiles),
		myArea:           myArea,
		opponentArea:     opponentArea,
		myRobots:         myRobots,
		opponentRobots:   opponentRobots,
		myRecycler:       myRecycler,
		opponentRecycler: opponentRecycler,
		myMatter:         turn.myMatter,
		opponentMatter:   turn.opponentMatter,
	}
}

type Game struct {
	height    Y          // Y 軸のサイズ
	width     X          // X 軸のサイズ
	positions []Position // すべての位置
}

func NewGame(height, width int) *Game {
	positions := make([]Position, 0, height*width)
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			pos := NewPosition(x, y)
			positions = append(positions, pos)
		}
	}
	return &Game{
		height:    Y(height),
		width:     X(width),
		positions: positions,
	}
}

func (g *Game) Positions() []Position {
	return g.positions
}

// タイルの数を取得します。
func (g *Game) GetTileNum() int {
	return int(g.height) * int(g.width)
}

var turnNo int

type Turn struct {
	turn           int                // ターン数
	myMatter       int                // 自分の素材
	opponentMatter int                // 敵の素材
	tiles          map[Position]*Tile // タイルの情報
}

type InputReader struct {
	scanner *bufio.Scanner
	game    *Game
}

func NewInputReader() *InputReader {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, 1000000), 1000000)
	return &InputReader{
		scanner: scanner,
	}
}

func (r *InputReader) Read() (*Game, *Turn) {
	if r.game == nil {
		r.game = r.readGame()
	}
	turn := r.readTurn()
	return r.game, turn
}

func (r *InputReader) readGame() *Game {
	r.scanner.Scan()
	var width, height int
	fmt.Sscan(r.scanner.Text(), &width, &height)
	return NewGame(height, width)
}

func (r *InputReader) readTurn() *Turn {
	turnNo++

	// 素材
	myMatter, opponentMatter := r.readMatter()

	// タイル
	tiles := make(map[Position]*Tile, r.game.GetTileNum())
	for i := 0; i < int(r.game.height); i++ {
		for j := 0; j < int(r.game.width); j++ {
			pos := NewPosition(j, i)
			tiles[pos] = r.readTile()
		}
	}
	return &Turn{
		turn:           turnNo,
		myMatter:       myMatter,
		opponentMatter: opponentMatter,
		tiles:          tiles,
	}
}

func (r *InputReader) readMatter() (myMatter, opponentMatter int) {
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &myMatter, &opponentMatter)
	return myMatter, opponentMatter
}

func (r *InputReader) readTile() *Tile {
	tile := &Tile{}
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &tile.scrapAmount, &tile.owner, &tile.units, &tile.recycler, &tile.canBuild, &tile.canSpawn, &tile.inRangeOfRecycler)
	return tile
}

// 次の行動を決定する
func GetNextActions(game *Game, turn *Turn) *Actions {
	// ボード情報を取得
	board := GetBoard(game, turn)
	// 部分情報を取得
	parts := GetParts(board)
	// 占有情報を取得
	occupationTiles := 0
	occupation := NewOccupation()
	for _, part := range parts {
		if part.hasMyTile && part.hasOpponentTile {
			occupation.Extend(GetOccupation(part.exTiles))
			continue
		}
		if part.hasMyTile {
			occupationTiles += len(part.exTiles)
		}
		if part.hasOpponentTile {
			occupationTiles -= len(part.exTiles)
		}
	}
	occupationTiles += occupation.MyOccupationTiles()
	t := occupation.OccupationTargets(0)
	for _, pos := range t {
		DebugLogLine("(%d, %d)", pos.X, pos.Y)
	}

	// 行動を決定
	actions := NewActions()

	// 最初の数ターンは領域の獲得を最優先
	// TODO: 経路の最適化
	// 総移動距離が最小になるような組み合わせを選びたい
	// ロボットの作成位置も考えたい

	// 勝利確定
	if !isWin(parts) {
		// リサイクラーを作る
		addRecyclerAction(actions, turn, board, parts, occupation, occupationTiles)
	}

	// 敗北確定

	// ロボットを作る
	addRobotAction(actions, parts, occupation)

	// ロボットを移動させる
	addMoveActions(actions, turn, parts)

	return actions
}

func addRecyclerAction(actions *Actions, turn *Turn, board *Board, parts []*Part, occupation *Occupation, occupationTiles int) {
	// 勝っていて材料の差が少ない場合はリサイクラーを作らない
	//if turn.turn < 5 {
	//	return
	//}
	if occupationTiles >= 0 && AbsInt(board.myMatter-board.opponentMatter) < 10 {
		return
	}
	// 対立している領域でロボット数が負けている領域がある場合はロボットを作りたい
	needsToMakeRobot := false
	for _, part := range parts {
		if part.hasMyTile && part.hasOpponentTile && part.myRobots <= part.opponentRobots {
			needsToMakeRobot = true
		}
	}
	if !needsToMakeRobot {
		return
	}
	pos := board.MaxRecycler()
	if pos != nil {
		actions.Build(*pos)
	}
}

func addRobotAction(actions *Actions, parts []*Part, occupation *Occupation) {
	for _, part := range parts {
		// 敵のタイルが存在しない場合
		if !part.hasOpponentTile {
			// フリーのタイルも存在しない
			if !part.hasFreeTile {
				continue
			}
			// 自身のロボットが存在する
			if part.myRobots > 0 {
				continue
			}
		}

		// 不足しているロボットの数
		needsRobots := part.opponentRobots - part.myRobots + 1

		for pos, tile := range part.exTiles {
			if needsRobots <= 0 {
				continue
			}
			if tile.CanAddRobot() {
				var isEdge bool
				for _, p := range GetNeighborPositionsRaw(pos) {
					t, ok := part.exTiles[p]
					if ok {
						if t.IsFreeArea() && t.Scraps() > 0 || t.IsOpponentArea() && !t.HasRecycler() {
							isEdge = true
						}
					}
				}
				if isEdge {
					actions.Spawn(1, pos)
					needsRobots--
				}
			}
		}
	}

	// 余った時用
	for i := 0; i < 10; i++ {
		for _, part := range parts {
			// 敵のタイルが存在しない場合
			if !part.hasOpponentTile {
				// フリーのタイルも存在しない
				if !part.hasFreeTile {
					continue
				}
				// 自身のロボットが存在する
				if part.myRobots > 0 {
					continue
				}
			}

			for pos, tile := range part.exTiles {
				if tile.CanAddRobot() {
					var isEdge bool
					for _, p := range GetNeighborPositionsRaw(pos) {
						t, ok := part.exTiles[p]
						if ok {
							if t.IsFreeArea() && t.Scraps() > 0 || t.IsOpponentArea() && !t.HasRecycler() {
								isEdge = true
							}
						}
					}
					if isEdge {
						actions.Spawn(1, pos)
					}
				}
			}
		}
	}
}

func addMoveActions(actions *Actions, turn *Turn, parts []*Part) {
	for _, part := range parts {
		targets := make(map[Position]struct{}, len(part.exTiles))
		obtainablePositions := part.OpponentPositions()
		obtainablePositions = append(obtainablePositions, part.FreePositions()...)
		for pos, tile := range part.exTiles {
			if tile.IsMyArea() && tile.Robots() > 0 {
				moveRobots := 0

				// 周囲のタイルを取得
				neighbors := GetNeighborPositionsRaw(pos)
				neighborTiles := make(map[Position]*ExTile, 4)
				for _, neighbor := range neighbors {
					tile := part.ExTiles.exTiles[neighbor]
					if tile != nil {
						neighborTiles[neighbor] = tile
					}
				}
				tiles := NewExTiles(neighborTiles)

				for _, neighbor := range tiles.ObtainOrder() {
					if moveRobots >= tile.Robots() {
						continue
					}
					tile := part.ExTiles.exTiles[neighbor]
					if !tile.IsMyArea() && !tile.HasRecycler() && tile.Score.obtain > 0 {
						moveRobots++
						actions.Move(1, pos, neighbor)
					}
				}

				// その他の領域へ移動
				positions := make([]Position, 0, len(obtainablePositions))
				for _, p := range obtainablePositions {
					if _, ok := targets[p]; !ok {
						positions = append(positions, p)
					}
				}
				for i := moveRobots; i < tile.Robots(); i++ {
					if len(positions) > 0 {
						idx := RandomInt(len(positions))
						actions.Move(1, pos, positions[idx])
					}
				}
			}
		}
	}
}

// すでに勝っているかどうかを判定する
func isWin(parts []*Part) bool {
	var myArea, opponentArea int
	for _, part := range parts {
		if !part.hasOpponentTile && part.hasMyTile {
			for _, tile := range part.ExTiles.exTiles {
				if !tile.WillRemoved {
					myArea += 1
				}
			}
		}
		if part.hasOpponentTile && !part.hasMyTile {
			for _, tile := range part.ExTiles.exTiles {
				if !tile.WillRemoved {
					opponentArea += 1
				}
			}
		}
		if part.hasOpponentTile && part.hasMyTile {
			for _, tile := range part.ExTiles.exTiles {
				if !tile.WillRemoved {
					opponentArea += 1
				}
			}
		}
	}
	return myArea > opponentArea
}

func main() {
	// 入力読み込み用の reader を作成
	reader := NewInputReader()

	for {
		// 入力を読み込み
		game, turn := reader.Read()

		// 行動を決定
		actions := GetNextActions(game, turn)

		// 行動を実行
		actions.Do()
	}
}

// 占有情報
type Occupation struct {
	myMinSteps       map[Position]int
	opponentMinSteps map[Position]int
	occupationMap    map[Position]int // 正の数なら相手の領域、負の数なら自分の領域
}

// TODO: 経路情報も含める？
// 各ステップでどこから来たかを記録しておいて、移動後の occupation map のトータルが良いものを採用する？
func NewOccupation() *Occupation {
	return &Occupation{
		myMinSteps:       make(map[Position]int),
		opponentMinSteps: make(map[Position]int),
		occupationMap:    make(map[Position]int),
	}
}

func (o *Occupation) Extend(occupation *Occupation) {
	for pos, step := range occupation.myMinSteps {
		o.myMinSteps[pos] = step
	}
	for pos, step := range occupation.opponentMinSteps {
		o.opponentMinSteps[pos] = step
	}
	for pos, step := range occupation.occupationMap {
		o.occupationMap[pos] = step
	}
}

// 占有しているタイルの差を取得
// 正の数なら有利、負の数なら不利
func (o *Occupation) MyOccupationTiles() int {
	occupations := 0
	for _, occupation := range o.occupationMap {
		if occupation > 0 {
			occupations--
		} else if occupation < 0 {
			occupations++
		}
	}
	return occupations
}

// 占有の対象を取得
func (o *Occupation) OccupationTargets(step int) []Position {
	positions := make([]Position, 0, len(o.occupationMap))
	for pos, s := range o.occupationMap {
		if s == step {
			positions = append(positions, pos)
		}
	}
	return positions
}

func GetOccupation(tiles map[Position]*ExTile) *Occupation {
	// 最小ステップ数を作成
	myMinSteps := calcMinSteps(tiles, false)
	opponentMinSteps := calcMinSteps(tiles, true)

	// 最小ステップ数の差を作成
	// 正の数なら相手の領域、負の数なら自分の領域
	occupationMap := make(map[Position]int, len(tiles))
	for pos, myMinStep := range myMinSteps {
		occupationMap[pos] = myMinStep - opponentMinSteps[pos]
	}

	return &Occupation{
		myMinSteps:       myMinSteps,
		opponentMinSteps: opponentMinSteps,
		occupationMap:    occupationMap,
	}
}

func calcMinSteps(tiles map[Position]*ExTile, opponent bool) map[Position]int {
	minSteps := make(map[Position]int, len(tiles))
	unused := make(map[Position]struct{}, len(tiles))

	isTarget := func(t *ExTile) bool {
		return t.IsMyArea()
	}
	if opponent {
		isTarget = func(t *ExTile) bool {
			return t.IsOpponentArea()
		}
	}

	// 占有中の領域を記録
	for pos, tile := range tiles {
		if isTarget(tile) {
			// すでに占有中の領域は 0
			minSteps[pos] = 0
		} else {
			minSteps[pos] = -1
			unused[pos] = struct{}{}
		}
	}

	// 全部使うまで繰り返す
	for len(unused) > 0 {
		for pos, steps := range minSteps {
			if steps >= 0 {
				continue
			}

			const maxStep = 10000000
			minStep := maxStep
			for _, neighborPos := range GetNeighborPositionsRaw(pos) {
				neighborTile, ok := tiles[neighborPos]
				if !ok {
					continue
				}

				if _, ok := unused[neighborPos]; !ok {
					if neighborTile.Robots() > 0 {
						// ロボットがいる占有中のタイルの隣は +1
						step := minSteps[neighborPos] + 1
						if step < minStep {
							minStep = step
						}
					} else if !neighborTile.HasRecycler() {
						// ロボットがいない占有中のタイルの隣は +2
						// ロボットがいない占有中でないタイルの隣は +1
						step := minSteps[neighborPos] + 1
						if minSteps[neighborPos] == 0 && neighborTile.Robots() == 0 {
							step++
						}
						if step < minStep {
							minStep = step
						}
					}
				}
			}
			if minStep < maxStep {
				// 最小手数を記録して使用済みにする
				minSteps[pos] = minStep
				delete(unused, pos)
			}
		}
	}

	return minSteps
}

// ボード上の独立した部分の情報
type Part struct {
	*ExTiles
	hasMyTile       bool
	hasOpponentTile bool
	hasFreeTile     bool
	myRobots        int
	opponentRobots  int
}

func NewPart() *Part {
	return &Part{
		ExTiles: NewExTiles(make(map[Position]*ExTile)),
	}
}

func (p *Part) Add(pos Position, exTile *ExTile) {
	p.ExTiles.exTiles[pos] = exTile
}

func GetParts(board *Board) []*Part {
	parts := make([]*Part, 0)
	unused := board.MovablePositionMap()

	for pos := unused.Select(); pos != nil; pos = unused.Select() {
		// 到達可能な位置を取得する
		positions := findPartPositions(unused, *pos)

		// 重心
		var wPos Position

		part := NewPart()
		for _, pos := range positions {
			exTile := board.exTiles[pos]
			part.Add(pos, exTile)

			// 重心計算用
			wPos.X += pos.X
			wPos.Y += pos.Y

			if exTile.IsMyArea() {
				part.hasMyTile = true
				part.myRobots += exTile.Robots()
			} else if exTile.IsOpponentArea() {
				part.hasOpponentTile = true
				part.opponentRobots += exTile.Robots()
			} else if exTile.IsFreeArea() {
				part.hasFreeTile = true
			}
		}

		// 重心
		wPos.X /= X(len(part.exTiles))
		wPos.Y /= Y(len(part.exTiles))

		// 部分ごとのスコアを処理
		for pos, tile := range part.exTiles {
			dist := AbsInt(int(pos.X-wPos.X)) + AbsInt(int(pos.Y-wPos.Y)) + 1
			tile.Score.obtain += 1000 / dist
		}

		parts = append(parts, part)
	}

	// 面積が広い順に並べ替え
	sort.Slice(parts, func(i, j int) bool { return len(parts[i].exTiles) > len(parts[j].exTiles) })

	return parts
}

func findPartPositions(unused *PositionMap, pos Position) []Position {
	positions := make([]Position, 0)

	// 使用済みの場合は追加せず
	if !unused.Has(pos) {
		return positions
	}

	// 現在位置を追加
	positions = append(positions, pos)
	unused.Delete(pos)

	// 隣の可能性がある位置を列挙
	neighbors := []Position{
		{X: pos.X - 1, Y: pos.Y},
		{X: pos.X, Y: pos.Y - 1},
		{X: pos.X + 1, Y: pos.Y},
		{X: pos.X, Y: pos.Y + 1},
	}
	for _, pos := range neighbors {
		if unused.Has(pos) {
			// そこからさらに移動可能な位置を追加
			positions = append(positions, findPartPositions(unused, pos)...)
		}
	}
	return positions
}

// X 軸
type X int

// Y 軸
type Y int

// 位置
type Position struct {
	X X
	Y Y
}

// Position を作成
func NewPosition(x, y int) Position {
	return Position{X: X(x), Y: Y(y)}
}

// 隣接する Position を取得する
func GetNeighborPositions(pos Position, width X, height Y) []Position {
	positions := make([]Position, 4)
	if pos.X > 0 {
		positions = append(positions, Position{X: pos.X - 1, Y: pos.Y})
	}
	if pos.Y > 0 {
		positions = append(positions, Position{X: pos.X, Y: pos.Y - 1})
	}
	if pos.X < width-1 {
		positions = append(positions, Position{X: pos.X + 1, Y: pos.Y})
	}
	if pos.Y < height-1 {
		positions = append(positions, Position{X: pos.X, Y: pos.Y + 1})
	}
	return positions
}

// 隣接する Position を取得する
func GetNeighborPositionsRaw(pos Position) []Position {
	return []Position{
		{X: pos.X - 1, Y: pos.Y}, {X: pos.X, Y: pos.Y - 1}, {X: pos.X + 1, Y: pos.Y}, {X: pos.X, Y: pos.Y + 1},
	}
}

// 位置のマップ
type PositionMap struct {
	positions map[Position]struct{}
}

func NewPositionMap() *PositionMap {
	return &PositionMap{
		positions: make(map[Position]struct{}),
	}
}

func (p *PositionMap) Add(pos Position) {
	p.positions[pos] = struct{}{}
}

func (p *PositionMap) Delete(pos Position) {
	delete(p.positions, pos)
}

func (p *PositionMap) Select() *Position {
	for pos := range p.positions {
		return &pos
	}
	return nil
}

func (p *PositionMap) Has(pos Position) bool {
	_, ok := p.positions[pos]
	return ok
}

type Score struct {
	recycle int // リサイクル
	obtain  int // タイルの取得・維持
}

func NewScore() *Score {
	return &Score{}
}

func (s *Score) FindTile(tile *Tile) {
	if tile.IsOpponentArea() {
		s.obtain += 100
	}
	if tile.IsFreeArea() {
		s.obtain += 50
	}
}

func (s *Score) FindNeighborRecycler(tile, neighbor *Tile) {
	if tile.scrapAmount <= neighbor.scrapAmount {
		s.obtain -= 1000
	}
}

func (s *Score) FindAccessibleScraps(scraps int) {
	s.recycle += scraps
}

func (s *Score) FindNeighborFreeTile(tile, neighbor *Tile) {
	if tile.scrapAmount >= neighbor.scrapAmount {
		s.recycle -= 2
	}
}

func (s *Score) FindNeighborMyTile(tile, neighbor *Tile) {
	if tile.scrapAmount >= neighbor.scrapAmount && !neighbor.HasRecycler() {
		s.recycle -= 5
	}
}

func (s *Score) FindNeighborOpponentTile(tile, neighbor *Tile) {
	if tile.scrapAmount <= neighbor.scrapAmount && !neighbor.HasRecycler() {
		s.recycle += 5
	}
	s.obtain += 10
}

// タイルの情報
type Tile struct {
	scrapAmount       int // スクラップの数
	owner             int // 1: 自陣 0: 敵陣 -1: それ以外
	units             int // ロボットの数
	recycler          int // リサイクラーがあるかどうか
	canBuild          int // 1: リサイクラー作成可能
	canSpawn          int // 1: ロボット作成可能
	inRangeOfRecycler int // 1: リサイクラーの範囲内
}

func (t Tile) Scraps() int {
	return t.scrapAmount
}

func (t Tile) Robots() int {
	return t.units
}

func (t Tile) HasRecycler() bool {
	return t.recycler == 1
}

func (t Tile) IsMyArea() bool {
	return t.owner == 1
}

func (t Tile) IsOpponentArea() bool {
	return t.owner == 0
}

func (t Tile) IsFreeArea() bool {
	return t.owner == -1
}

func (t Tile) CanAddRobot() bool {
	return t.canSpawn == 1
}

func (t Tile) CanAddRecycler() bool {
	return t.canBuild == 1
}

func (t Tile) InRangeOfRecycler() bool {
	return t.inRangeOfRecycler == 1
}

// 拡張タイル情報
type ExTile struct {
	// 元のタイル情報
	*Tile
	// 取得可能なスクラップの数
	AccessibleScraps int
	// 将来的に削除されるかどうか
	WillRemoved bool
	// どちらにも属さないタイルだけで構成されている
	FreePart bool
	// 敵のタイル以外のタイルだけで構成されている -> 自身の安全エリア
	MyPart bool
	// 自分のタイル以外のタイルだけで構成されている -> 敵の安全エリア
	OpponentPart bool
	// スコア
	Score *Score
}

// 拡張タイル情報を取得する
func GetExTiles(game *Game, turn *Turn) map[Position]*ExTile {
	exTiles := make(map[Position]*ExTile, len(turn.tiles))
	for pos, tile := range turn.tiles {
		score, accessibleScraps, willRemoved := getTileInfo(game, turn.tiles, pos)
		exTiles[pos] = &ExTile{
			Tile:             tile,
			AccessibleScraps: accessibleScraps,
			Score:            score,
			WillRemoved:      willRemoved,
		}
	}
	return exTiles
}

func getTileInfo(game *Game, tiles map[Position]*Tile, pos Position) (score *Score, accessibleScraps int, willRemoved bool) {
	score = NewScore()
	tile := tiles[pos]
	accessibleScraps = tile.Scraps()
	score.FindTile(tile)
	if tile.HasRecycler() {
		willRemoved = true
	}
	for _, p := range GetNeighborPositions(pos, game.width, game.height) {
		neighbor := tiles[p]
		if neighbor.HasRecycler() {
			score.FindNeighborRecycler(tile, neighbor)
			if neighbor.Scraps() > tile.Scraps() {
				willRemoved = true
			}
		}
		if neighbor.IsFreeArea() {
			score.FindNeighborFreeTile(tile, neighbor)
		}
		if neighbor.IsMyArea() {
			score.FindNeighborMyTile(tile, neighbor)
		}
		if neighbor.IsOpponentArea() {
			score.FindNeighborOpponentTile(tile, neighbor)
		}
		accessibleScraps += MaxInt(neighbor.Scraps(), tile.Scraps())
	}
	score.FindAccessibleScraps(accessibleScraps)
	return score, accessibleScraps, willRemoved
}

// 複数のタイルを扱う構造体
type ExTiles struct {
	exTiles map[Position]*ExTile
}

func NewExTiles(exTiles map[Position]*ExTile) *ExTiles {
	return &ExTiles{
		exTiles: exTiles,
	}
}

func (b *ExTiles) MyPositions() []Position {
	positions := make([]Position, 0, len(b.exTiles))
	for pos, tile := range b.exTiles {
		if tile.IsMyArea() {
			positions = append(positions, pos)
		}
	}
	return positions
}

func (b *ExTiles) OpponentPositions() []Position {
	positions := make([]Position, 0, len(b.exTiles))
	for pos, tile := range b.exTiles {
		if tile.IsOpponentArea() {
			positions = append(positions, pos)
		}
	}
	return positions
}

func (b *ExTiles) FreePositions() []Position {
	positions := make([]Position, 0, len(b.exTiles))
	for pos, tile := range b.exTiles {
		if tile.IsFreeArea() && tile.Scraps() > 0 {
			positions = append(positions, pos)
		}
	}
	return positions
}

func (b *ExTiles) MovablePositionMap() *PositionMap {
	positions := NewPositionMap()
	for pos, tile := range b.exTiles {
		if !tile.HasRecycler() && tile.Scraps() > 0 {
			positions.Add(pos)
		}
	}
	return positions
}

func (b *ExTiles) MaxRecycler() *Position {
	var maxPos *Position
	var maxScore = -100000000
	for pos, tile := range b.exTiles {
		posCopy := pos
		if tile.IsMyArea() && tile.CanAddRecycler() {
			if maxPos == nil || tile.Score.recycle > maxScore {
				maxPos = &posCopy
				maxScore = tile.Score.recycle
			}
		}
	}
	return maxPos
}

func (b *ExTiles) ObtainOrder() []Position {
	type tmp struct {
		pos   Position
		score int
	}
	tiles := make([]tmp, 0, len(b.exTiles))
	for p, t := range b.exTiles {
		tiles = append(tiles, tmp{p, t.Score.obtain})
	}

	sort.Slice(tiles, func(i, j int) bool { return tiles[i].score > tiles[j].score })

	positions := make([]Position, len(tiles))
	for i, t := range tiles {
		positions[i] = t.pos
	}
	return positions
}

const DEBUG = true

func DebugLog(format string, a ...interface{}) {
	if !DEBUG {
		return
	}
	_, _ = fmt.Fprintf(os.Stderr, format, a...)
}

func DebugLogLine(format string, a ...interface{}) {
	if !DEBUG {
		return
	}
	DebugLog(format+"\n", a...)
}

func MinInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func MaxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func AbsInt(a int) int {
	if a > 0 {
		return a
	}
	return -a
}

// N 未満の整数をランダムに生成
func RandomInt(n int) int {
	return rand.Intn(n)
}
