package main

// X 軸
type X int

// Y 軸
type Y int

// 位置
type Position struct {
	X X
	Y Y
}

// Position を作成
func NewPosition(x, y int) Position {
	return Position{X: X(x), Y: Y(y)}
}

// 隣接する Position を取得する
func GetNeighborPositions(pos Position, width X, height Y) []Position {
	positions := make([]Position, 4)
	if pos.X > 0 {
		positions = append(positions, Position{X: pos.X - 1, Y: pos.Y})
	}
	if pos.Y > 0 {
		positions = append(positions, Position{X: pos.X, Y: pos.Y - 1})
	}
	if pos.X < width-1 {
		positions = append(positions, Position{X: pos.X + 1, Y: pos.Y})
	}
	if pos.Y < height-1 {
		positions = append(positions, Position{X: pos.X, Y: pos.Y + 1})
	}
	return positions
}

// 隣接する Position を取得する
func GetNeighborPositionsRaw(pos Position) []Position {
	return []Position{
		{X: pos.X - 1, Y: pos.Y}, {X: pos.X, Y: pos.Y - 1}, {X: pos.X + 1, Y: pos.Y}, {X: pos.X, Y: pos.Y + 1},
	}
}

// 位置のマップ
type PositionMap struct {
	positions map[Position]struct{}
}

func NewPositionMap() *PositionMap {
	return &PositionMap{
		positions: make(map[Position]struct{}),
	}
}

func (p *PositionMap) Add(pos Position) {
	p.positions[pos] = struct{}{}
}

func (p *PositionMap) Delete(pos Position) {
	delete(p.positions, pos)
}

func (p *PositionMap) Select() *Position {
	for pos := range p.positions {
		return &pos
	}
	return nil
}

func (p *PositionMap) Has(pos Position) bool {
	_, ok := p.positions[pos]
	return ok
}
