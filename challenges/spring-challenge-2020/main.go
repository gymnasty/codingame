package main

import (
	"fmt"
	"runtime"
)

/**
 * Grab the pellets as fast as you can!
 **/

func main() {
	// capture exception
	defer func() {
		if err := recover(); err != nil {
			debugLogLine("[ERROR] %s\n", err)
			for depth := 0; ; depth++ {
				_, file, line, ok := runtime.Caller(depth)
				if !ok {
					break
				}
				debugLogLine("======> %d: %v:%d", depth, file, line)
			}
		}
	}()

	// read game input
	inputReader := newInputReader()
	gameInput := inputReader.readInitialInput()
	gameInput.Show()

	// grid
	grid := gameInput.grid
	// step map
	stepMap := newStepMap(grid)

	// read turn input and write output
	turn := 0
	processTurn := func() {
		// increment turn
		defer func() {
			turn++
		}()

		// input
		turnInput := inputReader.readTurnInput(turn)
		turnInput.Show()

		// 最初のターンのみ
		if turn == 0 {
			for _, pac := range turnInput.myPacs {
				// パックの位置のスコアを 0 にする
				grid.GetCell(pac.position).SetScore(0)
				// 敵のパックの位置のスコアを 0 にする
				p := pac.position
				p.x = grid.width - 1 - p.x
				grid.GetCell(p).SetScore(0)
			}
		}

		// update grid
		// 見える位置のスコアを一旦0にする
		for _, pac := range turnInput.myPacs {
			for _, c := range grid.AllStraightCells(pac.position) {
				c.SetScore(0)
			}
		}
		// スーパーペレットのスコアを一旦0にする
		for _, c := range grid.AllFloorCells() {
			if c.score == 10 {
				c.SetScore(0)
			}
		}
		grid.UpdateScore(turnInput.pellets)
		grid.Show()

		// score
		score := newScore(grid, stepMap)
		score.Show(grid)

		// command
		command := newCommand()

		// スーパーペレットが残っている場合

		floorCells := grid.AllFloorCells()
		for _, pac := range turnInput.myPacs {
			// 移動先のセルの候補
			steps := 1
			if pac.speedTurnsLeft > 0 {
				steps = 2
			}
			cells := stepMap.PositionsIn(pac.position, steps)

			// 敵の位置
			for _, enemy := range turnInput.enemyPacs {
				if enemy.IsDead() {
					continue
				}
				d := stepMap.Dist(pac.position, enemy.position)
				s := 1
				if enemy.speedTurnsLeft > 0 {
					s = 2
				}
				if d <= s {
					// 1ターンで自分の位置に到達可能
				}
			}

			// スキル
			if pac.abilityCoolDown == 0 {
				command.Speed(pac.id, "")
				continue
			}

			var maxScore float64
			var maxPos position
			var cellIndex int
			for i, c := range floorCells {
				if c.position == pac.position {
					continue
				}
				if pac.speedTurnsLeft > 0 && stepMap.Dist(pac.position, c.position) == 1 {
					continue
				}

				s := score.GetScore(c.position) / float64(stepMap.Dist(pac.position, c.position))
				if maxScore < s {
					maxPos = c.position
					maxScore = s
					cellIndex = i
				}
			}
			command.Move(pac.id, maxPos.x, maxPos.y, fmt.Sprintf("%d,%d (%f)", maxPos.x, maxPos.y, maxScore))

			// 移動対象のセルは削除してみる
			if len(floorCells) > 0 {
				floorCells = append(floorCells[0:cellIndex], floorCells[cellIndex+1:]...)
			}
		}

		if len(command.actions) == 0 {
			p := gameInput.grid.AllFloorCells()[0]
			command.Move(turnInput.myPacs[0].id, p.x, p.y, "avoid timeout")
		}
		command.Do()
	}

	// each turn
	for {
		processTurn()
	}
}
