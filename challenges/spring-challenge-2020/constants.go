package main

import "time"

const (
	DEBUG = true

	maxTurn = 200

	minPac = 2
	maxPac = 5

	mixWidth  = 29
	maxWidth  = 35
	minHeight = 10
	maxHeight = 17

	responseTimePerTurn         = 50 * time.Millisecond
	responseTimeForTheFirstTurn = 1000 * time.Millisecond
)
