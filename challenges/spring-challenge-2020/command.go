package main

import (
	"fmt"
	"strings"
)

const (
	// action
	actionMoveCmd   = "MOVE"
	actionSpeedCmd  = "SPEED"
	actionSwitchCmd = "SWITCH"
	// pack type
	packTypeRock     = "ROCK"
	packTypeScissors = "SCISSORS"
	packTypePaper    = "PAPER"
)

type command struct {
	actions []string
}

func newCommand() *command {
	return &command{
		actions: make([]string, 0, maxPac),
	}
}

func (c command) Do() {
	cmd := strings.Join(c.actions, " | ")
	fmt.Println(cmd)
}

func (c *command) Move(pacID int, x int, y int, message string) {
	cmd := fmt.Sprintf("%s %d %d %d %s", actionMoveCmd, pacID, x, y, message)
	c.actions = append(c.actions, cmd)
}

func (c *command) Speed(pacID int, message string) {
	cmd := fmt.Sprintf("%s %d %s", actionSpeedCmd, pacID, message)
	c.actions = append(c.actions, cmd)
}

func (c *command) Switch(pacID int, pacType string, message string) {
	cmd := fmt.Sprintf("%s %d %s %s", actionSwitchCmd, pacID, pacType, message)
	c.actions = append(c.actions, cmd)
}
