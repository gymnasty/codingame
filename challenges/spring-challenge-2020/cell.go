package main

import "fmt"

const (
	// cell
	cellTypeWall  = '#'
	cellTypeFloor = ' '
)

type cell struct {
	position
	cellType rune
	score    int
}

func newCell(x, y int) *cell {
	return &cell{
		position: newPosition(x, y),
	}
}

func (c *cell) IsWall() bool {
	return c.cellType == cellTypeWall
}

func (c *cell) IsFloor() bool {
	return c.cellType == cellTypeFloor
}

func (c *cell) SetType(cellType rune) {
	c.cellType = cellType
}

func (c *cell) SetScore(score int) {
	c.score = score
}

func (c *cell) String() string {
	if c.IsWall() {
		return "####"
	}
	if c.score > 0 {
		return fmt.Sprintf(" %02d ", c.score)
	}
	return "    "
}

func (c *cell) Show() {
	debugLog(c.String())
}
