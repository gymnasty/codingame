package main

// pos から別の場所までの距離を保持する構造体
type step struct {
	step map[position]int
}

func newStep(grid *grid, pos position) *step {
	// 床の数をカウント
	floors := len(grid.AllFloorCells())

	// 位置ごとのステップを保持するマップ
	s := make(map[position]int)
	// 初期位置は 0 ステップ
	s[pos] = 0
	// 直前のステップの位置を保存
	previousStepPos := []position{pos}
	steps := 1
	for {
		// 今のステップ数の位置を保存
		currentStepPos := make([]position, 0, len(previousStepPos)*4)
		// 前のステップの位置の隣でまだステップが記録されていない床が次のステップ
		for _, p := range previousStepPos {
			for _, np := range getNextPositions(p, grid.width, grid.height) {
				_, ok := s[np]
				if ok {
					continue
				}
				if grid.GetCell(np).IsWall() {
					continue
				}
				s[np] = steps
				currentStepPos = append(currentStepPos, np)
			}
		}
		steps++
		previousStepPos = currentStepPos

		if len(s) == floors {
			break
		}
	}
	return &step{
		step: s,
	}
}

func (s *step) Dist(pos position) int {
	return s.step[pos]
}

func (s *step) PositionsIn(step int) []position {
	positions := make([]position, 0)
	for p, s := range s.step {
		if s <= step {
			positions = append(positions, p)
		}
	}
	return positions
}

// 各地点から任意の場所までの距離を保持する構造体
type stepMap struct {
	steps map[position]*step
}

func newStepMap(grid *grid) *stepMap {
	s := make(map[position]*step)
	for _, c := range grid.AllFloorCells() {
		pos := c.position
		s[pos] = newStep(grid, pos)
	}
	return &stepMap{
		steps: s,
	}
}

func (s *stepMap) Dist(pos1, pos2 position) int {
	return s.steps[pos1].Dist(pos2)
}

func (s *stepMap) PositionsIn(pos position, step int) []position {
	return s.steps[pos].PositionsIn(step)
}
