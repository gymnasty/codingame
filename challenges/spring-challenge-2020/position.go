package main

// position
type position struct {
	x, y int
}

func newPosition(x, y int) position {
	return position{
		x: x,
		y: y,
	}
}

func (p position) IsValid(width, height int) bool {
	return p.x >= 0 && p.y >= 0 && p.x < width && p.y < height
}

func (p position) Up(height int) position {
	return position{x: p.x, y: (p.y - 1 + height) % height}
}

func (p position) Down(height int) position {
	return position{x: p.x, y: (p.y + 1) % height}
}

func (p position) Left(width int) position {
	return position{x: (p.x - 1 + width) % width, y: p.y}
}

func (p position) Right(width int) position {
	return position{x: (p.x + 1) % width, y: p.y}
}

func getNextPositions(p position, width, height int) []position {
	return []position{
		p.Up(height), p.Down(height), p.Left(width), p.Right(width),
	}
}
