package main

type grid struct {
	cells  [][]*cell
	width  int
	height int
}

func newGrid(width, height int) *grid {
	cells := make([][]*cell, height)
	for i := range cells {
		cells[i] = make([]*cell, width)
	}
	for y, cc := range cells {
		for x := range cc {
			ccc := newCell(x, y)
			ccc.SetScore(1)
			cells[y][x] = ccc
		}
	}
	return &grid{
		cells:  cells,
		width:  width,
		height: height,
	}
}

func (g *grid) SetCell(c *cell) {
	g.cells[c.y][c.x] = c
}

func (g *grid) GetCell(pos position) *cell {
	if pos.x >= 0 && pos.y >= 0 && pos.x < g.width && pos.y < g.height {
		return g.cells[pos.y][pos.x]
	}
	return nil
}

func (g *grid) AllCells() []*cell {
	cells := make([]*cell, 0, g.width*g.height)
	for _, cc := range g.cells {
		cells = append(cells, cc...)
	}
	return cells
}

func (g *grid) AllFloorCells() []*cell {
	cells := make([]*cell, 0, g.width*g.height)
	for _, cc := range g.cells {
		for _, ccc := range cc {
			if ccc.IsFloor() {
				cells = append(cells, ccc)
			}
		}
	}
	return cells
}

func (g *grid) AllStraightCells(pos position) []*cell {
	cells := make([]*cell, 0, g.width*g.height)
	// current pos
	cells = append(cells, g.GetCell(pos))
	// right
	p := pos
	for {
		p = p.Right(g.width)
		c := g.GetCell(p)
		if c.IsWall() {
			break
		}
		cells = append(cells, c)
	}
	// left
	p = pos
	for {
		p = p.Left(g.width)
		c := g.GetCell(p)
		if g.GetCell(p).IsWall() {
			break
		}
		cells = append(cells, c)
	}
	// up
	p = pos
	for {
		p = p.Up(g.height)
		c := g.GetCell(p)
		if g.GetCell(p).IsWall() {
			break
		}
		cells = append(cells, c)
	}
	// down
	p = pos
	for {
		p = p.Down(g.height)
		c := g.GetCell(p)
		if g.GetCell(p).IsWall() {
			break
		}
		cells = append(cells, c)
	}
	return cells
}

func (g *grid) UpdateScore(pellets []*pellet) {
	for _, p := range pellets {
		g.GetCell(p.position).SetScore(p.score)
	}
}

func (g *grid) ClearScore() {
	for _, c := range g.AllCells() {
		c.SetScore(0)
	}
}

func (g *grid) Show() {
	// header
	debugLog("    ")
	for i := range g.cells[0] {
		debugLog(" %2d  ", i)
	}
	debugLogLine("")
	// body
	for i, cs := range g.cells {
		debugLog("%2d :", i)
		for _, c := range cs {
			c.Show()
			debugLog(" ")
		}
		debugLogLine("")
	}
}
