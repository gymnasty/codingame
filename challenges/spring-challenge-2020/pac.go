package main

import "fmt"

type pac struct {
	position
	id              int
	mine            bool
	typeID          string
	speedTurnsLeft  int
	abilityCoolDown int
}

func newPac(ID int, isMine bool, x, y int, typeID string, speedTurnsLeft, abilityCoolDown int) *pac {
	return &pac{
		position:        newPosition(x, y),
		id:              ID,
		mine:            isMine,
		typeID:          typeID,
		speedTurnsLeft:  speedTurnsLeft,
		abilityCoolDown: abilityCoolDown,
	}
}

func (p *pac) IsMine() bool {
	return p.mine
}

func (p *pac) IsDead() bool {
	return p.typeID == "DEAD"
}

func (p *pac) String() string {
	return fmt.Sprintf("Pac%d: pos(%2d,%2d) %8s SpeedUp:%d CoolDown:%d", p.id, p.x, p.y, p.typeID, p.speedTurnsLeft, p.abilityCoolDown)
}

func (p *pac) Show() {
	debugLogLine(p.String())
}
