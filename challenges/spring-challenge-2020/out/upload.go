package main

import (
	"bufio"
	"fmt"
	"os"
	"runtime"
	"strings"
	"time"
)

const (
	// cell
	cellTypeWall  = '#'
	cellTypeFloor = ' '
)

type cell struct {
	position
	cellType rune
	score    int
}

func newCell(x, y int) *cell {
	return &cell{
		position: newPosition(x, y),
	}
}

func (c *cell) IsWall() bool {
	return c.cellType == cellTypeWall
}

func (c *cell) IsFloor() bool {
	return c.cellType == cellTypeFloor
}

func (c *cell) SetType(cellType rune) {
	c.cellType = cellType
}

func (c *cell) SetScore(score int) {
	c.score = score
}

func (c *cell) String() string {
	if c.IsWall() {
		return "####"
	}
	if c.score > 0 {
		return fmt.Sprintf(" %02d ", c.score)
	}
	return "    "
}

func (c *cell) Show() {
	debugLog(c.String())
}

const (
	// action
	actionMoveCmd   = "MOVE"
	actionSpeedCmd  = "SPEED"
	actionSwitchCmd = "SWITCH"
	// pack type
	packTypeRock     = "ROCK"
	packTypeScissors = "SCISSORS"
	packTypePaper    = "PAPER"
)

type command struct {
	actions []string
}

func newCommand() *command {
	return &command{
		actions: make([]string, 0, maxPac),
	}
}

func (c command) Do() {
	cmd := strings.Join(c.actions, " | ")
	fmt.Println(cmd)
}

func (c *command) Move(pacID int, x int, y int, message string) {
	cmd := fmt.Sprintf("%s %d %d %d %s", actionMoveCmd, pacID, x, y, message)
	c.actions = append(c.actions, cmd)
}

func (c *command) Speed(pacID int, message string) {
	cmd := fmt.Sprintf("%s %d %s", actionSpeedCmd, pacID, message)
	c.actions = append(c.actions, cmd)
}

func (c *command) Switch(pacID int, pacType string, message string) {
	cmd := fmt.Sprintf("%s %d %s %s", actionSwitchCmd, pacID, pacType, message)
	c.actions = append(c.actions, cmd)
}

const (
	DEBUG = true

	maxTurn = 200

	minPac = 2
	maxPac = 5

	mixWidth  = 29
	maxWidth  = 35
	minHeight = 10
	maxHeight = 17

	responseTimePerTurn         = 50 * time.Millisecond
	responseTimeForTheFirstTurn = 1000 * time.Millisecond
)

type grid struct {
	cells  [][]*cell
	width  int
	height int
}

func newGrid(width, height int) *grid {
	cells := make([][]*cell, height)
	for i := range cells {
		cells[i] = make([]*cell, width)
	}
	for y, cc := range cells {
		for x := range cc {
			ccc := newCell(x, y)
			ccc.SetScore(1)
			cells[y][x] = ccc
		}
	}
	return &grid{
		cells:  cells,
		width:  width,
		height: height,
	}
}

func (g *grid) SetCell(c *cell) {
	g.cells[c.y][c.x] = c
}

func (g *grid) GetCell(pos position) *cell {
	if pos.x >= 0 && pos.y >= 0 && pos.x < g.width && pos.y < g.height {
		return g.cells[pos.y][pos.x]
	}
	return nil
}

func (g *grid) AllCells() []*cell {
	cells := make([]*cell, 0, g.width*g.height)
	for _, cc := range g.cells {
		cells = append(cells, cc...)
	}
	return cells
}

func (g *grid) AllFloorCells() []*cell {
	cells := make([]*cell, 0, g.width*g.height)
	for _, cc := range g.cells {
		for _, ccc := range cc {
			if ccc.IsFloor() {
				cells = append(cells, ccc)
			}
		}
	}
	return cells
}

func (g *grid) AllStraightCells(pos position) []*cell {
	cells := make([]*cell, 0, g.width*g.height)
	// current pos
	cells = append(cells, g.GetCell(pos))
	// right
	p := pos
	for {
		p = p.Right(g.width)
		c := g.GetCell(p)
		if c.IsWall() {
			break
		}
		cells = append(cells, c)
	}
	// left
	p = pos
	for {
		p = p.Left(g.width)
		c := g.GetCell(p)
		if g.GetCell(p).IsWall() {
			break
		}
		cells = append(cells, c)
	}
	// up
	p = pos
	for {
		p = p.Up(g.height)
		c := g.GetCell(p)
		if g.GetCell(p).IsWall() {
			break
		}
		cells = append(cells, c)
	}
	// down
	p = pos
	for {
		p = p.Down(g.height)
		c := g.GetCell(p)
		if g.GetCell(p).IsWall() {
			break
		}
		cells = append(cells, c)
	}
	return cells
}

func (g *grid) UpdateScore(pellets []*pellet) {
	for _, p := range pellets {
		g.GetCell(p.position).SetScore(p.score)
	}
}

func (g *grid) ClearScore() {
	for _, c := range g.AllCells() {
		c.SetScore(0)
	}
}

func (g *grid) Show() {
	// header
	debugLog("    ")
	for i := range g.cells[0] {
		debugLog(" %2d  ", i)
	}
	debugLogLine("")
	// body
	for i, cs := range g.cells {
		debugLog("%2d :", i)
		for _, c := range cs {
			c.Show()
			debugLog(" ")
		}
		debugLogLine("")
	}
}

type gameInput struct {
	width, height int
	grid          *grid
}

func (i *gameInput) Show() {
	debugLogLine("width: %d, height: %d", i.width, i.height)
	i.grid.Show()
}

type turnInput struct {
	turn                int
	myScore, enemyScore int
	visiblePacCount     int
	myPacs              []*pac
	enemyPacs           []*pac
	visiblePelletCount  int
	pellets             []*pellet
}

func (i *turnInput) Show() {
	debugLogLine("Turn: %d", i.turn)
	debugLogLine("MyScore: %d, EnemyScore: %d", i.myScore, i.enemyScore)
	debugLogLine("MyPacs:")
	for _, v := range i.myPacs {
		v.Show()
	}
	debugLogLine("EnemyPacs:")
	for _, v := range i.enemyPacs {
		v.Show()
	}
	/*
		for _, v := range i.pellets {
			v.Show()
		}
	*/
	debugLogLine("Pellets: %d", len(i.pellets))
}

type inputReader struct {
	scanner *bufio.Scanner
}

func newInputReader() *inputReader {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, 1000000), 1000000)
	return &inputReader{
		scanner: scanner,
	}
}

func (r *inputReader) readInitialInput() *gameInput {
	r.scanner.Scan()

	// width, height
	var width, height int
	_, err := fmt.Sscan(r.scanner.Text(), &width, &height)
	if err != nil {
		panic(err)
	}

	// grid
	grid := newGrid(width, height)
	for y := 0; y < height; y++ {
		r.scanner.Scan()
		line := r.scanner.Text()
		for x, r := range line {
			pos := newPosition(x, y)
			cell := grid.GetCell(pos)
			cell.SetType(r)
		}
	}

	return &gameInput{
		width:  width,
		height: height,
		grid:   grid,
	}
}

func (r *inputReader) readTurnInput(turn int) *turnInput {
	// score
	var myScore, opponentScore int
	r.scanner.Scan()
	_, _ = fmt.Sscan(r.scanner.Text(), &myScore, &opponentScore)

	// pac
	var visiblePacCount int
	r.scanner.Scan()
	_, _ = fmt.Sscan(r.scanner.Text(), &visiblePacCount)
	pacs := make([]*pac, visiblePacCount)
	for i := range pacs {
		var pacId, mine, x, y, speedTurnsLeft, abilityCoolDown int
		var typeId string
		r.scanner.Scan()
		_, err := fmt.Sscan(r.scanner.Text(), &pacId, &mine, &x, &y, &typeId, &speedTurnsLeft, &abilityCoolDown)
		if err != nil {
			panic(err)
		}
		isMine := mine == 1
		pacs[i] = newPac(pacId, isMine, x, y, typeId, speedTurnsLeft, abilityCoolDown)
	}

	myPacs := make([]*pac, 0, visiblePacCount)
	enemyPacs := make([]*pac, 0, visiblePacCount)
	for _, p := range pacs {
		if p.mine {
			myPacs = append(myPacs, p)
		} else {
			enemyPacs = append(enemyPacs, p)
		}
	}

	// pellet
	var visiblePelletCount int
	r.scanner.Scan()
	_, _ = fmt.Sscan(r.scanner.Text(), &visiblePelletCount)
	pellets := make([]*pellet, visiblePelletCount)
	for i := range pellets {
		var x, y, score int
		r.scanner.Scan()
		_, err := fmt.Sscan(r.scanner.Text(), &x, &y, &score)
		if err != nil {
			panic(err)
		}
		pellets[i] = newPellet(x, y, score)
	}

	return &turnInput{
		turn:               turn,
		myScore:            myScore,
		enemyScore:         opponentScore,
		visiblePacCount:    visiblePacCount,
		myPacs:             myPacs,
		enemyPacs:          enemyPacs,
		visiblePelletCount: visiblePelletCount,
		pellets:            pellets,
	}
}

/**
 * Grab the pellets as fast as you can!
 **/

func main() {
	// capture exception
	defer func() {
		if err := recover(); err != nil {
			debugLogLine("[ERROR] %s\n", err)
			for depth := 0; ; depth++ {
				_, file, line, ok := runtime.Caller(depth)
				if !ok {
					break
				}
				debugLogLine("======> %d: %v:%d", depth, file, line)
			}
		}
	}()

	// read game input
	inputReader := newInputReader()
	gameInput := inputReader.readInitialInput()
	gameInput.Show()

	// grid
	grid := gameInput.grid
	// step map
	stepMap := newStepMap(grid)

	// read turn input and write output
	turn := 0
	processTurn := func() {
		// increment turn
		defer func() {
			turn++
		}()

		// input
		turnInput := inputReader.readTurnInput(turn)
		turnInput.Show()

		// 最初のターンのみ
		if turn == 0 {
			for _, pac := range turnInput.myPacs {
				// パックの位置のスコアを 0 にする
				grid.GetCell(pac.position).SetScore(0)
				// 敵のパックの位置のスコアを 0 にする
				p := pac.position
				p.x = grid.width - 1 - p.x
				grid.GetCell(p).SetScore(0)
			}
		}

		// update grid
		// 見える位置のスコアを一旦0にする
		for _, pac := range turnInput.myPacs {
			for _, c := range grid.AllStraightCells(pac.position) {
				c.SetScore(0)
			}
		}
		// スーパーペレットのスコアを一旦0にする
		for _, c := range grid.AllFloorCells() {
			if c.score == 10 {
				c.SetScore(0)
			}
		}
		grid.UpdateScore(turnInput.pellets)
		grid.Show()

		// score
		score := newScore(grid, stepMap)
		score.Show(grid)

		// command
		command := newCommand()

		// スーパーペレットが残っている場合

		floorCells := grid.AllFloorCells()
		for _, pac := range turnInput.myPacs {
			// 移動先のセルの候補
			steps := 1
			if pac.speedTurnsLeft > 0 {
				steps = 2
			}
			cells := stepMap.PositionsIn(pac.position, steps)

			// 敵の位置
			for _, enemy := range turnInput.enemyPacs {
				if enemy.IsDead() {
					continue
				}
				d := stepMap.Dist(pac.position, enemy.position)
				s := 1
				if enemy.speedTurnsLeft > 0 {
					s = 2
				}
				if d <= s {
					// 1ターンで自分の位置に到達可能
				}
			}

			// スキル
			if pac.abilityCoolDown == 0 {
				command.Speed(pac.id, "")
				continue
			}

			var maxScore float64
			var maxPos position
			var cellIndex int
			for i, c := range floorCells {
				if c.position == pac.position {
					continue
				}
				if pac.speedTurnsLeft > 0 && stepMap.Dist(pac.position, c.position) == 1 {
					continue
				}

				s := score.GetScore(c.position) / float64(stepMap.Dist(pac.position, c.position))
				if maxScore < s {
					maxPos = c.position
					maxScore = s
					cellIndex = i
				}
			}
			command.Move(pac.id, maxPos.x, maxPos.y, fmt.Sprintf("%d,%d (%f)", maxPos.x, maxPos.y, maxScore))

			// 移動対象のセルは削除してみる
			if len(floorCells) > 0 {
				floorCells = append(floorCells[0:cellIndex], floorCells[cellIndex+1:]...)
			}
		}

		if len(command.actions) == 0 {
			p := gameInput.grid.AllFloorCells()[0]
			command.Move(turnInput.myPacs[0].id, p.x, p.y, "avoid timeout")
		}
		command.Do()
	}

	// each turn
	for {
		processTurn()
	}
}

type pac struct {
	position
	id              int
	mine            bool
	typeID          string
	speedTurnsLeft  int
	abilityCoolDown int
}

func newPac(ID int, isMine bool, x, y int, typeID string, speedTurnsLeft, abilityCoolDown int) *pac {
	return &pac{
		position:        newPosition(x, y),
		id:              ID,
		mine:            isMine,
		typeID:          typeID,
		speedTurnsLeft:  speedTurnsLeft,
		abilityCoolDown: abilityCoolDown,
	}
}

func (p *pac) IsMine() bool {
	return p.mine
}

func (p *pac) IsDead() bool {
	return p.typeID == "DEAD"
}

func (p *pac) String() string {
	return fmt.Sprintf("Pac%d: pos(%2d,%2d) %8s SpeedUp:%d CoolDown:%d", p.id, p.x, p.y, p.typeID, p.speedTurnsLeft, p.abilityCoolDown)
}

func (p *pac) Show() {
	debugLogLine(p.String())
}

type pellet struct {
	position
	score int
}

func newPellet(x, y, score int) *pellet {
	return &pellet{
		position: newPosition(x, y),
		score:    score,
	}
}

func (p *pellet) String() string {
	return fmt.Sprintf("Pellet: pos(%d, %d)", p.x, p.y)
}

func (p *pellet) Show() {
	debugLogLine(p.String())
}

// position
type position struct {
	x, y int
}

func newPosition(x, y int) position {
	return position{
		x: x,
		y: y,
	}
}

func (p position) IsValid(width, height int) bool {
	return p.x >= 0 && p.y >= 0 && p.x < width && p.y < height
}

func (p position) Up(height int) position {
	return position{x: p.x, y: (p.y - 1 + height) % height}
}

func (p position) Down(height int) position {
	return position{x: p.x, y: (p.y + 1) % height}
}

func (p position) Left(width int) position {
	return position{x: (p.x - 1 + width) % width, y: p.y}
}

func (p position) Right(width int) position {
	return position{x: (p.x + 1) % width, y: p.y}
}

func getNextPositions(p position, width, height int) []position {
	return []position{
		p.Up(height), p.Down(height), p.Left(width), p.Right(width),
	}
}

type score struct {
	scoreMap map[position]float64
}

func newScore(grid *grid, stepMap *stepMap) *score {
	// スコア計算
	scoreMap := make(map[position]float64)
	for _, c1 := range grid.AllFloorCells() {
		var s float64
		for _, c2 := range grid.AllFloorCells() {
			if c1.position == c2.position {
				s += float64(c2.score) * 2
				continue
			}
			if c2.score > 0 {
				s += float64(c2.score) / float64(stepMap.Dist(c1.position, c2.position))
			}
		}
		scoreMap[c1.position] = s
	}
	return &score{scoreMap: scoreMap}
}

func (s score) GetScore(pos position) float64 {
	return s.scoreMap[pos]
}

func (s score) Show(grid *grid) {
	// header
	debugLog("    ")
	for i := range grid.cells[0] {
		debugLog(" %2d  ", i)
	}
	debugLogLine("")
	// body
	for i, cs := range grid.cells {
		debugLog("%2d :", i)
		for _, c := range cs {
			if c.IsWall() {
				c.Show()
			} else {
				debugLog("%4.0f", s.scoreMap[c.position])
			}
			debugLog(" ")
		}
		debugLogLine("")
	}
}

// pos から別の場所までの距離を保持する構造体
type step struct {
	step map[position]int
}

func newStep(grid *grid, pos position) *step {
	// 床の数をカウント
	floors := len(grid.AllFloorCells())

	// 位置ごとのステップを保持するマップ
	s := make(map[position]int)
	// 初期位置は 0 ステップ
	s[pos] = 0
	// 直前のステップの位置を保存
	previousStepPos := []position{pos}
	steps := 1
	for {
		// 今のステップ数の位置を保存
		currentStepPos := make([]position, 0, len(previousStepPos)*4)
		// 前のステップの位置の隣でまだステップが記録されていない床が次のステップ
		for _, p := range previousStepPos {
			for _, np := range getNextPositions(p, grid.width, grid.height) {
				_, ok := s[np]
				if ok {
					continue
				}
				if grid.GetCell(np).IsWall() {
					continue
				}
				s[np] = steps
				currentStepPos = append(currentStepPos, np)
			}
		}
		steps++
		previousStepPos = currentStepPos

		if len(s) == floors {
			break
		}
	}
	return &step{
		step: s,
	}
}

func (s *step) Dist(pos position) int {
	return s.step[pos]
}

func (s *step) PositionsIn(step int) []position {
	positions := make([]position, 0)
	for p, s := range s.step {
		if s <= step {
			positions = append(positions, p)
		}
	}
	return positions
}

// 各地点から任意の場所までの距離を保持する構造体
type stepMap struct {
	steps map[position]*step
}

func newStepMap(grid *grid) *stepMap {
	s := make(map[position]*step)
	for _, c := range grid.AllFloorCells() {
		pos := c.position
		s[pos] = newStep(grid, pos)
	}
	return &stepMap{
		steps: s,
	}
}

func (s *stepMap) Dist(pos1, pos2 position) int {
	return s.steps[pos1].Dist(pos2)
}

func (s *stepMap) PositionsIn(pos position, step int) []position {
	return s.steps[pos].PositionsIn(step)
}

func abs(a int) int {
	if a > 0 {
		return a
	}
	return -a
}

func minInt(a int, b int) int {
	if a > b {
		return b
	}
	return a
}

func maxInt(a int, b int) int {
	if a < b {
		return b
	}
	return a
}

func maxIndex(a []float64) int {
	if len(a) == 0 {
		return -1
	}
	index := 0
	m := a[index]
	for i := 1; i < len(a); i++ {
		if m < a[i] {
			index = i
			m = a[i]
		}
	}
	return index
}

func debugLog(format string, a ...interface{}) {
	if !DEBUG {
		return
	}
	_, _ = fmt.Fprintf(os.Stderr, format, a...)
}

func debugLogLine(format string, a ...interface{}) {
	if !DEBUG {
		return
	}
	debugLog(format+"\n", a...)
}
