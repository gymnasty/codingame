package main

import (
	"bufio"
	"fmt"
	"os"
)

type gameInput struct {
	width, height int
	grid          *grid
}

func (i *gameInput) Show() {
	debugLogLine("width: %d, height: %d", i.width, i.height)
	i.grid.Show()
}

type turnInput struct {
	turn                int
	myScore, enemyScore int
	visiblePacCount     int
	myPacs              []*pac
	enemyPacs           []*pac
	visiblePelletCount  int
	pellets             []*pellet
}

func (i *turnInput) Show() {
	debugLogLine("Turn: %d", i.turn)
	debugLogLine("MyScore: %d, EnemyScore: %d", i.myScore, i.enemyScore)
	debugLogLine("MyPacs:")
	for _, v := range i.myPacs {
		v.Show()
	}
	debugLogLine("EnemyPacs:")
	for _, v := range i.enemyPacs {
		v.Show()
	}
	/*
		for _, v := range i.pellets {
			v.Show()
		}
	*/
	debugLogLine("Pellets: %d", len(i.pellets))
}

type inputReader struct {
	scanner *bufio.Scanner
}

func newInputReader() *inputReader {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, 1000000), 1000000)
	return &inputReader{
		scanner: scanner,
	}
}

func (r *inputReader) readInitialInput() *gameInput {
	r.scanner.Scan()

	// width, height
	var width, height int
	_, err := fmt.Sscan(r.scanner.Text(), &width, &height)
	if err != nil {
		panic(err)
	}

	// grid
	grid := newGrid(width, height)
	for y := 0; y < height; y++ {
		r.scanner.Scan()
		line := r.scanner.Text()
		for x, r := range line {
			pos := newPosition(x, y)
			cell := grid.GetCell(pos)
			cell.SetType(r)
		}
	}

	return &gameInput{
		width:  width,
		height: height,
		grid:   grid,
	}
}

func (r *inputReader) readTurnInput(turn int) *turnInput {
	// score
	var myScore, opponentScore int
	r.scanner.Scan()
	_, _ = fmt.Sscan(r.scanner.Text(), &myScore, &opponentScore)

	// pac
	var visiblePacCount int
	r.scanner.Scan()
	_, _ = fmt.Sscan(r.scanner.Text(), &visiblePacCount)
	pacs := make([]*pac, visiblePacCount)
	for i := range pacs {
		var pacId, mine, x, y, speedTurnsLeft, abilityCoolDown int
		var typeId string
		r.scanner.Scan()
		_, err := fmt.Sscan(r.scanner.Text(), &pacId, &mine, &x, &y, &typeId, &speedTurnsLeft, &abilityCoolDown)
		if err != nil {
			panic(err)
		}
		isMine := mine == 1
		pacs[i] = newPac(pacId, isMine, x, y, typeId, speedTurnsLeft, abilityCoolDown)
	}

	myPacs := make([]*pac, 0, visiblePacCount)
	enemyPacs := make([]*pac, 0, visiblePacCount)
	for _, p := range pacs {
		if p.mine {
			myPacs = append(myPacs, p)
		} else {
			enemyPacs = append(enemyPacs, p)
		}
	}

	// pellet
	var visiblePelletCount int
	r.scanner.Scan()
	_, _ = fmt.Sscan(r.scanner.Text(), &visiblePelletCount)
	pellets := make([]*pellet, visiblePelletCount)
	for i := range pellets {
		var x, y, score int
		r.scanner.Scan()
		_, err := fmt.Sscan(r.scanner.Text(), &x, &y, &score)
		if err != nil {
			panic(err)
		}
		pellets[i] = newPellet(x, y, score)
	}

	return &turnInput{
		turn:               turn,
		myScore:            myScore,
		enemyScore:         opponentScore,
		visiblePacCount:    visiblePacCount,
		myPacs:             myPacs,
		enemyPacs:          enemyPacs,
		visiblePelletCount: visiblePelletCount,
		pellets:            pellets,
	}
}
