package main

type score struct {
	scoreMap map[position]float64
}

func newScore(grid *grid, stepMap *stepMap) *score {
	// スコア計算
	scoreMap := make(map[position]float64)
	for _, c1 := range grid.AllFloorCells() {
		var s float64
		for _, c2 := range grid.AllFloorCells() {
			if c1.position == c2.position {
				s += float64(c2.score) * 2
				continue
			}
			if c2.score > 0 {
				s += float64(c2.score) / float64(stepMap.Dist(c1.position, c2.position))
			}
		}
		scoreMap[c1.position] = s
	}
	return &score{scoreMap: scoreMap}
}

func (s score) GetScore(pos position) float64 {
	return s.scoreMap[pos]
}

func (s score) Show(grid *grid) {
	// header
	debugLog("    ")
	for i := range grid.cells[0] {
		debugLog(" %2d  ", i)
	}
	debugLogLine("")
	// body
	for i, cs := range grid.cells {
		debugLog("%2d :", i)
		for _, c := range cs {
			if c.IsWall() {
				c.Show()
			} else {
				debugLog("%4.0f", s.scoreMap[c.position])
			}
			debugLog(" ")
		}
		debugLogLine("")
	}
}
