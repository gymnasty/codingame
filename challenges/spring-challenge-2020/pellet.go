package main

import "fmt"

type pellet struct {
	position
	score int
}

func newPellet(x, y, score int) *pellet {
	return &pellet{
		position: newPosition(x, y),
		score:    score,
	}
}

func (p *pellet) String() string {
	return fmt.Sprintf("Pellet: pos(%d, %d)", p.x, p.y)
}

func (p *pellet) Show() {
	debugLogLine(p.String())
}
