package stage2

import "fmt"

func TopLeftCrestName(clans []Clan) string {
	matchCrest := func(c Clan) bool {
		tl, tr, bl, br := c.Corners[0], c.Corners[1], c.Corners[2], c.Corners[3]
		if tl.Color == "blue" && tl.SymbolType == "none" && tl.SymbolAmount == 0 &&
			tr.Color == "white" && tr.SymbolType == "disc" && tr.SymbolAmount == 3 &&
			bl.Color == "white" && bl.SymbolType == "disc" && bl.SymbolAmount == 3 &&
			br.Color == "blue" && br.SymbolType == "none" && br.SymbolAmount == 0 {
			return true
		}
		return false
	}

	candidate := make([]Clan, 0)
	for _, c := range clans {
		if matchCrest(c) {
			candidate = append(candidate, c)
		}
	}

	if len(candidate) != 1 {
		fmt.Println(candidate)
		return "failed"
	}
	return candidate[0].Name
}
