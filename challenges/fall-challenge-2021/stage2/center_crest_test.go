package stage2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCenterCrestName(t *testing.T) {
	clans := GetClans()
	name := CenterCrestName(clans)
	assert.Empty(t, name)
}
