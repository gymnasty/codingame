package stage2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetCastles(t *testing.T) {
	castles := GetCastles()
	assert.Equal(t, 182, len(castles))
}

func TestCastleNumber(t *testing.T) {
	GetCastleNumber(GetCastles())
	assert.Empty(t, "test")
}
