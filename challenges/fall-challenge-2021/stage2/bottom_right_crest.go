package stage2

import "fmt"

func BottomRightCrestName(clans []Clan) string {
	matchCrest := func(c Clan) bool {
		tl, tr := c.Corners[0], c.Corners[1]
		if tr.Color == "green" && tr.SymbolType == "none" {
			if tl.Color == "white" && tl.SymbolType == "rectangle" && tl.SymbolColor == "blue" {
				return true
			}
			if tl.Color == "blue" && tl.SymbolType == "disc" && tl.SymbolColor == "orange" {
				return true
			}
			return false
		}
		return false
	}

	candidate := make([]Clan, 0)
	for _, c := range clans {
		if matchCrest(c) {
			candidate = append(candidate, c)
		}
	}

	if len(candidate) != 1 {
		fmt.Println(candidate)
		return "failed"
	}
	return candidate[0].Name
}
