package stage2

import (
	"fmt"
	"strings"
)

func TopRightCrestName(clans []Clan) string {
	matchName := func(c Clan) bool {
		if len(c.Name) != 8 {
			return false
		}
		if !strings.ContainsRune(strings.ToUpper(c.Name), 'E') {
			return false
		}
		if !strings.ContainsRune(strings.ToUpper(c.Name), 'B') {
			return false
		}
		if !strings.ContainsRune(strings.ToUpper(c.Name), 'S') {
			return false
		}
		if !strings.ContainsRune(strings.ToUpper(c.Name), 'R') {
			return false
		}
		if !strings.ContainsRune(strings.ToUpper(c.Name), 'Y') {
			return false
		}
		return true
	}
	matchCrest := func(c Clan) bool {
		tl := c.Corners[0]
		if tl.Color == "white" && tl.SymbolType == "rectangle" && tl.SymbolColor == "blue" {
			return true
		}
		if tl.Color == "white" && tl.SymbolType == "star" && tl.SymbolColor == "blue" {
			return true
		}
		if tl.Color == "blue" && tl.SymbolType == "disc" && tl.SymbolColor == "orange" {
			return true
		}
		return false
	}

	candidate := make([]Clan, 0)
	for _, c := range clans {
		if matchName(c) && matchCrest(c) {
			candidate = append(candidate, c)
		}
	}

	if len(candidate) != 1 {
		fmt.Println(candidate)
		return "failed"
	}
	return candidate[0].Name
}
