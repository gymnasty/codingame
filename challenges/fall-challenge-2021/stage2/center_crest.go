package stage2

import "fmt"

func CenterCrestName(clans []Clan) string {
	matchCrest := func(c Clan) bool {
		tl, tr, bl, br := c.Corners[0], c.Corners[1], c.Corners[2], c.Corners[3]
		if tl.Color == "blue" && tl.SymbolType == "disc" && tl.SymbolAmount == 3 &&
			tr.Color == "red" && tr.SymbolType == "star" && tr.SymbolAmount == 4 &&
			bl.Color == "red" && bl.SymbolType == "disc" && bl.SymbolAmount == 3 &&
			br.Color == "green" && br.SymbolType == "rectangle" && br.SymbolAmount == 3 {
			return true
		}
		return false
	}

	candidate := make([]Clan, 0)
	for _, c := range clans {
		if matchCrest(c) {
			candidate = append(candidate, c)
		}
	}

	if len(candidate) != 1 {
		fmt.Println(candidate)
		return "failed"
	}
	return candidate[0].Name
}
