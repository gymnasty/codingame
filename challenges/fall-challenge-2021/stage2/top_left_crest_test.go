package stage2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTopLeftCrestName(t *testing.T) {
	clans := GetClans()
	name := TopLeftCrestName(clans)
	assert.Empty(t, name)
}
