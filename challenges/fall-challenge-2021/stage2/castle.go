package stage2

import (
	"encoding/json"
	"fmt"
	"strings"
)

// The number and owning clan of a castle.
type Castle struct {
	ClanName string `json:"clanName"`
	Number   int    `json:"number"`
}

func GetCastles() []Castle {
	lines := strings.Split(castleData, "\n")
	castles := make([]Castle, len(lines))
	for i, line := range lines {
		var c Castle
		err := json.Unmarshal([]byte(line), &c)
		if err != nil {
			panic(err)
		}
		castles[i] = c
	}
	return castles
}

func GetCastleNumber(castles []Castle) {
	for _, c := range castles {
		n := c.ClanName
		if n == "Campbell" || n == "Arbuthnott" || n == "Fergusson" || n == "Anderson" || n == "Rasberry" {
			fmt.Println(c)
		}
	}
}

var castleData = `{"clanName":"Gellinny","number":1}
{"clanName":"Gellinnyarry","number":2}
{"clanName":"Teofenna","number":3}
{"clanName":"Haig","number":4}
{"clanName":"Trolsbeyumpy","number":5}
{"clanName":"Burfbeys","number":6}
{"clanName":"Colville","number":7}
{"clanName":"Sremmingbonny","number":8}
{"clanName":"Briarley","number":9}
{"clanName":"Boyd","number":10}
{"clanName":"Woodcroft","number":11}
{"clanName":"Menmmuryay","number":12}
{"clanName":"MacAulay","number":13}
{"clanName":"Trinbess","number":14}
{"clanName":"Ross","number":15}
{"clanName":"Bruce","number":16}
{"clanName":"Appin","number":17}
{"clanName":"Melboury","number":18}
{"clanName":"Poultory","number":19}
{"clanName":"Waterbryay","number":20}
{"clanName":"Berclingsy","number":21}
{"clanName":"Dewar","number":22}
{"clanName":"Grant","number":23}
{"clanName":"Anstruther","number":24}
{"clanName":"Leask","number":25}
{"clanName":"Primrose","number":26}
{"clanName":"Heightry","number":27}
{"clanName":"Menmmury","number":28}
{"clanName":"Bannerman","number":29}
{"clanName":"Barbourl","number":30}
{"clanName":"Graham","number":31}
{"clanName":"Macnaghten","number":32}
{"clanName":"Rambelt","number":33}
{"clanName":"Forsyth","number":34}
{"clanName":"Wersingby","number":35}
{"clanName":"Burfbesyay","number":36}
{"clanName":"Johnson","number":37}
{"clanName":"Riversay","number":38}
{"clanName":"Rattray","number":39}
{"clanName":"Tregonnyammy","number":40}
{"clanName":"Rasberry","number":41}
{"clanName":"Menmmure","number":42}
{"clanName":"Armstrong","number":43}
{"clanName":"Moncreiff","number":44}
{"clanName":"Wedderburn","number":45}
{"clanName":"Lindsay","number":46}
{"clanName":"MacArthur","number":47}
{"clanName":"Stirling","number":48}
{"clanName":"Waterbra","number":49}
{"clanName":"Campbell","number":50}
{"clanName":"Harbingstery","number":51}
{"clanName":"Weminglytresppy","number":52}
{"clanName":"Theffanyay","number":53}
{"clanName":"McBain","number":54}
{"clanName":"Bolstoryammy","number":55}
{"clanName":"Brielley","number":56}
{"clanName":"Oliphant","number":57}
{"clanName":"Gwentheyay","number":58}
{"clanName":"Sinclair","number":59}
{"clanName":"Cassedan","number":60}
{"clanName":"Borgsey","number":61}
{"clanName":"MacLean","number":62}
{"clanName":"Lumsden","number":63}
{"clanName":"McLeonny","number":64}
{"clanName":"Poultora","number":65}
{"clanName":"Boarlleyay","number":66}
{"clanName":"MacThomas","number":67}
{"clanName":"Lachlan","number":68}
{"clanName":"Theffano","number":69}
{"clanName":"Leslie","number":70}
{"clanName":"Arbuthnott","number":71}
{"clanName":"Spens","number":72}
{"clanName":"Agnew","number":73}
{"clanName":"Carnegie","number":74}
{"clanName":"Weathersby","number":75}
{"clanName":"Maitland","number":76}
{"clanName":"Bestorray","number":77}
{"clanName":"Cameron","number":78}
{"clanName":"Cumming","number":79}
{"clanName":"Fergbulystey","number":80}
{"clanName":"Lennox","number":81}
{"clanName":"Innes","number":82}
{"clanName":"Trinkesberggy","number":83}
{"clanName":"Trolsberty","number":84}
{"clanName":"Davidson","number":85}
{"clanName":"Anderson","number":86}
{"clanName":"Teofennyarpy","number":87}
{"clanName":"Kennedy","number":88}
{"clanName":"Logan","number":89}
{"clanName":"Hannay","number":90}
{"clanName":"Moffat","number":91}
{"clanName":"Cathcart","number":92}
{"clanName":"Trinbesyelly","number":93}
{"clanName":"Nesbitt","number":94}
{"clanName":"Malcolm","number":95}
{"clanName":"Borthwick","number":96}
{"clanName":"Riversal","number":97}
{"clanName":"Farquharson","number":98}
{"clanName":"Rawlsbryey","number":99}
{"clanName":"Jardine","number":100}
{"clanName":"Barboury","number":101}
{"clanName":"Guthrie","number":102}
{"clanName":"Rawlsbry","number":103}
{"clanName":"Weabbers","number":104}
{"clanName":"Comyn","number":105}
{"clanName":"Durie","number":106}
{"clanName":"Walterry","number":107}
{"clanName":"Fergusson","number":108}
{"clanName":"Cambridge","number":109}
{"clanName":"Eliott","number":110}
{"clanName":"Ramsay","number":111}
{"clanName":"Kerr","number":112}
{"clanName":"Pembridge","number":113}
{"clanName":"Ranald","number":114}
{"clanName":"Tregonna","number":115}
{"clanName":"Semphony","number":116}
{"clanName":"Reetlesammy","number":117}
{"clanName":"MacNeacail","number":118}
{"clanName":"Walterra","number":119}
{"clanName":"Weabberyarry","number":120}
{"clanName":"Rollo","number":121}
{"clanName":"Boarlley","number":122}
{"clanName":"Hasburry","number":123}
{"clanName":"Home","number":124}
{"clanName":"Montgomery","number":125}
{"clanName":"Stewart","number":126}
{"clanName":"Lochbuie","number":127}
{"clanName":"Melbouryally","number":128}
{"clanName":"Walterryenny","number":129}
{"clanName":"Bolstory","number":130}
{"clanName":"MacNab","number":131}
{"clanName":"Craunston","number":132}
{"clanName":"Bolstori","number":133}
{"clanName":"Hamilton","number":134}
{"clanName":"Hope","number":135}
{"clanName":"Hembennyanny","number":136}
{"clanName":"Riversayary","number":137}
{"clanName":"Hembenny","number":138}
{"clanName":"MacLaren","number":139}
{"clanName":"Stuart","number":140}
{"clanName":"Riddell","number":141}
{"clanName":"Nicolson","number":142}
{"clanName":"Fergbulb","number":143}
{"clanName":"Bergsy","number":144}
{"clanName":"Lewis","number":145}
{"clanName":"MacLeod","number":146}
{"clanName":"Rennbury","number":147}
{"clanName":"Hembenna","number":148}
{"clanName":"Gwenthey","number":149}
{"clanName":"Lokhart","number":150}
{"clanName":"Gordon","number":151}
{"clanName":"Johnstone","number":152}
{"clanName":"Bute","number":153}
{"clanName":"Erbingorstery","number":154}
{"clanName":"Colquhoun","number":155}
{"clanName":"McLeonnyay","number":156}
{"clanName":"Monty","number":157}
{"clanName":"Menzies","number":158}
{"clanName":"MacCallum","number":159}
{"clanName":"Wallace","number":160}
{"clanName":"Carmichael","number":161}
{"clanName":"Boyle","number":162}
{"clanName":"Teofenny","number":163}
{"clanName":"Brodie","number":164}
{"clanName":"Gwenther","number":165}
{"clanName":"Lamont","number":166}
{"clanName":"Munro","number":167}
{"clanName":"Makgill","number":168}
{"clanName":"Waterbry","number":169}
{"clanName":"Chisholm","number":170}
{"clanName":"Cochrane","number":171}
{"clanName":"Theffany","number":172}
{"clanName":"Ruthven","number":173}
{"clanName":"Montray","number":174}
{"clanName":"MacLaine","number":175}
{"clanName":"Poultoryally","number":176}
{"clanName":"Swinton","number":177}
{"clanName":"Trolsber","number":178}
{"clanName":"Sutherland","number":179}
{"clanName":"MacMillan","number":180}
{"clanName":"McLeonni","number":181}
{"clanName":"Tregonny","number":182}`
