# README

## Ghost says

### King NOAH II

"Turn the sh"
You hear those words being repeated by the ghost in a spooky yet imperious voice.

### Oscar MANNING

"to the le"
You hear those words being repeated by the ghost in a spooky, angry voice.

### Gabriel HOLDEN

"our times"
You hear those words being repeated by the ghost in a spooky, gruff voice.

### Shona LAMB

"ield three"
You hear those words being repeated by the ghost in a spooky yet kind voice.

### Tyler DALY

"times to the r"
You hear those words being repeated by the ghost in a spooky yet joyful voice.

### Corner ATKINS

"ft and twice"
You hear those words being repeated by the ghost in a spooky yet mellow voice.

### Isobel CLAYTON

"to the right"
You hear those words being repeated by the ghost in a spooky yet dreamy voice.

### Nicolas PEARCE

"ight, f"
You hear those words being repeated by the ghost in a spooky yet slurred voice.

### coherent sentence

Turn the shield three times to the right, four times to the left and twice to the right

## A TIME-WORN PARCHMENT

In the many items strewn across the floor, you find a parchment that reads:

"An alliance between four clans gives birth to a new clan.
Ancient heraldic law dictates the way the new clan's crest be created out of those from the allied clans, as seen in this example."

* 該当の位置のシンボルとシンボルカラーを継承
* 該当の位置のカラーはシンボルがない場所のカラーを継承

## EERIE TEXT ON THE WALL

"There is a path that connects all the castles of the alliance. This path does not pass by any other castle.
This path starts at Campbell castle, where the 4 clans became one. This path is the key."

Campbell -> Fergusson -> Rasberry -> Arbuthnott -> Anderson
