package stage2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBottomRightCrestName(t *testing.T) {
	clans := GetClans()
	name := BottomRightCrestName(clans)
	assert.Empty(t, name)
}
