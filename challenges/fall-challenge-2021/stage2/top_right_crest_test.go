package stage2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTopRightCrestName(t *testing.T) {
	clans := GetClans()
	name := TopRightCrestName(clans)
	assert.Empty(t, name)
}
