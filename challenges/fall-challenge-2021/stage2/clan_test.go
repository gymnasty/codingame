package stage2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetClans(t *testing.T) {
	clans := GetClans()
	assert.Equal(t, 182, len(clans))
}
