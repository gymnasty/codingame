package stage2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBottomLeftCrestName(t *testing.T) {
	clans := GetClans()
	name := BottomLeftCrestName(clans)
	assert.Empty(t, name)
}
