package stage3

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetNodes(t *testing.T) {
	nodes := GetNodes()
	assert.Equal(t, 286, len(nodes))
}
