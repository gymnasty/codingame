# README

## Axe

SOMEWHAT DAMAGED PLAQUE

The text on this plaque is hard to make out:

"This axe belonged to ##################, first-cousin to the grandson of the only child of ####################
Her only regret was not having more than one child."

## Crosbow

HIGHLY DAMAGED PLAQUE

The text on this plaque is practically illegible:

```text
This crosbow belonged to ##############
Buried #####################
############################

########################### family tree ##################
###### the fact that the total number of his descendants
is equal to 48, the exact age at which he ######
```

## Double-edged ax

WARRIOR'S STATUE

A statue that has been written on:

"Thief, your eyes betray you! The true owner of the axe can be traced back through guilty looks.

Nicholas PEARCE

## Sword

DESECRATED STONE COFFIN

A scuffed carving reads:
"Here lies ################, in the only tomb to have been taken from the castle cemetery to honour their memory within this sacred place.

擦り切れた彫刻には次のように書かれています。
「ここにあるのは################、この神聖な場所での彼らの記憶を称えるために城の墓地から運ばれた唯一の墓です。

## hints

### SCATTERED CANDLES

The floor is littered with candles with names on them.
These must be linked to the ritual that created the curse you are here to dispel.

床には名前が書かれたろうそくが散らばっています。
これらは、あなたが払拭するためにここにいる呪いを生み出した儀式に関連している必要があります。

### SUSPICIOUS PATCH OF GROUND

Upon further investigation, you notice this empty patch of ground is lower than the rest of the terrain.

さらに調査すると、この空の地面のパッチが他の地形よりも低いことがわかります。
