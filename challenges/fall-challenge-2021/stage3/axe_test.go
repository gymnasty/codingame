package stage3

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAxeOwnerName(t *testing.T) {
	nodes := GetNodes()
	name := AxeOwnerName(nodes)
	assert.Empty(t, name)
}
