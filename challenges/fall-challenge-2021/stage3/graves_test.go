package stage3

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetGraves(t *testing.T) {
	graves := GetGraves()
	assert.Equal(t, 164, len(graves))
}

func TestGetGraveName(t *testing.T) {
	name := GetGraveName(894)
	assert.Empty(t, name)
}
