package stage3

import (
	"encoding/json"
	"strings"
)

// A name and number.
type Grave struct {
	Name   string `json:"name"`
	Number int    `json:"number"`
}

func GetGraves() []Grave {
	lines := strings.Split(graveData, "\n")
	graves := make([]Grave, len(lines))
	for i, line := range lines {
		var g Grave
		err := json.Unmarshal([]byte(line), &g)
		if err != nil {
			panic(err)
		}
		graves[i] = g
	}
	return graves
}

func GetGraveName(number int) string {
	for _, grave := range GetGraves() {
		if grave.Number == number {
			return grave.Name
		}
	}
	return ""
}

var graveData = `{"name":"Wendy Digby","number":110}
{"name":"Dane Brassington","number":123}
{"name":"Shepherd Seagrave","number":123}
{"name":"Clovis Bircher","number":125}
{"name":"Aldrich Borley","number":127}
{"name":"Marsden Chatburn","number":128}
{"name":"Cleveland Ridley","number":129}
{"name":"Ransford Bircher","number":130}
{"name":"Eartha Ketteringham","number":131}
{"name":"Eldridge Bickerton","number":132}
{"name":"Harold Northcott","number":134}
{"name":"Currier Bickerton","number":134}
{"name":"Dixie Delve","number":135}
{"name":"Demelza Kenley","number":136}
{"name":"Barclay Delve","number":137}
{"name":"Sandon Stanley","number":138}
{"name":"Newman Law","number":139}
{"name":"Lawson Snowdon","number":140}
{"name":"Llewellyn Darlington","number":141}
{"name":"Joy Weddington","number":142}
{"name":"Tomkin Ketteringham","number":177}
{"name":"Makepeace Copeland","number":264}
{"name":"Webster Weddington","number":288}
{"name":"Blade Atterton","number":295}
{"name":"Beacher Eastham","number":328}
{"name":"Rodney Snowdon","number":342}
{"name":"Bob Brassington","number":342}
{"name":"Kenley Riley","number":352}
{"name":"Orman Brady","number":399}
{"name":"Selby Arscott","number":404}
{"name":"Rylan Eastham","number":407}
{"name":"Esmond Stanley","number":429}
{"name":"Piper Copeland","number":438}
{"name":"Ransley Sutcliffe","number":452}
{"name":"Osmond Tinley","number":453}
{"name":"Trudy Chatburn","number":454}
{"name":"Knox Brady","number":455}
{"name":"Brook Alston","number":501}
{"name":"Wylie Tinley","number":502}
{"name":"Parry Seagrave","number":503}
{"name":"Woodrow Ketteringham","number":585}
{"name":"Love Bircher","number":654}
{"name":"Garyson Riley","number":671}
{"name":"Carrington Shakerley","number":680}
{"name":"Grayson Snowdon","number":691}
{"name":"Wilona Clayden","number":713}
{"name":"Spike Clayden","number":716}
{"name":"Shipley Kimberley","number":734}
{"name":"Hadley Tetlow","number":745}
{"name":"Honey Kenley","number":750}
{"name":"Payton Borley","number":752}
{"name":"Ida Digby","number":753}
{"name":"Eaton Ridley","number":780}
{"name":"Denver Lockwood","number":785}
{"name":"Baron Bromley","number":794}
{"name":"Harden Tetlow","number":795}
{"name":"Claiborne Delve","number":875}
{"name":"Wheaton Seagrave","number":885}
{"name":"Dawn Brady","number":886}
{"name":"Kenton Hayes","number":890}
{"name":"Thistle Riley","number":891}
{"name":"Dudley Snowdon","number":892}
{"name":"Dalton Law","number":893}
{"name":"Oswald Sherrington","number":894}
{"name":"Rochester Addington","number":895}
{"name":"Nyle Cocksedge","number":897}
{"name":"Scott Addington","number":898}
{"name":"Wheeler Brassington","number":899}
{"name":"Ward Bircher","number":900}
{"name":"Booth Northcott","number":901}
{"name":"Free Copeland","number":902}
{"name":"Darwin Seagrave","number":903}
{"name":"Atherol Oakes","number":904}
{"name":"Sherlock Oakes","number":905}
{"name":"Sherman Clayden","number":906}
{"name":"Litton Lockwood","number":907}
{"name":"Maitland Oakes","number":908}
{"name":"Sanford Alston","number":909}
{"name":"Shirley Copeland","number":910}
{"name":"Kipp Sutcliffe","number":911}
{"name":"Burne Clayden","number":912}
{"name":"Addison Bickerton","number":913}
{"name":"Yedda Sandilands","number":914}
{"name":"Fleming Shakerley","number":915}
{"name":"Kim Wheatley","number":920}
{"name":"Dorset Darlington","number":921}
{"name":"Winthrop Oakes","number":922}
{"name":"Rawlins Sandilands","number":923}
{"name":"Dover Eastham","number":924}
{"name":"Wilfred Hayes","number":925}
{"name":"Ravinger Digby","number":926}
{"name":"Huntley Wheatley","number":927}
{"name":"Winston Riley","number":928}
{"name":"Chad Bromley","number":929}
{"name":"Locke Cherrington","number":930}
{"name":"Donald Chatburn","number":931}
{"name":"Garrick Tinley","number":932}
{"name":"Linwood Sandilands","number":933}
{"name":"Pell Kenley","number":934}
{"name":"Tranter Arscott","number":935}
{"name":"Oswald Atterton","number":936}
{"name":"Ena Cocksedge","number":937}
{"name":"Chilton Kenley","number":938}
{"name":"Jonesy Weddington","number":939}
{"name":"Cynric Cherrington","number":940}
{"name":"Thane Ridley","number":941}
{"name":"Broderick Atterton","number":942}
{"name":"Kenelm Darlington","number":943}
{"name":"Remington Borley","number":944}
{"name":"Corin Northcott","number":945}
{"name":"Keyon Cherrington","number":946}
{"name":"Palma Tinley","number":947}
{"name":"Whitfield Alston","number":948}
{"name":"Brayden Stanley","number":949}
{"name":"Creighton Blackwood","number":950}
{"name":"Harvey Sandilands","number":951}
{"name":"Carling Shakerley","number":952}
{"name":"Kinsey Brady","number":953}
{"name":"Nelson Arscott","number":954}
{"name":"Townsend Lockwood","number":955}
{"name":"Stokley Digby","number":956}
{"name":"Kent Wheatley","number":957}
{"name":"Tripp Cocksedge","number":958}
{"name":"Corliss Law","number":959}
{"name":"Carver Chatburn","number":960}
{"name":"Lincoln Kimberley","number":961}
{"name":"Putnam Atterton","number":962}
{"name":"Goldie Cocksedge","number":963}
{"name":"Blossom Hayes","number":964}
{"name":"Emerson Blackwood","number":965}
{"name":"Vala Eastham","number":966}
{"name":"Bancroft Tetlow","number":967}
{"name":"Colton Bromley","number":968}
{"name":"Digby Brassington","number":969}
{"name":"Winslow Shakerley","number":970}
{"name":"Whitney Blackwood","number":971}
{"name":"Daralis Bromley","number":972}
{"name":"Heather Southcott","number":973}
{"name":"Dwennon Sutcliffe","number":974}
{"name":"Averill Ketteringham","number":975}
{"name":"Booker Blackwood","number":976}
{"name":"Ralph Bickerton","number":977}
{"name":"Walton Weddington","number":978}
{"name":"Beaman Stanley","number":979}
{"name":"Tatum Kimberley","number":980}
{"name":"Devon Arscott","number":981}
{"name":"Washington Borley","number":982}
{"name":"Alcott Ridley","number":983}
{"name":"Hunter Southcott","number":984}
{"name":"Gytha Sutcliffe","number":985}
{"name":"Terrel Kimberley","number":986}
{"name":"Audrey Addington","number":987}
{"name":"Penn Alston","number":988}
{"name":"Alfred Lockwood","number":989}
{"name":"Misty Sandilands","number":990}
{"name":"Telford Darlington","number":991}
{"name":"Ford Addington","number":992}
{"name":"Hilton Law","number":993}
{"name":"Edlyn Delve","number":994}
{"name":"Winter Cherrington","number":995}
{"name":"Rutherford Whitewood","number":996}
{"name":"Maida Seagrave","number":997}
{"name":"Dempster Wheatley","number":998}
{"name":"Tyne Hayes","number":999}`
