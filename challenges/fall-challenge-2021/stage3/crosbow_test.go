package stage3

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCrosbowOwnerName(t *testing.T) {
	nodes := GetNodes()
	name := CrosbowOwnerName(nodes)
	assert.Empty(t, name)
}
