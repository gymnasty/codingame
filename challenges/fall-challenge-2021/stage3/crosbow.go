package stage3

import "fmt"

func CrosbowOwnerName(nodes []Node) string {
	// 子孫の数が 48 である男の名前を見つける

	nodeMap := make(map[float64]*Node, len(nodes))
	for i := range nodes {
		n := nodes[i]
		nodeMap[n.Id] = &n
	}

	var getDescendants func(n Node) []float64
	getDescendants = func(n Node) []float64 {
		descendants := make([]float64, 0)
		for _, id := range n.ChildIds {
			childId := float64(id)
			descendants = append(descendants, childId)

			childNode := nodeMap[childId]
			dd := getDescendants(*childNode)
			descendants = append(descendants, dd...)
		}
		return descendants
	}

	candidates := make([]string, 0)
	for _, n := range nodes {
		// 女の人はスキップ
		if n.Gender == "F" {
			continue
		}
		descendants := getDescendants(n)
		if len(descendants) == 48 {
			candidates = append(candidates, n.Name)
		}
	}

	if len(candidates) == 1 {
		return candidates[0]
	}

	fmt.Println(candidates)
	return "fail"
}
