package stage3

import (
	"encoding/json"
	"strings"
)

// A node in this family tree.
type Node struct {
	Id       float64 `json:"id"`
	Name     string  `json:"name"`
	FatherId float64 `json:"fatherId"`
	MotherId float64 `json:"motherId"`
	SpouseId float64 `json:"spouseId"`
	Gender   string  `json:"gender"`
	ChildIds []int   `json:"childIds"`
}

func GetNodes() []Node {
	lines := strings.Split(nodeData, "\n")
	nodes := make([]Node, len(lines))
	for i, line := range lines {
		var n Node
		err := json.Unmarshal([]byte(line), &n)
		if err != nil {
			panic(err)
		}
		nodes[i] = n
	}
	return nodes
}

var nodeData = `{"id":0,"name":"Hunter Northcott","fatherId":-1,"motherId":-1,"spouseId":2,"gender":"M","childIds":[3,4,5]}
{"id":1,"name":"Rutherford Blackwood","fatherId":-1,"motherId":-1,"spouseId":6,"gender":"M","childIds":[7]}
{"id":2,"name":"Dawn Northcott","fatherId":-1,"motherId":-1,"spouseId":0,"gender":"F","childIds":[3,4,5]}
{"id":3,"name":"Darwin Northcott","fatherId":0,"motherId":2,"spouseId":7,"gender":"M","childIds":[8,9,14,15]}
{"id":4,"name":"Wendy Brassington","fatherId":0,"motherId":2,"spouseId":10,"gender":"F","childIds":[]}
{"id":5,"name":"Remington Northcott","fatherId":0,"motherId":2,"spouseId":11,"gender":"M","childIds":[12,13]}
{"id":6,"name":"Misty Blackwood","fatherId":-1,"motherId":-1,"spouseId":1,"gender":"F","childIds":[7]}
{"id":7,"name":"Maida Northcott","fatherId":1,"motherId":6,"spouseId":3,"gender":"F","childIds":[8,9,14,15]}
{"id":8,"name":"Sherman Northcott","fatherId":3,"motherId":7,"spouseId":16,"gender":"M","childIds":[17]}
{"id":9,"name":"Payton Addington","fatherId":3,"motherId":7,"spouseId":18,"gender":"F","childIds":[19,20,21]}
{"id":10,"name":"Winston Brassington","fatherId":-1,"motherId":-1,"spouseId":4,"gender":"M","childIds":[]}
{"id":11,"name":"Tatum Northcott","fatherId":-1,"motherId":-1,"spouseId":5,"gender":"F","childIds":[12,13]}
{"id":12,"name":"Grayson Northcott","fatherId":5,"motherId":11,"spouseId":22,"gender":"M","childIds":[23,24,25]}
{"id":13,"name":"Cynric Northcott","fatherId":5,"motherId":11,"spouseId":26,"gender":"M","childIds":[27,28,29]}
{"id":14,"name":"Corliss Tinley","fatherId":3,"motherId":7,"spouseId":30,"gender":"F","childIds":[]}
{"id":15,"name":"Vala Atterton","fatherId":3,"motherId":7,"spouseId":31,"gender":"F","childIds":[32]}
{"id":16,"name":"Demelza Northcott","fatherId":-1,"motherId":-1,"spouseId":8,"gender":"F","childIds":[17]}
{"id":17,"name":"Baron Northcott","fatherId":8,"motherId":16,"spouseId":33,"gender":"M","childIds":[34]}
{"id":18,"name":"Tranter Addington","fatherId":-1,"motherId":-1,"spouseId":9,"gender":"M","childIds":[19,20,21]}
{"id":19,"name":"Eaton Addington","fatherId":18,"motherId":9,"spouseId":35,"gender":"M","childIds":[]}
{"id":20,"name":"Huntley Addington","fatherId":18,"motherId":9,"spouseId":-1,"gender":"M","childIds":[]}
{"id":21,"name":"Winter Kenley","fatherId":18,"motherId":9,"spouseId":36,"gender":"F","childIds":[37]}
{"id":22,"name":"Audrey Northcott","fatherId":-1,"motherId":-1,"spouseId":12,"gender":"F","childIds":[23,24,25]}
{"id":23,"name":"Daralis Digby","fatherId":12,"motherId":22,"spouseId":38,"gender":"F","childIds":[]}
{"id":24,"name":"Winthrop Northcott","fatherId":12,"motherId":22,"spouseId":39,"gender":"M","childIds":[40]}
{"id":25,"name":"Joy Alton","fatherId":12,"motherId":22,"spouseId":41,"gender":"F","childIds":[]}
{"id":26,"name":"Goldie Northcott","fatherId":-1,"motherId":-1,"spouseId":13,"gender":"F","childIds":[27,28,29]}
{"id":27,"name":"Love Northcott","fatherId":13,"motherId":26,"spouseId":-1,"gender":"F","childIds":[]}
{"id":28,"name":"Blade Northcott","fatherId":13,"motherId":26,"spouseId":42,"gender":"M","childIds":[]}
{"id":29,"name":"Shirley Maxey","fatherId":13,"motherId":26,"spouseId":43,"gender":"F","childIds":[44,45,46]}
{"id":30,"name":"Beacher Tinley","fatherId":-1,"motherId":-1,"spouseId":14,"gender":"M","childIds":[]}
{"id":31,"name":"Free Atterton","fatherId":-1,"motherId":-1,"spouseId":15,"gender":"M","childIds":[32]}
{"id":32,"name":"Shandy Atterton","fatherId":31,"motherId":15,"spouseId":47,"gender":"F","childIds":[]}
{"id":33,"name":"Thistle Northcott","fatherId":-1,"motherId":-1,"spouseId":17,"gender":"F","childIds":[34]}
{"id":34,"name":"Locke Brindley","fatherId":17,"motherId":33,"spouseId":48,"gender":"F","childIds":[49,50]}
{"id":35,"name":"Eartha Addington","fatherId":-1,"motherId":-1,"spouseId":19,"gender":"F","childIds":[]}
{"id":36,"name":"Payton Kenley","fatherId":-1,"motherId":-1,"spouseId":21,"gender":"M","childIds":[37]}
{"id":37,"name":"Kim Swancott","fatherId":36,"motherId":21,"spouseId":51,"gender":"F","childIds":[]}
{"id":38,"name":"Carrington Digby","fatherId":-1,"motherId":-1,"spouseId":23,"gender":"M","childIds":[]}
{"id":39,"name":"Clovis Northcott","fatherId":-1,"motherId":-1,"spouseId":24,"gender":"F","childIds":[40]}
{"id":40,"name":"Spike Northcott","fatherId":24,"motherId":39,"spouseId":52,"gender":"M","childIds":[53,54,55]}
{"id":41,"name":"Wheeler Alton","fatherId":-1,"motherId":-1,"spouseId":25,"gender":"M","childIds":[]}
{"id":42,"name":"Heather Northcott","fatherId":-1,"motherId":-1,"spouseId":28,"gender":"F","childIds":[]}
{"id":43,"name":"Rodney Maxey","fatherId":-1,"motherId":-1,"spouseId":29,"gender":"M","childIds":[44,45,46]}
{"id":44,"name":"Fern Hayden","fatherId":43,"motherId":29,"spouseId":56,"gender":"F","childIds":[57,58]}
{"id":45,"name":"Keyon Maxey","fatherId":43,"motherId":29,"spouseId":59,"gender":"M","childIds":[60]}
{"id":46,"name":"Kinsey Maxey","fatherId":43,"motherId":29,"spouseId":61,"gender":"M","childIds":[62]}
{"id":47,"name":"Chilton Atterton","fatherId":-1,"motherId":-1,"spouseId":32,"gender":"M","childIds":[]}
{"id":48,"name":"Woodrow Brindley","fatherId":-1,"motherId":-1,"spouseId":34,"gender":"M","childIds":[49,50]}
{"id":49,"name":"Colton Brindley","fatherId":48,"motherId":34,"spouseId":63,"gender":"M","childIds":[64,65]}
{"id":50,"name":"Nelson Brindley","fatherId":48,"motherId":34,"spouseId":66,"gender":"M","childIds":[67,68]}
{"id":51,"name":"Thane Swancott","fatherId":-1,"motherId":-1,"spouseId":37,"gender":"M","childIds":[]}
{"id":52,"name":"Poppy Northcott","fatherId":-1,"motherId":-1,"spouseId":40,"gender":"F","childIds":[53,54,55]}
{"id":53,"name":"Edsel Scarboro","fatherId":40,"motherId":52,"spouseId":69,"gender":"F","childIds":[70,71]}
{"id":54,"name":"Ward Northcott","fatherId":40,"motherId":52,"spouseId":-1,"gender":"M","childIds":[]}
{"id":55,"name":"Maitland Northcott","fatherId":40,"motherId":52,"spouseId":72,"gender":"M","childIds":[73,74]}
{"id":56,"name":"Telford Hayden","fatherId":-1,"motherId":-1,"spouseId":44,"gender":"M","childIds":[57,58]}
{"id":57,"name":"Baxter Atherton","fatherId":56,"motherId":44,"spouseId":75,"gender":"F","childIds":[76,77]}
{"id":58,"name":"Jillian Morley","fatherId":56,"motherId":44,"spouseId":78,"gender":"F","childIds":[]}
{"id":59,"name":"Missy Maxey","fatherId":-1,"motherId":-1,"spouseId":45,"gender":"F","childIds":[60]}
{"id":60,"name":"Makepeace Maxey","fatherId":45,"motherId":59,"spouseId":79,"gender":"M","childIds":[80,81]}
{"id":61,"name":"Winifred Maxey","fatherId":-1,"motherId":-1,"spouseId":46,"gender":"F","childIds":[62]}
{"id":62,"name":"Ashley Asheton","fatherId":46,"motherId":61,"spouseId":82,"gender":"F","childIds":[83,84]}
{"id":63,"name":"Ember Brindley","fatherId":-1,"motherId":-1,"spouseId":49,"gender":"F","childIds":[64,65]}
{"id":64,"name":"Marsden Brindley","fatherId":49,"motherId":63,"spouseId":85,"gender":"M","childIds":[]}
{"id":65,"name":"Amberjill Ashley","fatherId":49,"motherId":63,"spouseId":86,"gender":"F","childIds":[87,88,89]}
{"id":66,"name":"Queena Brindley","fatherId":-1,"motherId":-1,"spouseId":50,"gender":"F","childIds":[67,68]}
{"id":67,"name":"Parry Brindley","fatherId":50,"motherId":66,"spouseId":90,"gender":"M","childIds":[91,92,93]}
{"id":68,"name":"Aldercy Sanford","fatherId":50,"motherId":66,"spouseId":94,"gender":"F","childIds":[95,96,97]}
{"id":69,"name":"Aldrich Scarboro","fatherId":-1,"motherId":-1,"spouseId":53,"gender":"M","childIds":[70,71]}
{"id":70,"name":"Ulla Redburn","fatherId":69,"motherId":53,"spouseId":98,"gender":"F","childIds":[]}
{"id":71,"name":"Hadley Scarboro","fatherId":69,"motherId":53,"spouseId":99,"gender":"M","childIds":[100]}
{"id":72,"name":"Hazel Northcott","fatherId":-1,"motherId":-1,"spouseId":55,"gender":"F","childIds":[73,74]}
{"id":73,"name":"Garyson Northcott","fatherId":55,"motherId":72,"spouseId":101,"gender":"M","childIds":[102]}
{"id":74,"name":"Lincoln Northcott","fatherId":55,"motherId":72,"spouseId":103,"gender":"M","childIds":[]}
{"id":75,"name":"Dudley Atherton","fatherId":-1,"motherId":-1,"spouseId":57,"gender":"M","childIds":[76,77]}
{"id":76,"name":"Winter Atherton","fatherId":75,"motherId":57,"spouseId":-1,"gender":"M","childIds":[]}
{"id":77,"name":"Fleta Leet","fatherId":75,"motherId":57,"spouseId":104,"gender":"F","childIds":[105]}
{"id":78,"name":"Whitfield Morley","fatherId":-1,"motherId":-1,"spouseId":58,"gender":"M","childIds":[]}
{"id":79,"name":"Gleda Maxey","fatherId":-1,"motherId":-1,"spouseId":60,"gender":"F","childIds":[80,81]}
{"id":80,"name":"Cleveland Maxey","fatherId":60,"motherId":79,"spouseId":106,"gender":"M","childIds":[]}
{"id":81,"name":"Kent Maxey","fatherId":60,"motherId":79,"spouseId":107,"gender":"M","childIds":[108]}
{"id":82,"name":"Atherol Asheton","fatherId":-1,"motherId":-1,"spouseId":62,"gender":"M","childIds":[83,84]}
{"id":83,"name":"Columbia Abdon","fatherId":82,"motherId":62,"spouseId":109,"gender":"F","childIds":[]}
{"id":84,"name":"Hilton Asheton","fatherId":82,"motherId":62,"spouseId":-1,"gender":"M","childIds":[]}
{"id":85,"name":"Edolie Brindley","fatherId":-1,"motherId":-1,"spouseId":64,"gender":"F","childIds":[]}
{"id":86,"name":"Brayden Ashley","fatherId":-1,"motherId":-1,"spouseId":65,"gender":"M","childIds":[87,88,89]}
{"id":87,"name":"Dover Ashley","fatherId":86,"motherId":65,"spouseId":110,"gender":"M","childIds":[]}
{"id":88,"name":"Dwennon Ashley","fatherId":86,"motherId":65,"spouseId":111,"gender":"M","childIds":[112,113,114]}
{"id":89,"name":"Wendy Birkenshaw","fatherId":86,"motherId":65,"spouseId":115,"gender":"F","childIds":[]}
{"id":90,"name":"Whitney Brindley","fatherId":-1,"motherId":-1,"spouseId":67,"gender":"F","childIds":[91,92,93]}
{"id":91,"name":"Misty Hilburn","fatherId":67,"motherId":90,"spouseId":116,"gender":"F","childIds":[117,118]}
{"id":92,"name":"Emerson Brindley","fatherId":67,"motherId":90,"spouseId":119,"gender":"M","childIds":[]}
{"id":93,"name":"Blossom Allerton","fatherId":67,"motherId":90,"spouseId":120,"gender":"F","childIds":[121,122,123]}
{"id":94,"name":"Wheaton Sanford","fatherId":-1,"motherId":-1,"spouseId":68,"gender":"M","childIds":[95,96,97]}
{"id":95,"name":"Carrington Emsworth","fatherId":94,"motherId":68,"spouseId":124,"gender":"F","childIds":[125,126]}
{"id":96,"name":"Washington Sanford","fatherId":94,"motherId":68,"spouseId":127,"gender":"M","childIds":[128]}
{"id":97,"name":"Brook Tyndall","fatherId":94,"motherId":68,"spouseId":129,"gender":"F","childIds":[130]}
{"id":98,"name":"Winslow Redburn","fatherId":-1,"motherId":-1,"spouseId":70,"gender":"M","childIds":[]}
{"id":99,"name":"Corliss Scarboro","fatherId":-1,"motherId":-1,"spouseId":71,"gender":"F","childIds":[100]}
{"id":100,"name":"Burne Scarboro","fatherId":71,"motherId":99,"spouseId":131,"gender":"M","childIds":[132,133]}
{"id":101,"name":"Demelza Northcott","fatherId":-1,"motherId":-1,"spouseId":73,"gender":"F","childIds":[102]}
{"id":102,"name":"Yedda Stockley","fatherId":73,"motherId":101,"spouseId":134,"gender":"F","childIds":[135,136]}
{"id":103,"name":"Carling Northcott","fatherId":-1,"motherId":-1,"spouseId":74,"gender":"F","childIds":[]}
{"id":104,"name":"Dixie Leet","fatherId":-1,"motherId":-1,"spouseId":77,"gender":"M","childIds":[105]}
{"id":105,"name":"Claiborne Becker","fatherId":104,"motherId":77,"spouseId":137,"gender":"F","childIds":[138,139]}
{"id":106,"name":"Winter Maxey","fatherId":-1,"motherId":-1,"spouseId":80,"gender":"F","childIds":[]}
{"id":107,"name":"Audrey Maxey","fatherId":-1,"motherId":-1,"spouseId":81,"gender":"F","childIds":[108]}
{"id":108,"name":"Chad Maxey","fatherId":81,"motherId":107,"spouseId":-1,"gender":"M","childIds":[]}
{"id":109,"name":"Alcott Abdon","fatherId":-1,"motherId":-1,"spouseId":83,"gender":"M","childIds":[]}
{"id":110,"name":"Joy Ashley","fatherId":-1,"motherId":-1,"spouseId":87,"gender":"F","childIds":[]}
{"id":111,"name":"Goldie Ashley","fatherId":-1,"motherId":-1,"spouseId":88,"gender":"F","childIds":[112,113,114]}
{"id":112,"name":"Love Patmore","fatherId":88,"motherId":111,"spouseId":140,"gender":"F","childIds":[141,142]}
{"id":113,"name":"Dorset Ashley","fatherId":88,"motherId":111,"spouseId":143,"gender":"M","childIds":[144,145]}
{"id":114,"name":"Dalton Ashley","fatherId":88,"motherId":111,"spouseId":146,"gender":"M","childIds":[]}
{"id":115,"name":"Wylie Birkenshaw","fatherId":-1,"motherId":-1,"spouseId":89,"gender":"M","childIds":[]}
{"id":116,"name":"Broderick Hilburn","fatherId":-1,"motherId":-1,"spouseId":91,"gender":"M","childIds":[117,118]}
{"id":117,"name":"Esmond Hilburn","fatherId":116,"motherId":91,"spouseId":147,"gender":"M","childIds":[148,149]}
{"id":118,"name":"Rylan Hilburn","fatherId":116,"motherId":91,"spouseId":150,"gender":"M","childIds":[151,152]}
{"id":119,"name":"Locke Brindley","fatherId":-1,"motherId":-1,"spouseId":92,"gender":"F","childIds":[]}
{"id":120,"name":"Kipp Allerton","fatherId":-1,"motherId":-1,"spouseId":93,"gender":"M","childIds":[121,122,123]}
{"id":121,"name":"Selby Breeden","fatherId":120,"motherId":93,"spouseId":153,"gender":"F","childIds":[]}
{"id":122,"name":"Stokley Allerton","fatherId":120,"motherId":93,"spouseId":154,"gender":"M","childIds":[155,156,157]}
{"id":123,"name":"Donald Allerton","fatherId":120,"motherId":93,"spouseId":158,"gender":"M","childIds":[159,160,161]}
{"id":124,"name":"Booker Emsworth","fatherId":-1,"motherId":-1,"spouseId":95,"gender":"M","childIds":[125,126]}
{"id":125,"name":"Piper Warwick","fatherId":124,"motherId":95,"spouseId":162,"gender":"F","childIds":[163,164,165]}
{"id":126,"name":"Honey Hackney","fatherId":124,"motherId":95,"spouseId":166,"gender":"F","childIds":[]}
{"id":127,"name":"Heather Sanford","fatherId":-1,"motherId":-1,"spouseId":96,"gender":"F","childIds":[128]}
{"id":128,"name":"Lyndon Sanford","fatherId":96,"motherId":127,"spouseId":167,"gender":"M","childIds":[168]}
{"id":129,"name":"Blake Tyndall","fatherId":-1,"motherId":-1,"spouseId":97,"gender":"M","childIds":[130]}
{"id":130,"name":"Radella Blakeley","fatherId":129,"motherId":97,"spouseId":169,"gender":"F","childIds":[170]}
{"id":131,"name":"Landon Scarboro","fatherId":-1,"motherId":-1,"spouseId":100,"gender":"F","childIds":[132,133]}
{"id":132,"name":"Pierson Scarboro","fatherId":100,"motherId":131,"spouseId":171,"gender":"M","childIds":[]}
{"id":133,"name":"Leigh Scarboro","fatherId":100,"motherId":131,"spouseId":-1,"gender":"F","childIds":[]}
{"id":134,"name":"Rishley Stockley","fatherId":-1,"motherId":-1,"spouseId":102,"gender":"M","childIds":[135,136]}
{"id":135,"name":"Watson Stockley","fatherId":134,"motherId":102,"spouseId":172,"gender":"M","childIds":[173]}
{"id":136,"name":"Kaelyn Morton","fatherId":134,"motherId":102,"spouseId":174,"gender":"F","childIds":[]}
{"id":137,"name":"North Becker","fatherId":-1,"motherId":-1,"spouseId":105,"gender":"M","childIds":[138,139]}
{"id":138,"name":"Edsel Glasscock","fatherId":137,"motherId":105,"spouseId":175,"gender":"F","childIds":[176]}
{"id":139,"name":"Jagger Becker","fatherId":137,"motherId":105,"spouseId":177,"gender":"M","childIds":[]}
{"id":140,"name":"Landon Patmore","fatherId":-1,"motherId":-1,"spouseId":112,"gender":"M","childIds":[141,142]}
{"id":141,"name":"Wallace Patmore","fatherId":140,"motherId":112,"spouseId":-1,"gender":"M","childIds":[]}
{"id":142,"name":"Beardsley Patmore","fatherId":140,"motherId":112,"spouseId":178,"gender":"M","childIds":[]}
{"id":143,"name":"Jillian Ashley","fatherId":-1,"motherId":-1,"spouseId":113,"gender":"F","childIds":[144,145]}
{"id":144,"name":"Calder Ashley","fatherId":113,"motherId":143,"spouseId":179,"gender":"M","childIds":[180,181,182]}
{"id":145,"name":"Luella Popplewell","fatherId":113,"motherId":143,"spouseId":183,"gender":"F","childIds":[]}
{"id":146,"name":"Winifred Ashley","fatherId":-1,"motherId":-1,"spouseId":114,"gender":"F","childIds":[]}
{"id":147,"name":"Ashley Hilburn","fatherId":-1,"motherId":-1,"spouseId":117,"gender":"F","childIds":[148,149]}
{"id":148,"name":"Millard Hilburn","fatherId":117,"motherId":147,"spouseId":184,"gender":"M","childIds":[185,186]}
{"id":149,"name":"Dayton Hilburn","fatherId":117,"motherId":147,"spouseId":187,"gender":"M","childIds":[]}
{"id":150,"name":"Amberjill Hilburn","fatherId":-1,"motherId":-1,"spouseId":118,"gender":"F","childIds":[151,152]}
{"id":151,"name":"Queena Langley","fatherId":118,"motherId":150,"spouseId":188,"gender":"F","childIds":[189,190,191]}
{"id":152,"name":"Burt Hilburn","fatherId":118,"motherId":150,"spouseId":192,"gender":"M","childIds":[193,194]}
{"id":153,"name":"Oxford Breeden","fatherId":-1,"motherId":-1,"spouseId":121,"gender":"M","childIds":[]}
{"id":154,"name":"Gypsy Allerton","fatherId":-1,"motherId":-1,"spouseId":122,"gender":"F","childIds":[155,156,157]}
{"id":155,"name":"Waverly Allerton","fatherId":122,"motherId":154,"spouseId":195,"gender":"M","childIds":[196]}
{"id":156,"name":"Ackley Allerton","fatherId":122,"motherId":154,"spouseId":197,"gender":"M","childIds":[198]}
{"id":157,"name":"Hazel Bradly","fatherId":122,"motherId":154,"spouseId":199,"gender":"F","childIds":[200,201]}
{"id":158,"name":"Tuesday Allerton","fatherId":-1,"motherId":-1,"spouseId":123,"gender":"F","childIds":[159,160,161]}
{"id":159,"name":"Chelsea Trumble","fatherId":123,"motherId":158,"spouseId":202,"gender":"F","childIds":[203,204]}
{"id":160,"name":"Bronson Allerton","fatherId":123,"motherId":158,"spouseId":205,"gender":"M","childIds":[206,207]}
{"id":161,"name":"Gail Allerton","fatherId":123,"motherId":158,"spouseId":-1,"gender":"F","childIds":[]}
{"id":162,"name":"Lyre Warwick","fatherId":-1,"motherId":-1,"spouseId":125,"gender":"M","childIds":[163,164,165]}
{"id":163,"name":"Afton Hampton","fatherId":162,"motherId":125,"spouseId":208,"gender":"F","childIds":[209,210]}
{"id":164,"name":"Brinley Warwick","fatherId":162,"motherId":125,"spouseId":211,"gender":"M","childIds":[212,213]}
{"id":165,"name":"Maitane Shirley","fatherId":162,"motherId":125,"spouseId":214,"gender":"F","childIds":[]}
{"id":166,"name":"Forbes Hackney","fatherId":-1,"motherId":-1,"spouseId":126,"gender":"M","childIds":[]}
{"id":167,"name":"Elmar Sanford","fatherId":-1,"motherId":-1,"spouseId":128,"gender":"F","childIds":[168]}
{"id":168,"name":"Rae Sanford","fatherId":128,"motherId":167,"spouseId":215,"gender":"M","childIds":[216,217]}
{"id":169,"name":"Brewster Blakeley","fatherId":-1,"motherId":-1,"spouseId":130,"gender":"M","childIds":[170]}
{"id":170,"name":"Edolie Willoughby","fatherId":169,"motherId":130,"spouseId":218,"gender":"F","childIds":[219,220]}
{"id":171,"name":"Edlyn Scarboro","fatherId":-1,"motherId":-1,"spouseId":132,"gender":"F","childIds":[]}
{"id":172,"name":"Dawn Stockley","fatherId":-1,"motherId":-1,"spouseId":135,"gender":"F","childIds":[173]}
{"id":173,"name":"Gytha Barney","fatherId":135,"motherId":172,"spouseId":221,"gender":"F","childIds":[]}
{"id":174,"name":"Brandon Morton","fatherId":-1,"motherId":-1,"spouseId":136,"gender":"M","childIds":[]}
{"id":175,"name":"Elden Glasscock","fatherId":-1,"motherId":-1,"spouseId":138,"gender":"M","childIds":[176]}
{"id":176,"name":"Wesley Glasscock","fatherId":175,"motherId":138,"spouseId":222,"gender":"M","childIds":[223,224,225]}
{"id":177,"name":"Maida Becker","fatherId":-1,"motherId":-1,"spouseId":139,"gender":"F","childIds":[]}
{"id":178,"name":"Blossom Patmore","fatherId":-1,"motherId":-1,"spouseId":142,"gender":"F","childIds":[]}
{"id":179,"name":"Payton Ashley","fatherId":-1,"motherId":-1,"spouseId":144,"gender":"F","childIds":[180,181,182]}
{"id":180,"name":"Carrington Clayton","fatherId":144,"motherId":179,"spouseId":226,"gender":"F","childIds":[227,228,229]}
{"id":181,"name":"Lomar Ashley","fatherId":144,"motherId":179,"spouseId":230,"gender":"M","childIds":[231,232,233]}
{"id":182,"name":"Bede Ashley","fatherId":144,"motherId":179,"spouseId":234,"gender":"M","childIds":[]}
{"id":183,"name":"Brock Popplewell","fatherId":-1,"motherId":-1,"spouseId":145,"gender":"M","childIds":[]}
{"id":184,"name":"Corliss Hilburn","fatherId":-1,"motherId":-1,"spouseId":148,"gender":"F","childIds":[185,186]}
{"id":185,"name":"Elton Hilburn","fatherId":148,"motherId":184,"spouseId":235,"gender":"M","childIds":[236]}
{"id":186,"name":"Wayland Hilburn","fatherId":148,"motherId":184,"spouseId":237,"gender":"M","childIds":[238]}
{"id":187,"name":"Yedda Hilburn","fatherId":-1,"motherId":-1,"spouseId":149,"gender":"F","childIds":[]}
{"id":188,"name":"Packard Langley","fatherId":-1,"motherId":-1,"spouseId":151,"gender":"M","childIds":[189,190,191]}
{"id":189,"name":"Wilona Cove","fatherId":188,"motherId":151,"spouseId":239,"gender":"F","childIds":[]}
{"id":190,"name":"Leland Langley","fatherId":188,"motherId":151,"spouseId":-1,"gender":"M","childIds":[]}
{"id":191,"name":"Hugh Langley","fatherId":188,"motherId":151,"spouseId":240,"gender":"M","childIds":[241]}
{"id":192,"name":"Audrey Hilburn","fatherId":-1,"motherId":-1,"spouseId":152,"gender":"F","childIds":[193,194]}
{"id":193,"name":"Daralis Bloxham","fatherId":152,"motherId":192,"spouseId":242,"gender":"F","childIds":[]}
{"id":194,"name":"Devon Tooze","fatherId":152,"motherId":192,"spouseId":243,"gender":"F","childIds":[244]}
{"id":195,"name":"Joy Allerton","fatherId":-1,"motherId":-1,"spouseId":155,"gender":"F","childIds":[196]}
{"id":196,"name":"Wyndam Allerton","fatherId":155,"motherId":195,"spouseId":245,"gender":"M","childIds":[]}
{"id":197,"name":"Love Allerton","fatherId":-1,"motherId":-1,"spouseId":156,"gender":"F","childIds":[198]}
{"id":198,"name":"Palma Allerton","fatherId":156,"motherId":197,"spouseId":-1,"gender":"F","childIds":[]}
{"id":199,"name":"Penley Bradly","fatherId":-1,"motherId":-1,"spouseId":157,"gender":"M","childIds":[200,201]}
{"id":200,"name":"Dwight Bradly","fatherId":199,"motherId":157,"spouseId":246,"gender":"M","childIds":[]}
{"id":201,"name":"Trudy Kingsnorth","fatherId":199,"motherId":157,"spouseId":247,"gender":"F","childIds":[248,249,250]}
{"id":202,"name":"Olin Trumble","fatherId":-1,"motherId":-1,"spouseId":159,"gender":"M","childIds":[203,204]}
{"id":203,"name":"Farley Trumble","fatherId":202,"motherId":159,"spouseId":251,"gender":"M","childIds":[]}
{"id":204,"name":"Locke Marley","fatherId":202,"motherId":159,"spouseId":252,"gender":"F","childIds":[253,254]}
{"id":205,"name":"Eartha Allerton","fatherId":-1,"motherId":-1,"spouseId":160,"gender":"F","childIds":[206,207]}
{"id":206,"name":"Selby Huxtable","fatherId":160,"motherId":205,"spouseId":255,"gender":"F","childIds":[]}
{"id":207,"name":"Whit Allerton","fatherId":160,"motherId":205,"spouseId":256,"gender":"M","childIds":[257,258,259]}
{"id":208,"name":"Carlyle Hampton","fatherId":-1,"motherId":-1,"spouseId":163,"gender":"M","childIds":[209,210]}
{"id":209,"name":"Eldon Hampton","fatherId":208,"motherId":163,"spouseId":260,"gender":"M","childIds":[261,262]}
{"id":210,"name":"Rodman Hampton","fatherId":208,"motherId":163,"spouseId":263,"gender":"M","childIds":[264]}
{"id":211,"name":"Honey Warwick","fatherId":-1,"motherId":-1,"spouseId":164,"gender":"F","childIds":[212,213]}
{"id":212,"name":"Carleton Warwick","fatherId":164,"motherId":211,"spouseId":265,"gender":"M","childIds":[266]}
{"id":213,"name":"Holly Tickner","fatherId":164,"motherId":211,"spouseId":267,"gender":"F","childIds":[268,269]}
{"id":214,"name":"Siddel Shirley","fatherId":-1,"motherId":-1,"spouseId":165,"gender":"M","childIds":[]}
{"id":215,"name":"Radella Sanford","fatherId":-1,"motherId":-1,"spouseId":168,"gender":"F","childIds":[216,217]}
{"id":216,"name":"Cameron Sanford","fatherId":168,"motherId":215,"spouseId":270,"gender":"M","childIds":[271,272]}
{"id":217,"name":"West Sanford","fatherId":168,"motherId":215,"spouseId":273,"gender":"M","childIds":[274,275]}
{"id":218,"name":"Charles Willoughby","fatherId":-1,"motherId":-1,"spouseId":170,"gender":"M","childIds":[219,220]}
{"id":219,"name":"Edda Willoughby","fatherId":218,"motherId":170,"spouseId":-1,"gender":"F","childIds":[]}
{"id":220,"name":"Amsden Willoughby","fatherId":218,"motherId":170,"spouseId":276,"gender":"M","childIds":[277,278]}
{"id":221,"name":"Stanley Barney","fatherId":-1,"motherId":-1,"spouseId":173,"gender":"M","childIds":[]}
{"id":222,"name":"Poppy Glasscock","fatherId":-1,"motherId":-1,"spouseId":176,"gender":"F","childIds":[223,224,225]}
{"id":223,"name":"Reginald Glasscock","fatherId":176,"motherId":222,"spouseId":279,"gender":"M","childIds":[280,281]}
{"id":224,"name":"Stroud Glasscock","fatherId":176,"motherId":222,"spouseId":282,"gender":"M","childIds":[]}
{"id":225,"name":"Harva Horsford","fatherId":176,"motherId":222,"spouseId":283,"gender":"F","childIds":[284,285]}
{"id":226,"name":"Shaw Clayton","fatherId":-1,"motherId":-1,"spouseId":180,"gender":"M","childIds":[227,228,229]}
{"id":227,"name":"Baxter Clayton","fatherId":226,"motherId":180,"spouseId":-1,"gender":"F","childIds":[]}
{"id":228,"name":"Rudyard Clayton","fatherId":226,"motherId":180,"spouseId":-1,"gender":"M","childIds":[]}
{"id":229,"name":"Missy Clayton","fatherId":226,"motherId":180,"spouseId":-1,"gender":"F","childIds":[]}
{"id":230,"name":"Luella Ashley","fatherId":-1,"motherId":-1,"spouseId":181,"gender":"F","childIds":[231,232,233]}
{"id":231,"name":"Winifred Ashley","fatherId":181,"motherId":230,"spouseId":-1,"gender":"F","childIds":[]}
{"id":232,"name":"Garfield Ashley","fatherId":181,"motherId":230,"spouseId":-1,"gender":"M","childIds":[]}
{"id":233,"name":"Ember Ashley","fatherId":181,"motherId":230,"spouseId":-1,"gender":"F","childIds":[]}
{"id":234,"name":"Blythe Ashley","fatherId":-1,"motherId":-1,"spouseId":182,"gender":"F","childIds":[]}
{"id":235,"name":"Amberjill Hilburn","fatherId":-1,"motherId":-1,"spouseId":185,"gender":"F","childIds":[236]}
{"id":236,"name":"Wetherby Hilburn","fatherId":185,"motherId":235,"spouseId":-1,"gender":"M","childIds":[]}
{"id":237,"name":"Cameron Hilburn","fatherId":-1,"motherId":-1,"spouseId":186,"gender":"F","childIds":[238]}
{"id":238,"name":"Aldercy Hilburn","fatherId":186,"motherId":237,"spouseId":-1,"gender":"F","childIds":[]}
{"id":239,"name":"Robert Cove","fatherId":-1,"motherId":-1,"spouseId":189,"gender":"M","childIds":[]}
{"id":240,"name":"Ulla Langley","fatherId":-1,"motherId":-1,"spouseId":191,"gender":"F","childIds":[241]}
{"id":241,"name":"Wetherby Langley","fatherId":191,"motherId":240,"spouseId":-1,"gender":"F","childIds":[]}
{"id":242,"name":"Tye Bloxham","fatherId":-1,"motherId":-1,"spouseId":193,"gender":"M","childIds":[]}
{"id":243,"name":"Carter Tooze","fatherId":-1,"motherId":-1,"spouseId":194,"gender":"M","childIds":[244]}
{"id":244,"name":"Redford Tooze","fatherId":243,"motherId":194,"spouseId":-1,"gender":"M","childIds":[]}
{"id":245,"name":"Haylee Allerton","fatherId":-1,"motherId":-1,"spouseId":196,"gender":"F","childIds":[]}
{"id":246,"name":"Gail Bradly","fatherId":-1,"motherId":-1,"spouseId":200,"gender":"F","childIds":[]}
{"id":247,"name":"Durward Kingsnorth","fatherId":-1,"motherId":-1,"spouseId":201,"gender":"M","childIds":[248,249,250]}
{"id":248,"name":"Afton Kingsnorth","fatherId":247,"motherId":201,"spouseId":-1,"gender":"F","childIds":[]}
{"id":249,"name":"Lind Kingsnorth","fatherId":247,"motherId":201,"spouseId":-1,"gender":"M","childIds":[]}
{"id":250,"name":"Manning Kingsnorth","fatherId":247,"motherId":201,"spouseId":-1,"gender":"M","childIds":[]}
{"id":251,"name":"Beverly Trumble","fatherId":-1,"motherId":-1,"spouseId":203,"gender":"F","childIds":[]}
{"id":252,"name":"Bromley Marley","fatherId":-1,"motherId":-1,"spouseId":204,"gender":"M","childIds":[253,254]}
{"id":253,"name":"Halsey Marley","fatherId":252,"motherId":204,"spouseId":-1,"gender":"M","childIds":[]}
{"id":254,"name":"Acton Marley","fatherId":252,"motherId":204,"spouseId":-1,"gender":"M","childIds":[]}
{"id":255,"name":"Chapman Huxtable","fatherId":-1,"motherId":-1,"spouseId":206,"gender":"M","childIds":[]}
{"id":256,"name":"Edlyn Allerton","fatherId":-1,"motherId":-1,"spouseId":207,"gender":"F","childIds":[257,258,259]}
{"id":257,"name":"Dawn Allerton","fatherId":207,"motherId":256,"spouseId":-1,"gender":"F","childIds":[]}
{"id":258,"name":"Wakefield Allerton","fatherId":207,"motherId":256,"spouseId":-1,"gender":"M","childIds":[]}
{"id":259,"name":"Parr Allerton","fatherId":207,"motherId":256,"spouseId":-1,"gender":"M","childIds":[]}
{"id":260,"name":"Whitney Hampton","fatherId":-1,"motherId":-1,"spouseId":209,"gender":"F","childIds":[261,262]}
{"id":261,"name":"Misty Hampton","fatherId":209,"motherId":260,"spouseId":-1,"gender":"F","childIds":[]}
{"id":262,"name":"Maida Hampton","fatherId":209,"motherId":260,"spouseId":-1,"gender":"F","childIds":[]}
{"id":263,"name":"Blossom Hampton","fatherId":-1,"motherId":-1,"spouseId":210,"gender":"F","childIds":[264]}
{"id":264,"name":"Dallin Hampton","fatherId":210,"motherId":263,"spouseId":-1,"gender":"M","childIds":[]}
{"id":265,"name":"Carrington Warwick","fatherId":-1,"motherId":-1,"spouseId":212,"gender":"F","childIds":[266]}
{"id":266,"name":"Tatum Warwick","fatherId":212,"motherId":265,"spouseId":-1,"gender":"F","childIds":[]}
{"id":267,"name":"Denton Tickner","fatherId":-1,"motherId":-1,"spouseId":213,"gender":"M","childIds":[268,269]}
{"id":268,"name":"Darrel Tickner","fatherId":267,"motherId":213,"spouseId":-1,"gender":"M","childIds":[]}
{"id":269,"name":"Corliss Tickner","fatherId":267,"motherId":213,"spouseId":-1,"gender":"F","childIds":[]}
{"id":270,"name":"Vala Sanford","fatherId":-1,"motherId":-1,"spouseId":216,"gender":"F","childIds":[271,272]}
{"id":271,"name":"Elmar Sanford","fatherId":216,"motherId":270,"spouseId":-1,"gender":"M","childIds":[]}
{"id":272,"name":"Yedda Sanford","fatherId":216,"motherId":270,"spouseId":-1,"gender":"F","childIds":[]}
{"id":273,"name":"Carling Sanford","fatherId":-1,"motherId":-1,"spouseId":217,"gender":"F","childIds":[274,275]}
{"id":274,"name":"Wilona Sanford","fatherId":217,"motherId":273,"spouseId":-1,"gender":"F","childIds":[]}
{"id":275,"name":"Dunstan Sanford","fatherId":217,"motherId":273,"spouseId":-1,"gender":"M","childIds":[]}
{"id":276,"name":"Winter Willoughby","fatherId":-1,"motherId":-1,"spouseId":220,"gender":"F","childIds":[277,278]}
{"id":277,"name":"Radcliff Willoughby","fatherId":220,"motherId":276,"spouseId":-1,"gender":"M","childIds":[]}
{"id":278,"name":"Rigby Willoughby","fatherId":220,"motherId":276,"spouseId":-1,"gender":"M","childIds":[]}
{"id":279,"name":"Devon Glasscock","fatherId":-1,"motherId":-1,"spouseId":223,"gender":"F","childIds":[280,281]}
{"id":280,"name":"Kimberley Glasscock","fatherId":223,"motherId":279,"spouseId":-1,"gender":"M","childIds":[]}
{"id":281,"name":"Goldie Glasscock","fatherId":223,"motherId":279,"spouseId":-1,"gender":"F","childIds":[]}
{"id":282,"name":"Love Glasscock","fatherId":-1,"motherId":-1,"spouseId":224,"gender":"F","childIds":[]}
{"id":283,"name":"Harman Horsford","fatherId":-1,"motherId":-1,"spouseId":225,"gender":"M","childIds":[284,285]}
{"id":284,"name":"Alvin Horsford","fatherId":283,"motherId":225,"spouseId":-1,"gender":"M","childIds":[]}
{"id":285,"name":"Miller Horsford","fatherId":283,"motherId":225,"spouseId":-1,"gender":"M","childIds":[]}`
