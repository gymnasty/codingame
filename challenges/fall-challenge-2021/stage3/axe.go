package stage3

import "fmt"

func AxeOwnerName(nodes []Node) string {
	// 唯一の子供の孫（男）のいとこ（女）で子供がひとりの人を探す

	nodeMap := make(map[float64]*Node, len(nodes))
	for i := range nodes {
		n := nodes[i]
		nodeMap[n.Id] = &n
	}

	candidates := make(map[string]struct{})

	for _, n := range nodes {
		// 子供が1でない人は無視
		if len(n.ChildIds) != 1 {
			continue
		}

		// 子供の孫を探す
		onlyChild := nodeMap[float64(n.ChildIds[0])]
		grandChildrenMap := make(map[float64]*Node, len(nodes))
		for _, childId := range onlyChild.ChildIds {
			id := float64(childId)
			child := nodeMap[id]
			for _, grandChildId := range child.ChildIds {
				id := float64(grandChildId)
				grandChild := nodeMap[id]
				// 孫を格納
				grandChildrenMap[grandChild.Id] = grandChild
			}
		}

		// 孫の中に男と女がいる
		var hasGrandson bool
		for _, n := range grandChildrenMap {
			if n.Gender == "M" {
				hasGrandson = true
			}
		}
		if !hasGrandson {
			// 男がいなければ対象でない
			continue
		}

		// 女の人には子供が一人しかいない
		for _, n := range grandChildrenMap {
			if n.Gender == "F" && len(n.ChildIds) == 1 {
				candidates[n.Name] = struct{}{}
			}
		}
	}

	if len(candidates) == 1 {
		for k := range candidates {
			return k
		}
	}

	fmt.Println(len(candidates))
	fmt.Println(candidates)
	return "fail"
}
