package stage1

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDoubleTriangleHeadStone(t *testing.T) {
	ans := DoubleTriangleHeadStone(wall)
	assert.Empty(t, ans)
}
