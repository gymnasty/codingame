package stage1

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTriangleHeadStone(t *testing.T) {
	ans := TriangleHeadStone(letters)
	assert.Empty(t, ans)
}
