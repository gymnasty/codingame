package stage1

import (
	"strings"
)

// A wall with a magical path between the stones.
const wall = `                                                                                                                        
                                                                                                                        
                                                                                                                        
                                                                                                                        
                                                                                                                        
                                                                                                                        
                                                                                          _       _                   _|
                                                                                        _| |_   _| |_   _           _|  
              _       _   _   _   _                   _   _                   _   _   _|     |_|     |_| |_   _   _|    
            _| |_   _| |_| |_| |_| |_   _       _   _| |_| |_               _| |_| |_|                     |_| |_|      
      _   _|     |_|                 |_| |_   _| |_|         |_           _|                                            
    _| |_|                                 |_|                 |_   _   _|                                              
  _|                                                             |_| |_|                                                
_|                                                                                                                      
                                                                                                                        
                                                                                                                        
                                                                                                                        
                                                                                                                        
                                                                                                                        
                                                                                                                        

`

// head stone with double triangle mark
func DoubleTriangleHeadStone(wall string) []string {
	ans := make([]string, 0)
	// split lines
	wallLines := strings.Split(wall, "\n")
	// transpose
	transpose := make([]string, len(wallLines[0]))
	for _, line := range wallLines {
		for i, c := range line {
			transpose[i] += string([]rune{c})
		}
	}
	// UP/DOWN の判定
	for i, horizontalLine := range transpose {
		for j, c := range horizontalLine {
			// _ 以外はスキップ
			if c != '_' {
				continue
			}
			// 最後の行ならスキップ
			if i >= len(transpose)-1 {
				continue
			}
			nextLine := transpose[i+1]
			// 次の行が | なら UP
			if nextLine[j] == '|' {
				ans = append(ans, "UP")
			}
			// 次の行の一つ下が | なら DOWN
			if j+1 < len(nextLine) && nextLine[j+1] == '|' {
				ans = append(ans, "DOWN")
			}
		}
	}
	return ans
}
