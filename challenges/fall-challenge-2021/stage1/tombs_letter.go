package stage1

// Some mysterious and incomprehensible letters written on these three tombs
const letters = "WETYDGWEUFGYITQFGHEJGFEQWHKJHDIQWEFQWEKJHKDHKHJQWEGHFDGWEJLJGCFQWEICGEQKJGWEL"

// head stone with triangle mark
func TriangleHeadStone(letters string) []string {
	ans := make([]string, 0)
	for _, c := range letters {
		if c == 'E' {
			ans = append(ans, "UP")
		}
		if c == 'G' {
			ans = append(ans, "DOWN")
		}
	}
	return ans
}
