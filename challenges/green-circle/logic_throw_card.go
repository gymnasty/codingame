package main

func ThrowCard(turn *Turn, commander Commander) {
	hand := turn.CardPiles.Get(CARD_PILE_TYPE_HAND)

	if releaseTarget != nil {
		// 不要なカードから捨てる
		card := FindNotNeededCard(hand, releaseTarget)
		if card != nil {
			commander.Throw(card.CardType)
			return
		}
	}

	// 不要な順に捨てる
	for _, card := range []*Card{
		TaskPrioritizationCard,
		ArchitectureStudyCard,
		DailyRoutineCard,
		BonusCard,
		CodeReviewCard,
		RefactoringCard,
		TraningCard,
		CodingCard,
		ContinuousIntegrationCard,
	} {
		if hand.Has(card, 1) {
			commander.Throw(card.CardType)
			return
		}
	}

	turn.PossibleCommands.PlayRandomly(commander)
}
