package main

// ゲームのフェーズ
type GamePhase string

const (
	MOVE       GamePhase = "MOVE"
	GIVE_CARD  GamePhase = "GIVE_CARD"
	THROW_CARD GamePhase = "THROW_CARD"
	PLAY_CARD  GamePhase = "PLAY_CARD"
	RELEASE    GamePhase = "RELEASE"
)

type Turn struct {
	GamePhase          GamePhase
	ApplicationCount   int
	Applications       Applications
	Players            Players
	CardLocationsCount int
	CardPiles          CardPiles
	PossibleCommands   PossibleCommands
}

func (t Turn) Show() {
	DebugLogLine("Trun")
	DebugLogLine("GamePhase: %s", t.GamePhase)
	//t.Applications.Show()
	//t.Players.Show()
	t.CardPiles.Show()
	t.PossibleCommands.Show()
}
