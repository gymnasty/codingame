package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReleaseRequirements_RequireSkills(t *testing.T) {
	r := ReleaseRequirements{
		SKILL_TYPE_TRAINING:               3,
		SKILL_TYPE_CODING:                 0,
		SKILL_TYPE_DAILY_ROUTINE:          2,
		SKILL_TYPE_TASK_PRIORITIZATION:    0,
		SKILL_TYPE_ARCHITECTURE_STUDY:     0,
		SKILL_TYPE_CONTINUOUS_INTEGRATION: 4,
		SKILL_TYPE_CODE_REVIEW:            1,
		SKILL_TYPE_REFACTORING:            5,
	}
	assert.Equal(
		t,
		[]SkillType{SKILL_TYPE_REFACTORING, SKILL_TYPE_CONTINUOUS_INTEGRATION, SKILL_TYPE_TRAINING, SKILL_TYPE_DAILY_ROUTINE, SKILL_TYPE_CODE_REVIEW},
		r.RequireSkills(),
	)

	r = ReleaseRequirements{
		SKILL_TYPE_TRAINING:               3,
		SKILL_TYPE_CODING:                 6,
		SKILL_TYPE_DAILY_ROUTINE:          2,
		SKILL_TYPE_TASK_PRIORITIZATION:    8,
		SKILL_TYPE_ARCHITECTURE_STUDY:     7,
		SKILL_TYPE_CONTINUOUS_INTEGRATION: 4,
		SKILL_TYPE_CODE_REVIEW:            1,
		SKILL_TYPE_REFACTORING:            5,
	}
	assert.Equal(
		t,
		[]SkillType{SKILL_TYPE_TASK_PRIORITIZATION, SKILL_TYPE_ARCHITECTURE_STUDY, SKILL_TYPE_CODING, SKILL_TYPE_REFACTORING, SKILL_TYPE_CONTINUOUS_INTEGRATION, SKILL_TYPE_TRAINING, SKILL_TYPE_DAILY_ROUTINE, SKILL_TYPE_CODE_REVIEW},
		r.RequireSkills(),
	)
}

func TestFindReleasableApplicationWithouCost2(t *testing.T) {
	app1 := &Application{ReleaseRequirements: ReleaseRequirements{
		SKILL_TYPE_CONTINUOUS_INTEGRATION: 4,
		SKILL_TYPE_CODING:                 4,
	}}
	app2 := &Application{ReleaseRequirements: ReleaseRequirements{
		SKILL_TYPE_REFACTORING:         4,
		SKILL_TYPE_TASK_PRIORITIZATION: 4,
	}}
	apps := Applications{app1, app2}

	// 手札だけで OK
	app := FindReleasableApplicationWithouCost2(
		&CardPile{Cards: CardCounts{
			ContinuousIntegrationCard: 2,
			CodingCard:                2,
		}},
		&CardPile{Cards: CardCounts{}},
		&CardPile{Cards: CardCounts{}},
		apps, false, false)
	assert.Equal(t, app1, app)

	// 全部合わせて OK
	app = FindReleasableApplicationWithouCost2(
		&CardPile{Cards: CardCounts{
			ContinuousIntegrationCard: 1,
			CodingCard:                1,
		}},
		&CardPile{Cards: CardCounts{
			CodingCard: 1,
		}},
		&CardPile{Cards: CardCounts{
			ContinuousIntegrationCard: 1,
		}},
		apps, false, false)
	assert.Equal(t, app1, app)

	// OK
	app = FindReleasableApplicationWithouCost2(
		&CardPile{Cards: CardCounts{
			CodingCard:     1,
			CodeReviewCard: 2,
			BonusCard:      1,
		}},
		&CardPile{Cards: CardCounts{
			ContinuousIntegrationCard: 1,
			BonusCard:                 1,
		}},
		&CardPile{Cards: CardCounts{
			ContinuousIntegrationCard: 1,
		}},
		apps, true, false)
	assert.Equal(t, app1, app)

	// 追加札は移動で使用できないので NG
	app = FindReleasableApplicationWithouCost2(
		&CardPile{Cards: CardCounts{
			CodingCard:                1,
			CodeReviewCard:            1,
			BonusCard:                 1,
			ContinuousIntegrationCard: 1,
		}},
		&CardPile{Cards: CardCounts{
			ContinuousIntegrationCard: 1,
			BonusCard:                 1,
		}},
		&CardPile{Cards: CardCounts{
			CodeReviewCard: 1,
		}},
		apps, true, false)
	assert.Nil(t, app)

	// 移動先で捨てるものは追加したものでも OK
	app = FindReleasableApplicationWithouCost2(
		&CardPile{Cards: CardCounts{
			ContinuousIntegrationCard: 1,
			CodingCard:                1,
			CodeReviewCard:            2,
			BonusCard:                 1,
		}},
		&CardPile{Cards: CardCounts{
			ContinuousIntegrationCard: 1,
			BonusCard:                 1,
		}},
		&CardPile{Cards: CardCounts{
			RefactoringCard: 1,
		}},
		apps, true, true)
	assert.Equal(t, app1, app)
}
