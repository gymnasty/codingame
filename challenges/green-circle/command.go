package main

import (
	"fmt"
	"math/rand"
	"strings"
)

// コマンド
type Command string

const (
	COMMAND_RANDOM              Command = "RANDOM"
	COMMAND_MOVE                Command = "MOVE"
	COMMAND_RELEASE             Command = "RELEASE"
	COMMAND_WAIT                Command = "WAIT"
	COMMAND_GIVE                Command = "GIVE"
	COMMAND_THROW               Command = "THROW"
	COMMAND_TRAINING            Command = "TRAINING"
	COMMAND_CODING              Command = "CODING"
	COMMAND_DAILY_ROUTINE       Command = "DAILY_ROUTINE"
	COMMAND_TASK_PRIORITIZATION Command = "TASK_PRIORITIZATION"
	COMMAND_ARCHITECTURE_STUDY  Command = "ARCHITECTURE_STUDY"
	COMMAND_CONTINUOUS_DELIVERY Command = "CONTINUOUS_INTEGRATION"
	COMMAND_CODE_REVIEW         Command = "CODE_REVIEW"
	COMMAND_REFACTORING         Command = "REFACTORING"
)

// 実行可能なコマンド
type PossibleCommand struct {
	Command Command
	Args    []int
}

func (c PossibleCommand) Play(commander Commander) {
	switch c.Command {
	//case COMMAND_RANDOM:
	//	commander.Random()
	case COMMAND_MOVE:
		if len(c.Args) > 1 {
			commander.Move(PlayerLocation(c.Args[0]), PlayerLocation(c.Args[1]))
		}
		commander.Move(PlayerLocation(c.Args[0]))
	case COMMAND_RELEASE:
		commander.Release(c.Args[0])
	case COMMAND_WAIT:
		commander.Wait()
	case COMMAND_GIVE:
		commander.Give(CardType(c.Args[0]))
	case COMMAND_THROW:
		commander.Throw(CardType(c.Args[0]))
	case COMMAND_TRAINING:
		commander.Traning()
	case COMMAND_CODING:
		commander.Coding()
	case COMMAND_DAILY_ROUTINE:
		commander.DailyRoutine()
	case COMMAND_TASK_PRIORITIZATION:
		commander.TaskPrioritization(CardType(c.Args[0]), CardType(c.Args[1]))
	case COMMAND_ARCHITECTURE_STUDY:
		commander.ArchitectureStudy()
	case COMMAND_CONTINUOUS_DELIVERY:
		commander.ContinuousDelivery(CardType(c.Args[0]))
	case COMMAND_CODE_REVIEW:
		commander.CodeReview()
	case COMMAND_REFACTORING:
		commander.Refactoring()
	default:
		panic(c.Command)
	}
}

func (c PossibleCommand) Show() {
	DebugLog("  %s", c.Command)
	for _, arg := range c.Args {
		DebugLog(" %d", arg)
	}
	DebugLog("\n")
}

func ParsePossibleCommand(s string) *PossibleCommand {
	p := strings.Split(s, " ")
	args := make([]int, len(p)-1)
	for i := range args {
		fmt.Sscan(p[i+1], &args[i])
	}
	return &PossibleCommand{
		Command: Command(p[0]),
		Args:    args,
	}
}

type PossibleCommands []*PossibleCommand

func (c PossibleCommands) PlayRandomly(commander Commander) {
	i := rand.Intn(len(c))
	c[i].Play(commander)
}

func (c PossibleCommands) Show() {
	DebugLogLine("PossibleCommands")
	for _, cmd := range c {
		cmd.Show()
	}
}

type Commander struct{}

// ランダムは禁止
/*
func (c Commander) Random() {
	fmt.Println(COMMAND_RANDOM)
}
*/

func (c Commander) Wait() {
	fmt.Println(COMMAND_WAIT)
}

// 移動

func (c Commander) Move(moveTo PlayerLocation, pickFrom ...PlayerLocation) {
	if len(pickFrom) > 0 {
		fmt.Printf("%s %d %d\n", COMMAND_MOVE, moveTo, pickFrom[0])
	} else {
		fmt.Printf("%s %d\n", COMMAND_MOVE, moveTo)
	}
}

// カードの受け渡し

func (c Commander) Give(cardType CardType) {
	fmt.Printf("%s %d\n", COMMAND_GIVE, cardType)
}

func (c Commander) Throw(cardType CardType) {
	fmt.Printf("%s %d\n", COMMAND_THROW, cardType)
}

// スキル

// 山札から2枚引き、さらに追加でスキルカードを使用
func (c Commander) Traning() {
	fmt.Println(COMMAND_TRAINING)
}

// 山札から1枚引き、さらに追加で2枚スキルカードを使用できる
func (c Commander) Coding() {
	fmt.Println(COMMAND_CODING)
}

// 移動後に隣のデスクからカードを取れるようになる
func (c Commander) DailyRoutine() {
	fmt.Println(COMMAND_DAILY_ROUTINE)
}

// ターン開始時に1枚多くカードを引く
func (c Commander) ArchitectureStudy() {
	fmt.Println(COMMAND_ARCHITECTURE_STUDY)
}

// ボーナスカードを2枚受け取って捨山に置く
func (c Commander) CodeReview() {
	fmt.Println(COMMAND_CODE_REVIEW)
}

// 負債カードを1枚場に戻す
func (c Commander) Refactoring() {
	fmt.Println(COMMAND_REFACTORING)
}

// スキルカードをリリース時に自動的に利用されるようにする
func (c Commander) ContinuousDelivery(cardType CardType) {
	fmt.Printf("%s %d\n", COMMAND_CONTINUOUS_DELIVERY, cardType)
	// CD は使用してもなくならないので記録しない
}

// スキルカードを交換する
func (c Commander) TaskPrioritization(throw, take CardType) {
	fmt.Printf("%s %d %d\n", COMMAND_TASK_PRIORITIZATION, throw, take)
}

// リリース

func (c Commander) Release(id int) {
	fmt.Printf("%s %d\n", COMMAND_RELEASE, id)
}
