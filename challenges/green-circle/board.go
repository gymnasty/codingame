package main

func CalcBoardPile(cardPiles CardPiles, opponentPlayer *Player) *CardPile {
	boardCards := NewCardPile()
	boardCards.CardPileType = CARD_PILE_TYPE_BOARD

	// 見えているカード以外のカードがボード上に存在する
	for _, card := range AllCards {
		// 残り枚数を確認
		total := 0
		for _, pile := range cardPiles {
			total += pile.GetCount(card)
		}

		// 相手がプレイ中のカードを考慮
		if card == DailyRoutineCard {
			total += opponentPlayer.PermanentDailyRoutineCards
		}
		if card == ArchitectureStudyCard {
			total += opponentPlayer.PermanentArchitectureStudyCards
		}

		rest := TotalCards(card.CardType) - total
		boardCards.SetCount(card, rest)
	}

	return boardCards
}
