package main

/**
 * Complete the hackathon before your opponent by following the principles of Green IT
 **/

// 次にリリースする予定のアプリケーション
var releaseTarget *Application

// 次にプレイする予定のカード
var playTarget *Card

func main() {
	reader := NewInputReader()
	commander := Commander{}

	for {
		// 入力を読み込み
		turn := reader.Read()
		// turn.Show()

		switch turn.GamePhase {
		case MOVE:
			releaseTarget = nil
			playTarget = nil
			Move(turn, commander)

		case THROW_CARD:
			ThrowCard(turn, commander)

		case GIVE_CARD:
			GiveCard(turn, commander)

		case PLAY_CARD:
			PlayCard(turn, commander)

		case RELEASE:
			Release(turn, commander)
		}
	}
}
