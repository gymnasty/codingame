package main

func Move(turn *Turn, commander Commander) {
	board := turn.CardPiles.Get(CARD_PILE_TYPE_BOARD)
	hand := turn.CardPiles.Get(CARD_PILE_TYPE_HAND)

	// 初手
	if turn.Players.MySelf().Location == PLAYER_LOCATION_INIT {
		if turn.Players.Opponent().Location == PLAYER_LOCATION_INIT || turn.Players.Opponent().Location == PLAYER_LOCATION_TRAINING {
			commander.Move(PLAYER_LOCATION_DAILY_ROUTINE)
			return
		}
		if turn.Players.Opponent().Location == PLAYER_LOCATION_CONTINUOUS_INTEGRATION || turn.Players.Opponent().Location == PLAYER_LOCATION_ARCHITECTURE_STUDY {
			commander.Move(PLAYER_LOCATION_DAILY_ROUTINE)
			return
		}
		if turn.Players.Opponent().Location == PLAYER_LOCATION_DAILY_ROUTINE {
			commander.Move(PLAYER_LOCATION_CONTINUOUS_INTEGRATION)
			return
		}
	}

	// ノーコストでリリースを行えるときはリリースを行う
	{
		for _, moveTo := range AllDesks {
			// 今いるデスクには移動できない
			if moveTo.Location == turn.Players.MySelf().Location {
				continue
			}

			taskManagement := turn.Players.MySelf().Location > moveTo.Location
			noisyDesk := FindNearDesks(turn.Players.Opponent().Location, 1).Has(moveTo)

			automated := turn.CardPiles.Get(CARD_PILE_TYPE_AUTOMATED)

			for _, pickFrom := range FindNearDesks(moveTo.Location, turn.Players.MySelf().PermanentDailyRoutineCards) {
				card := pickFrom.SkillCard
				if board.GetCount(card) == 0 {
					if board.GetCount(BonusCard) > 0 {
						card = BonusCard
					} else {
						card = nil
					}
				}

				additional := NewCardPile()
				if card != nil {
					additional.Cards[card] = 1
				}
				app := FindReleasableApplicationWithouCost2(hand, automated, additional, turn.Applications, taskManagement, noisyDesk)
				if app != nil {
					commander.Move(moveTo.Location, pickFrom.Location)
					releaseTarget = app
					DebugLogLine("will release %d", app.ID)
					return
				}

				// カードのプレイも加味
				if !taskManagement && !noisyDesk {
					handPlayed := hand.Copy()
					additionalPlayed := hand.Copy()
					if hand.Has(CodeReviewCard, 1) {
						handPlayed.Cards[CodeReviewCard]--
						handPlayed.Cards[BonusCard] += 2
					} else if additional.Has(CodeReviewCard, 1) {
						additionalPlayed.Cards[CodeReviewCard]--
						additionalPlayed.Cards[BonusCard] += 2
					}

					app := FindReleasableApplicationWithouCost2(handPlayed, automated, additionalPlayed, turn.Applications, taskManagement, noisyDesk)
					if app != nil {
						commander.Move(moveTo.Location, pickFrom.Location)
						releaseTarget = app
						DebugLogLine("will release %d", app.ID)
						return
					}
				}
			}
		}
	}

	if hand.Has(BonusCard, 1) && !hand.Has(ContinuousIntegrationCard, 1) {
		// 取れるなら CI カードを取りに行く
		if board.Has(ContinuousIntegrationCard, 1) {
			desks := FindNearDesks(PLAYER_LOCATION_CONTINUOUS_INTEGRATION, turn.Players.MySelf().PermanentDailyRoutineCards)
			exclusives := FindNearDesks(turn.Players.Opponent().Location, 1)
			deskCandidates := DesksWithout(desks, exclusives)
			sortedDesks := SortedMoveToDesks(turn.Players.MySelf().Location, deskCandidates)
			if len(sortedDesks) > 0 {
				moveTo := sortedDesks[0]
				if moveTo.Location > turn.Players.MySelf().Location {
					if moveTo.Location != PLAYER_LOCATION_CONTINUOUS_INTEGRATION {
						commander.Move(moveTo.Location, PLAYER_LOCATION_CONTINUOUS_INTEGRATION)
						return
					} else {
						commander.Move(moveTo.Location)
						return
					}
				}
			}
		}
	}

	// 次にリリースするために必要なカードを取りに行く

	// ダメージのない最も近い場所へ移動
	deskCandidates := DesksWithout(ListQuietDesk(turn.Players.Opponent().Location), []*Desk{GetPlayerDesk(turn.Players.MySelf().Location)})
	if turn.Players.MySelf().Location >= PLAYER_LOCATION_CONTINUOUS_INTEGRATION && hand.Cards.TotalSkillCards() == 0 {
		deskCandidates = DesksWithout(deskCandidates, []*Desk{CodeReviewDesk, RefactoringDesk})
	}
	desk := FindNearestDesk(deskCandidates, turn.Players.MySelf().Location)
	if turn.Players.MySelf().PermanentDailyRoutineCards > 0 {
		// 欲しいカードから取る
		nearDesks := FindNearDesks(desk.Location, turn.Players.MySelf().PermanentDailyRoutineCards)
		automated := turn.CardPiles[CARD_PILE_TYPE_AUTOMATED]
		needs := CalcSkillNeeds(automated, turn.Applications)
		for _, need := range needs.RequireSkills() {
			for _, d := range nearDesks {
				if need == d.SkillCard.SkillType {
					commander.Move(desk.Location, d.Location)
					return
				}
			}
		}
	} else {
		commander.Move(desk.Location)
	}
}

type MoveInfo struct {
	MoveTo   *Desk
	PickFrom *Desk
}

func FindObtainableCardsWithoutCost(turn *Turn) map[*Card]MoveInfo {
	board := turn.CardPiles.Get(CARD_PILE_TYPE_BOARD)
	current := turn.Players.MySelf().Location
	noisy := FindNearDesks(turn.Players.Opponent().Location, 1)
	moveToDesks := DesksWithout(ListDesksAfter(current), noisy)

	obtainableDesks := make(map[*Desk]*Desk, PLAYER_LOCATION_NUM)
	for _, moveTo := range moveToDesks {
		for _, d := range FindNearDesks(moveTo.Location, turn.Players.MySelf().PermanentDailyRoutineCards) {
			obtainableDesks[d] = moveTo
		}
	}

	obtainableCards := make(map[*Card]MoveInfo, len(obtainableDesks))
	for moveTo, pickFrom := range obtainableDesks {
		card := BonusCard
		if board.Has(pickFrom.SkillCard, 1) {
			card = pickFrom.SkillCard
		}
		obtainableCards[card] = MoveInfo{moveTo, pickFrom}
	}

	return obtainableCards
}
