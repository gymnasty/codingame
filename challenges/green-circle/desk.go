package main

type Desk struct {
	Location  PlayerLocation
	SkillCard *Card
}

func (d Desk) PreviousDesk() *Desk {
	location := (d.Location - 1 + PLAYER_LOCATION_NUM) % PLAYER_LOCATION_NUM
	return GetPlayerDesk(location)
}

func (d Desk) NextDesk() *Desk {
	location := (d.Location + 1) % PLAYER_LOCATION_NUM
	return GetPlayerDesk(location)
}

func (d Desk) Show() {
	DebugLogLine("  %s: %d", d.SkillCard.SkillType)
}

type Desks []*Desk

func (d Desks) Has(desk *Desk) bool {
	for _, e := range d {
		if e == desk {
			return true
		}
	}
	return false
}

var (
	TraningDesk               = &Desk{PLAYER_LOCATION_TRAINING, TraningCard}
	CodingDesk                = &Desk{PLAYER_LOCATION_CODING, CodingCard}
	DailyRoutineDesk          = &Desk{PLAYER_LOCATION_DAILY_ROUTINE, DailyRoutineCard}
	TaskPrioritizationDesk    = &Desk{PLAYER_LOCATION_TASK_PRIORITIZATION, TaskPrioritizationCard}
	ArchitectureStudyDesk     = &Desk{PLAYER_LOCATION_ARCHITECTURE_STUDY, ArchitectureStudyCard}
	ContinuousIntegrationDesk = &Desk{PLAYER_LOCATION_CONTINUOUS_INTEGRATION, ContinuousIntegrationCard}
	CodeReviewDesk            = &Desk{PLAYER_LOCATION_CODE_REVIEW, CodeReviewCard}
	RefactoringDesk           = &Desk{PLAYER_LOCATION_REFACTORING, RefactoringCard}
)

var AllDesks = []*Desk{
	TraningDesk,
	CodingDesk,
	DailyRoutineDesk,
	TaskPrioritizationDesk,
	ArchitectureStudyDesk,
	ContinuousIntegrationDesk,
	CodeReviewDesk,
	RefactoringDesk,
}

func GetSkillDesk(skill SkillType) *Desk {
	switch skill {
	case SKILL_TYPE_TRAINING:
		return TraningDesk
	case SKILL_TYPE_CODING:
		return CodingDesk
	case SKILL_TYPE_DAILY_ROUTINE:
		return DailyRoutineDesk
	case SKILL_TYPE_TASK_PRIORITIZATION:
		return TaskPrioritizationDesk
	case SKILL_TYPE_ARCHITECTURE_STUDY:
		return ArchitectureStudyDesk
	case SKILL_TYPE_CONTINUOUS_INTEGRATION:
		return ContinuousIntegrationDesk
	case SKILL_TYPE_CODE_REVIEW:
		return CodeReviewDesk
	case SKILL_TYPE_REFACTORING:
		return RefactoringDesk
	default:
		return nil
	}
}

func GetPlayerDesk(location PlayerLocation) *Desk {
	switch location {
	case PLAYER_LOCATION_TRAINING:
		return TraningDesk
	case PLAYER_LOCATION_CODING:
		return CodingDesk
	case PLAYER_LOCATION_DAILY_ROUTINE:
		return DailyRoutineDesk
	case PLAYER_LOCATION_TASK_PRIORITIZATION:
		return TaskPrioritizationDesk
	case PLAYER_LOCATION_ARCHITECTURE_STUDY:
		return ArchitectureStudyDesk
	case PLAYER_LOCATION_CONTINUOUS_INTEGRATION:
		return ContinuousIntegrationDesk
	case PLAYER_LOCATION_CODE_REVIEW:
		return CodeReviewDesk
	case PLAYER_LOCATION_REFACTORING:
		return RefactoringDesk
	default:
		return nil
	}
}

func ListQuietDesk(location PlayerLocation) Desks {
	switch location {
	case PLAYER_LOCATION_TRAINING:
		return []*Desk{DailyRoutineDesk, TaskPrioritizationDesk, ArchitectureStudyDesk, ContinuousIntegrationDesk, CodeReviewDesk}
	case PLAYER_LOCATION_CODING:
		return []*Desk{TaskPrioritizationDesk, ArchitectureStudyDesk, ContinuousIntegrationDesk, CodeReviewDesk, RefactoringDesk}
	case PLAYER_LOCATION_DAILY_ROUTINE:
		return []*Desk{TraningDesk, ArchitectureStudyDesk, ContinuousIntegrationDesk, CodeReviewDesk, RefactoringDesk}
	case PLAYER_LOCATION_TASK_PRIORITIZATION:
		return []*Desk{TraningDesk, CodingDesk, ContinuousIntegrationDesk, CodeReviewDesk, RefactoringDesk}
	case PLAYER_LOCATION_ARCHITECTURE_STUDY:
		return []*Desk{TraningDesk, CodingDesk, DailyRoutineDesk, CodeReviewDesk, RefactoringDesk}
	case PLAYER_LOCATION_CONTINUOUS_INTEGRATION:
		return []*Desk{TraningDesk, CodingDesk, DailyRoutineDesk, TaskPrioritizationDesk, RefactoringDesk}
	case PLAYER_LOCATION_CODE_REVIEW:
		return []*Desk{TraningDesk, CodingDesk, DailyRoutineDesk, TaskPrioritizationDesk, ArchitectureStudyDesk}
	case PLAYER_LOCATION_REFACTORING:
		return []*Desk{CodingDesk, DailyRoutineDesk, TaskPrioritizationDesk, ArchitectureStudyDesk, ContinuousIntegrationDesk}
	default:
		return AllDesks
	}
}

func ListDesksAfter(location PlayerLocation) Desks {
	return AllDesks[location+1:]
}

func DesksWithout(desks, exclusives []*Desk) Desks {
	exclusiveMap := make(map[*Desk]interface{}, len(exclusives))
	for _, d := range exclusives {
		exclusiveMap[d] = struct{}{}
	}
	result := make([]*Desk, 0, len(desks))
	for _, d := range desks {
		if _, ok := exclusiveMap[d]; !ok {
			result = append(result, d)
		}
	}
	return result
}

func FindNearestDesk(desks []*Desk, location PlayerLocation) *Desk {
	if location == PLAYER_LOCATION_INIT {
		var minDesk *Desk
		min := PLAYER_LOCATION_NUM
		for _, desk := range desks {
			if desk.Location < min {
				min = desk.Location
				minDesk = desk
			}
		}
		return minDesk
	}
	var minDesk *Desk
	minDist := int(PLAYER_LOCATION_NUM)
	for _, desk := range desks {
		dist := CalcPlayerLocationDistance(location, desk.Location)
		if dist < minDist {
			minDist = dist
			minDesk = desk
		}
	}
	return minDesk
}

func FindNearDesks(location PlayerLocation, steps int) Desks {
	deskNum := MinInt(int(PLAYER_LOCATION_NUM), steps*2+1)
	var desks []*Desk
	switch location {
	case PLAYER_LOCATION_INIT:
		return []*Desk{}
	case PLAYER_LOCATION_TRAINING:
		desks = []*Desk{TraningDesk, CodingDesk, RefactoringDesk, DailyRoutineDesk, CodeReviewDesk, TaskPrioritizationDesk, ContinuousIntegrationDesk, ArchitectureStudyDesk}
	case PLAYER_LOCATION_CODING:
		desks = []*Desk{CodingDesk, DailyRoutineDesk, TraningDesk, TaskPrioritizationDesk, RefactoringDesk, ArchitectureStudyDesk, CodeReviewDesk, ContinuousIntegrationDesk}
	case PLAYER_LOCATION_DAILY_ROUTINE:
		desks = []*Desk{DailyRoutineDesk, TaskPrioritizationDesk, CodingDesk, ArchitectureStudyDesk, TraningDesk, ContinuousIntegrationDesk, RefactoringDesk, CodeReviewDesk}
	case PLAYER_LOCATION_TASK_PRIORITIZATION:
		desks = []*Desk{TaskPrioritizationDesk, ArchitectureStudyDesk, DailyRoutineDesk, ContinuousIntegrationDesk, CodingDesk, CodeReviewDesk, TraningDesk, RefactoringDesk}
	case PLAYER_LOCATION_ARCHITECTURE_STUDY:
		desks = []*Desk{ArchitectureStudyDesk, ContinuousIntegrationDesk, TaskPrioritizationDesk, CodeReviewDesk, DailyRoutineDesk, RefactoringDesk, CodingDesk, TraningDesk}
	case PLAYER_LOCATION_CONTINUOUS_INTEGRATION:
		desks = []*Desk{ContinuousIntegrationDesk, CodeReviewDesk, ArchitectureStudyDesk, RefactoringDesk, TaskPrioritizationDesk, TraningDesk, DailyRoutineDesk, CodingDesk}
	case PLAYER_LOCATION_CODE_REVIEW:
		desks = []*Desk{CodeReviewDesk, RefactoringDesk, ContinuousIntegrationDesk, TraningDesk, ArchitectureStudyDesk, CodingDesk, TaskPrioritizationDesk, DailyRoutineDesk}
	case PLAYER_LOCATION_REFACTORING:
		desks = []*Desk{RefactoringDesk, TraningDesk, CodeReviewDesk, CodingDesk, ContinuousIntegrationDesk, DailyRoutineDesk, ArchitectureStudyDesk, TaskPrioritizationDesk}
	default:
		panic("unexpected player location")
	}
	return desks[:deskNum]
}

func CalcMoveSteps(from, to PlayerLocation) int {
	return int(to-from+PLAYER_LOCATION_NUM) % int(PLAYER_LOCATION_NUM)
}

// 移動先のデスクを近い順に返します。
func SortedMoveToDesks(current PlayerLocation, desks []*Desk) []*Desk {
	moveTo := make([]*Desk, 0, len(desks))
	for i := PlayerLocation(1); i < PLAYER_LOCATION_NUM; i++ {
		location := (current + i) % PLAYER_LOCATION_NUM
		for _, desk := range desks {
			if location == desk.Location {
				moveTo = append(moveTo, desk)
			}
		}
	}
	return moveTo
}
