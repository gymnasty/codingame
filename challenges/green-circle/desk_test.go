package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestListDesksAfter(t *testing.T) {
	assert.Equal(t, []*Desk{CodeReviewDesk, RefactoringDesk}, ListDesksAfter(PLAYER_LOCATION_CONTINUOUS_INTEGRATION))
	assert.Equal(t, []*Desk{RefactoringDesk}, ListDesksAfter(PLAYER_LOCATION_CODE_REVIEW))
	assert.Equal(t, []*Desk{}, ListDesksAfter(PLAYER_LOCATION_REFACTORING))
}

func TestFindNearDesks(t *testing.T) {
	assert.Equal(t, []*Desk{TraningDesk}, FindNearDesks(PLAYER_LOCATION_TRAINING, 0))
	assert.Equal(t, []*Desk{TraningDesk, CodingDesk, RefactoringDesk}, FindNearDesks(PLAYER_LOCATION_TRAINING, 1))
	assert.Equal(t, []*Desk{TraningDesk, CodingDesk, RefactoringDesk, DailyRoutineDesk, CodeReviewDesk, TaskPrioritizationDesk, ContinuousIntegrationDesk, ArchitectureStudyDesk}, FindNearDesks(PLAYER_LOCATION_TRAINING, 8))
}

func TestCalcMoveSteps(t *testing.T) {
	assert.Equal(t, 0, CalcMoveSteps(PLAYER_LOCATION_TRAINING, PLAYER_LOCATION_TRAINING))
	assert.Equal(t, 1, CalcMoveSteps(PLAYER_LOCATION_TRAINING, PLAYER_LOCATION_CODING))
	assert.Equal(t, 7, CalcMoveSteps(PLAYER_LOCATION_CODING, PLAYER_LOCATION_TRAINING))
	assert.Equal(t, 7, CalcMoveSteps(PLAYER_LOCATION_TRAINING, PLAYER_LOCATION_REFACTORING))
}
