package main

func GiveCard(turn *Turn, commander Commander) {
	if releaseTarget != nil {
		// 不要なカードから渡す
		card := FindNotNeededCard(turn.CardPiles[CARD_PILE_TYPE_HAND], releaseTarget)
		if card != nil {
			commander.Give(card.CardType)
			return
		}
	}

	turn.PossibleCommands.PlayRandomly(commander)
}
