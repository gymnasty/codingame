package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"sort"
	"strings"
)

// アプリケーション
type Application struct {
	ID                  int                 // application id
	ObjectType          string              // object type
	ReleaseRequirements ReleaseRequirements // リリースに必要なポイント
}

func NewApplication() *Application {
	return &Application{
		ReleaseRequirements: make(ReleaseRequirements),
	}
}

func (a Application) Show() {
	DebugLogLine("Application")
	DebugLogLine("  ID: %s", a.ID)
	DebugLogLine("  ObjectType: %s", a.ObjectType)
	a.ReleaseRequirements.Show()
}

func (a Application) RequirePoints() int {
	p := 0
	for _, n := range a.ReleaseRequirements {
		p += n
	}
	return p
}

// すべてのアプリケーション
type Applications []*Application

func (a Applications) Show() {
	for _, app := range a {
		app.Show()
	}
}

// アプリケーションのリリースに必要なスキル
type ReleaseRequirements map[SkillType]int

func (r ReleaseRequirements) TotalPoints() int {
	p := 0
	for _, v := range r {
		p += v
	}
	return p
}

// 要求されているスキルをその量が大きい順に並べ替えて返す。
func (r ReleaseRequirements) RequireSkills() []SkillType {
	skills := make([]SkillType, len(AllSkills))
	copy(skills, AllSkills)
	sort.SliceStable(skills, func(i, j int) bool {
		return r[skills[i]] > r[skills[j]]
	})
	for i, skill := range skills {
		if r[skill] == 0 {
			return skills[:i]
		}
	}
	return skills
}

func (r ReleaseRequirements) Copy() ReleaseRequirements {
	requirements := make(ReleaseRequirements)
	for k, v := range r {
		requirements[k] = v
	}
	return requirements
}

func (r ReleaseRequirements) Show() {
	DebugLogLine("  SkillRequirements:")
	for _, skill := range AllSkills {
		if requirements, ok := r[skill]; ok {
			DebugLogLine("    %s: %d", skill, requirements)
		}
	}
}

// リリースによる技術的負債を計算する
func CalcApplicationReleaseCost(pile, automatedPile *CardPile, app *Application) int {
	// リリース可能かどうかを調べる
	requirements := app.ReleaseRequirements.Copy()
	for skill, requiredPoint := range requirements {
		skillPoint := pile.SkillPoints(skill) + automatedPile.SkillPoints(skill)
		if requiredPoint > skillPoint {
			requirements[skill] -= skillPoint
		} else {
			requirements[skill] = 0
		}
	}

	total := requirements.TotalPoints()
	alt := pile.AltanativePoints() + automatedPile.AltanativePoints()
	bonus := pile.SkillPoints(SKILL_TYPE_BONUS) + automatedPile.SkillPoints(SKILL_TYPE_BONUS)
	if total > alt+bonus {
		return -1
	}

	return MaxInt(0, total-bonus)
}

// 不足しているスキルを計算する
func CalcInsufficientSkills(pile, automatedPile *CardPile, app *Application) (insufficientSkills map[SkillType]int, insufficientPoints int) {
	insufficientPoints = 0
	insufficientSkills = make(map[SkillType]int)
	for skill, requirement := range app.ReleaseRequirements {
		skillPoints := pile.SkillPoints(skill)
		if skillPoints < requirement {
			insufficientSkills[skill] = requirement - skillPoints
			insufficientPoints += requirement - skillPoints
		}
	}
	insufficientPoints -= MinInt(pile.SkillPoints(SKILL_TYPE_BONUS), insufficientPoints)
	return insufficientSkills, insufficientPoints
}

// リリースにかかるコストが最も低いアプリケーションを探す
func FindLowestReleaseCostApplication(pile, automatedPile *CardPile, apps Applications) (app *Application, cost int) {
	minCost := 100
	var minApp *Application
	for _, app := range apps {
		cost := CalcApplicationReleaseCost(pile, automatedPile, app)
		if cost < 0 {
			continue
		}
		if cost < minCost {
			minCost = cost
			minApp = app
		}
	}
	return minApp, minCost
}

// コスト 0 でリリース可能なアプリケーションを探す
func FindReleasableApplicationWithouCost(hand *CardPile, automated *CardPile, additional *CardPile, apps Applications) *Application {
	for _, app := range apps {
		requirements := app.ReleaseRequirements.Copy()
		for skill, requiredPoint := range requirements {
			skillPoint := hand.SkillPoints(skill) + automated.SkillPoints(skill) + additional.SkillPoints(skill)
			if requiredPoint > skillPoint {
				requirements[skill] -= skillPoint
			} else {
				requirements[skill] = 0
			}
		}
		total := requirements.TotalPoints()
		bonus := hand.SkillPoints(SKILL_TYPE_BONUS) + automated.SkillPoints(SKILL_TYPE_BONUS) + additional.SkillPoints(SKILL_TYPE_BONUS)
		if total <= bonus {
			return app
		}
	}
	return nil
}

// コスト 0 でリリース可能なアプリケーションを探す
func FindReleasableApplicationWithouCost2(hand *CardPile, automated *CardPile, additional *CardPile, apps Applications, taskManagementCost, noisyCost bool) *Application {
	// taskManagementCost が必要な場合は手札から2枚捨てる
	// noisyCost が必要な場合は手札もしくは追加カードから1枚捨てる
	requires := make(map[*Application]ReleaseRequirements)
	for _, app := range apps {
		req := app.ReleaseRequirements.Copy()
		for skill, requiredPoint := range req {
			skillPoint := automated.SkillPoints(skill)
			if requiredPoint > skillPoint {
				req[skill] -= skillPoint
			} else {
				req[skill] = 0
			}
		}
		requires[app] = req
	}
	// ここまでで automated のボーナスカード以外は使用済み

	for _, app := range apps {
		handCards := hand.Cards.Copy()
		additionalCards := additional.Cards.Copy()
		requirements := requires[app].Copy()
		handSkillUsed := 0
		additionalSkillUsed := 0

		for skill, requiredPoint := range requirements {
			card := SkillCardMap[skill]
			requireCards := requiredPoint/2 + requiredPoint%2

			// 追加カードを使用
			if additionalCards[card] >= requireCards {
				additionalSkillUsed += requireCards
				additionalCards[card] -= requireCards
				requireCards = 0
				requirements[card.SkillType] = 0
			} else {
				additionalSkillUsed += additionalCards[card]
				requireCards -= additionalCards[card]
				requirements[card.SkillType] -= additionalCards[card] * 2
				additionalCards[card] = 0
			}

			// 手札を使用
			if handCards[card] >= requireCards {
				handSkillUsed += requireCards
				handCards[card] -= requireCards
				requireCards = 0
				requirements[card.SkillType] = 0
			} else {
				handSkillUsed += handCards[card]
				requireCards -= handCards[card]
				requirements[card.SkillType] -= handCards[card] * 2
				handCards[card] = 0
			}
		}

		// ここまででボーナスカード以外のすべてのカードを使用済み

		skillReturned := 0
		requirePoints := requirements.TotalPoints()
		if requirePoints < automated.Cards[BonusCard] {
			returnNum := (automated.Cards[BonusCard] - requirePoints) / 2
			skillReturned += returnNum
			requirePoints = 0
		} else {
			requirePoints -= automated.Cards[BonusCard]
		}

		// ここまでで automated はすべて使用済み

		if requirePoints > additionalCards[BonusCard] {
			requirePoints -= additionalCards[BonusCard]
			additionalCards[BonusCard] = 0
		} else {
			additionalCards[BonusCard] -= requirePoints
			requirePoints = 0
		}

		// ここまでで追加カードはすべて使用済み

		if requirePoints > handCards[BonusCard] {
			requirePoints -= handCards[BonusCard]
			handCards[BonusCard] = 0
		} else {
			handCards[BonusCard] -= requirePoints
			requirePoints = 0
		}

		// ここまでですべてのカードを使用済み

		if requirePoints > 0 {
			continue
		}

		// カードを戻す
		handReturned := MinInt(skillReturned, handSkillUsed)
		skillReturned -= handReturned
		additionalReturned := MinInt(skillReturned, additionalSkillUsed)
		skillReturned -= skillReturned

		// 手札が 2 枚余っている必要がある
		handCosts := 0
		if taskManagementCost {
			if handCards.TotalSkillCards()+handReturned < 2 {
				continue
			}
			handCosts = 2
		}

		// 手札もしくは追加カードが 1 枚余っている必要がある
		if noisyCost {
			if handCards.TotalSkillCards()+handReturned-handCosts == 0 && additionalCards.TotalSkillCards()+additionalReturned == 0 {
				continue
			}
		}

		// リリース可能
		return app
	}
	return nil
}

func FindNotNeededCard(hand *CardPile, releaseTarget *Application) *Card {
	for _, skill := range AllSkills {
		card := SkillCardMap[skill]
		if hand.Cards[card] == 0 {
			continue
		}
		if releaseTarget.ReleaseRequirements[skill] == 0 {
			return card
		}
	}
	if hand.Cards[BonusCard] > 0 {
		return BonusCard
	}
	return nil
}

func CalcSkillNeeds(automatedPile *CardPile, apps Applications) ReleaseRequirements {
	needs := make(map[SkillType]int, SKILL_TYPE_NUM)
	automated := make(map[SkillType]int, SKILL_TYPE_NUM)
	for _, skill := range AllSkills {
		needs[skill] = 0
		automated[skill] = automatedPile.SkillPoints(skill)
	}
	for _, app := range apps {
		for skill, requirements := range app.ReleaseRequirements {
			needs[skill] += MaxInt(0, requirements-automated[skill])
		}
	}
	return needs
}

func CalcBoardPile(cardPiles CardPiles, opponentPlayer *Player) *CardPile {
	boardCards := NewCardPile()
	boardCards.CardPileType = CARD_PILE_TYPE_BOARD

	// 見えているカード以外のカードがボード上に存在する
	for _, card := range AllCards {
		// 残り枚数を確認
		total := 0
		for _, pile := range cardPiles {
			total += pile.GetCount(card)
		}

		// 相手がプレイ中のカードを考慮
		if card == DailyRoutineCard {
			total += opponentPlayer.PermanentDailyRoutineCards
		}
		if card == ArchitectureStudyCard {
			total += opponentPlayer.PermanentArchitectureStudyCards
		}

		rest := TotalCards(card.CardType) - total
		boardCards.SetCount(card, rest)
	}

	return boardCards
}

const (
	TotalCardsOnEachPlayableSkill = 5
	TotalBonusCards               = 36
	TotalDebtCards                = 100
)

// カードの総数
func TotalCards(cardType CardType) int {
	switch cardType {
	case CARD_TYPE_BONUS:
		return TotalBonusCards
	case CARD_TYPE_DEBT:
		return TotalDebtCards
	default:
		return TotalCardsOnEachPlayableSkill
	}
}

// カードの種類
type CardType int

const (
	CARD_TYPE_TRAINING CardType = iota
	CARD_TYPE_CODING
	CARD_TYPE_DAILY_ROUTINE
	CARD_TYPE_TASK_PRIORITIZATION
	CARD_TYPE_ARCHITECTURE_STUDY
	CARD_TYPE_CONTINUOUS_INTEGRATION
	CARD_TYPE_CODE_REVIEW
	CARD_TYPE_REFACTORING
	CARD_TYPE_BONUS
	CARD_TYPE_DEBT
)

var CardTypeNames = map[CardType]string{
	CARD_TYPE_TRAINING:               "TRAINING",
	CARD_TYPE_CODING:                 "CODING",
	CARD_TYPE_DAILY_ROUTINE:          "DAILY_ROUTINE",
	CARD_TYPE_TASK_PRIORITIZATION:    "TASK_PRIORITIZATION",
	CARD_TYPE_ARCHITECTURE_STUDY:     "ARCHITECTURE_STUDY",
	CARD_TYPE_CONTINUOUS_INTEGRATION: "CONTINUOUS_INTEGRATION",
	CARD_TYPE_CODE_REVIEW:            "CODE_REVIEW",
	CARD_TYPE_REFACTORING:            "REFACTORING",
	CARD_TYPE_BONUS:                  "BONUS",
	CARD_TYPE_DEBT:                   "DEBT",
}

// すべてのカードの種類
var AllCardTypes = []CardType{
	CARD_TYPE_TRAINING,
	CARD_TYPE_CODING,
	CARD_TYPE_DAILY_ROUTINE,
	CARD_TYPE_TASK_PRIORITIZATION,
	CARD_TYPE_ARCHITECTURE_STUDY,
	CARD_TYPE_CONTINUOUS_INTEGRATION,
	CARD_TYPE_CODE_REVIEW,
	CARD_TYPE_REFACTORING,
	CARD_TYPE_BONUS,
	CARD_TYPE_DEBT,
}

type Card struct {
	CardType         CardType  // カードの種類
	SkillType        SkillType // スキルの種類
	SkillPoints      int       // 必要なスキルに割り当てられるポイント
	AltanativePoints int       // 不足しているスキルに割り当てられるポイント
	DebtPoints       int       // 負債ポイント
}

var (
	TraningCard               = &Card{CARD_TYPE_TRAINING, SKILL_TYPE_TRAINING, 2, 2, 0}
	CodingCard                = &Card{CARD_TYPE_CODING, SKILL_TYPE_CODING, 2, 2, 0}
	DailyRoutineCard          = &Card{CARD_TYPE_DAILY_ROUTINE, SKILL_TYPE_DAILY_ROUTINE, 2, 2, 0}
	TaskPrioritizationCard    = &Card{CARD_TYPE_TASK_PRIORITIZATION, SKILL_TYPE_TASK_PRIORITIZATION, 2, 2, 0}
	ArchitectureStudyCard     = &Card{CARD_TYPE_ARCHITECTURE_STUDY, SKILL_TYPE_ARCHITECTURE_STUDY, 2, 2, 0}
	ContinuousIntegrationCard = &Card{CARD_TYPE_CONTINUOUS_INTEGRATION, SKILL_TYPE_CONTINUOUS_INTEGRATION, 2, 2, 0}
	CodeReviewCard            = &Card{CARD_TYPE_CODE_REVIEW, SKILL_TYPE_CODE_REVIEW, 2, 2, 0}
	RefactoringCard           = &Card{CARD_TYPE_REFACTORING, SKILL_TYPE_REFACTORING, 2, 2, 0}
	BonusCard                 = &Card{CARD_TYPE_BONUS, SKILL_TYPE_BONUS, 1, 1, 0}
	DebtCard                  = &Card{CARD_TYPE_DEBT, SKILL_TYPE_NONE, 0, 0, 1}
)

// すべてのカード
var AllCards = []*Card{
	TraningCard,
	CodingCard,
	DailyRoutineCard,
	TaskPrioritizationCard,
	ArchitectureStudyCard,
	ContinuousIntegrationCard,
	CodeReviewCard,
	RefactoringCard,
	BonusCard,
	DebtCard,
}

// スキルカード
var SkillCards = []*Card{
	TraningCard,
	CodingCard,
	DailyRoutineCard,
	TaskPrioritizationCard,
	ArchitectureStudyCard,
	ContinuousIntegrationCard,
	CodeReviewCard,
	RefactoringCard,
	BonusCard,
}

// プレイ可能なスキルカード
var PlayableSkillCards = []*Card{
	TraningCard,
	CodingCard,
	DailyRoutineCard,
	TaskPrioritizationCard,
	ArchitectureStudyCard,
	ContinuousIntegrationCard,
	CodeReviewCard,
	RefactoringCard,
}

var SkillCardMap = map[SkillType]*Card{
	SKILL_TYPE_TRAINING:               TraningCard,
	SKILL_TYPE_CODING:                 CodingCard,
	SKILL_TYPE_DAILY_ROUTINE:          DailyRoutineCard,
	SKILL_TYPE_TASK_PRIORITIZATION:    TaskPrioritizationCard,
	SKILL_TYPE_ARCHITECTURE_STUDY:     ArchitectureStudyCard,
	SKILL_TYPE_CONTINUOUS_INTEGRATION: ContinuousIntegrationCard,
	SKILL_TYPE_CODE_REVIEW:            CodeReviewCard,
	SKILL_TYPE_REFACTORING:            RefactoringCard,
	SKILL_TYPE_BONUS:                  BonusCard,
}

// カードの山の種類
type CardPileType string

const (
	CARD_PILE_TYPE_HAND               CardPileType = "HAND"               // 自分の手札
	CARD_PILE_TYPE_DRAW               CardPileType = "DRAW"               // 自分の山札
	CARD_PILE_TYPE_DISCARD            CardPileType = "DISCARD"            // 自分の捨札
	CARD_PILE_TYPE_PLAYED             CardPileType = "PLAYED_CARDS"       // 自分がこのターンに使用したスキルカード
	CARD_PILE_TYPE_AUTOMATED          CardPileType = "AUTOMATED"          // 自分の自動で利用されるカード
	CARD_PILE_TYPE_OPPONENT_CARDS     CardPileType = "OPPONENT_CARDS"     // 相手のすべてのカード
	CARD_PILE_TYPE_OPPONENT_AUTOMATED CardPileType = "OPPONENT_AUTOMATED" // 相手の自動で利用されるカード
	CARD_PILE_TYPE_BOARD              CardPileType = "BOARD"              // 場のカード
)

// すべてのカードの山の種類
var AllCardPileTypes = []CardPileType{
	CARD_PILE_TYPE_HAND,
	CARD_PILE_TYPE_DRAW,
	CARD_PILE_TYPE_DISCARD,
	CARD_PILE_TYPE_PLAYED,
	CARD_PILE_TYPE_AUTOMATED,
	CARD_PILE_TYPE_OPPONENT_CARDS,
	CARD_PILE_TYPE_OPPONENT_AUTOMATED,
	CARD_PILE_TYPE_BOARD,
}

// カードの山
type CardPile struct {
	CardPileType CardPileType
	Cards        CardCounts
}

func NewCardPile() *CardPile {
	return &CardPile{
		Cards: make(CardCounts),
	}
}

func (c *CardPile) GetCount(card *Card) int {
	return c.Cards[card]
}

func (c *CardPile) SetCount(card *Card, count int) {
	c.Cards[card] = count
}

func (c *CardPile) Add(card *Card) {
	c.Cards[card]++
}

func (c CardPile) Copy() *CardPile {
	p := NewCardPile()
	p.CardPileType = p.CardPileType
	p.Cards = p.Cards.Copy()
	return p
}

func (c CardPile) Has(card *Card, count int) bool {
	return c.Cards[card] >= count
}

func (c CardPile) CountPlayable() int {
	count := 0
	for _, card := range PlayableSkillCards {
		count += c.Cards[card]
	}
	return count
}

func (c CardPile) AltanativePoints() int {
	p := 0
	for card, num := range c.Cards {
		p += card.AltanativePoints * num
	}
	return p
}

func (c CardPile) Show() {
	DebugLogLine("  CardPile")
	DebugLogLine("  Type: %s", c.CardPileType)
	c.Cards.Show()
}

func (c CardPile) SkillPoints(skill SkillType) int {
	card := SkillCardMap[skill]
	num := c.Cards[card]
	return card.SkillPoints * num
}

// すべてのカードの山
type CardPiles map[CardPileType]*CardPile

func (c CardPiles) Get(cardPileType CardPileType) *CardPile {
	cardPile, ok := c[cardPileType]
	if ok {
		return cardPile
	}
	return NewCardPile()
}

func (c CardPiles) Show() {
	DebugLogLine("CardPiles")
	for _, l := range AllCardPileTypes {
		if p, ok := c[l]; ok {
			p.Show()
		}
	}
}

// カードの枚数
type CardCounts map[*Card]int

func (c CardCounts) TotalSkillCards() int {
	total := 0
	for card, count := range c {
		if card != DebtCard {
			total += count
		}
	}
	return total
}

func (c CardCounts) Copy() CardCounts {
	cpy := make(CardCounts, len(c))
	for k, v := range c {
		cpy[k] = v
	}
	return cpy
}

func (c CardCounts) Show() {
	DebugLogLine("  Cards:")
	for _, card := range AllCards {
		if count, ok := c[card]; ok {
			DebugLogLine("    %s: %d", CardTypeNames[card.CardType], count)
		}
	}
}

// コマンド
type Command string

const (
	COMMAND_RANDOM              Command = "RANDOM"
	COMMAND_MOVE                Command = "MOVE"
	COMMAND_RELEASE             Command = "RELEASE"
	COMMAND_WAIT                Command = "WAIT"
	COMMAND_GIVE                Command = "GIVE"
	COMMAND_THROW               Command = "THROW"
	COMMAND_TRAINING            Command = "TRAINING"
	COMMAND_CODING              Command = "CODING"
	COMMAND_DAILY_ROUTINE       Command = "DAILY_ROUTINE"
	COMMAND_TASK_PRIORITIZATION Command = "TASK_PRIORITIZATION"
	COMMAND_ARCHITECTURE_STUDY  Command = "ARCHITECTURE_STUDY"
	COMMAND_CONTINUOUS_DELIVERY Command = "CONTINUOUS_INTEGRATION"
	COMMAND_CODE_REVIEW         Command = "CODE_REVIEW"
	COMMAND_REFACTORING         Command = "REFACTORING"
)

// 実行可能なコマンド
type PossibleCommand struct {
	Command Command
	Args    []int
}

func (c PossibleCommand) Play(commander Commander) {
	switch c.Command {
	//case COMMAND_RANDOM:
	//	commander.Random()
	case COMMAND_MOVE:
		if len(c.Args) > 1 {
			commander.Move(PlayerLocation(c.Args[0]), PlayerLocation(c.Args[1]))
		}
		commander.Move(PlayerLocation(c.Args[0]))
	case COMMAND_RELEASE:
		commander.Release(c.Args[0])
	case COMMAND_WAIT:
		commander.Wait()
	case COMMAND_GIVE:
		commander.Give(CardType(c.Args[0]))
	case COMMAND_THROW:
		commander.Throw(CardType(c.Args[0]))
	case COMMAND_TRAINING:
		commander.Traning()
	case COMMAND_CODING:
		commander.Coding()
	case COMMAND_DAILY_ROUTINE:
		commander.DailyRoutine()
	case COMMAND_TASK_PRIORITIZATION:
		commander.TaskPrioritization(CardType(c.Args[0]), CardType(c.Args[1]))
	case COMMAND_ARCHITECTURE_STUDY:
		commander.ArchitectureStudy()
	case COMMAND_CONTINUOUS_DELIVERY:
		commander.ContinuousDelivery(CardType(c.Args[0]))
	case COMMAND_CODE_REVIEW:
		commander.CodeReview()
	case COMMAND_REFACTORING:
		commander.Refactoring()
	default:
		panic(c.Command)
	}
}

func (c PossibleCommand) Show() {
	DebugLog("  %s", c.Command)
	for _, arg := range c.Args {
		DebugLog(" %d", arg)
	}
	DebugLog("\n")
}

func ParsePossibleCommand(s string) *PossibleCommand {
	p := strings.Split(s, " ")
	args := make([]int, len(p)-1)
	for i := range args {
		fmt.Sscan(p[i+1], &args[i])
	}
	return &PossibleCommand{
		Command: Command(p[0]),
		Args:    args,
	}
}

type PossibleCommands []*PossibleCommand

func (c PossibleCommands) PlayRandomly(commander Commander) {
	i := rand.Intn(len(c))
	c[i].Play(commander)
}

func (c PossibleCommands) Show() {
	DebugLogLine("PossibleCommands")
	for _, cmd := range c {
		cmd.Show()
	}
}

type Commander struct{}

// ランダムは禁止
/*
func (c Commander) Random() {
	fmt.Println(COMMAND_RANDOM)
}
*/

func (c Commander) Wait() {
	fmt.Println(COMMAND_WAIT)
}

// 移動

func (c Commander) Move(moveTo PlayerLocation, pickFrom ...PlayerLocation) {
	if len(pickFrom) > 0 {
		fmt.Printf("%s %d %d\n", COMMAND_MOVE, moveTo, pickFrom[0])
	} else {
		fmt.Printf("%s %d\n", COMMAND_MOVE, moveTo)
	}
}

// カードの受け渡し

func (c Commander) Give(cardType CardType) {
	fmt.Printf("%s %d\n", COMMAND_GIVE, cardType)
}

func (c Commander) Throw(cardType CardType) {
	fmt.Printf("%s %d\n", COMMAND_THROW, cardType)
}

// スキル

// 山札から2枚引き、さらに追加でスキルカードを使用
func (c Commander) Traning() {
	fmt.Println(COMMAND_TRAINING)
}

// 山札から1枚引き、さらに追加で2枚スキルカードを使用できる
func (c Commander) Coding() {
	fmt.Println(COMMAND_CODING)
}

// 移動後に隣のデスクからカードを取れるようになる
func (c Commander) DailyRoutine() {
	fmt.Println(COMMAND_DAILY_ROUTINE)
}

// ターン開始時に1枚多くカードを引く
func (c Commander) ArchitectureStudy() {
	fmt.Println(COMMAND_ARCHITECTURE_STUDY)
}

// ボーナスカードを2枚受け取って捨山に置く
func (c Commander) CodeReview() {
	fmt.Println(COMMAND_CODE_REVIEW)
}

// 負債カードを1枚場に戻す
func (c Commander) Refactoring() {
	fmt.Println(COMMAND_REFACTORING)
}

// スキルカードをリリース時に自動的に利用されるようにする
func (c Commander) ContinuousDelivery(cardType CardType) {
	fmt.Printf("%s %d\n", COMMAND_CONTINUOUS_DELIVERY, cardType)
	// CD は使用してもなくならないので記録しない
}

// スキルカードを交換する
func (c Commander) TaskPrioritization(throw, take CardType) {
	fmt.Printf("%s %d %d\n", COMMAND_TASK_PRIORITIZATION, throw, take)
}

// リリース

func (c Commander) Release(id int) {
	fmt.Printf("%s %d\n", COMMAND_RELEASE, id)
}

type Desk struct {
	Location  PlayerLocation
	SkillCard *Card
}

func (d Desk) PreviousDesk() *Desk {
	location := (d.Location - 1 + PLAYER_LOCATION_NUM) % PLAYER_LOCATION_NUM
	return GetPlayerDesk(location)
}

func (d Desk) NextDesk() *Desk {
	location := (d.Location + 1) % PLAYER_LOCATION_NUM
	return GetPlayerDesk(location)
}

func (d Desk) Show() {
	DebugLogLine("  %s: %d", d.SkillCard.SkillType)
}

type Desks []*Desk

func (d Desks) Has(desk *Desk) bool {
	for _, e := range d {
		if e == desk {
			return true
		}
	}
	return false
}

var (
	TraningDesk               = &Desk{PLAYER_LOCATION_TRAINING, TraningCard}
	CodingDesk                = &Desk{PLAYER_LOCATION_CODING, CodingCard}
	DailyRoutineDesk          = &Desk{PLAYER_LOCATION_DAILY_ROUTINE, DailyRoutineCard}
	TaskPrioritizationDesk    = &Desk{PLAYER_LOCATION_TASK_PRIORITIZATION, TaskPrioritizationCard}
	ArchitectureStudyDesk     = &Desk{PLAYER_LOCATION_ARCHITECTURE_STUDY, ArchitectureStudyCard}
	ContinuousIntegrationDesk = &Desk{PLAYER_LOCATION_CONTINUOUS_INTEGRATION, ContinuousIntegrationCard}
	CodeReviewDesk            = &Desk{PLAYER_LOCATION_CODE_REVIEW, CodeReviewCard}
	RefactoringDesk           = &Desk{PLAYER_LOCATION_REFACTORING, RefactoringCard}
)

var AllDesks = []*Desk{
	TraningDesk,
	CodingDesk,
	DailyRoutineDesk,
	TaskPrioritizationDesk,
	ArchitectureStudyDesk,
	ContinuousIntegrationDesk,
	CodeReviewDesk,
	RefactoringDesk,
}

func GetSkillDesk(skill SkillType) *Desk {
	switch skill {
	case SKILL_TYPE_TRAINING:
		return TraningDesk
	case SKILL_TYPE_CODING:
		return CodingDesk
	case SKILL_TYPE_DAILY_ROUTINE:
		return DailyRoutineDesk
	case SKILL_TYPE_TASK_PRIORITIZATION:
		return TaskPrioritizationDesk
	case SKILL_TYPE_ARCHITECTURE_STUDY:
		return ArchitectureStudyDesk
	case SKILL_TYPE_CONTINUOUS_INTEGRATION:
		return ContinuousIntegrationDesk
	case SKILL_TYPE_CODE_REVIEW:
		return CodeReviewDesk
	case SKILL_TYPE_REFACTORING:
		return RefactoringDesk
	default:
		return nil
	}
}

func GetPlayerDesk(location PlayerLocation) *Desk {
	switch location {
	case PLAYER_LOCATION_TRAINING:
		return TraningDesk
	case PLAYER_LOCATION_CODING:
		return CodingDesk
	case PLAYER_LOCATION_DAILY_ROUTINE:
		return DailyRoutineDesk
	case PLAYER_LOCATION_TASK_PRIORITIZATION:
		return TaskPrioritizationDesk
	case PLAYER_LOCATION_ARCHITECTURE_STUDY:
		return ArchitectureStudyDesk
	case PLAYER_LOCATION_CONTINUOUS_INTEGRATION:
		return ContinuousIntegrationDesk
	case PLAYER_LOCATION_CODE_REVIEW:
		return CodeReviewDesk
	case PLAYER_LOCATION_REFACTORING:
		return RefactoringDesk
	default:
		return nil
	}
}

func ListQuietDesk(location PlayerLocation) Desks {
	switch location {
	case PLAYER_LOCATION_TRAINING:
		return []*Desk{DailyRoutineDesk, TaskPrioritizationDesk, ArchitectureStudyDesk, ContinuousIntegrationDesk, CodeReviewDesk}
	case PLAYER_LOCATION_CODING:
		return []*Desk{TaskPrioritizationDesk, ArchitectureStudyDesk, ContinuousIntegrationDesk, CodeReviewDesk, RefactoringDesk}
	case PLAYER_LOCATION_DAILY_ROUTINE:
		return []*Desk{TraningDesk, ArchitectureStudyDesk, ContinuousIntegrationDesk, CodeReviewDesk, RefactoringDesk}
	case PLAYER_LOCATION_TASK_PRIORITIZATION:
		return []*Desk{TraningDesk, CodingDesk, ContinuousIntegrationDesk, CodeReviewDesk, RefactoringDesk}
	case PLAYER_LOCATION_ARCHITECTURE_STUDY:
		return []*Desk{TraningDesk, CodingDesk, DailyRoutineDesk, CodeReviewDesk, RefactoringDesk}
	case PLAYER_LOCATION_CONTINUOUS_INTEGRATION:
		return []*Desk{TraningDesk, CodingDesk, DailyRoutineDesk, TaskPrioritizationDesk, RefactoringDesk}
	case PLAYER_LOCATION_CODE_REVIEW:
		return []*Desk{TraningDesk, CodingDesk, DailyRoutineDesk, TaskPrioritizationDesk, ArchitectureStudyDesk}
	case PLAYER_LOCATION_REFACTORING:
		return []*Desk{CodingDesk, DailyRoutineDesk, TaskPrioritizationDesk, ArchitectureStudyDesk, ContinuousIntegrationDesk}
	default:
		return AllDesks
	}
}

func ListDesksAfter(location PlayerLocation) Desks {
	return AllDesks[location+1:]
}

func DesksWithout(desks, exclusives []*Desk) Desks {
	exclusiveMap := make(map[*Desk]interface{}, len(exclusives))
	for _, d := range exclusives {
		exclusiveMap[d] = struct{}{}
	}
	result := make([]*Desk, 0, len(desks))
	for _, d := range desks {
		if _, ok := exclusiveMap[d]; !ok {
			result = append(result, d)
		}
	}
	return result
}

func FindNearestDesk(desks []*Desk, location PlayerLocation) *Desk {
	if location == PLAYER_LOCATION_INIT {
		var minDesk *Desk
		min := PLAYER_LOCATION_NUM
		for _, desk := range desks {
			if desk.Location < min {
				min = desk.Location
				minDesk = desk
			}
		}
		return minDesk
	}
	var minDesk *Desk
	minDist := int(PLAYER_LOCATION_NUM)
	for _, desk := range desks {
		dist := CalcPlayerLocationDistance(location, desk.Location)
		if dist < minDist {
			minDist = dist
			minDesk = desk
		}
	}
	return minDesk
}

func FindNearDesks(location PlayerLocation, steps int) Desks {
	deskNum := MinInt(int(PLAYER_LOCATION_NUM), steps*2+1)
	var desks []*Desk
	switch location {
	case PLAYER_LOCATION_INIT:
		return []*Desk{}
	case PLAYER_LOCATION_TRAINING:
		desks = []*Desk{TraningDesk, CodingDesk, RefactoringDesk, DailyRoutineDesk, CodeReviewDesk, TaskPrioritizationDesk, ContinuousIntegrationDesk, ArchitectureStudyDesk}
	case PLAYER_LOCATION_CODING:
		desks = []*Desk{CodingDesk, DailyRoutineDesk, TraningDesk, TaskPrioritizationDesk, RefactoringDesk, ArchitectureStudyDesk, CodeReviewDesk, ContinuousIntegrationDesk}
	case PLAYER_LOCATION_DAILY_ROUTINE:
		desks = []*Desk{DailyRoutineDesk, TaskPrioritizationDesk, CodingDesk, ArchitectureStudyDesk, TraningDesk, ContinuousIntegrationDesk, RefactoringDesk, CodeReviewDesk}
	case PLAYER_LOCATION_TASK_PRIORITIZATION:
		desks = []*Desk{TaskPrioritizationDesk, ArchitectureStudyDesk, DailyRoutineDesk, ContinuousIntegrationDesk, CodingDesk, CodeReviewDesk, TraningDesk, RefactoringDesk}
	case PLAYER_LOCATION_ARCHITECTURE_STUDY:
		desks = []*Desk{ArchitectureStudyDesk, ContinuousIntegrationDesk, TaskPrioritizationDesk, CodeReviewDesk, DailyRoutineDesk, RefactoringDesk, CodingDesk, TraningDesk}
	case PLAYER_LOCATION_CONTINUOUS_INTEGRATION:
		desks = []*Desk{ContinuousIntegrationDesk, CodeReviewDesk, ArchitectureStudyDesk, RefactoringDesk, TaskPrioritizationDesk, TraningDesk, DailyRoutineDesk, CodingDesk}
	case PLAYER_LOCATION_CODE_REVIEW:
		desks = []*Desk{CodeReviewDesk, RefactoringDesk, ContinuousIntegrationDesk, TraningDesk, ArchitectureStudyDesk, CodingDesk, TaskPrioritizationDesk, DailyRoutineDesk}
	case PLAYER_LOCATION_REFACTORING:
		desks = []*Desk{RefactoringDesk, TraningDesk, CodeReviewDesk, CodingDesk, ContinuousIntegrationDesk, DailyRoutineDesk, ArchitectureStudyDesk, TaskPrioritizationDesk}
	default:
		panic("unexpected player location")
	}
	return desks[:deskNum]
}

func CalcMoveSteps(from, to PlayerLocation) int {
	return int(to-from+PLAYER_LOCATION_NUM) % int(PLAYER_LOCATION_NUM)
}

// 移動先のデスクを近い順に返します。
func SortedMoveToDesks(current PlayerLocation, desks []*Desk) []*Desk {
	moveTo := make([]*Desk, 0, len(desks))
	for i := PlayerLocation(1); i < PLAYER_LOCATION_NUM; i++ {
		location := (current + i) % PLAYER_LOCATION_NUM
		for _, desk := range desks {
			if location == desk.Location {
				moveTo = append(moveTo, desk)
			}
		}
	}
	return moveTo
}

// ゲームのフェーズ
type GamePhase string

const (
	MOVE       GamePhase = "MOVE"
	GIVE_CARD  GamePhase = "GIVE_CARD"
	THROW_CARD GamePhase = "THROW_CARD"
	PLAY_CARD  GamePhase = "PLAY_CARD"
	RELEASE    GamePhase = "RELEASE"
)

type Turn struct {
	GamePhase          GamePhase
	ApplicationCount   int
	Applications       Applications
	Players            Players
	CardLocationsCount int
	CardPiles          CardPiles
	PossibleCommands   PossibleCommands
}

func (t Turn) Show() {
	DebugLogLine("Trun")
	DebugLogLine("GamePhase: %s", t.GamePhase)
	//t.Applications.Show()
	//t.Players.Show()
	t.CardPiles.Show()
	t.PossibleCommands.Show()
}

type InputReader struct {
	scanner *bufio.Scanner
}

func NewInputReader() *InputReader {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, 1000000), 1000000)
	return &InputReader{
		scanner: scanner,
	}
}

func (r *InputReader) Read() *Turn {
	turn := &Turn{}

	// gamePhase: can be MOVE, GIVE_CARD, THROW_CARD, PLAY_CARD or RELEASE
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &turn.GamePhase)

	var applicationsCount int
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &applicationsCount)
	turn.ApplicationCount = applicationsCount

	applications := make(Applications, applicationsCount)
	for i := range applications {
		// trainingNeeded: number of TRAINING skills needed to release this application
		// codingNeeded: number of CODING skills needed to release this application
		// dailyRoutineNeeded: number of DAILY_ROUTINE skills needed to release this application
		// taskPrioritizationNeeded: number of TASK_PRIORITIZATION skills needed to release this application
		// architectureStudyNeeded: number of ARCHITECTURE_STUDY skills needed to release this application
		// continuousDeliveryNeeded: number of CONTINUOUS_DELIVERY skills needed to release this application
		// codeReviewNeeded: number of CODE_REVIEW skills needed to release this application
		// refactoringNeeded: number of REFACTORING skills needed to release this application
		app := NewApplication()
		r.scanner.Scan()
		var training, coding, dailyRoutine, taskPrioritization, architectureStudy, continuousDelivery, codeReview, refactoring int
		fmt.Sscan(r.scanner.Text(), &app.ObjectType, &app.ID, &training, &coding, &dailyRoutine, &taskPrioritization, &architectureStudy, &continuousDelivery, &codeReview, &refactoring)
		app.ReleaseRequirements[SKILL_TYPE_TRAINING] = training
		app.ReleaseRequirements[SKILL_TYPE_CODING] = coding
		app.ReleaseRequirements[SKILL_TYPE_DAILY_ROUTINE] = dailyRoutine
		app.ReleaseRequirements[SKILL_TYPE_TASK_PRIORITIZATION] = taskPrioritization
		app.ReleaseRequirements[SKILL_TYPE_ARCHITECTURE_STUDY] = architectureStudy
		app.ReleaseRequirements[SKILL_TYPE_CONTINUOUS_INTEGRATION] = continuousDelivery
		app.ReleaseRequirements[SKILL_TYPE_CODE_REVIEW] = codeReview
		app.ReleaseRequirements[SKILL_TYPE_REFACTORING] = refactoring

		applications[i] = app
	}
	turn.Applications = applications

	players := make(Players, 2)
	for i := range players {
		// playerLocation: id of the zone in which the player is located
		// playerPermanentDailyRoutineCards: number of DAILY_ROUTINE the player has played. It allows them to take cards from the adjacent zones
		// playerPermanentArchitectureStudyCards: number of ARCHITECTURE_STUDY the player has played. It allows them to draw more cards
		player := &Player{}
		r.scanner.Scan()
		fmt.Sscan(r.scanner.Text(), &player.Location, &player.Score,
			&player.PermanentDailyRoutineCards, &player.PermanentArchitectureStudyCards)
		players[i] = player
	}
	turn.Players = players

	var cardLocationsCount int
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &cardLocationsCount)
	turn.CardLocationsCount = cardLocationsCount

	cardPiles := make(CardPiles, cardLocationsCount)
	for i := 0; i < cardLocationsCount; i++ {
		// cardsLocation: the location of the card list. It can be HAND, DRAW, DISCARD or OPPONENT_CARDS (AUTOMATED and OPPONENT_AUTOMATED will appear in later leagues)
		pile := NewCardPile()
		r.scanner.Scan()
		var location string
		var training, coding, dailyRoutine, taskPrioritization, architectureStudy, continuousDelivery, codeReview, refactoring, bonus, technicalDebt int
		fmt.Sscan(r.scanner.Text(), &location, &training, &coding, &dailyRoutine, &taskPrioritization, &architectureStudy, &continuousDelivery, &codeReview, &refactoring, &bonus, &technicalDebt)
		pile.CardPileType = CardPileType(location)
		pile.SetCount(TraningCard, training)
		pile.SetCount(CodingCard, coding)
		pile.SetCount(DailyRoutineCard, dailyRoutine)
		pile.SetCount(TaskPrioritizationCard, taskPrioritization)
		pile.SetCount(ArchitectureStudyCard, architectureStudy)
		pile.SetCount(ContinuousIntegrationCard, continuousDelivery)
		pile.SetCount(CodeReviewCard, codeReview)
		pile.SetCount(RefactoringCard, refactoring)
		pile.SetCount(BonusCard, bonus)
		pile.SetCount(DebtCard, technicalDebt)

		cardPiles[pile.CardPileType] = pile
	}
	// 場のカードを確認しておく
	boardPile := CalcBoardPile(cardPiles, players.Opponent())
	cardPiles[CARD_PILE_TYPE_BOARD] = boardPile
	// 与えられなかった山は空
	for _, t := range AllCardPileTypes {
		if _, ok := cardPiles[t]; !ok {
			cardPiles[t] = NewCardPile()
		}
	}

	turn.CardPiles = cardPiles

	var possibleMovesCount int
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &possibleMovesCount)

	possibleCommands := make(PossibleCommands, 0, possibleMovesCount)
	for i := 0; i < possibleMovesCount; i++ {
		r.scanner.Scan()
		possibleMove := r.scanner.Text()
		if possibleMove != string(COMMAND_RANDOM) {
			possibleCommands = append(possibleCommands, ParsePossibleCommand(possibleMove))
		}
	}
	turn.PossibleCommands = possibleCommands

	return turn
}

func GiveCard(turn *Turn, commander Commander) {
	if releaseTarget != nil {
		// 不要なカードから渡す
		card := FindNotNeededCard(turn.CardPiles[CARD_PILE_TYPE_HAND], releaseTarget)
		if card != nil {
			commander.Give(card.CardType)
			return
		}
	}

	turn.PossibleCommands.PlayRandomly(commander)
}

func Move(turn *Turn, commander Commander) {
	board := turn.CardPiles.Get(CARD_PILE_TYPE_BOARD)
	hand := turn.CardPiles.Get(CARD_PILE_TYPE_HAND)

	// 初手
	if turn.Players.MySelf().Location == PLAYER_LOCATION_INIT {
		if turn.Players.Opponent().Location == PLAYER_LOCATION_INIT || turn.Players.Opponent().Location == PLAYER_LOCATION_TRAINING {
			commander.Move(PLAYER_LOCATION_DAILY_ROUTINE)
			return
		}
		if turn.Players.Opponent().Location == PLAYER_LOCATION_CONTINUOUS_INTEGRATION || turn.Players.Opponent().Location == PLAYER_LOCATION_ARCHITECTURE_STUDY {
			commander.Move(PLAYER_LOCATION_DAILY_ROUTINE)
			return
		}
		if turn.Players.Opponent().Location == PLAYER_LOCATION_DAILY_ROUTINE {
			commander.Move(PLAYER_LOCATION_CONTINUOUS_INTEGRATION)
			return
		}
	}

	// ノーコストでリリースを行えるときはリリースを行う
	{
		for _, moveTo := range AllDesks {
			// 今いるデスクには移動できない
			if moveTo.Location == turn.Players.MySelf().Location {
				continue
			}

			taskManagement := turn.Players.MySelf().Location > moveTo.Location
			noisyDesk := FindNearDesks(turn.Players.Opponent().Location, 1).Has(moveTo)

			automated := turn.CardPiles.Get(CARD_PILE_TYPE_AUTOMATED)

			for _, pickFrom := range FindNearDesks(moveTo.Location, turn.Players.MySelf().PermanentDailyRoutineCards) {
				card := pickFrom.SkillCard
				if board.GetCount(card) == 0 {
					if board.GetCount(BonusCard) > 0 {
						card = BonusCard
					} else {
						card = nil
					}
				}

				additional := NewCardPile()
				if card != nil {
					additional.Cards[card] = 1
				}
				app := FindReleasableApplicationWithouCost2(hand, automated, additional, turn.Applications, taskManagement, noisyDesk)
				if app != nil {
					commander.Move(moveTo.Location, pickFrom.Location)
					releaseTarget = app
					DebugLogLine("will release %d", app.ID)
					return
				}

				// カードのプレイも加味
				if !taskManagement && !noisyDesk {
					handPlayed := hand.Copy()
					additionalPlayed := hand.Copy()
					if hand.Has(CodeReviewCard, 1) {
						handPlayed.Cards[CodeReviewCard]--
						handPlayed.Cards[BonusCard] += 2
					} else if additional.Has(CodeReviewCard, 1) {
						additionalPlayed.Cards[CodeReviewCard]--
						additionalPlayed.Cards[BonusCard] += 2
					}

					app := FindReleasableApplicationWithouCost2(handPlayed, automated, additionalPlayed, turn.Applications, taskManagement, noisyDesk)
					if app != nil {
						commander.Move(moveTo.Location, pickFrom.Location)
						releaseTarget = app
						DebugLogLine("will release %d", app.ID)
						return
					}
				}
			}
		}
	}

	if hand.Has(BonusCard, 1) && !hand.Has(ContinuousIntegrationCard, 1) {
		// 取れるなら CI カードを取りに行く
		if board.Has(ContinuousIntegrationCard, 1) {
			desks := FindNearDesks(PLAYER_LOCATION_CONTINUOUS_INTEGRATION, turn.Players.MySelf().PermanentDailyRoutineCards)
			exclusives := FindNearDesks(turn.Players.Opponent().Location, 1)
			deskCandidates := DesksWithout(desks, exclusives)
			sortedDesks := SortedMoveToDesks(turn.Players.MySelf().Location, deskCandidates)
			if len(sortedDesks) > 0 {
				moveTo := sortedDesks[0]
				if moveTo.Location > turn.Players.MySelf().Location {
					if moveTo.Location != PLAYER_LOCATION_CONTINUOUS_INTEGRATION {
						commander.Move(moveTo.Location, PLAYER_LOCATION_CONTINUOUS_INTEGRATION)
						return
					} else {
						commander.Move(moveTo.Location)
						return
					}
				}
			}
		}
	}

	// 次にリリースするために必要なカードを取りに行く

	// ダメージのない最も近い場所へ移動
	deskCandidates := DesksWithout(ListQuietDesk(turn.Players.Opponent().Location), []*Desk{GetPlayerDesk(turn.Players.MySelf().Location)})
	if turn.Players.MySelf().Location >= PLAYER_LOCATION_CONTINUOUS_INTEGRATION && hand.Cards.TotalSkillCards() == 0 {
		deskCandidates = DesksWithout(deskCandidates, []*Desk{CodeReviewDesk, RefactoringDesk})
	}
	desk := FindNearestDesk(deskCandidates, turn.Players.MySelf().Location)
	if turn.Players.MySelf().PermanentDailyRoutineCards > 0 {
		// 欲しいカードから取る
		nearDesks := FindNearDesks(desk.Location, turn.Players.MySelf().PermanentDailyRoutineCards)
		automated := turn.CardPiles[CARD_PILE_TYPE_AUTOMATED]
		needs := CalcSkillNeeds(automated, turn.Applications)
		for _, need := range needs.RequireSkills() {
			for _, d := range nearDesks {
				if need == d.SkillCard.SkillType {
					commander.Move(desk.Location, d.Location)
					return
				}
			}
		}
	} else {
		commander.Move(desk.Location)
	}
}

type MoveInfo struct {
	MoveTo   *Desk
	PickFrom *Desk
}

func FindObtainableCardsWithoutCost(turn *Turn) map[*Card]MoveInfo {
	board := turn.CardPiles.Get(CARD_PILE_TYPE_BOARD)
	current := turn.Players.MySelf().Location
	noisy := FindNearDesks(turn.Players.Opponent().Location, 1)
	moveToDesks := DesksWithout(ListDesksAfter(current), noisy)

	obtainableDesks := make(map[*Desk]*Desk, PLAYER_LOCATION_NUM)
	for _, moveTo := range moveToDesks {
		for _, d := range FindNearDesks(moveTo.Location, turn.Players.MySelf().PermanentDailyRoutineCards) {
			obtainableDesks[d] = moveTo
		}
	}

	obtainableCards := make(map[*Card]MoveInfo, len(obtainableDesks))
	for moveTo, pickFrom := range obtainableDesks {
		card := BonusCard
		if board.Has(pickFrom.SkillCard, 1) {
			card = pickFrom.SkillCard
		}
		obtainableCards[card] = MoveInfo{moveTo, pickFrom}
	}

	return obtainableCards
}

/*
 * `TRAINING` 山札から2枚引き、さらに追加でスキルカードを使用できる
 * `CODING` 山札から1枚引き、さらに追加で2枚スキルカードを使用できる
 * `DAILY_ROUTINE` 移動後に隣のデスクからカードを取れるようになる。重ねて使用した場合はさらに隣のデスクからカードを取れる。リリースするまで効果が継続。
 * `TASK_PRIORITIZATION` デスクにカードを戻して、別のデスクからカードを取る
 * `ARCHITECTURE_STUDY` ターン開始時に1枚多くカードを引く。リリースするまで効果が継続
 * `CONTINUOUS_INTEGRATION` 特定のスキルをリリース時に常に利用できるようにする。リリースに使用してもなくならない
 * `CODE_REVIEW` ボーナスカードを2枚受け取って捨山に置く
 * `REFACTORING` 負債カードを1枚取り除く。負債カードがない場合は何も起こらない
 */

func PlayCard(turn *Turn, commander Commander) {
	hand := turn.CardPiles.Get(CARD_PILE_TYPE_HAND)
	board := turn.CardPiles.Get(CARD_PILE_TYPE_BOARD)
	automatedPile := turn.CardPiles.Get(CARD_PILE_TYPE_AUTOMATED)

	// 移動時の指示に従う
	if releaseTarget != nil && playTarget == nil {
		commander.Wait()
		return
	}
	if playTarget != nil && hand.Has(playTarget, 1) {
		if playTarget == CodeReviewCard {
			commander.CodeReview()
			return
		}
	}

	// 序盤はボーナスが CD できるときはする
	if turn.Players.MySelf().Score < 3 && hand.Has(ContinuousIntegrationCard, 1) && hand.Has(BonusCard, 1) {
		commander.ContinuousDelivery(BonusCard.CardType)
		return
	}

	// 低コストでリリースできそうなときはスキルを利用しない
	minApp, minCost := FindLowestReleaseCostApplication(hand, automatedPile, turn.Applications)
	if turn.Players.MySelf().Score == 4 && minApp != nil && minCost == 0 || turn.Players.MySelf().Score < 4 && minCost <= AllowedCost(turn) {
		commander.Wait()
		DebugLogLine("wait for release app %d", minApp.ID)
		return
	}

	// `TRAINING` 山札から2枚引き、さらに追加でスキルカードを使用できる
	if hand.Has(TraningCard, 1) {
		commander.Traning()
		return
	}

	// `CODING` 山札から1枚引き、さらに追加で2枚スキルカードを使用できる
	if hand.Has(CodingCard, 1) {
		commander.Coding()
		return
	}

	// リリースに必要なものが CD できるときはする
	if hand.Has(ContinuousIntegrationCard, 1) {
		// ボーナスカードがあれば、必ずボーナスカードを CD する
		if hand.Has(BonusCard, 1) {
			commander.ContinuousDelivery(BonusCard.CardType)
			return
		}

		// その他のスキルカードも CD を試みる
		skills := CalcSkillNeeds(automatedPile, turn.Applications).RequireSkills()
		for _, skill := range skills {
			count := 1
			if skill == SKILL_TYPE_CONTINUOUS_INTEGRATION {
				count++
			}
			card := SkillCardMap[skill]
			if hand.Has(card, count) {
				commander.ContinuousDelivery(card.CardType)
				return
			}
		}
	}

	// `CODE_REVIEW` ボーナスカードを2枚受け取って捨山に置く
	if hand.Has(CodeReviewCard, 1) {
		commander.CodeReview()
		return
	}

	// `REFACTORING` 負債カードを1枚取り除く。負債カードがない場合は何も起こらない
	if hand.Has(RefactoringCard, 1) && hand.Has(DebtCard, 1) {
		commander.Refactoring()
		return
	}

	// `DAILY_ROUTINE` 移動後に隣のデスクからカードを取れるようになる。重ねて使用した場合はさらに隣のデスクからカードを取れる。リリースするまで効果が継続。
	if hand.Has(DailyRoutineCard, 1) {
		commander.DailyRoutine()
		return
	}

	// `ARCHITECTURE_STUDY` ターン開始時に1枚多くカードを引く。リリースするまで効果が継続
	if hand.Has(ArchitectureStudyCard, 1) {
		commander.ArchitectureStudy()
		return
	}

	// `TASK_PRIORITIZATION` デスクにカードを戻して、別のデスクからカードを取る
	if hand.Has(TaskPrioritizationCard, 1) && hand.Has(DailyRoutineCard, 1) && board.Has(ContinuousIntegrationCard, 1) {
		commander.TaskPrioritization(DailyRoutineCard.CardType, ContinuousIntegrationCard.CardType)
		return
	}

	commander.Wait()
}

func Release(turn *Turn, commander Commander) {
	pile := turn.CardPiles.Get(CARD_PILE_TYPE_HAND)
	automatedPile := turn.CardPiles.Get(CARD_PILE_TYPE_AUTOMATED)
	if turn.Players.MySelf().Score < 4 {
		app, cost := FindLowestReleaseCostApplication(pile, automatedPile, turn.Applications)
		if app != nil && cost <= AllowedCost(turn) {
			commander.Release(app.ID)
		} else {
			commander.Wait()
		}
	} else {
		// 最後は必ずリリースする
		turn.PossibleCommands[0].Play(commander)
	}
}

func AllowedCost(turn *Turn) int {
	hand := turn.CardPiles.Get(CARD_PILE_TYPE_HAND)
	draw := turn.CardPiles.Get(CARD_PILE_TYPE_DRAW)
	discard := turn.CardPiles.Get(CARD_PILE_TYPE_DISCARD)
	skills := hand.Cards.TotalSkillCards() + draw.Cards.TotalSkillCards() + discard.Cards.TotalSkillCards()
	debtCards := hand.Cards[DebtCard] + draw.Cards[DebtCard] + discard.Cards[DebtCard]
	switch turn.Players.MySelf().Score {
	case 0:
		return skills - debtCards
	case 1:
		return skills - debtCards + 1
	case 2:
		return skills - debtCards + 2
	case 3:
		return skills - debtCards + 3
	}
	return 0
}

func ThrowCard(turn *Turn, commander Commander) {
	hand := turn.CardPiles.Get(CARD_PILE_TYPE_HAND)

	if releaseTarget != nil {
		// 不要なカードから捨てる
		card := FindNotNeededCard(hand, releaseTarget)
		if card != nil {
			commander.Throw(card.CardType)
			return
		}
	}

	// 不要な順に捨てる
	for _, card := range []*Card{
		TaskPrioritizationCard,
		ArchitectureStudyCard,
		DailyRoutineCard,
		BonusCard,
		CodeReviewCard,
		RefactoringCard,
		TraningCard,
		CodingCard,
		ContinuousIntegrationCard,
	} {
		if hand.Has(card, 1) {
			commander.Throw(card.CardType)
			return
		}
	}

	turn.PossibleCommands.PlayRandomly(commander)
}

/**
 * Complete the hackathon before your opponent by following the principles of Green IT
 **/

// 次にリリースする予定のアプリケーション
var releaseTarget *Application

// 次にプレイする予定のカード
var playTarget *Card

func main() {
	reader := NewInputReader()
	commander := Commander{}

	for {
		// 入力を読み込み
		turn := reader.Read()
		// turn.Show()

		switch turn.GamePhase {
		case MOVE:
			releaseTarget = nil
			playTarget = nil
			Move(turn, commander)

		case THROW_CARD:
			ThrowCard(turn, commander)

		case GIVE_CARD:
			GiveCard(turn, commander)

		case PLAY_CARD:
			PlayCard(turn, commander)

		case RELEASE:
			Release(turn, commander)
		}
	}
}

// プレーヤーの位置
type PlayerLocation int

const (
	PLAYER_LOCATION_INIT                   PlayerLocation = -1
	PLAYER_LOCATION_TRAINING               PlayerLocation = 0
	PLAYER_LOCATION_CODING                 PlayerLocation = 1
	PLAYER_LOCATION_DAILY_ROUTINE          PlayerLocation = 2
	PLAYER_LOCATION_TASK_PRIORITIZATION    PlayerLocation = 3
	PLAYER_LOCATION_ARCHITECTURE_STUDY     PlayerLocation = 4
	PLAYER_LOCATION_CONTINUOUS_INTEGRATION PlayerLocation = 5
	PLAYER_LOCATION_CODE_REVIEW            PlayerLocation = 6
	PLAYER_LOCATION_REFACTORING            PlayerLocation = 7
	PLAYER_LOCATION_NUM                    PlayerLocation = 8
)

const (
	PLAYER_INDEX_MYSELF   = 0
	PLAYER_INDEX_OPPONENT = 1
)

func CalcPlayerLocationDistance(from, to PlayerLocation) int {
	return int((to - from + PLAYER_LOCATION_NUM) % PLAYER_LOCATION_NUM)
}

// プレーヤー
type Player struct {
	Location                        PlayerLocation // id of the zone in which the player is located
	Score                           int            // the amount of released applications
	PermanentDailyRoutineCards      int            // number of DAILY_ROUTINE the player has played. It allows them to take cards from the adjacent zones
	PermanentArchitectureStudyCards int            // number of ARCHITECTURE_STUDY the player has played. It allows them to draw more cards
}

func (p Player) Show() {
	DebugLogLine("Player")
	DebugLogLine("  Location: %d", p.Location)
	DebugLogLine("  Score: %d", p.Score)
	DebugLogLine("  PermanentDailyRoutineCards: %d", p.PermanentDailyRoutineCards)
	DebugLogLine("  PermanentArchitectureStudyCards: %d", p.PermanentArchitectureStudyCards)
}

type Players []*Player

func (p Players) MySelf() *Player {
	return p[PLAYER_INDEX_MYSELF]
}

func (p Players) Opponent() *Player {
	return p[PLAYER_INDEX_OPPONENT]
}

func (p Players) Show() {
	for _, player := range p {
		player.Show()
	}
}

// スキルの種類
type SkillType string

const (
	SKILL_TYPE_TRAINING               SkillType = "TRANING"
	SKILL_TYPE_CODING                 SkillType = "CODING"
	SKILL_TYPE_DAILY_ROUTINE          SkillType = "DAILY_ROUTINE"
	SKILL_TYPE_TASK_PRIORITIZATION    SkillType = "TASK_PRIORITIZATION"
	SKILL_TYPE_ARCHITECTURE_STUDY     SkillType = "ARCHITECTURE_STUDY"
	SKILL_TYPE_CONTINUOUS_INTEGRATION SkillType = "CONTINUOUS_INTEGRATION"
	SKILL_TYPE_CODE_REVIEW            SkillType = "CODE_REVIEW"
	SKILL_TYPE_REFACTORING            SkillType = "REFACTORING"
	SKILL_TYPE_BONUS                  SkillType = "BONUS"
	SKILL_TYPE_NONE                   SkillType = "NONE"
	SKILL_TYPE_NUM                              = 10
)

// すべてのスキル
var AllSkills = []SkillType{
	SKILL_TYPE_TRAINING,
	SKILL_TYPE_CODING,
	SKILL_TYPE_DAILY_ROUTINE,
	SKILL_TYPE_TASK_PRIORITIZATION,
	SKILL_TYPE_ARCHITECTURE_STUDY,
	SKILL_TYPE_CONTINUOUS_INTEGRATION,
	SKILL_TYPE_CODE_REVIEW,
	SKILL_TYPE_REFACTORING,
}

type Skill struct {
	SkillType SkillType
}

const DEBUG = true

func DebugLog(format string, a ...interface{}) {
	if !DEBUG {
		return
	}
	_, _ = fmt.Fprintf(os.Stderr, format, a...)
}

func DebugLogLine(format string, a ...interface{}) {
	if !DEBUG {
		return
	}
	DebugLog(format+"\n", a...)
}

func MinInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func MaxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}
