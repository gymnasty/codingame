# Green Circle

## Goal

The game takes place in a company IT department. Management is organizing a hackathon on the Green IT theme. Two development teams are competing to be the most efficient team. Release the required applications before the opponent team to have more points, but respect the Green IT requirements or you'll get drowned in the Technical Debt.

ゲームは会社のIT部門で行われます。経営陣は、グリーンITをテーマにしたハッカソンを開催しています。 2つの開発チームが最も効率的なチームになるために競争しています。対戦相手チームの前に必要なアプリケーションをリリースしてポイントを増やしますが、グリーンIT要件を尊重しないと、技術的負債に溺れてしまいます。

## Rules

Each player has an IT development team. The game takes place in an office with 8 desks. The hackathon will last many turns. Each turn, the players will play one after the other.

各プレーヤーにはIT開発チームがあります。ゲームは8つの机があるオフィスで行われます。ハッカソンは何ターンも続きます。毎ターン、プレイヤーは次々とプレイします。

This game is based on Deck Building. Each team will have its own deck of cards that will get bigger during the game.

このゲームはデッキビルディングに基づいています。各チームには、ゲーム中に大きくなる独自のカードのデッキがあります。

Each team starts the game with 4 BONUS skill cards and 4 Technical Debt cards. Those cards will be their personal draw pile. And their personal discard pile will be reshuffled into the draw pile when it gets empty.

各チームは、4枚のボーナススキルカードと4枚の技術的負債カードでゲームを開始します。それらのカードは彼らの個人的なドローパイルになります。そして、彼らの個人的な廃棄パイルは、それが空になるとドローパイルに再シャッフルされます。

Deckbuilding: the player discard pile is reshuffled to create the player's draw pile

デッキ構築：プレイヤーの捨て札パイルが再シャッフルされ、プレイヤーのドローパイルが作成されます

The team will get more cards and will lose some during the game.

チームはより多くのカードを取得し、ゲーム中にいくつかを失います。

### The game space (the office)

The office is made of 8 desks. Each desk is dedicated to a specific skill

オフィスは8つの机で構成されています。各デスクは特定のスキルに専念しています

* TRAINING (0)
* CODING (1)
* DAILY_ROUTINE (2)
* TASK_PRIORITIZATION (3)
* ARCHITECTURE_STUDY (4)
* CONTINUOUS_INTEGRATION (5)
* CODE_REVIEW (6)
* REFACTORING (7)

Those desks are numbered from 0 to 7. Each of those desks contains 5 skill cards at the beginning of the game.

それらの机には0から7までの番号が付けられています。これらの机のそれぞれには、ゲームの開始時に5枚のスキルカードが含まれています。

Due to covid-19, Management enforced a one way path in the office so as not to bump into other people. You must always move in the same direction.

covid-19により、経営陣は他の人にぶつからないようにオフィスで一方向の経路を強制しました。常に同じ方向に移動する必要があります。

This forces you to go through the administrative task desk when you move from the desk 7 to the desk 0.

これにより、デスク7からデスク0に移動するときに、管理タスクデスクを通過する必要があります。

### Applications

Each application needs some skills to be released. Applications are the same for both players. Once an application has been released, it is removed from the game for both players.

各アプリケーションをリリースするには、いくつかのスキルが必要です。アプリケーションは両方のプレーヤーで同じです。アプリケーションがリリースされると、両方のプレーヤーのゲームから削除されます。

Applications will get bigger through the leagues.

アプリケーションはリーグを通じて大きくなります。

A small application needs 3 sets of 2 skills (like 2 REFACTORING, 2 TRAINING and 2 CODING)

小さなアプリケーションには、2つのスキルの3つのセットが必要です（2つのリファクタリング、2つのトレーニング、2つのコーディングなど）

A big application needs 2 sets of 4 skills (like 4 DAILY_ROUTINE and 4 CODE_REVIEW)

大きなアプリケーションには、4つのスキルの2つのセットが必要です（4つのDAILY_ROUTINEと4つのCODE_REVIEWなど）

Each BONUS skill card provides one good skill (any of them) and one shoddy skill.

各ボーナススキルカードは、1つの優れたスキル（それらのいずれか）と1つの粗雑なスキルを提供します。

Each specific skill card provides 2 good skills (of this specific type) and 2 shoddy skills.

それぞれの特定のスキルカードは、2つの優れたスキル（この特定のタイプの）と2つの粗雑なスキルを提供します。

For instance, a CODING card provides 2 CODING skills and 2 shoddy skills. Those shoddy skills can be used to replace any missing skill, but the quality will not be there.

たとえば、コーディングカードは2つのコーディングスキルと2つの粗雑なスキルを提供します。これらの粗雑なスキルは、不足しているスキルを置き換えるために使用できますが、品質はそこにありません。

Each shoddy skill used during an application release will give you a Technical Debt card. Those cards are useless. They are only here to slow you down since you can draw some from your draw pile.

アプリケーションのリリース中に使用される粗雑なスキルごとに、技術的負債カードが提供されます。それらのカードは役に立たない。ドローパイルからいくらか引くことができるので、彼らはあなたを遅くするためだけにここにいます。

Examples of how to release an application with various skills

さまざまなスキルを持つアプリケーションをリリースする方法の例

### Turn Description

At the beginning of your turn, your team draws 4 cards at random from their draw pile.

あなたのターンの開始時に、あなたのチームはドローパイルからランダムに4枚のカードを引きます。

#### 1. Move

The team begins by moving to another desk (they must leave the desk for the other team).
They will get one skill card from the desk they are moving to (TRAINING, CODE_REVIEW, REFACTORING, CODING...).
If the desk is empty (no more cards), the team will get a BONUS skill card instead.

チームは別のデスクに移動することから始めます（他のチームのためにデスクを離れる必要があります）。
移動先のデスクからスキルカードを1枚取得します（トレーニング、CODE_REVIEW、リファクタリング、コーディング...）。
デスクが空の場合（カードがなくなった場合）、チームは代わりにボーナススキルカードを取得します。

If the team arrives on the opponent's desk (or on a desk next to the opponent), they will disturb the opponent (they are noisy when they work).
As an apology, the team must GIVE one skill card from their hand to the opponent (the team can choose which one).
If they do not have any skill card in hand, they get 2 Technical Debt cards.

チームが対戦相手の机（または対戦相手の隣の机）に到着すると、対戦相手の邪魔をします（作業中は騒がしいです）。
謝罪として、チームは自分の手札から対戦相手に1枚のスキルカードを渡さなければなりません（チームはどちらかを選択できます）。
手札にスキルカードがない場合は、2枚の技術的負債カードを受け取ります。

If the team goes through the administrative task desk (between the desks 7 and 0), they lose 2 skills cards from their hand (they can choose which ones).

チームが管理タスクデスク（デスク7と0の間）を通過すると、手札から2枚のスキルカードを失います（どちらかを選択できます）。

#### 2. Use a skill

After moving, the team can use one of their available skills in hand. This phase will only appear if you can play a skill.

移動後、チームは利用可能なスキルの1つを手に使用できます。このフェーズは、スキルをプレイできる場合にのみ表示されます。

The skills and their actions:

* TRAINING (0).
  * The team draws 2 cards and can play 1 more card from their hand.
  * チームは2枚のカードを引き、手札からさらに1枚のカードをプレイできます。
* CODING (1).
  * The team draws 1 card and can play 2 more cards from their hand.
  * チームは1枚のカードを引き、手札からさらに2枚のカードをプレイできます。
* DAILY_ROUTINE (2).
  * This is a permanent skill: once played, it stays active until the team has released one application.
  * これは永続的なスキルです。一度プレイすると、チームが1つのアプリケーションをリリースするまでアクティブなままになります。
  * After moving, the team can get their new skill card from a desk one step away. If you play many cards, their effects will add up.
  * 引っ越した後、チームは一歩離れた机から新しいスキルカードを入手できます。あなたがたくさんのカードをプレイする場合、それらの効果は合計されます。
* TASK_PRIORITIZATION (3).
  * The team will remove one skill card from their hand and put it back on the board.
  * チームは手札からスキルカードを1枚取り出し、ボードに戻します。
  * They will then take another available skill card from the board.
  * その後、ボードから別の利用可能なスキルカードを取得します。
* ARCHITECTURE_STUDY (4).
  * This is a permanent skill: once played, it stays active until the team has released one application.
  * The team will draw one more card at the beginning of their turn. If you play many cards, their effects will add up.
  * これは永続的なスキルです。一度プレイすると、チームが1つのアプリケーションをリリースするまでアクティブなままになります。
  * チームは自分のターンの開始時にもう1枚のカードを引きます。あなたがたくさんのカードをプレイする場合、それらの効果は合計されます。
* CONTINUOUS_INTEGRATION (5).
  * The team automate one of their skills from their hand.
  * チームは自分のスキルの1つを手から自動化します。
  * This card will never be discarded and will always be available, but only to release applications.
  * このカードは破棄されることはなく、いつでも利用できますが、アプリケーションをリリースする場合に限ります。
* CODE_REVIEW (6).
  * The team gets 2 BONUS skill cards from the board and puts them in their discard pile.
  * チームはボードから2枚のボーナススキルカードを受け取り、それらを捨て札の山に置きます。
* REFACTORING (7).
  * The team can remove a Technical Debt card from their hand and put it back on the board.
  * チームは技術的負債カードを手札から取り除き、ボードに戻すことができます。

#### 3. Release an application

The team can use their available skills in hand to RELEASE an application.
This phase will only appear if you have enough skills to do it.
Don't forget, if you use shoddy skills, you'll receive Technical Debt cards!

チームは、利用可能なスキルを手元に使用して、アプリケーションをリリースできます。
このフェーズは、それを実行するのに十分なスキルがある場合にのみ表示されます。
粗雑なスキルを使用すると、技術的負債カードを受け取ることを忘れないでください！

When you release an application, your permanent skills are discarded.

アプリケーションをリリースすると、永続的なスキルは破棄されます。

#### 4. End of the turn

All cards in hand (skills and Technical Debt) are discarded.

手札のカード（スキルと技術的負債）はすべて捨てられます。

### Game end

The hackathon stops as soon as one team releases its 5th application.

1つのチームが5番目のアプリケーションをリリースするとすぐに、ハッカソンは停止します。

Beware, since this hackathon is on the theme of Green IT, the referees will be paying close attention to the quality of the last released application.
The 5th application of each team cannot be released with shoddy skills!

このハッカソンはグリーンITをテーマにしているため、レフリーは最後にリリースされたアプリケーションの品質に細心の注意を払うことに注意してください。
各チームの5番目のアプリケーションは、粗雑なスキルではリリースできません。

When a team has released 5 applications, the game will end once all teams have played the same amount of turns.

チームが5つのアプリケーションをリリースすると、すべてのチームが同じターン数をプレイするとゲームが終了します。

The winner is the team that released the most applications.
In case of a tie, the winning team will be the one that has the less Technical Debt cards.

勝者は、最も多くのアプリケーションをリリースしたチームです。
同点の場合、勝者のチームは技術的負債カードの数が少ないチームになります。

### Victory Conditions

You released 5 applications before your opponent.
You released more applications than your opponent after 200 game phases.
In case of a tie, you have less Technical Debt cards than your opponent.

対戦相手の前に5つのアプリケーションをリリースしました。
200のゲームフェーズの後、対戦相手よりも多くのアプリケーションをリリースしました。
同点の場合、あなたは対戦相手よりも少ない技術的負債カードを持っています。

### Defeat Conditions

Your program does not provide a valid command in time.

プログラムが時間内に有効なコマンドを提供していません。

## Debugging tips

Hover over a player, card or card pile to see extra information about it
Append text after any command and that text will appear above your player
Press the gear icon on the viewer to access extra display options
Use the keyboard to control the action: space to play/pause, arrows to step 1 frame at a time

プレーヤー、カード、またはカードの山にカーソルを合わせると、それに関する追加情報が表示されます
コマンドの後にテキストを追加すると、そのテキストがプレーヤーの上に表示されます
ビューアの歯車アイコンを押して、追加の表示オプションにアクセスします
キーボードを使用してアクションを制御します：再生/一時停止するスペース、一度に1フレームずつステップする矢印

## Technical Details

The game starts with 5 skill cards on each desk, 36 BONUS skill cards and 100 Technical Debt cards in all the game.

ゲームは、各デスクに5枚のスキルカード、36枚のボーナススキルカード、100枚の技術的負債カードから始まります。

Each phase is given to the players only if they can perform an action. If they have no choice and the action is mandatory, it will be played automatically.

各フェーズは、プレーヤーがアクションを実行できる場合にのみプレーヤーに与えられます。選択の余地がなく、アクションが必須の場合は、自動的に再生されます。

If you need to draw a skill card from a desk and there are none left, you will draw a BONUS skill card instead.
If there is no BONUS skill card left, you will get no card.

机からスキルカードを引く必要があり、残っていない場合は、代わりにボーナススキルカードを引くことになります。
ボーナススキルカードが残っていない場合、カードはもらえません。

In case of a tie, the player who gained the lowest amount of Technical Debt will win.

同点の場合、技術的負債の額が最も少ないプレーヤーが勝ちます。

The 5th application cannot be released with shoddy skills. You will need to have the exact skills.

5番目のアプリケーションは、見苦しいスキルでリリースすることはできません。あなたは正確なスキルを持っている必要があります。

The source code of this game is available on this GitHub repo.

このゲームのソースコードは、このGitHubリポジトリで入手できます。

<https://github.com/societe-generale/GreenCircle>

## Game Input/Output

### Input for One Game Turn

Line 1: `gamePhase` the name of the current game phase. It can be `MOVE`, `RELEASE`
Line 2: `applicationCount` for the amount of applications that are still to be released.
Next `applicationCount` lines: the word APPLICATION followed by 9 integers, the description of the application to release and the needed skills to release them (APPLICATION `applicationId` `trainingNeeded` `codingNeeded` `dailyRoutineNeeded` `taskPrioritizationNeeded` `architectureStudyNeeded` `continuousDeliveryNeeded` `codeReviewNeeded` `refactoringNeeded`).
1 line per player: 4 integers (Your data is always given first):
`location` : the desk currently used by the player (-1 in the first turn).
`score` : the amount of released applications.
`permanentDailyRoutineCards` : ignore for this league.
`permanentArchitectureStudyCards` : ignore for this league.

Next line: `cardLocationsCount` for the amount of locations where the players have some cards.
Next `cardLocationsCount` lines: the location name followed by 10 integers, the number of cards in this location (`cardLocation` `trainingCardsCount` `codingCardsCount` `dailyRoutineCardsCount` `taskPrioritizationCardsCount` `architectureStudyCardsCount` `continuousDeliveryCardsCount` `codeReviewCardsCount` `refactoringCardsCount` `bonusCardsCount` `technicalDebtCardsCount`). Location can be `HAND`, `DRAW`, `DISCARD` or `OPPONENT_CARDS` (all the opponent's cards in their hand, draw and discard)
Next line: `possibleMovesCount` for the amount of possible actions.
Next `possibleMovesCount` lines: a string, one possible action.

### Output for One Game Turn

1 line containing one of the following actions depending on the game phase:

* RANDOM: the player decides to do one of the possible action at random.
* WAIT: the player decides not to do any optional action.
* MOVE zoneId: the player moves to another desk.
  * This action is mandatory and is only available in the MOVE phase.
* MOVE zoneId cardTypeToTake: the player moves to another desk and gets a skill card from another desk.
  * This action is mandatory and is only available in the MOVE phase and if the player has already played some DAILY_ROUTINE cards.
* GIVE cardTypeId: the player gives a skill card from their hand to their opponent.
  * This action is mandatory and is only available in the GIVE_CARD phase.
* THROW cardTypeId: the player puts back a skill card from their hand to the game board.
  * This action is mandatory and is only available in the THROW_CARD phase.
* TRAINING: the player plays a TRAINING card from their hand.
  * This action is optional and is only available in the PLAY_CARD phase.
* CODING: the player plays a CODING card from their hand.
  * This action is optional and is only available in the PLAY_CARD phase.
* DAILY_ROUTINE: the player plays a DAILY_ROUTINE card from their hand.
  * This action is optional and is only available in the PLAY_CARD phase.
* TASK_PRIORITIZATION cardTypeToThrow cardTypeToTake: the player plays a TASK_PRIORITIZATION card from their hand.
  * This action is optional and is only available in the PLAY_CARD phase.
* ARCHITECTURE_STUDY: the player plays a ARCHITECTURE_STUDY card from their hand.
  * This action is optional and is only available in the PLAY_CARD phase.
* CONTINUOUS_INTEGRATION cardTypeToAutomate: the player plays a CONTINUOUS_INTEGRATION card from their hand.
  * This action is optional and is only available in the PLAY_CARD phase.
* CODE_REVIEW: the player plays a CODE_REVIEW card from their hand.
  * This action is optional and is only available in the PLAY_CARD phase.
* REFACTORING: the player plays a REFACTORING card from their hand.
  * This action is optional and is only available in the PLAY_CARD phase.
* RELEASE applicationId: the player releases an application.
  * This action is optional and is only available in the RELEASE phase.

You may append text to a command to have it displayed in the viewer above your player.

Examples:
MOVE 3
RELEASE 16
GIVE 8 a gift for you!
REFACTORING goodbye technical debt!
CONTINUOUS_INTEGRATION 1
TASK_PRIORITIZATION 8 2
WAIT nothing to do...
RANDOM got no idea...

### Constraints

Response time per turn ≤ 50ms (Doing Green IT means sparing resources)
Response time for the first turn ≤ 1000ms