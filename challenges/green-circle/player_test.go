package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCalcPlayerLocationDistance(t *testing.T) {
	assert.Equal(t, 1, CalcPlayerLocationDistance(PLAYER_LOCATION_INIT, PLAYER_LOCATION_TRAINING))
	assert.Equal(t, 2, CalcPlayerLocationDistance(PLAYER_LOCATION_INIT, PLAYER_LOCATION_CODING))
	assert.Equal(t, 2, CalcPlayerLocationDistance(PLAYER_LOCATION_TRAINING, PLAYER_LOCATION_DAILY_ROUTINE))
	assert.Equal(t, 0, CalcPlayerLocationDistance(PLAYER_LOCATION_TRAINING, PLAYER_LOCATION_TRAINING))
	assert.Equal(t, 5, CalcPlayerLocationDistance(PLAYER_LOCATION_TASK_PRIORITIZATION, PLAYER_LOCATION_TRAINING))
}
