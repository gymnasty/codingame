package main

const (
	TotalCardsOnEachPlayableSkill = 5
	TotalBonusCards               = 36
	TotalDebtCards                = 100
)

// カードの総数
func TotalCards(cardType CardType) int {
	switch cardType {
	case CARD_TYPE_BONUS:
		return TotalBonusCards
	case CARD_TYPE_DEBT:
		return TotalDebtCards
	default:
		return TotalCardsOnEachPlayableSkill
	}
}

// カードの種類
type CardType int

const (
	CARD_TYPE_TRAINING CardType = iota
	CARD_TYPE_CODING
	CARD_TYPE_DAILY_ROUTINE
	CARD_TYPE_TASK_PRIORITIZATION
	CARD_TYPE_ARCHITECTURE_STUDY
	CARD_TYPE_CONTINUOUS_INTEGRATION
	CARD_TYPE_CODE_REVIEW
	CARD_TYPE_REFACTORING
	CARD_TYPE_BONUS
	CARD_TYPE_DEBT
)

var CardTypeNames = map[CardType]string{
	CARD_TYPE_TRAINING:               "TRAINING",
	CARD_TYPE_CODING:                 "CODING",
	CARD_TYPE_DAILY_ROUTINE:          "DAILY_ROUTINE",
	CARD_TYPE_TASK_PRIORITIZATION:    "TASK_PRIORITIZATION",
	CARD_TYPE_ARCHITECTURE_STUDY:     "ARCHITECTURE_STUDY",
	CARD_TYPE_CONTINUOUS_INTEGRATION: "CONTINUOUS_INTEGRATION",
	CARD_TYPE_CODE_REVIEW:            "CODE_REVIEW",
	CARD_TYPE_REFACTORING:            "REFACTORING",
	CARD_TYPE_BONUS:                  "BONUS",
	CARD_TYPE_DEBT:                   "DEBT",
}

// すべてのカードの種類
var AllCardTypes = []CardType{
	CARD_TYPE_TRAINING,
	CARD_TYPE_CODING,
	CARD_TYPE_DAILY_ROUTINE,
	CARD_TYPE_TASK_PRIORITIZATION,
	CARD_TYPE_ARCHITECTURE_STUDY,
	CARD_TYPE_CONTINUOUS_INTEGRATION,
	CARD_TYPE_CODE_REVIEW,
	CARD_TYPE_REFACTORING,
	CARD_TYPE_BONUS,
	CARD_TYPE_DEBT,
}

type Card struct {
	CardType         CardType  // カードの種類
	SkillType        SkillType // スキルの種類
	SkillPoints      int       // 必要なスキルに割り当てられるポイント
	AltanativePoints int       // 不足しているスキルに割り当てられるポイント
	DebtPoints       int       // 負債ポイント
}

var (
	TraningCard               = &Card{CARD_TYPE_TRAINING, SKILL_TYPE_TRAINING, 2, 2, 0}
	CodingCard                = &Card{CARD_TYPE_CODING, SKILL_TYPE_CODING, 2, 2, 0}
	DailyRoutineCard          = &Card{CARD_TYPE_DAILY_ROUTINE, SKILL_TYPE_DAILY_ROUTINE, 2, 2, 0}
	TaskPrioritizationCard    = &Card{CARD_TYPE_TASK_PRIORITIZATION, SKILL_TYPE_TASK_PRIORITIZATION, 2, 2, 0}
	ArchitectureStudyCard     = &Card{CARD_TYPE_ARCHITECTURE_STUDY, SKILL_TYPE_ARCHITECTURE_STUDY, 2, 2, 0}
	ContinuousIntegrationCard = &Card{CARD_TYPE_CONTINUOUS_INTEGRATION, SKILL_TYPE_CONTINUOUS_INTEGRATION, 2, 2, 0}
	CodeReviewCard            = &Card{CARD_TYPE_CODE_REVIEW, SKILL_TYPE_CODE_REVIEW, 2, 2, 0}
	RefactoringCard           = &Card{CARD_TYPE_REFACTORING, SKILL_TYPE_REFACTORING, 2, 2, 0}
	BonusCard                 = &Card{CARD_TYPE_BONUS, SKILL_TYPE_BONUS, 1, 1, 0}
	DebtCard                  = &Card{CARD_TYPE_DEBT, SKILL_TYPE_NONE, 0, 0, 1}
)

// すべてのカード
var AllCards = []*Card{
	TraningCard,
	CodingCard,
	DailyRoutineCard,
	TaskPrioritizationCard,
	ArchitectureStudyCard,
	ContinuousIntegrationCard,
	CodeReviewCard,
	RefactoringCard,
	BonusCard,
	DebtCard,
}

// スキルカード
var SkillCards = []*Card{
	TraningCard,
	CodingCard,
	DailyRoutineCard,
	TaskPrioritizationCard,
	ArchitectureStudyCard,
	ContinuousIntegrationCard,
	CodeReviewCard,
	RefactoringCard,
	BonusCard,
}

// プレイ可能なスキルカード
var PlayableSkillCards = []*Card{
	TraningCard,
	CodingCard,
	DailyRoutineCard,
	TaskPrioritizationCard,
	ArchitectureStudyCard,
	ContinuousIntegrationCard,
	CodeReviewCard,
	RefactoringCard,
}

var SkillCardMap = map[SkillType]*Card{
	SKILL_TYPE_TRAINING:               TraningCard,
	SKILL_TYPE_CODING:                 CodingCard,
	SKILL_TYPE_DAILY_ROUTINE:          DailyRoutineCard,
	SKILL_TYPE_TASK_PRIORITIZATION:    TaskPrioritizationCard,
	SKILL_TYPE_ARCHITECTURE_STUDY:     ArchitectureStudyCard,
	SKILL_TYPE_CONTINUOUS_INTEGRATION: ContinuousIntegrationCard,
	SKILL_TYPE_CODE_REVIEW:            CodeReviewCard,
	SKILL_TYPE_REFACTORING:            RefactoringCard,
	SKILL_TYPE_BONUS:                  BonusCard,
}

// カードの山の種類
type CardPileType string

const (
	CARD_PILE_TYPE_HAND               CardPileType = "HAND"               // 自分の手札
	CARD_PILE_TYPE_DRAW               CardPileType = "DRAW"               // 自分の山札
	CARD_PILE_TYPE_DISCARD            CardPileType = "DISCARD"            // 自分の捨札
	CARD_PILE_TYPE_PLAYED             CardPileType = "PLAYED_CARDS"       // 自分がこのターンに使用したスキルカード
	CARD_PILE_TYPE_AUTOMATED          CardPileType = "AUTOMATED"          // 自分の自動で利用されるカード
	CARD_PILE_TYPE_OPPONENT_CARDS     CardPileType = "OPPONENT_CARDS"     // 相手のすべてのカード
	CARD_PILE_TYPE_OPPONENT_AUTOMATED CardPileType = "OPPONENT_AUTOMATED" // 相手の自動で利用されるカード
	CARD_PILE_TYPE_BOARD              CardPileType = "BOARD"              // 場のカード
)

// すべてのカードの山の種類
var AllCardPileTypes = []CardPileType{
	CARD_PILE_TYPE_HAND,
	CARD_PILE_TYPE_DRAW,
	CARD_PILE_TYPE_DISCARD,
	CARD_PILE_TYPE_PLAYED,
	CARD_PILE_TYPE_AUTOMATED,
	CARD_PILE_TYPE_OPPONENT_CARDS,
	CARD_PILE_TYPE_OPPONENT_AUTOMATED,
	CARD_PILE_TYPE_BOARD,
}

// カードの山
type CardPile struct {
	CardPileType CardPileType
	Cards        CardCounts
}

func NewCardPile() *CardPile {
	return &CardPile{
		Cards: make(CardCounts),
	}
}

func (c *CardPile) GetCount(card *Card) int {
	return c.Cards[card]
}

func (c *CardPile) SetCount(card *Card, count int) {
	c.Cards[card] = count
}

func (c *CardPile) Add(card *Card) {
	c.Cards[card]++
}

func (c CardPile) Copy() *CardPile {
	p := NewCardPile()
	p.CardPileType = p.CardPileType
	p.Cards = p.Cards.Copy()
	return p
}

func (c CardPile) Has(card *Card, count int) bool {
	return c.Cards[card] >= count
}

func (c CardPile) CountPlayable() int {
	count := 0
	for _, card := range PlayableSkillCards {
		count += c.Cards[card]
	}
	return count
}

func (c CardPile) AltanativePoints() int {
	p := 0
	for card, num := range c.Cards {
		p += card.AltanativePoints * num
	}
	return p
}

func (c CardPile) Show() {
	DebugLogLine("  CardPile")
	DebugLogLine("  Type: %s", c.CardPileType)
	c.Cards.Show()
}

func (c CardPile) SkillPoints(skill SkillType) int {
	card := SkillCardMap[skill]
	num := c.Cards[card]
	return card.SkillPoints * num
}

// すべてのカードの山
type CardPiles map[CardPileType]*CardPile

func (c CardPiles) Get(cardPileType CardPileType) *CardPile {
	cardPile, ok := c[cardPileType]
	if ok {
		return cardPile
	}
	return NewCardPile()
}

func (c CardPiles) Show() {
	DebugLogLine("CardPiles")
	for _, l := range AllCardPileTypes {
		if p, ok := c[l]; ok {
			p.Show()
		}
	}
}

// カードの枚数
type CardCounts map[*Card]int

func (c CardCounts) TotalSkillCards() int {
	total := 0
	for card, count := range c {
		if card != DebtCard {
			total += count
		}
	}
	return total
}

func (c CardCounts) Copy() CardCounts {
	cpy := make(CardCounts, len(c))
	for k, v := range c {
		cpy[k] = v
	}
	return cpy
}

func (c CardCounts) Show() {
	DebugLogLine("  Cards:")
	for _, card := range AllCards {
		if count, ok := c[card]; ok {
			DebugLogLine("    %s: %d", CardTypeNames[card.CardType], count)
		}
	}
}
