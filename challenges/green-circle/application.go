package main

import (
	"sort"
)

// アプリケーション
type Application struct {
	ID                  int                 // application id
	ObjectType          string              // object type
	ReleaseRequirements ReleaseRequirements // リリースに必要なポイント
}

func NewApplication() *Application {
	return &Application{
		ReleaseRequirements: make(ReleaseRequirements),
	}
}

func (a Application) Show() {
	DebugLogLine("Application")
	DebugLogLine("  ID: %s", a.ID)
	DebugLogLine("  ObjectType: %s", a.ObjectType)
	a.ReleaseRequirements.Show()
}

func (a Application) RequirePoints() int {
	p := 0
	for _, n := range a.ReleaseRequirements {
		p += n
	}
	return p
}

// すべてのアプリケーション
type Applications []*Application

func (a Applications) Show() {
	for _, app := range a {
		app.Show()
	}
}

// アプリケーションのリリースに必要なスキル
type ReleaseRequirements map[SkillType]int

func (r ReleaseRequirements) TotalPoints() int {
	p := 0
	for _, v := range r {
		p += v
	}
	return p
}

// 要求されているスキルをその量が大きい順に並べ替えて返す。
func (r ReleaseRequirements) RequireSkills() []SkillType {
	skills := make([]SkillType, len(AllSkills))
	copy(skills, AllSkills)
	sort.SliceStable(skills, func(i, j int) bool {
		return r[skills[i]] > r[skills[j]]
	})
	for i, skill := range skills {
		if r[skill] == 0 {
			return skills[:i]
		}
	}
	return skills
}

func (r ReleaseRequirements) Copy() ReleaseRequirements {
	requirements := make(ReleaseRequirements)
	for k, v := range r {
		requirements[k] = v
	}
	return requirements
}

func (r ReleaseRequirements) Show() {
	DebugLogLine("  SkillRequirements:")
	for _, skill := range AllSkills {
		if requirements, ok := r[skill]; ok {
			DebugLogLine("    %s: %d", skill, requirements)
		}
	}
}

// リリースによる技術的負債を計算する
func CalcApplicationReleaseCost(pile, automatedPile *CardPile, app *Application) int {
	// リリース可能かどうかを調べる
	requirements := app.ReleaseRequirements.Copy()
	for skill, requiredPoint := range requirements {
		skillPoint := pile.SkillPoints(skill) + automatedPile.SkillPoints(skill)
		if requiredPoint > skillPoint {
			requirements[skill] -= skillPoint
		} else {
			requirements[skill] = 0
		}
	}

	total := requirements.TotalPoints()
	alt := pile.AltanativePoints() + automatedPile.AltanativePoints()
	bonus := pile.SkillPoints(SKILL_TYPE_BONUS) + automatedPile.SkillPoints(SKILL_TYPE_BONUS)
	if total > alt+bonus {
		return -1
	}

	return MaxInt(0, total-bonus)
}

// 不足しているスキルを計算する
func CalcInsufficientSkills(pile, automatedPile *CardPile, app *Application) (insufficientSkills map[SkillType]int, insufficientPoints int) {
	insufficientPoints = 0
	insufficientSkills = make(map[SkillType]int)
	for skill, requirement := range app.ReleaseRequirements {
		skillPoints := pile.SkillPoints(skill)
		if skillPoints < requirement {
			insufficientSkills[skill] = requirement - skillPoints
			insufficientPoints += requirement - skillPoints
		}
	}
	insufficientPoints -= MinInt(pile.SkillPoints(SKILL_TYPE_BONUS), insufficientPoints)
	return insufficientSkills, insufficientPoints
}

// リリースにかかるコストが最も低いアプリケーションを探す
func FindLowestReleaseCostApplication(pile, automatedPile *CardPile, apps Applications) (app *Application, cost int) {
	minCost := 100
	var minApp *Application
	for _, app := range apps {
		cost := CalcApplicationReleaseCost(pile, automatedPile, app)
		if cost < 0 {
			continue
		}
		if cost < minCost {
			minCost = cost
			minApp = app
		}
	}
	return minApp, minCost
}

// コスト 0 でリリース可能なアプリケーションを探す
func FindReleasableApplicationWithouCost(hand *CardPile, automated *CardPile, additional *CardPile, apps Applications) *Application {
	for _, app := range apps {
		requirements := app.ReleaseRequirements.Copy()
		for skill, requiredPoint := range requirements {
			skillPoint := hand.SkillPoints(skill) + automated.SkillPoints(skill) + additional.SkillPoints(skill)
			if requiredPoint > skillPoint {
				requirements[skill] -= skillPoint
			} else {
				requirements[skill] = 0
			}
		}
		total := requirements.TotalPoints()
		bonus := hand.SkillPoints(SKILL_TYPE_BONUS) + automated.SkillPoints(SKILL_TYPE_BONUS) + additional.SkillPoints(SKILL_TYPE_BONUS)
		if total <= bonus {
			return app
		}
	}
	return nil
}

// コスト 0 でリリース可能なアプリケーションを探す
func FindReleasableApplicationWithouCost2(hand *CardPile, automated *CardPile, additional *CardPile, apps Applications, taskManagementCost, noisyCost bool) *Application {
	// taskManagementCost が必要な場合は手札から2枚捨てる
	// noisyCost が必要な場合は手札もしくは追加カードから1枚捨てる
	requires := make(map[*Application]ReleaseRequirements)
	for _, app := range apps {
		req := app.ReleaseRequirements.Copy()
		for skill, requiredPoint := range req {
			skillPoint := automated.SkillPoints(skill)
			if requiredPoint > skillPoint {
				req[skill] -= skillPoint
			} else {
				req[skill] = 0
			}
		}
		requires[app] = req
	}
	// ここまでで automated のボーナスカード以外は使用済み

	for _, app := range apps {
		handCards := hand.Cards.Copy()
		additionalCards := additional.Cards.Copy()
		requirements := requires[app].Copy()
		handSkillUsed := 0
		additionalSkillUsed := 0

		for skill, requiredPoint := range requirements {
			card := SkillCardMap[skill]
			requireCards := requiredPoint/2 + requiredPoint%2

			// 追加カードを使用
			if additionalCards[card] >= requireCards {
				additionalSkillUsed += requireCards
				additionalCards[card] -= requireCards
				requireCards = 0
				requirements[card.SkillType] = 0
			} else {
				additionalSkillUsed += additionalCards[card]
				requireCards -= additionalCards[card]
				requirements[card.SkillType] -= additionalCards[card] * 2
				additionalCards[card] = 0
			}

			// 手札を使用
			if handCards[card] >= requireCards {
				handSkillUsed += requireCards
				handCards[card] -= requireCards
				requireCards = 0
				requirements[card.SkillType] = 0
			} else {
				handSkillUsed += handCards[card]
				requireCards -= handCards[card]
				requirements[card.SkillType] -= handCards[card] * 2
				handCards[card] = 0
			}
		}

		// ここまででボーナスカード以外のすべてのカードを使用済み

		skillReturned := 0
		requirePoints := requirements.TotalPoints()
		if requirePoints < automated.Cards[BonusCard] {
			returnNum := (automated.Cards[BonusCard] - requirePoints) / 2
			skillReturned += returnNum
			requirePoints = 0
		} else {
			requirePoints -= automated.Cards[BonusCard]
		}

		// ここまでで automated はすべて使用済み

		if requirePoints > additionalCards[BonusCard] {
			requirePoints -= additionalCards[BonusCard]
			additionalCards[BonusCard] = 0
		} else {
			additionalCards[BonusCard] -= requirePoints
			requirePoints = 0
		}

		// ここまでで追加カードはすべて使用済み

		if requirePoints > handCards[BonusCard] {
			requirePoints -= handCards[BonusCard]
			handCards[BonusCard] = 0
		} else {
			handCards[BonusCard] -= requirePoints
			requirePoints = 0
		}

		// ここまでですべてのカードを使用済み

		if requirePoints > 0 {
			continue
		}

		// カードを戻す
		handReturned := MinInt(skillReturned, handSkillUsed)
		skillReturned -= handReturned
		additionalReturned := MinInt(skillReturned, additionalSkillUsed)
		skillReturned -= skillReturned

		// 手札が 2 枚余っている必要がある
		handCosts := 0
		if taskManagementCost {
			if handCards.TotalSkillCards()+handReturned < 2 {
				continue
			}
			handCosts = 2
		}

		// 手札もしくは追加カードが 1 枚余っている必要がある
		if noisyCost {
			if handCards.TotalSkillCards()+handReturned-handCosts == 0 && additionalCards.TotalSkillCards()+additionalReturned == 0 {
				continue
			}
		}

		// リリース可能
		return app
	}
	return nil
}

func FindNotNeededCard(hand *CardPile, releaseTarget *Application) *Card {
	for _, skill := range AllSkills {
		card := SkillCardMap[skill]
		if hand.Cards[card] == 0 {
			continue
		}
		if releaseTarget.ReleaseRequirements[skill] == 0 {
			return card
		}
	}
	if hand.Cards[BonusCard] > 0 {
		return BonusCard
	}
	return nil
}

func CalcSkillNeeds(automatedPile *CardPile, apps Applications) ReleaseRequirements {
	needs := make(map[SkillType]int, SKILL_TYPE_NUM)
	automated := make(map[SkillType]int, SKILL_TYPE_NUM)
	for _, skill := range AllSkills {
		needs[skill] = 0
		automated[skill] = automatedPile.SkillPoints(skill)
	}
	for _, app := range apps {
		for skill, requirements := range app.ReleaseRequirements {
			needs[skill] += MaxInt(0, requirements-automated[skill])
		}
	}
	return needs
}
