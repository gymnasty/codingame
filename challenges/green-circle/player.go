package main

// プレーヤーの位置
type PlayerLocation int

const (
	PLAYER_LOCATION_INIT                   PlayerLocation = -1
	PLAYER_LOCATION_TRAINING               PlayerLocation = 0
	PLAYER_LOCATION_CODING                 PlayerLocation = 1
	PLAYER_LOCATION_DAILY_ROUTINE          PlayerLocation = 2
	PLAYER_LOCATION_TASK_PRIORITIZATION    PlayerLocation = 3
	PLAYER_LOCATION_ARCHITECTURE_STUDY     PlayerLocation = 4
	PLAYER_LOCATION_CONTINUOUS_INTEGRATION PlayerLocation = 5
	PLAYER_LOCATION_CODE_REVIEW            PlayerLocation = 6
	PLAYER_LOCATION_REFACTORING            PlayerLocation = 7
	PLAYER_LOCATION_NUM                    PlayerLocation = 8
)

const (
	PLAYER_INDEX_MYSELF   = 0
	PLAYER_INDEX_OPPONENT = 1
)

func CalcPlayerLocationDistance(from, to PlayerLocation) int {
	return int((to - from + PLAYER_LOCATION_NUM) % PLAYER_LOCATION_NUM)
}

// プレーヤー
type Player struct {
	Location                        PlayerLocation // id of the zone in which the player is located
	Score                           int            // the amount of released applications
	PermanentDailyRoutineCards      int            // number of DAILY_ROUTINE the player has played. It allows them to take cards from the adjacent zones
	PermanentArchitectureStudyCards int            // number of ARCHITECTURE_STUDY the player has played. It allows them to draw more cards
}

func (p Player) Show() {
	DebugLogLine("Player")
	DebugLogLine("  Location: %d", p.Location)
	DebugLogLine("  Score: %d", p.Score)
	DebugLogLine("  PermanentDailyRoutineCards: %d", p.PermanentDailyRoutineCards)
	DebugLogLine("  PermanentArchitectureStudyCards: %d", p.PermanentArchitectureStudyCards)
}

type Players []*Player

func (p Players) MySelf() *Player {
	return p[PLAYER_INDEX_MYSELF]
}

func (p Players) Opponent() *Player {
	return p[PLAYER_INDEX_OPPONENT]
}

func (p Players) Show() {
	for _, player := range p {
		player.Show()
	}
}
