package main

func Release(turn *Turn, commander Commander) {
	pile := turn.CardPiles.Get(CARD_PILE_TYPE_HAND)
	automatedPile := turn.CardPiles.Get(CARD_PILE_TYPE_AUTOMATED)
	if turn.Players.MySelf().Score < 4 {
		app, cost := FindLowestReleaseCostApplication(pile, automatedPile, turn.Applications)
		if app != nil && cost <= AllowedCost(turn) {
			commander.Release(app.ID)
		} else {
			commander.Wait()
		}
	} else {
		// 最後は必ずリリースする
		turn.PossibleCommands[0].Play(commander)
	}
}

func AllowedCost(turn *Turn) int {
	hand := turn.CardPiles.Get(CARD_PILE_TYPE_HAND)
	draw := turn.CardPiles.Get(CARD_PILE_TYPE_DRAW)
	discard := turn.CardPiles.Get(CARD_PILE_TYPE_DISCARD)
	skills := hand.Cards.TotalSkillCards() + draw.Cards.TotalSkillCards() + discard.Cards.TotalSkillCards()
	debtCards := hand.Cards[DebtCard] + draw.Cards[DebtCard] + discard.Cards[DebtCard]
	switch turn.Players.MySelf().Score {
	case 0:
		return skills - debtCards
	case 1:
		return skills - debtCards + 1
	case 2:
		return skills - debtCards + 2
	case 3:
		return skills - debtCards + 3
	}
	return 0
}
