package main

/*
 * `TRAINING` 山札から2枚引き、さらに追加でスキルカードを使用できる
 * `CODING` 山札から1枚引き、さらに追加で2枚スキルカードを使用できる
 * `DAILY_ROUTINE` 移動後に隣のデスクからカードを取れるようになる。重ねて使用した場合はさらに隣のデスクからカードを取れる。リリースするまで効果が継続。
 * `TASK_PRIORITIZATION` デスクにカードを戻して、別のデスクからカードを取る
 * `ARCHITECTURE_STUDY` ターン開始時に1枚多くカードを引く。リリースするまで効果が継続
 * `CONTINUOUS_INTEGRATION` 特定のスキルをリリース時に常に利用できるようにする。リリースに使用してもなくならない
 * `CODE_REVIEW` ボーナスカードを2枚受け取って捨山に置く
 * `REFACTORING` 負債カードを1枚取り除く。負債カードがない場合は何も起こらない
 */

func PlayCard(turn *Turn, commander Commander) {
	hand := turn.CardPiles.Get(CARD_PILE_TYPE_HAND)
	board := turn.CardPiles.Get(CARD_PILE_TYPE_BOARD)
	automatedPile := turn.CardPiles.Get(CARD_PILE_TYPE_AUTOMATED)

	// 移動時の指示に従う
	if releaseTarget != nil && playTarget == nil {
		commander.Wait()
		return
	}
	if playTarget != nil && hand.Has(playTarget, 1) {
		if playTarget == CodeReviewCard {
			commander.CodeReview()
			return
		}
	}

	// 序盤はボーナスが CD できるときはする
	if turn.Players.MySelf().Score < 3 && hand.Has(ContinuousIntegrationCard, 1) && hand.Has(BonusCard, 1) {
		commander.ContinuousDelivery(BonusCard.CardType)
		return
	}

	// 低コストでリリースできそうなときはスキルを利用しない
	minApp, minCost := FindLowestReleaseCostApplication(hand, automatedPile, turn.Applications)
	if turn.Players.MySelf().Score == 4 && minApp != nil && minCost == 0 || turn.Players.MySelf().Score < 4 && minCost <= AllowedCost(turn) {
		commander.Wait()
		DebugLogLine("wait for release app %d", minApp.ID)
		return
	}

	// `TRAINING` 山札から2枚引き、さらに追加でスキルカードを使用できる
	if hand.Has(TraningCard, 1) {
		commander.Traning()
		return
	}

	// `CODING` 山札から1枚引き、さらに追加で2枚スキルカードを使用できる
	if hand.Has(CodingCard, 1) {
		commander.Coding()
		return
	}

	// リリースに必要なものが CD できるときはする
	if hand.Has(ContinuousIntegrationCard, 1) {
		// ボーナスカードがあれば、必ずボーナスカードを CD する
		if hand.Has(BonusCard, 1) {
			commander.ContinuousDelivery(BonusCard.CardType)
			return
		}

		// その他のスキルカードも CD を試みる
		skills := CalcSkillNeeds(automatedPile, turn.Applications).RequireSkills()
		for _, skill := range skills {
			count := 1
			if skill == SKILL_TYPE_CONTINUOUS_INTEGRATION {
				count++
			}
			card := SkillCardMap[skill]
			if hand.Has(card, count) {
				commander.ContinuousDelivery(card.CardType)
				return
			}
		}
	}

	// `CODE_REVIEW` ボーナスカードを2枚受け取って捨山に置く
	if hand.Has(CodeReviewCard, 1) {
		commander.CodeReview()
		return
	}

	// `REFACTORING` 負債カードを1枚取り除く。負債カードがない場合は何も起こらない
	if hand.Has(RefactoringCard, 1) && hand.Has(DebtCard, 1) {
		commander.Refactoring()
		return
	}

	// `DAILY_ROUTINE` 移動後に隣のデスクからカードを取れるようになる。重ねて使用した場合はさらに隣のデスクからカードを取れる。リリースするまで効果が継続。
	if hand.Has(DailyRoutineCard, 1) {
		commander.DailyRoutine()
		return
	}

	// `ARCHITECTURE_STUDY` ターン開始時に1枚多くカードを引く。リリースするまで効果が継続
	if hand.Has(ArchitectureStudyCard, 1) {
		commander.ArchitectureStudy()
		return
	}

	// `TASK_PRIORITIZATION` デスクにカードを戻して、別のデスクからカードを取る
	if hand.Has(TaskPrioritizationCard, 1) && hand.Has(DailyRoutineCard, 1) && board.Has(ContinuousIntegrationCard, 1) {
		commander.TaskPrioritization(DailyRoutineCard.CardType, ContinuousIntegrationCard.CardType)
		return
	}

	commander.Wait()
}
