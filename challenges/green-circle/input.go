package main

import (
	"bufio"
	"fmt"
	"os"
)

type InputReader struct {
	scanner *bufio.Scanner
}

func NewInputReader() *InputReader {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, 1000000), 1000000)
	return &InputReader{
		scanner: scanner,
	}
}

func (r *InputReader) Read() *Turn {
	turn := &Turn{}

	// gamePhase: can be MOVE, GIVE_CARD, THROW_CARD, PLAY_CARD or RELEASE
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &turn.GamePhase)

	var applicationsCount int
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &applicationsCount)
	turn.ApplicationCount = applicationsCount

	applications := make(Applications, applicationsCount)
	for i := range applications {
		// trainingNeeded: number of TRAINING skills needed to release this application
		// codingNeeded: number of CODING skills needed to release this application
		// dailyRoutineNeeded: number of DAILY_ROUTINE skills needed to release this application
		// taskPrioritizationNeeded: number of TASK_PRIORITIZATION skills needed to release this application
		// architectureStudyNeeded: number of ARCHITECTURE_STUDY skills needed to release this application
		// continuousDeliveryNeeded: number of CONTINUOUS_DELIVERY skills needed to release this application
		// codeReviewNeeded: number of CODE_REVIEW skills needed to release this application
		// refactoringNeeded: number of REFACTORING skills needed to release this application
		app := NewApplication()
		r.scanner.Scan()
		var training, coding, dailyRoutine, taskPrioritization, architectureStudy, continuousDelivery, codeReview, refactoring int
		fmt.Sscan(r.scanner.Text(), &app.ObjectType, &app.ID, &training, &coding, &dailyRoutine, &taskPrioritization, &architectureStudy, &continuousDelivery, &codeReview, &refactoring)
		app.ReleaseRequirements[SKILL_TYPE_TRAINING] = training
		app.ReleaseRequirements[SKILL_TYPE_CODING] = coding
		app.ReleaseRequirements[SKILL_TYPE_DAILY_ROUTINE] = dailyRoutine
		app.ReleaseRequirements[SKILL_TYPE_TASK_PRIORITIZATION] = taskPrioritization
		app.ReleaseRequirements[SKILL_TYPE_ARCHITECTURE_STUDY] = architectureStudy
		app.ReleaseRequirements[SKILL_TYPE_CONTINUOUS_INTEGRATION] = continuousDelivery
		app.ReleaseRequirements[SKILL_TYPE_CODE_REVIEW] = codeReview
		app.ReleaseRequirements[SKILL_TYPE_REFACTORING] = refactoring

		applications[i] = app
	}
	turn.Applications = applications

	players := make(Players, 2)
	for i := range players {
		// playerLocation: id of the zone in which the player is located
		// playerPermanentDailyRoutineCards: number of DAILY_ROUTINE the player has played. It allows them to take cards from the adjacent zones
		// playerPermanentArchitectureStudyCards: number of ARCHITECTURE_STUDY the player has played. It allows them to draw more cards
		player := &Player{}
		r.scanner.Scan()
		fmt.Sscan(r.scanner.Text(), &player.Location, &player.Score,
			&player.PermanentDailyRoutineCards, &player.PermanentArchitectureStudyCards)
		players[i] = player
	}
	turn.Players = players

	var cardLocationsCount int
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &cardLocationsCount)
	turn.CardLocationsCount = cardLocationsCount

	cardPiles := make(CardPiles, cardLocationsCount)
	for i := 0; i < cardLocationsCount; i++ {
		// cardsLocation: the location of the card list. It can be HAND, DRAW, DISCARD or OPPONENT_CARDS (AUTOMATED and OPPONENT_AUTOMATED will appear in later leagues)
		pile := NewCardPile()
		r.scanner.Scan()
		var location string
		var training, coding, dailyRoutine, taskPrioritization, architectureStudy, continuousDelivery, codeReview, refactoring, bonus, technicalDebt int
		fmt.Sscan(r.scanner.Text(), &location, &training, &coding, &dailyRoutine, &taskPrioritization, &architectureStudy, &continuousDelivery, &codeReview, &refactoring, &bonus, &technicalDebt)
		pile.CardPileType = CardPileType(location)
		pile.SetCount(TraningCard, training)
		pile.SetCount(CodingCard, coding)
		pile.SetCount(DailyRoutineCard, dailyRoutine)
		pile.SetCount(TaskPrioritizationCard, taskPrioritization)
		pile.SetCount(ArchitectureStudyCard, architectureStudy)
		pile.SetCount(ContinuousIntegrationCard, continuousDelivery)
		pile.SetCount(CodeReviewCard, codeReview)
		pile.SetCount(RefactoringCard, refactoring)
		pile.SetCount(BonusCard, bonus)
		pile.SetCount(DebtCard, technicalDebt)

		cardPiles[pile.CardPileType] = pile
	}
	// 場のカードを確認しておく
	boardPile := CalcBoardPile(cardPiles, players.Opponent())
	cardPiles[CARD_PILE_TYPE_BOARD] = boardPile
	// 与えられなかった山は空
	for _, t := range AllCardPileTypes {
		if _, ok := cardPiles[t]; !ok {
			cardPiles[t] = NewCardPile()
		}
	}

	turn.CardPiles = cardPiles

	var possibleMovesCount int
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &possibleMovesCount)

	possibleCommands := make(PossibleCommands, 0, possibleMovesCount)
	for i := 0; i < possibleMovesCount; i++ {
		r.scanner.Scan()
		possibleMove := r.scanner.Text()
		if possibleMove != string(COMMAND_RANDOM) {
			possibleCommands = append(possibleCommands, ParsePossibleCommand(possibleMove))
		}
	}
	turn.PossibleCommands = possibleCommands

	return turn
}
