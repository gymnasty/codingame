package main

import (
	"encoding/gob"
	"fmt"
	"os"
)

type node struct {
	State  state
	Action action
}

type score struct {
	score map[node]float64
}

func newScore() *score {
	return &score{
		score: make(map[node]float64),
	}
}

func (s score) get(state state, action action) float64 {
	n := node{state, action}
	return s.score[n]
}

func (s *score) apply(state state, action action, scoreDelta float64) {
	n := node{state, action}
	if score, ok := s.score[n]; ok {
		s.score[n] = score + scoreDelta
	} else {
		s.score[n] = scoreDelta
	}
}

func (s *score) dump(filename string) {
	file, err := os.Create(filename)
	if err != nil {
		fmt.Printf("failed to create score file: %s : %v\n", filename, err)
	}
	defer file.Close()

	enc := gob.NewEncoder(file)
	if err := enc.Encode(s.score); err != nil {
		fmt.Printf("failed to write score file: %s : %v\n", filename, err)
	}
}

func (s *score) load(filename string) {
	file, err := os.Open(filename)
	if err != nil {
		fmt.Printf("failed to open score file: %s : %v\n", filename, err)
	}
	defer file.Close()

	dec := gob.NewDecoder(file)
	if err := dec.Decode(&s.score); err != nil {
		fmt.Printf("failed to load score file: %s : %v\n", filename, err)
	}
}

type nodeHistory struct {
	nodes []node
}

func newNodeHistory() nodeHistory {
	return nodeHistory{
		nodes: make([]node, 0, 200),
	}
}

func (h *nodeHistory) append(n node) {
	h.nodes = append(h.nodes, n)
}
