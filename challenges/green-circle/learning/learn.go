package main

import (
	"math/rand"
)

type qLearner struct {
	history      nodeHistory
	discountRate float64
	greedyRate   float64
	score        *score
}

func newQLearner(discountRate, greedyRate float64, score *score) *qLearner {
	return &qLearner{
		discountRate: discountRate,
		greedyRate:   greedyRate,
		score:        score,
	}
}

func (q *qLearner) StartEpisode() {
	q.history = newNodeHistory()
}

func (q *qLearner) EndEpisode(reward float64) {
	q.applyFromLast(reward)
}

// 後ろから割引しつつ報酬を伝搬する
func (q *qLearner) applyFromLast(r float64) {
	lastIdx := len(q.history.nodes) - 1
	rGamma := r
	for i := lastIdx; i >= 0; i -= 1 {
		q.score.apply(
			q.history.nodes[i].State,
			q.history.nodes[i].Action,
			rGamma,
		)
		rGamma *= q.discountRate
	}
}

func (q *qLearner) UpdateGreedyRate(greedyRate float64) {
	q.greedyRate = greedyRate
}

func (q *qLearner) useGreedy() bool {
	g := int(q.greedyRate * 100)
	return rand.Int()%100 < g
}

func (q *qLearner) NextAction(state state, possibleActions []action) action {
	var act action
	if q.useGreedy() {
		act = possibleActions[rand.Int()%len(possibleActions)]
	} else {
		act = possibleActions[0]
		actScore := q.score.get(state, act)
		for _, a := range possibleActions[1:] {
			score := q.score.get(state, a)
			if score > actScore {
				act = a
				actScore = score
			}
		}
	}
	q.history.append(node{state, act})
	return act
}
