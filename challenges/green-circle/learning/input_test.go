package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInput(t *testing.T) {
	input := `agent1
MOVE
12
APPLICATION 18 0 0 0 4 4 0 0 0
APPLICATION 23 0 0 0 0 4 0 4 0
APPLICATION 10 0 4 0 0 0 4 0 0
APPLICATION 21 0 0 0 4 0 0 0 4
APPLICATION 12 0 4 0 0 0 0 0 4
APPLICATION 15 0 0 4 0 0 4 0 0
APPLICATION 8 0 4 0 4 0 0 0 0
APPLICATION 3 4 0 0 0 4 0 0 0
APPLICATION 2 4 0 0 4 0 0 0 0
APPLICATION 24 0 0 0 0 4 0 0 4
APPLICATION 26 0 0 0 0 0 4 0 4
APPLICATION 4 4 0 0 0 0 4 0 0
-1 0 0 0
-1 0 0 0
3
HAND 0 0 0 0 0 0 0 0 1 3
DRAW 0 0 0 0 0 0 0 0 3 1
OPPONENT_CARDS 0 0 0 0 0 0 0 0 4 4
9
MOVE 0
MOVE 1
MOVE 2
MOVE 3
MOVE 4
MOVE 5
MOVE 6
MOVE 7
RANDOM`

	r := NewInputReader(strings.NewReader(input))
	agentName, status, actions := r.Read()
	assert.Equal(t, "agent1", agentName)
	assert.NotNil(t, status)
	assert.Equal(t, []action{
		{"MOVE 0"},
		{"MOVE 1"},
		{"MOVE 2"},
		{"MOVE 3"},
		{"MOVE 4"},
		{"MOVE 5"},
		{"MOVE 6"},
		{"MOVE 7"},
	}, actions)
}
