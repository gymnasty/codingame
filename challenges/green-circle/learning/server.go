package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
)

type learnServer struct {
	port         int
	discountRate float64
	greedyRate   float64
	score        *score
	agents       map[string]*qLearner
	buffer       []string
	bufferLen    int
	mu           sync.Mutex
}

func newLearnServer(port int) *learnServer {
	return &learnServer{
		port:         port,
		discountRate: 0.3,
		greedyRate:   1.0,
		score:        newScore(),
		agents:       make(map[string]*qLearner),
		buffer:       make([]string, 1000000),
		bufferLen:    1000000,
	}
}

func (s *learnServer) Serve() {
	fmt.Println("Serve")
	http.HandleFunc("/StartEpisode", s.startEpisodeHandler)
	http.HandleFunc("/EndEpisode", s.endEpisodeHandler)
	http.HandleFunc("/NextAction", s.nextActionHandler)
	http.HandleFunc("/UpdateParams", s.updateParamsHandler)
	http.HandleFunc("/DumpScore", s.dumpScoreHandler)
	http.HandleFunc("/LoadScore", s.loadScoreHandler)
	err := http.ListenAndServe(fmt.Sprintf(":%d", s.port), nil)
	panic(err)
}

func (s *learnServer) startEpisodeHandler(w http.ResponseWriter, r *http.Request) {
	s.mu.Lock()
	defer s.mu.Unlock()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}
	agentName := string(body)
	agent, ok := s.agents[agentName]
	if !ok {
		agent = newQLearner(s.discountRate, s.greedyRate, s.score)
		s.agents[agentName] = agent
	}
	agent.StartEpisode()
}

func (s *learnServer) endEpisodeHandler(w http.ResponseWriter, r *http.Request) {
	s.mu.Lock()
	defer s.mu.Unlock()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}
	var agentName string
	var reward float64
	_, err = fmt.Sscan(string(body), &agentName, &reward)
	if err != nil {
		w.WriteHeader(401)
		w.Write([]byte(fmt.Sprintf("invalid request: %s", string(body))))
		return
	}

	agent, ok := s.agents[agentName]
	if !ok {
		w.WriteHeader(404)
		w.Write([]byte(fmt.Sprintf("%s not found", agentName)))
		return
	}
	agent.EndEpisode(reward)
	fmt.Printf("agent %s got reward %f\n", agentName, reward)
}

func (s *learnServer) nextActionHandler(w http.ResponseWriter, r *http.Request) {
	s.mu.Lock()
	defer s.mu.Unlock()

	// 読み込み
	inputReader := NewInputReader(r.Body)
	agentName, state, actions := inputReader.Read()

	agent, ok := s.agents[agentName]
	if !ok {
		w.WriteHeader(404)
		w.Write([]byte(fmt.Sprintf("%s not found", agentName)))
		return
	}

	// 次のアクション
	action := agent.NextAction(state, actions)
	w.Write([]byte(action.Command))
}

func (s *learnServer) updateParamsHandler(w http.ResponseWriter, r *http.Request) {
	s.mu.Lock()
	defer s.mu.Unlock()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}
	w.Write(body)

	var discountRate float64
	var greedyRate float64
	_, err = fmt.Sscan(string(body), &discountRate, &greedyRate)
	if err != nil {
		w.WriteHeader(401)
		w.Write([]byte(fmt.Sprintf("invalid request: %s", string(body))))
		return
	}

	s.discountRate = discountRate
	s.greedyRate = greedyRate
	fmt.Printf("parameter updated: discountRate=%f, greedyRate=%f\n", discountRate, greedyRate)
}

func (s *learnServer) dumpScoreHandler(w http.ResponseWriter, r *http.Request) {
	s.mu.Lock()
	defer s.mu.Unlock()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}
	suffix := string(body)
	filename := fmt.Sprintf("score%s.dump", suffix)
	s.score.dump(filename)
	fmt.Printf("score dumped: %s\n", filename)
}

func (s *learnServer) loadScoreHandler(w http.ResponseWriter, r *http.Request) {
	s.mu.Lock()
	defer s.mu.Unlock()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}
	suffix := string(body)
	filename := fmt.Sprintf("score%s.dump", suffix)
	s.score.load(filename)
	fmt.Printf("score loaded: %s\n", filename)
}
