package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type learnClient struct {
	baseUrl string
}

func newLearnClient(port int) *learnClient {
	return &learnClient{
		baseUrl: fmt.Sprintf("http://localhost:%d", port),
	}
}

func (c *learnClient) start(name string) {
	_, err := http.Post(
		c.baseUrl+"/StartEpisode",
		"text/plain",
		strings.NewReader(name),
	)
	if err != nil {
		panic(err)
	}
}

func (c *learnClient) end(name string, score float64) {
	_, err := http.Post(
		c.baseUrl+"/EndEpisode",
		"text/plain",
		strings.NewReader(fmt.Sprintf("%s %f", name, score)),
	)
	if err != nil {
		panic(err)
	}
}

func (c *learnClient) next(name string, input string) {
	resp, err := http.Post(
		c.baseUrl+"/NextAction",
		"text/plain",
		strings.NewReader(name+"\n"+input),
	)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(body))
}

func (c *learnClient) updateParams(discountRate float64, greedyRate float64) {
	resp, err := http.Post(
		c.baseUrl+"/UpdateParams",
		"text/plain",
		strings.NewReader(fmt.Sprintf("%f %f", discountRate, greedyRate)),
	)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(body))
}

func (c *learnClient) dump(suffix string) {
	_, err := http.Post(
		c.baseUrl+"/DumpScore",
		"text/plain",
		strings.NewReader(suffix),
	)
	if err != nil {
		panic(err)
	}
}

func (c *learnClient) load(suffix string) {
	_, err := http.Post(
		c.baseUrl+"/LoadScore",
		"text/plain",
		strings.NewReader(suffix),
	)
	if err != nil {
		panic(err)
	}
}
