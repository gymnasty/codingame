package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	flag.Parse()

	var port int
	flag.IntVar(&port, "port", 8080, "port")

	switch flag.Arg(0) {
	case "server":
		s := newLearnServer(port)
		s.Serve()
	case "client":
		c := newLearnClient(port)
		switch flag.Arg(1) {
		case "start":
			c.start(flag.Arg(2))
		case "end":
			score, err := strconv.ParseFloat(flag.Arg(3), 64)
			if err != nil {
				panic("invalid arg")
			}
			c.end(flag.Arg(2), score)
		case "next":
			c.next(flag.Arg(2), flag.Arg(3))
		case "params":
			discountRate, err := strconv.ParseFloat(flag.Arg(2), 64)
			if err != nil {
				panic("invalid arg")
			}
			greedyRate, err := strconv.ParseFloat(flag.Arg(3), 64)
			if err != nil {
				panic("invalid arg")
			}
			c.updateParams(discountRate, greedyRate)
		case "dump":
			c.dump(flag.Arg(2))
		case "load":
			c.load(flag.Arg(2))
		}
	case "agent":
		fmt.Fprintln(os.Stderr, "agent")
		c := newLearnClient(port)
		agentName := flag.Arg(1)
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Buffer(make([]byte, 1000000), 1000000)
		for {
			input := readAgentInput(scanner)
			fmt.Fprintln(os.Stderr, string(input))
			c.next(agentName, string(input))
		}
	}
}

func readAgentInput(scanner *bufio.Scanner) string {
	lines := make([]string, 0, 100)
	// gamePhase: can be MOVE, GIVE_CARD, THROW_CARD, PLAY_CARD or RELEASE
	scanner.Scan()
	lines = append(lines, scanner.Text())

	// application
	var applicationsCount int
	scanner.Scan()
	line := scanner.Text()
	fmt.Sscan(line, &applicationsCount)
	lines = append(lines, line)

	for i := 0; i < applicationsCount; i++ {
		scanner.Scan()
		lines = append(lines, scanner.Text())
	}

	// player myself
	scanner.Scan()
	lines = append(lines, scanner.Text())
	// player opponent
	scanner.Scan()
	lines = append(lines, scanner.Text())

	// cards
	var cardLocationsCount int
	scanner.Scan()
	line = scanner.Text()
	fmt.Sscan(line, &cardLocationsCount)
	lines = append(lines, line)
	for i := 0; i < cardLocationsCount; i++ {
		scanner.Scan()
		lines = append(lines, scanner.Text())
	}

	var possibleMovesCount int
	scanner.Scan()
	line = scanner.Text()
	fmt.Sscan(line, &possibleMovesCount)
	lines = append(lines, line)
	for i := 0; i < possibleMovesCount; i++ {
		scanner.Scan()
		lines = append(lines, scanner.Text())
	}

	fmt.Fprintln(os.Stderr, lines)

	return strings.Join(lines, "\n")
}
