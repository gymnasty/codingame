package main

import (
	"io/ioutil"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestServer(t *testing.T) {
	s := newLearnServer(8080)
	go s.Serve()

	resp, _ := http.Post("http://localhost:8080/StartEpisode", "text/plain", strings.NewReader("test\ntest1"))
	assert.Equal(t, 200, resp.StatusCode)
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, []byte("test\ntest1"), body)
}
