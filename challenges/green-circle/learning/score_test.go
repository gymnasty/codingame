package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestScore(t *testing.T) {
	s := newScore()
	s1 := state{Phase: 1}
	s2 := state{Phase: 2}
	s3 := state{Phase: 1}
	a1 := action{}

	assert.Equal(t, float64(0), s.get(s1, a1))
	assert.Equal(t, float64(0), s.get(s2, a1))
	assert.Equal(t, float64(0), s.get(s3, a1))

	s.apply(s1, a1, 1.1)
	assert.Equal(t, 1.1, s.get(s1, a1))
	assert.Equal(t, float64(0), s.get(s2, a1))
	assert.Equal(t, 1.1, s.get(s3, a1))

	s.apply(s1, a1, 1.5)
	assert.Equal(t, 2.6, s.get(s1, a1))
	assert.Equal(t, float64(0), s.get(s2, a1))
	assert.Equal(t, 2.6, s.get(s3, a1))
}

func TestNodeHistory(t *testing.T) {
	h := newNodeHistory()
	n1 := node{state{Phase: 1}, action{}}
	n2 := node{state{Phase: 2}, action{}}
	h.append(n1)
	h.append(n2)
}
