package main

import (
	"bufio"
	"fmt"
	"io"
	"sort"
)

const (
	InitialApplicationCount       = 12
	TotalCardsOnEachPlayableSkill = 5
	TotalBonusCards               = 36
	TotalDebtCards                = 100
)
const (
	CARD_PILE_LOCATION_HAND               = "HAND"               // 自分の手札
	CARD_PILE_LOCATION_DRAW               = "DRAW"               // 自分の山札
	CARD_PILE_LOCATION_DISCARD            = "DISCARD"            // 自分の捨札
	CARD_PILE_LOCATION_PLAYED             = "PLAYED_CARDS"       // 自分がこのターンに使用したスキルカード
	CARD_PILE_LOCATION_AUTOMATED          = "AUTOMATED"          // 自分の自動で利用されるカード
	CARD_PILE_LOCATION_OPPONENT_CARDS     = "OPPONENT_CARDS"     // 相手のすべてのカード
	CARD_PILE_LOCATION_OPPONENT_AUTOMATED = "OPPONENT_AUTOMATED" // 相手の自動で利用されるカード
)

type InputReader struct {
	scanner *bufio.Scanner
}

func NewInputReader(r io.Reader) *InputReader {
	scanner := bufio.NewScanner(r)
	scanner.Buffer(make([]byte, 1000000), 1000000)
	return &InputReader{
		scanner: scanner,
	}
}

func (r *InputReader) Read() (string, state, []action) {
	r.scanner.Scan()
	var agentName string
	fmt.Sscan(r.scanner.Text(), &agentName)

	s := state{}

	// gamePhase: can be MOVE, GIVE_CARD, THROW_CARD, PLAY_CARD or RELEASE
	r.scanner.Scan()
	var phase string
	fmt.Sscan(r.scanner.Text(), &phase)
	s.Phase = getGamePhase(phase)

	// application
	var applicationsCount int
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &applicationsCount)
	appIDs := make([]int8, InitialApplicationCount)
	for i := range appIDs {
		if i < applicationsCount {
			var objectType string
			r.scanner.Scan()
			var appID, training, coding, dailyRoutine, taskPrioritization, architectureStudy, continuousDelivery, codeReview, refactoring int8
			fmt.Sscan(r.scanner.Text(), &objectType, &appID, &training, &coding, &dailyRoutine, &taskPrioritization, &architectureStudy, &continuousDelivery, &codeReview, &refactoring)
			appIDs[i] = appID
		} else {
			appIDs[i] = APPLICATION_ID_RELEASED
		}
	}
	sort.SliceStable(appIDs, func(i, j int) bool {
		return appIDs[i] < appIDs[j]
	})
	s.App1, s.App2, s.App3, s.App4, s.App5, s.App6 = appIDs[0], appIDs[1], appIDs[2], appIDs[3], appIDs[4], appIDs[5]
	s.App7, s.App8, s.App9, s.App10, s.App11, s.App12 = appIDs[6], appIDs[7], appIDs[8], appIDs[9], appIDs[10], appIDs[11]

	// player myself
	{
		r.scanner.Scan()
		fmt.Sscan(r.scanner.Text(), &s.Location, &s.Score, &s.Daily, &s.Arch)
	}
	// player opponent
	{
		r.scanner.Scan()
		fmt.Sscan(r.scanner.Text(), &s.OpponentLocation, &s.OpponentScore, &s.OpponentDaily, &s.OpponentArch)
	}

	// cards
	var cardLocationsCount int
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &cardLocationsCount)
	s.BoardTraning = TotalCardsOnEachPlayableSkill
	s.BoardCoding = TotalCardsOnEachPlayableSkill
	s.BoardDaily = TotalCardsOnEachPlayableSkill
	s.BoardTask = TotalCardsOnEachPlayableSkill
	s.BoardArch = TotalCardsOnEachPlayableSkill
	s.BoardCI = TotalCardsOnEachPlayableSkill
	s.BoardReview = TotalCardsOnEachPlayableSkill
	s.BoardRefactor = TotalCardsOnEachPlayableSkill
	s.BoardBonus = TotalBonusCards
	s.BoardDebt = TotalDebtCards
	for i := 0; i < cardLocationsCount; i++ {
		r.scanner.Scan()
		var location string
		var training, coding, dailyRoutine, taskPrioritization, architectureStudy, continuousDelivery, codeReview, refactoring, bonus, technicalDebt int8
		fmt.Sscan(r.scanner.Text(), &location, &training, &coding, &dailyRoutine, &taskPrioritization, &architectureStudy, &continuousDelivery, &codeReview, &refactoring, &bonus, &technicalDebt)
		switch location {
		case CARD_PILE_LOCATION_HAND:
			s.HandTraning, s.HandCoding, s.HandDaily, s.HandTask = training, coding, dailyRoutine, taskPrioritization
			s.HandArch, s.HandCI, s.HandReview, s.HandRefactor = architectureStudy, continuousDelivery, codeReview, refactoring
			s.HandBonus, s.HandDebt = bonus, technicalDebt
		case CARD_PILE_LOCATION_DRAW:
			s.DrawTraning, s.DrawCoding, s.DrawDaily, s.DrawTask = training, coding, dailyRoutine, taskPrioritization
			s.DrawArch, s.DrawCI, s.DrawReview, s.DrawRefactor = architectureStudy, continuousDelivery, codeReview, refactoring
			s.DrawBonus, s.DrawDebt = bonus, technicalDebt
		case CARD_PILE_LOCATION_DISCARD:
			s.DiscardTraning, s.DiscardCoding, s.DiscardDaily, s.DiscardTask = training, coding, dailyRoutine, taskPrioritization
			s.DiscardArch, s.DiscardCI, s.DiscardReview, s.DiscardRefactor = architectureStudy, continuousDelivery, codeReview, refactoring
			s.DiscardBonus, s.DiscardDebt = bonus, technicalDebt
		case CARD_PILE_LOCATION_AUTOMATED:
			s.AutoTraning, s.AutoCoding, s.AutoDaily, s.AutoTask = training, coding, dailyRoutine, taskPrioritization
			s.AutoArch, s.AutoCI, s.AutoReview, s.AutoRefactor = architectureStudy, continuousDelivery, codeReview, refactoring
			s.AutoBonus, s.AutoDebt = bonus, technicalDebt
		}
		s.BoardTraning -= training
		s.BoardCoding -= coding
		s.BoardDaily -= dailyRoutine
		s.BoardTask -= taskPrioritization
		s.BoardArch -= architectureStudy
		s.BoardCI -= continuousDelivery
		s.BoardReview -= codeReview
		s.BoardRefactor -= refactoring
		s.BoardBonus -= bonus
		s.BoardDebt -= technicalDebt
	}

	var possibleMovesCount int
	r.scanner.Scan()
	fmt.Sscan(r.scanner.Text(), &possibleMovesCount)

	actions := make([]action, 0, possibleMovesCount)
	for i := 0; i < possibleMovesCount; i++ {
		r.scanner.Scan()
		possibleMove := r.scanner.Text()
		if possibleMove != COMMAND_RANDOM {
			actions = append(actions, action{possibleMove})
		}
	}

	return agentName, s, actions
}
