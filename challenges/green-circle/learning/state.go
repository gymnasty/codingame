package main

// アプリケーションは同じ種類が重複して出ることはない
// また、常に同じスキルが必要なものに対して同じ名前がついている
// 存在しないアプリケーションを 28 として、アプリケーション ID を昇順に記録する
const (
	// リリース済みのアプリケーション ID
	APPLICATION_ID_RELEASED = 28
	// フェーズ
	MOVE                  = "MOVE"
	THROW_CARD            = "THROW_CARD"
	GIVE_CARD             = "GIVE_CARD"
	PLAY_CARD             = "PLAY_CARD"
	RELEASE               = "RELEASE"
	GAME_PHASE_MOVE       = 0
	GAME_PHASE_THROW_CARD = 1
	GAME_PHASE_GIVE_CARD  = 2
	GAME_PHASE_PLAY_CARD  = 3
	GAME_PHASE_RELEASE    = 4
)

func getGamePhase(s string) int8 {
	switch s {
	case MOVE:
		return GAME_PHASE_MOVE
	case THROW_CARD:
		return GAME_PHASE_THROW_CARD
	case GIVE_CARD:
		return GAME_PHASE_GIVE_CARD
	case PLAY_CARD:
		return GAME_PHASE_PLAY_CARD
	case RELEASE:
		return GAME_PHASE_RELEASE
	default:
		return -1
	}
}

// 状態
type state struct {
	Phase                                                        int8
	Location, Score, Daily, Arch                                 int8
	OpponentLocation, OpponentScore, OpponentDaily, OpponentArch int8

	App1, App2, App3, App4, App5, App6, App7, App8, App9, App10, App11, App12                                                                   int8 // アプリケーション ID を昇順に記録
	HandTraning, HandCoding, HandDaily, HandTask, HandArch, HandCI, HandReview, HandRefactor, HandBonus, HandDebt                               int8 // 手札
	DrawTraning, DrawCoding, DrawDaily, DrawTask, DrawArch, DrawCI, DrawReview, DrawRefactor, DrawBonus, DrawDebt                               int8 // 山札
	DiscardTraning, DiscardCoding, DiscardDaily, DiscardTask, DiscardArch, DiscardCI, DiscardReview, DiscardRefactor, DiscardBonus, DiscardDebt int8 // 捨札
	AutoTraning, AutoCoding, AutoDaily, AutoTask, AutoArch, AutoCI, AutoReview, AutoRefactor, AutoBonus, AutoDebt                               int8 // automated
	BoardTraning, BoardCoding, BoardDaily, BoardTask, BoardArch, BoardCI, BoardReview, BoardRefactor, BoardBonus, BoardDebt                     int8 // 場
}
