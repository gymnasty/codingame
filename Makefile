# setup
.PHONY: setup
setup:
	./scripts/setup.sh

# amadeus
AMADEUS_DIR		:= ./challenges/unleash-the-geek-amadeus
AMADEUS_SRC		:= $(AMADEUS_DIR)/*.go
AMADEUS_TARGET	:= $(AMADEUS_DIR)/out/upload.go
AMADEUS_REFEREE	:= $(AMADEUS_DIR)/referee
AMADEUS_CMD1    := "go run ../out/upload.go"
AMADEUS_CMD2    := "go run ../out/upload.go"

$(AMADEUS_TARGET): $(AMADEUS_SRC)
	@mkdir -p $(@D)
	@gocat -n $(^) > $(@)

.PHONY: amadeus
amadeus: $(AMADEUS_TARGET)

.PHONY: amadeus-run
amadeus-run:
	@cd $(AMADEUS_REFEREE) \
	mvn test-compile clean && mvn test-compile && mvn -X exec:java -Dexec.mainClass=UTG2019Main -Dexec.classpathScope=test -DaddResourcesToClasspath=true -DadditioanlClasspathElements=test

.PHONY: amadeus-build-simulator
amadeus-build-simulator:
	@cd $(AMADEUS_REFEREE); mvn clean test-compile

.PHONY: amadeus-run-simulator
amadeus-run-simulator:
	@cd $(AMADEUS_REFEREE); mvn -X exec:java -Dexec.mainClass=Simulator -Dexec.classpathScope=test -DaddResourcesToClasspath=true -DadditioanlClasspathElements=test -Dexec.args='$(AMADEUS_CMD1) $(AMADEUS_CMD1)'

# spring challenge 2020
PACMAN_DIR 		:= ./challenges/spring-challenge-2020
PACMAN_SRC 		:= $(PACMAN_DIR)/*.go
PACMAN_TARGET	:= $(PACMAN_DIR)/out/upload.go

$(PACMAN_TARGET): $(PACMAN_SRC)
	@mkdir -p $(@D)
	@gocat -n $(^) > $(@)

.PHONY: pacman
pacman: $(PACMAN_TARGET)

# green circle
GREEN_CIRCLE_DIR				:= ./challenges/green-circle
GREEN_CIRCLE_SRC				:= $(GREEN_CIRCLE_DIR)/*.go
GREEN_CIRCLE_TARGET				:= $(GREEN_CIRCLE_DIR)/out/upload.go
GREEN_CIRCLE_TARGET_BIN			:= $(GREEN_CIRCLE_DIR)/out/upload
GREEN_CIRCLE_LEARNING_DIR		:= $(GREEN_CIRCLE_DIR)/learning
GREEN_CIRCLE_LEARNING_SRC		:= $(GREEN_CIRCLE_LEARNING_DIR)/*.go
GREEN_CIRCLE_LEARNING_TARGET	:= $(GREEN_CIRCLE_LEARNING_DIR)/learning

$(GREEN_CIRCLE_TARGET): $(GREEN_CIRCLE_SRC)
	@mkdir -p $(@D)
	@gocat -n $(^) > $(@)

$(GREEN_CIRCLE_TARGET_BIN): $(GREEN_CIRCLE_TARGET)
	@go build -o $(GREEN_CIRCLE_TARGET_BIN) $(GREEN_CIRCLE_TARGET)

$(GREEN_CIRCLE_LEARNING_TARGET): $(GREEN_CIRCLE_LEARNING_SRC)
	@cd $(GREEN_CIRCLE_LEARNING_DIR); go build

.PHONY: green-circle
green-circle: $(GREEN_CIRCLE_TARGET) $(GREEN_CIRCLE_TARGET_BIN)

.PHONY: green-circle-learning
green-circle-learning: $(GREEN_CIRCLE_LEARNING_TARGET)

# fall challenge 2022
FALL2022_DIR 	:= ./challenges/fall-challenge-2022
FALL2022_SRC 	:= $(FALL2022_DIR)/*.go
FALL2022_TARGET	:= $(FALL2022_DIR)/out/upload.go

$(FALL2022_TARGET): $(FALL2022_SRC)
	@mkdir -p $(@D)
	@gocat -n $(^) > $(@)

.PHONY: fall2022
fall2022: $(FALL2022_TARGET)
